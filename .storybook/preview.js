import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { HelmetProvider } from 'react-helmet-async';
import { QueryClient, QueryClientProvider } from 'react-query';
import rootReducer from '@src/reducers/rootReducer';
import { ThemeProvider } from 'styled-components';
import {
  ThemeProvider as MuiThemeProvider,
  StyledEngineProvider,
  CssBaseline,
} from '@mui/material';
import theme from '@src/styles/theme';
import muiTheme from '@src/styles/muiTheme';
import { Reset } from 'styled-reset';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

const store = configureStore({ reducer: rootReducer });
const queryClient = new QueryClient();

export const decorators = [
  (Story) => (
    <HelmetProvider>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
              <MuiThemeProvider theme={muiTheme}>
                <>
                  <Reset />
                  <CssBaseline />
                  <Story />
                </>
              </MuiThemeProvider>
            </ThemeProvider>
          </StyledEngineProvider>
        </QueryClientProvider>
      </Provider>
    </HelmetProvider>
  ),
];
