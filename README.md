# PANKI WEB APP

## Overview

PANKI 웹 프로젝트


## Project Stack

- Create React App
- React
- TypeScript
- Storybook
- styled-components
- React Redux
- Redux Toolkit
- React Query
- Recharts
- Docker Compose

## Environment

.env sample 파일들을 참고하여 env 파일을 작성해야 합니다.

- development : .env.development
- production : .env.production


## Getting started

### Run development mode

```shell
$ npm run start
```

### Run test

```shell
$ npm run test
```

### Run test with coverage

```shell
$ npm run test:cov
```

### Run Storybook

```shell
$ npm run storybook
```

### Run bundle analyzer

```shell
$ npm run report
```