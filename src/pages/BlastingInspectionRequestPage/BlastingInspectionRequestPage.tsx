import { Helmet } from 'react-helmet-async';
import BlastingInspectionRequest from '../../components/ship/BlastingInspectionRequest';
import { BLASTING_INSPECTION_REQUEST } from '../../constants/constantString';

export type BlastingInspectionRequestPageProps = {};

const BlastingInspectionRequestPage = () => (
  <>
    <Helmet>
      <title>{BLASTING_INSPECTION_REQUEST}</title>
    </Helmet>
    <BlastingInspectionRequest />
  </>
);

export default BlastingInspectionRequestPage;
