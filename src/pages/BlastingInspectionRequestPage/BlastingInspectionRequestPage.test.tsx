import { render } from '@testing-library/react';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import rootState from '@src/test/fixtures/reducers/rootState';
import BlastingInspectionRequestPage from '.';

describe('<BlastingInspectionRequestPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <BlastingInspectionRequestPage />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup(rootState);

    expect(container).toBeInTheDocument();
  });
});
