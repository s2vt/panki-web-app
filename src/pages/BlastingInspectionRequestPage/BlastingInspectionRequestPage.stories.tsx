import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import BlastingInspectionRequestPage from './BlastingInspectionRequestPage';

export default {
  title: 'Pages/BlastingInspectionRequestPage',
  component: BlastingInspectionRequestPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <BlastingInspectionRequestPage {...args} />;

export const Basic = Template.bind({});
