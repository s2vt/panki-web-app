import InspectionResultReport from '@src/components/inspectionReport/InspectionResultReport';
import { Helmet } from 'react-helmet-async';
import { INSPECTION_RESULT } from '../../constants/constantString';

const InspectionResultReportPage = () => (
  <>
    <Helmet>
      <title>{INSPECTION_RESULT}</title>
    </Helmet>
    <InspectionResultReport />
  </>
);

export default InspectionResultReportPage;
