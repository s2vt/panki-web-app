import { Meta, Story } from '@storybook/react';
import InspectionResultReportPage from './InspectionResultReportPage';

export default {
  title: 'Pages/InspectionResultReportPage',
  component: InspectionResultReportPage,
} as Meta;

const Template: Story = (args) => <InspectionResultReportPage {...args} />;

export const Basic = Template.bind({});
