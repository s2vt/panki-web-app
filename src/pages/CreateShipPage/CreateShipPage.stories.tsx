import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import CreateShipPage from './CreateShipPage';

export default {
  title: 'Pages/CreateShipPage',
  component: CreateShipPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <CreateShipPage {...args} />;

export const Basic = Template.bind({});
