import { Helmet } from 'react-helmet-async';
import CreateShip from '../../components/ship/CreateShip';
import { CREATE_SHIP } from '../../constants/constantString';

export type CreateShipPageProps = {};

const CreateShipPage = () => (
  <>
    <Helmet>
      <title>{CREATE_SHIP}</title>
    </Helmet>
    <CreateShip />
  </>
);

export default CreateShipPage;
