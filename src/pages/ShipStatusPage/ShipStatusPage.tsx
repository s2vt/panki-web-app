import ShipStatus from '@src/components/ship/ShipStatus';
import { SHIP_AND_BLOCK } from '@src/constants/constantString';
import { Helmet } from 'react-helmet-async';

export type ShipStatusPageProps = {};

const ShipStatusPage = () => (
  <>
    <Helmet>
      <title>{SHIP_AND_BLOCK}</title>
    </Helmet>
    <ShipStatus />
  </>
);

export default ShipStatusPage;
