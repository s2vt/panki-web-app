import { Meta, Story } from '@storybook/react';
import ShipStatusPage from './ShipStatusPage';

export default {
  title: 'Pages/ShipStatusPage',
  component: ShipStatusPage,
} as Meta;

const Template: Story = (args) => <ShipStatusPage {...args} />;

export const Basic = Template.bind({});
