import { Helmet } from 'react-helmet-async';
import StatisticsOverView from '@src/components/statistics/StatisticsOverView';
import { INSPECTION_RESULT } from '@src/constants/constantString';

const StatisticsPage = () => (
  <>
    <Helmet>
      <title>{INSPECTION_RESULT}</title>
    </Helmet>
    <StatisticsOverView />
  </>
);

export default StatisticsPage;
