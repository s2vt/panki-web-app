import { Meta, Story } from '@storybook/react';
import StatisticsPage from './StatisticsPage';

export default {
  title: 'Pages/StatisticsPage',
  component: StatisticsPage,
} as Meta;

const Template: Story = (args) => <StatisticsPage {...args} />;

export const Basic = Template.bind({});
