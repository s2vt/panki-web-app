import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import StatisticsPage from '.';

describe('<StatisticsPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <StatisticsPage />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup(rootState);

    expect(container).toBeInTheDocument();
  });
});
