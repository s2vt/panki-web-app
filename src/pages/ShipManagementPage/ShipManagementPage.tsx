import { Helmet } from 'react-helmet-async';
import ShipManagement from '../../components/ship/ShipManagement';
import { SHIP_AND_BLOCK } from '../../constants/constantString';

export type ShipPageProps = {};

const ShipManagementPage = () => (
  <>
    <Helmet>
      <title>{SHIP_AND_BLOCK}</title>
    </Helmet>
    <ShipManagement />
  </>
);

export default ShipManagementPage;
