import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import ShipManagementPage from './ShipManagementPage';

export default {
  title: 'Pages/ShipManagementPage',
  component: ShipManagementPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <ShipManagementPage {...args} />;

export const Basic = Template.bind({});
