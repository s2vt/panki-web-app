import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import ForgotPasswordPage from './ForgotPasswordPage';

export default {
  title: 'Pages/ForgotPasswordPage',
  component: ForgotPasswordPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <ForgotPasswordPage {...args} />;

export const Basic = Template.bind({});
