import { Helmet } from 'react-helmet-async';
import ForgotPassword from '@src//components/auth/ForgotPassword';
import { FIND_PASSWORD } from '@src//constants/constantString';

const ForgotPasswordPage = () => (
  <>
    <Helmet>
      <title>{FIND_PASSWORD}</title>
    </Helmet>
    <ForgotPassword />
  </>
);

export default ForgotPasswordPage;
