import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import InspectionRequestPage from './InspectionRequestPage';

export default {
  title: 'Pages/InspectionRequestPage',
  component: InspectionRequestPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <InspectionRequestPage {...args} />;

export const Basic = Template.bind({});
