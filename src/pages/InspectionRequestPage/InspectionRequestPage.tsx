import { Helmet } from 'react-helmet-async';
import InspectionRequest from '../../components/ship/InspectionRequest';
import { INSPECTION_REQUEST } from '../../constants/constantString';

export type InspectionRequestPageProps = {};

const InspectionRequestPage = () => (
  <>
    <Helmet>
      <title>{INSPECTION_REQUEST}</title>
    </Helmet>
    <InspectionRequest />
  </>
);
export default InspectionRequestPage;
