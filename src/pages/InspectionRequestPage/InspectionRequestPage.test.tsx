import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import { RootState } from '@src//reducers/rootReducer';
import prepareMockWrapper from '@src//test/utils/prepareMockWrapper';
import InspectionRequestPage from '.';

describe('<InspectionRequestPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <InspectionRequestPage />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup(rootState);

    expect(container).toBeInTheDocument();
  });
});
