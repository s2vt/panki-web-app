import { Meta, Story } from '@storybook/react';
import UserManagementPage from './UserManagementPage';

export default {
  title: 'Pages/UserManagementPage',
  component: UserManagementPage,
} as Meta;

const Template: Story = (args) => <UserManagementPage {...args} />;

export const Basic = Template.bind({});
