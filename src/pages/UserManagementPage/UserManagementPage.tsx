import { Helmet } from 'react-helmet-async';
import UserManagement from '../../components/user/UserManagement';
import { ORGANIZATION_AND_ROLE } from '../../constants/constantString';

export type UserManagementPageProps = {};

const UserManagementPage = () => (
  <>
    <Helmet>
      <title>{ORGANIZATION_AND_ROLE}</title>
    </Helmet>
    <UserManagement />
  </>
);

export default UserManagementPage;
