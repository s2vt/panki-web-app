import styled from 'styled-components';

const NotFoundPage = () => <Block>Not Found</Block>;

const Block = styled.div`
  width: 100%;
  height: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
`;

export default NotFoundPage;
