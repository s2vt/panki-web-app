import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import NotFoundPage from './NotFoundPage';

export default {
  title: 'Pages/NotFoundPage',
  component: NotFoundPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <NotFoundPage {...args} />;

export const Basic = Template.bind({});
