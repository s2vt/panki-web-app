import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import NotFoundPage from '.';

describe('<NotFoundPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <NotFoundPage />
      </Wrapper>,
    );

    const notFoundText = result.getByText('Not Found');

    return { ...result, notFoundText };
  };

  it('should render properly', () => {
    const { container, notFoundText } = setup(rootState);

    expect(container).toBeInTheDocument();
    expect(notFoundText).toBeInTheDocument();
  });
});
