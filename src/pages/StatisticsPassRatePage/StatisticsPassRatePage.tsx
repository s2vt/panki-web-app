import InspectionPassRate from '@src/components/statistics/StatisticsPassRate';
import { Helmet } from 'react-helmet-async';
import { INSPECTION_PASS_RATE } from '../../constants/constantString';

const StatisticsPassRatePage = () => (
  <>
    <Helmet>
      <title>{INSPECTION_PASS_RATE}</title>
    </Helmet>
    <InspectionPassRate />
  </>
);

export default StatisticsPassRatePage;
