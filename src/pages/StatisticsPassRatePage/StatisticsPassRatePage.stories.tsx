import { Meta, Story } from '@storybook/react';
import StatisticsPassRatePage from './StatisticsPassRatePage';

export default {
  title: 'Pages/StatisticsPassRatePage',
  component: StatisticsPassRatePage,
} as Meta;

const Template: Story = (args) => <StatisticsPassRatePage {...args} />;

export const Basic = Template.bind({});
