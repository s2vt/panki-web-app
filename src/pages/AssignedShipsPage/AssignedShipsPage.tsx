import { Helmet } from 'react-helmet-async';
import AssignedShips from '../../components/ship/AssignedShips';
import { SHIP_LIST } from '../../constants/constantString';

export type ShipPageProps = {};

const AssignedShipsPage = () => (
  <>
    <Helmet>
      <title>{SHIP_LIST}</title>
    </Helmet>
    <AssignedShips />
  </>
);

export default AssignedShipsPage;
