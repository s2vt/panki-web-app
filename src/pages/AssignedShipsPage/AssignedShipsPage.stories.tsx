import { Meta, Story } from '@storybook/react';
import AssignedShipsPage from './AssignedShipsPage';

export default {
  title: 'Components/common/ShipPage',
  component: AssignedShipsPage,
} as Meta;

const Template: Story = (args) => <AssignedShipsPage {...args} />;

export const Basic = Template.bind({});
