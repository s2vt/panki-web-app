import { Meta, Story } from '@storybook/react';
import StatisticsByCompanyPage from './StatisticsByCompanyPage';

export default {
  title: 'Pages/StatisticsByCompanyPage',
  component: StatisticsByCompanyPage,
} as Meta;

const Template: Story = (args) => <StatisticsByCompanyPage {...args} />;

export const Basic = Template.bind({});
