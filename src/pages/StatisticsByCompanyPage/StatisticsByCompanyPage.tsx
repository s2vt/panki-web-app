import { Helmet } from 'react-helmet-async';
import StatisticsByCompany from '../../components/statistics/StatisticsByCompany';
import { STATISTICS_BY_COMPANY } from '../../constants/constantString';

export type StatisticsByCompanyPageProps = {};

const StatisticsByCompanyPage = () => (
  <>
    <Helmet>
      <title>{STATISTICS_BY_COMPANY}</title>
    </Helmet>
    <StatisticsByCompany />
  </>
);

export default StatisticsByCompanyPage;
