import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import SignInPage from './SignInPage';

export default {
  title: 'Pages/SignInPage',
  component: SignInPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <SignInPage {...args} />;

export const Basic = Template.bind({});
