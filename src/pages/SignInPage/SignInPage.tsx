import { Helmet } from 'react-helmet-async';
import SignInForm from '@src/components/auth/SignInForm';
import { LOGIN } from '@src/constants/constantString';

const SignInPage = () => (
  <>
    <Helmet>
      <title>{LOGIN}</title>
    </Helmet>
    <SignInForm />
  </>
);

export default SignInPage;
