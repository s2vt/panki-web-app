import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import CoatingInspectionRequestPage from './CoatingInspectionRequestPage';

export default {
  title: 'Pages/CoatingInspectionRequestPage',
  component: CoatingInspectionRequestPage,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <CoatingInspectionRequestPage {...args} />;

export const Basic = Template.bind({});
