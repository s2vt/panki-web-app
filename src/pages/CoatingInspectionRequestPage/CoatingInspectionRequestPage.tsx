import { Helmet } from 'react-helmet-async';
import CoatingInspectionRequest from '../../components/ship/CoatingInspectionRequest';
import { COATING_INSPECTION_REQUEST } from '../../constants/constantString';

export type CoatingInspectionRequestPageProps = {};

const CoatingInspectionRequestPage = () => (
  <>
    <Helmet>
      <title>{COATING_INSPECTION_REQUEST}</title>
    </Helmet>
    <CoatingInspectionRequest />
  </>
);

export default CoatingInspectionRequestPage;
