import { Helmet } from 'react-helmet-async';
import SemanticAnalysis from '../../components/statistics/SemanticAnalysis';
import { SEMANTIC_ANALYSIS } from '../../constants/constantString';

export type SemanticAnalysisPageProps = {};

const SemanticAnalysisPage = () => (
  <>
    <Helmet>
      <title>{SEMANTIC_ANALYSIS}</title>
    </Helmet>
    <SemanticAnalysis />
  </>
);

export default SemanticAnalysisPage;
