import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import SemanticAnalysisPage from '.';

describe('<SemanticAnalysisPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <SemanticAnalysisPage />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup(rootState);

    expect(container).toBeInTheDocument();
  });
});
