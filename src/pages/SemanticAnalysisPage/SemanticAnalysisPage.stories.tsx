import { Meta, Story } from '@storybook/react';
import SemanticAnalysisPage from './SemanticAnalysisPage';

export default {
  title: 'Pages/SemanticAnalysisPage',
  component: SemanticAnalysisPage,
} as Meta;

const Template: Story = (args) => <SemanticAnalysisPage {...args} />;

export const Basic = Template.bind({});
