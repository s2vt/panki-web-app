import { Meta, Story } from '@storybook/react';
import ForbiddenPage from './ForbiddenPage';

export default {
  title: 'Pages/ForbiddenPage',
  component: ForbiddenPage,
} as Meta;

const Template: Story = (args) => <ForbiddenPage {...args} />;

export const Basic = Template.bind({});
