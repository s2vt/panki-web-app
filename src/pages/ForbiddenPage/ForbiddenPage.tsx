import styled from 'styled-components';

/** 권한이 없을 경우 보여줄 페이지 */
const ForbiddenPage = () => (
  <Block>
    <p>권한이 없습니다</p>
  </Block>
);

const Block = styled.div`
  width: 100%;
  height: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
  font-size: 2rem;
`;

export default ForbiddenPage;
