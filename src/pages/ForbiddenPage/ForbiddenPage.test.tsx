import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import { RootState } from '@src//reducers/rootReducer';
import prepareMockWrapper from '@src//test/utils/prepareMockWrapper';
import ForbiddenPage from '.';

describe('<ForbiddenPage />', () => {
  const setup = (initialState: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <ForbiddenPage />
      </Wrapper>,
    );

    const forbiddenText = result.getByText('권한이 없습니다');

    return { ...result, forbiddenText };
  };

  it('should render properly', () => {
    const { container, forbiddenText } = setup(rootState);

    expect(container).toBeInTheDocument();
    expect(forbiddenText).toBeInTheDocument();
  });
});
