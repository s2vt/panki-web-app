import { ApiResponse } from '@src/types/http.types';
import { Role, UserClass } from '@src/types/user.types';
import apiClient from './apiClient';

const API_GET_ALL_ROLES = 'roles/all';
const API_GET_CLASS_ROLES = 'roles/class-name';

export type RolesResponse = ApiResponse & { roles: Role[] };

const getAllRoles = async () => {
  const response = await apiClient.get<RolesResponse>(API_GET_ALL_ROLES);
  const { roles } = response.data;

  return roles;
};

const getClassRoles = async (className: UserClass | undefined) => {
  const response = await apiClient.get<RolesResponse>(
    `${API_GET_CLASS_ROLES}/${className}`,
  );

  const { roles } = response.data;

  return roles;
};

export default { getAllRoles, getClassRoles };
