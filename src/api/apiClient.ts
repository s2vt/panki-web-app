import { AnyAction } from '@reduxjs/toolkit';
import { SERVER_HOST, TOKEN_PREFIX } from '@src/constants';
import axios, { AxiosError } from 'axios';
import { createBrowserHistory } from 'history';
import { actions as alertDialogAction } from '@src/reducers/alertDialog';
import { actions as userActions } from '@src/reducers/user';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import tokenStorage from '@src/storage/tokenStorage';
import { store } from '@src/store';
import { HttpErrorStatus } from '@src/types/http.types';
import HttpError from './model/HttpError';

const apiClient = axios.create({
  baseURL: SERVER_HOST,
});

apiClient.interceptors.request.use((config) => {
  const token = tokenStorage.getToken();
  config.headers.Authorization = `${TOKEN_PREFIX} ${token}` || '';
  return config;
});

apiClient.interceptors.response.use(
  (response) => response,
  (error: AxiosError) => {
    const httpError = new HttpError(
      error.response?.status,
      error.response?.data.resultCode,
    );

    if (httpError.status === HttpErrorStatus.Unauthorized) {
      handleUnauthorized(httpError);
    }

    return Promise.reject(httpError);
  },
);

const handleUnauthorized = (httpError: HttpError) => {
  store.dispatch(
    alertDialogAction.openAlertDialogWithDelay({
      text: httpError.message,
      position: 'rightTop',
      openDialogTime: 1.5 * 1000,
    }) as unknown as AnyAction,
  );
  store.dispatch(userActions.clearUser());
  tokenStorage.clearToken();
  createBrowserHistory().push(ROUTE_PATHS.root);
};

export default apiClient;
