import {
  CreateCompanyPayload,
  SubcontractorCompany,
  SupervisorCompany,
} from '@src/types/company.types';
import { ApiResponse } from '@src/types/http.types';
import { Company, CompanyWithUsers } from '@src/types/user.types';
import apiClient from './apiClient';

const API_COMPANIES = 'companies';
const API_COMPANIES_ME_ORGANIZATION_CHART = 'companies/me/organization-chart';
const API_COMPANIES_ME_ORGANIZATION_CHART_MANAGERS = `companies/me/organization-chart/managers`;
const API_COMPANIES_SUPERVISOR_SEARCH = 'companies/supervisor/search';
const API_SUBCONTRACTOR = 'subcontractor';
const API_COMPANIES_SUBCONTRACTOR = `${API_COMPANIES}/${API_SUBCONTRACTOR}`;

type OrganizationChartResponse = ApiResponse & {
  company: Company;
};
type OrganizationChartWithUsersResponse = ApiResponse & {
  company: CompanyWithUsers;
};
type SupervisorCompaniesResponse = ApiResponse & {
  getSupervisors: SupervisorCompany[];
};
type SubcontractorCompaniesResponse = ApiResponse & {
  result: SubcontractorCompany[];
};

const getOrganizationChart = async () => {
  const response = await apiClient.get<OrganizationChartResponse>(
    API_COMPANIES_ME_ORGANIZATION_CHART,
  );
  const { company } = response.data;

  return company;
};

const getOrganizationChartWithUsers = async () => {
  const response = await apiClient.get<OrganizationChartWithUsersResponse>(
    API_COMPANIES_ME_ORGANIZATION_CHART_MANAGERS,
  );

  const { company } = response.data;

  return company;
};

const getSupervisorCompanies = async (ownerCompanyName: string) => {
  const trimmedOwnerCompanyName = ownerCompanyName.split(' ').join('');

  const response = await apiClient.get<SupervisorCompaniesResponse>(
    API_COMPANIES_SUPERVISOR_SEARCH,
    { params: { q: trimmedOwnerCompanyName } },
  );

  const { getSupervisors } = response.data;

  return getSupervisors;
};

const getSubcontractorCompanies = async (companyId: string) => {
  const response = await apiClient.get<SubcontractorCompaniesResponse>(
    `${API_COMPANIES}/${companyId}/${API_SUBCONTRACTOR}`,
  );

  const { result } = response.data;

  return result;
};

const createCompany = async (addCompanyPayload: CreateCompanyPayload) => {
  const response = await apiClient.post(
    API_COMPANIES_SUBCONTRACTOR,
    addCompanyPayload,
  );

  return response.data;
};

const deleteCompany = async (companyId: string) => {
  const response = await apiClient.delete(`${API_COMPANIES}/${companyId}`);

  return response.data;
};

export default {
  getOrganizationChart,
  getOrganizationChartWithUsers,
  getSupervisorCompanies,
  getSubcontractorCompanies,
  createCompany,
  deleteCompany,
};
