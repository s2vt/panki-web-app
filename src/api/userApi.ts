import { ApiResponse } from '@src/types/http.types';
import {
  SaveUserPayload,
  serializeSaveUserPayload,
  User,
} from '@src/types/user.types';
import apiClient from './apiClient';

const API_USERS = 'users';
const API_USERS_ME = 'users/me';
const API_GET_COMPANY_USERS = 'users/company-name';

type UserResponse = ApiResponse & { user: User };
type UsersResponse = ApiResponse & { users: User[] };
type SaveUserResponse = UserResponse & {
  temporaryPassword?: string;
};

const getCurrentUser = async () => {
  const response = await apiClient.get<UserResponse>(API_USERS_ME);
  const { user } = response.data;

  return user;
};

const getCompanyUsers = async (companyName: string) => {
  const response = await apiClient.get<UsersResponse>(
    `${API_GET_COMPANY_USERS}/${companyName}`,
  );

  const { users } = response.data;

  return users;
};

const createUser = async (saveUserPayload: SaveUserPayload) => {
  const response = await apiClient.post<SaveUserResponse>(
    API_USERS,
    serializeSaveUserPayload(saveUserPayload),
  );

  const { user, temporaryPassword } = response.data;

  return { ...user, temporaryPassword };
};

const updateUser = async (saveUserPayload: SaveUserPayload) => {
  const response = await apiClient.patch<SaveUserResponse>(
    API_USERS,
    serializeSaveUserPayload(saveUserPayload),
  );

  const { user, temporaryPassword } = response.data;

  return { ...user, temporaryPassword };
};

const deleteUsers = async (userIds: String[]) => {
  const response = await apiClient.delete<ApiResponse>(API_USERS, {
    params: { ids: userIds.toString() },
  });

  return response.data;
};

export default {
  getCurrentUser,
  getCompanyUsers,
  createUser,
  updateUser,
  deleteUsers,
};
