import { ApiResponse } from '@src/types/http.types';
import {
  Block,
  BlockDivision,
  BlockInOut,
  BlockState,
  BlockTemplate,
} from '@src/types/block.types';
import apiClient from './apiClient';

const API_BLOCKS_NAME = 'blocks/name';
const API_BLOCKS_SHIP = 'blocks/ship';

type BlocksResponse = ApiResponse & {
  blocks: Block[];
};
type TemplateBlocksResponse = ApiResponse & {
  blocks: BlockTemplate[];
};

const getTemplateBlocks = async () => {
  const response = await apiClient.get<TemplateBlocksResponse>(API_BLOCKS_NAME);

  const { blocks } = response.data;

  const inOutBlocks: Block[] = [];

  // block name 테이블에는 inOut 구분이 없기 때문에 inOut을 임의로 추가
  Object.values(BlockInOut).forEach((inOut) => {
    blocks.forEach((block) =>
      inOutBlocks.push({
        ...block,
        inOut,
        state: BlockState.CREATE,
        blockDivision: BlockDivision.CENTER,
      }),
    );
  });

  return inOutBlocks;
};

const getEnableBlocks = async (shipId: string) => {
  const response = await apiClient.get<BlocksResponse>(
    `${API_BLOCKS_SHIP}/${shipId}`,
  );

  const { blocks } = response.data;

  const enableBlocks = blocks.filter((block) => !block.disable);

  return enableBlocks;
};

export default { getTemplateBlocks, getEnableBlocks };
