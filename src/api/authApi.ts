import {
  PasswordResetPayload,
  SignInPayload,
  SmsVerifyPayload,
  UpdatePasswordPayload,
} from '@src/types/auth.types';
import { ApiResponse, ResultCode } from '@src/types/http.types';
import { User } from '@src/types/user.types';
import apiClient from './apiClient';

const API_SIGN_IN = 'auth/signin';
const API_SEND_SMS_PASSWORD_CODE = 'auth/send-password-code/sms';
const API_REFRESH_SMS_PASSWORD_CODE = 'auth/refresh/sms';
const API_VERIFY_SMS_PASSWORD_CODE = 'auth/confirm-sms';
const API_UPDATE_PASSWORD_WITH_SMS = 'auth/update-password/sms';
const API_UPDATE_PASSWORD_LOGIN = 'auth/update-password/login';

type VerifySmsPasswordCodeResponse = ApiResponse & { token: string };
export type SignInResponse = ApiResponse & {
  token?: string;
  user?: User;
  maxFailCount?: number;
  failCount?: number;
};

const signIn = async (signInPayload: SignInPayload) => {
  const response = await apiClient.post<SignInResponse>(
    API_SIGN_IN,
    signInPayload,
  );

  const { resultCode } = response.data;

  if (resultCode !== ResultCode.success) {
    throw response.data;
  }

  return response.data;
};

const sendSmsPasswordCode = async (phone: string) => {
  const response = await apiClient.post<ApiResponse>(
    API_SEND_SMS_PASSWORD_CODE,
    {
      phone,
    },
  );

  return response.data;
};

const refreshSmsPasswordCode = async (phone: string) => {
  const response = await apiClient.post<ApiResponse>(
    API_REFRESH_SMS_PASSWORD_CODE,
    {
      phone,
    },
  );

  return response.data;
};

const verifySmsPasswordCode = async (smsVerifyPayload: SmsVerifyPayload) => {
  const response = await apiClient.post<VerifySmsPasswordCodeResponse>(
    API_VERIFY_SMS_PASSWORD_CODE,
    smsVerifyPayload,
  );

  const { token } = response.data;

  return token;
};

const updatePasswordWithSms = async (
  passwordResetPayload: Omit<PasswordResetPayload, 'passwordCheck'>,
) => {
  const response = await apiClient.post<ApiResponse>(
    API_UPDATE_PASSWORD_WITH_SMS,
    passwordResetPayload,
  );

  return response.data;
};

const updatePassword = async (updatePasswordPayload: UpdatePasswordPayload) => {
  const response = await apiClient.patch<ApiResponse>(
    API_UPDATE_PASSWORD_LOGIN,
    updatePasswordPayload,
  );

  return response.data;
};

export default {
  signIn,
  sendSmsPasswordCode,
  refreshSmsPasswordCode,
  verifySmsPasswordCode,
  updatePasswordWithSms,
  updatePassword,
};
