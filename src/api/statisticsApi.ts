import {
  YardStatistic,
  RecentStatistic,
  CompanyStatistic,
  ChartFilterDateType,
  CompanyChartFilterParams,
  ShipChartFilterParams,
} from '@src/types/statistics.types';
import { ApiResponse } from '@src/types/http.types';
import apiClient from './apiClient';

const API_STATISTICS_MAIN = 'statistics/main';
const API_STATISTICS_RESULT_SUCCESSES = 'statistics/result/successes';
const API_STATISTICS_RESULT_SUCCESSES_COMPANY =
  'statistics/result/successes/company';

type RecentStatisticsResponse = ApiResponse & {
  result: RecentStatistic[];
};

type YardStatisticsResponse = ApiResponse & {
  result: YardStatistic[];
};

type CompanyStatisticsResponse = {
  result: CompanyStatistic;
};

const getRecentStatistics = async () => {
  const response = await apiClient.get<RecentStatisticsResponse>(
    API_STATISTICS_MAIN,
  );

  const { result } = response.data;

  return result;
};

const getYardStatistics = async (
  chartFilterParams: CompanyChartFilterParams,
) => {
  const params = { ...chartFilterParams };

  if (chartFilterParams.searchDateType === ChartFilterDateType.year) {
    delete params.from;
    delete params.to;
  }

  const response = await apiClient.get<YardStatisticsResponse>(
    API_STATISTICS_RESULT_SUCCESSES,
    { params },
  );

  const { result } = response.data;

  return result;
};

const getCompanyStatistics = async (
  companyId: string,
  chartFilterParams: ShipChartFilterParams,
) => {
  const params = { companyId, ...chartFilterParams };

  if (chartFilterParams.searchDateType === ChartFilterDateType.year) {
    delete params.from;
    delete params.to;
  }

  const response = await apiClient.get<CompanyStatisticsResponse>(
    API_STATISTICS_RESULT_SUCCESSES_COMPANY,
    { params },
  );

  const { result } = response.data;

  return result;
};

export default {
  getRecentStatistics,
  getYardStatistics,
  getCompanyStatistics,
};
