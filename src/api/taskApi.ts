import { ApiResponse } from '@src/types/http.types';
import {
  BlastingInspectionRequestPayload,
  CoatingInspectionRequestPayload,
} from '@src/types/ship.types';
import {
  deserializeInspectionSchedule,
  SentInspectionRequests,
} from '@src/types/task.types';
import apiClient from './apiClient';

const API_TASKS_BLASTING_REQUEST_INSPECTS = 'tasks/blasting/request/inspects';
const API_TASKS_COATING_REQUEST_INSPECTS = 'tasks/ctg-measure/request/inspects';
const API_TASKS_ME_SENT_REQUEST_INSPECTIONS =
  'tasks/me/sent/request/inspections';

type SentInspectionRequestsResponse = ApiResponse & {
  result: SentInspectionRequests;
};

const requestBlastingInspection = async (
  blastingInspectionRequestPayload: BlastingInspectionRequestPayload,
) => {
  const response = await apiClient.post<ApiResponse>(
    API_TASKS_BLASTING_REQUEST_INSPECTS,
    blastingInspectionRequestPayload,
  );

  return response.data;
};

const requestCoatingInspection = async (
  coatingInspectionRequestPayload: CoatingInspectionRequestPayload,
) => {
  const response = await apiClient.post<ApiResponse>(
    API_TASKS_COATING_REQUEST_INSPECTS,
    coatingInspectionRequestPayload,
  );

  return response.data;
};

const getSentInspectionRequests = async (): Promise<SentInspectionRequests> => {
  const response = await apiClient.get<SentInspectionRequestsResponse>(
    API_TASKS_ME_SENT_REQUEST_INSPECTIONS,
  );

  const { blastingApplications, ctgApplications } = response.data.result;

  const deserializedBlastingRequests = blastingApplications.map(
    deserializeInspectionSchedule,
  );
  const deserializedCoatingRequests = ctgApplications.map(
    deserializeInspectionSchedule,
  );

  return {
    blastingApplications: deserializedBlastingRequests,
    ctgApplications: deserializedCoatingRequests,
  };
};

export default {
  requestBlastingInspection,
  requestCoatingInspection,
  getSentInspectionRequests,
};
