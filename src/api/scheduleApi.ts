import { ApiResponse } from '@src/types/http.types';
import {
  deserializeInspectionSchedule,
  InspectionSchedule,
} from '@src/types/task.types';
import apiClient from './apiClient';

const API_SCHEDULE_INSPECTIONS = '/schedule/inspections';
const API_SCHEDULE_DEFER = '/schedule/defer/tasks';

type InspectionSchedulesResponse = ApiResponse & {
  result: InspectionSchedule[];
};

const getInspectionSchedules = async () => {
  const response = await apiClient.get<InspectionSchedulesResponse>(
    API_SCHEDULE_INSPECTIONS,
  );

  const { result } = response.data;
  const deserialized = result.map(deserializeInspectionSchedule);

  return deserialized;
};

const deferInspectionSchedules = async (taskIds: string[]) => {
  const response = await apiClient.post<ApiResponse>(API_SCHEDULE_DEFER, {
    taskIds,
  });

  return response.data;
};

export default {
  getInspectionSchedules,
  deferInspectionSchedules,
};
