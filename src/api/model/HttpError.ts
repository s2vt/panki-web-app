import {
  BAD_REQUEST_ERROR,
  FORBIDDEN_ERROR,
  NOT_FOUND_ERROR,
  REQUEST_TIMEOUT_ERROR,
  SERVER_ERROR,
  UNAUTHORIZED_ERROR,
} from '@src/constants/constantString';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';

class HttpError {
  constructor(status: number | undefined, resultCode: ResultCode) {
    this.status = status;
    this.message = this.getMessageByStatus();
    this.resultCode = resultCode;
  }

  readonly status: number | undefined;

  readonly message: string;

  readonly resultCode: ResultCode;

  private getMessageByStatus() {
    switch (this.status) {
      case HttpErrorStatus.BadRequest:
        return BAD_REQUEST_ERROR;
      case HttpErrorStatus.Forbidden:
        return FORBIDDEN_ERROR;
      case HttpErrorStatus.NotFound:
        return NOT_FOUND_ERROR;
      case HttpErrorStatus.Unauthorized:
        return UNAUTHORIZED_ERROR;
      case HttpErrorStatus.RequestTimeout:
        return REQUEST_TIMEOUT_ERROR;
      default:
        return SERVER_ERROR;
    }
  }
}

export default HttpError;
