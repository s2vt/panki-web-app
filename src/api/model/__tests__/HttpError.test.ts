import {
  BAD_REQUEST_ERROR,
  FORBIDDEN_ERROR,
  NOT_FOUND_ERROR,
  REQUEST_TIMEOUT_ERROR,
  SERVER_ERROR,
  UNAUTHORIZED_ERROR,
} from '@src/constants/constantString';
import { ResultCode } from '@src/types/http.types';
import HttpError from '../HttpError';

describe('HttpError class', () => {
  describe('constructor', () => {
    it('should set bad request message when status is 400', () => {
      // given
      const status = 400;

      // when
      const httpError = new HttpError(status, ResultCode.invalidRequest);

      // then
      expect(httpError.message).toEqual(BAD_REQUEST_ERROR);
    });

    it('should set forbidden message when status is 403', () => {
      // given
      const status = 403;

      // when
      const httpError = new HttpError(status, ResultCode.invalidRequest);

      // then
      expect(httpError.message).toEqual(FORBIDDEN_ERROR);
    });

    it('should set not found message when status is 404', () => {
      // given
      const status = 404;

      // when
      const httpError = new HttpError(status, ResultCode.invalidRequest);

      // then
      expect(httpError.message).toEqual(NOT_FOUND_ERROR);
    });

    it('should set unauthorized message when status is 401', () => {
      // given
      const status = 401;

      // when
      const httpError = new HttpError(status, ResultCode.invalidRequest);

      // then
      expect(httpError.message).toEqual(UNAUTHORIZED_ERROR);
    });

    it('should set request timeout message when status is 408', () => {
      // given
      const status = 408;

      // when
      const httpError = new HttpError(status, ResultCode.invalidRequest);

      // then
      expect(httpError.message).toEqual(REQUEST_TIMEOUT_ERROR);
    });

    it('should set server error message when any cases', () => {
      // given
      const status = 500;

      // when
      const httpError = new HttpError(status, ResultCode.error);

      // then
      expect(httpError.message).toEqual(SERVER_ERROR);
    });

    it('should set server error message when unspecified cases', () => {
      // given
      const status = undefined;

      // when
      const httpError = new HttpError(status, ResultCode.error);

      // then
      expect(httpError.message).toEqual(SERVER_ERROR);
    });
  });
});
