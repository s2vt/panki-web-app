import { ApiResponse } from '@src/types/http.types';
import {
  CreateShipPayload,
  Ship,
  UpdateShipPayload,
} from '@src/types/ship.types';
import apiClient from './apiClient';

const API_SHIPS = 'ships';
const API_USERS_ME_COMPANY_SHIPS = 'users/me/company/ship';

type GetShipsResponse = ApiResponse & {
  ships: Ship[];
};

const createShip = async (createShipPayload: CreateShipPayload) => {
  const response = await apiClient.post<ApiResponse>(
    API_SHIPS,
    createShipPayload,
  );

  return response.data;
};

const deleteShips = async (shipIds: string[]) => {
  const response = await apiClient.delete<ApiResponse>(API_SHIPS, {
    params: { ids: shipIds.toString() },
  });

  return response.data;
};

const updateShip = async (updateShipPayload: UpdateShipPayload) => {
  const { shipId, ...rest } = updateShipPayload;

  const response = await apiClient.patch<ApiResponse>(
    `${API_SHIPS}/${shipId}`,
    rest,
  );

  return response.data;
};

const getCurrentUserShips = async () => {
  const response = await apiClient.get<GetShipsResponse>(
    API_USERS_ME_COMPANY_SHIPS,
  );

  const { ships } = response.data;

  return ships;
};

export default {
  createShip,
  deleteShips,
  updateShip,
  getCurrentUserShips,
};
