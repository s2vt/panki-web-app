import { sprintf } from 'sprintf-js';
import { BlockInOut } from '@src/types/block.types';
import { ApiResponse } from '@src/types/http.types';
import {
  InspectionReportHistory,
  InspectionReport,
  InspectionReportResult,
  ReportFilterPayload,
} from '@src/types/report.types';
import { TaskStep } from '@src/types/task.types';
import * as dateUtil from '@src/utils/dateUtil';
import apiClient from './apiClient';

const API_GET_INSPECTION_REPORTS = '/tasks/inspection/report';
const API_GET_INSPECTION_REPORT_BLOCK =
  '/tasks/inspection/report/block/%(blockId)s';
const API_REPORTS_BLASTING_FINAL = '/reports/block/%(blockId)s/blasting/final';
const API_REPORTS_COATING_FINAL = '/reports/block/%(blockId)s/coating/final';
const API_REPORTS_COATING_INSPECTION = '/reports/coating-inspection';

type InspectionReportsResponse = ApiResponse & {
  inspectionReports: InspectionReport[];
};

type InspectionReportHistoriesResponse = ApiResponse & {
  reportHistories: InspectionReportHistory[];
};

const getMyInspectionReports = async (
  reportFilterPayload: ReportFilterPayload,
) => {
  const response = await apiClient.get<InspectionReportsResponse>(
    API_GET_INSPECTION_REPORTS,
    {
      params: reportFilterPayload,
    },
  );

  const { inspectionReports } = response.data;
  const deserialized = inspectionReports.map(deserializeInspectionReports);

  return deserialized;
};

const getInspectionReportHistories = async (blockId: string) => {
  const response = await apiClient.get<InspectionReportHistoriesResponse>(
    sprintf(API_GET_INSPECTION_REPORT_BLOCK, { blockId }),
  );

  const { reportHistories } = response.data;
  const deserialized = reportHistories.map(deserializeReportHistory);

  return deserialized;
};

const getSurfacePreparationReport = async (blockId: string) => {
  const response = await apiClient.get<ArrayBuffer>(
    sprintf(API_REPORTS_BLASTING_FINAL, { blockId }),
    {
      responseType: 'arraybuffer',
    },
  );

  return response.data;
};

const getFinalCoatingReport = async (blockId: string) => {
  const response = await apiClient.get<ArrayBuffer>(
    sprintf(API_REPORTS_COATING_FINAL, { blockId }),
    {
      responseType: 'arraybuffer',
    },
  );

  return response.data;
};

const getCoatingInspectionReport = async (blockId: string) => {
  const response = await apiClient.get<ArrayBuffer>(
    `${API_REPORTS_COATING_INSPECTION}/${blockId}`,
    {
      responseType: 'arraybuffer',
    },
  );

  return response.data;
};

const deserializeInspectionReports = (data: { [key: string]: unknown }) => {
  const {
    blockId,
    blockName,
    shipName,
    inOut,
    recentInspectionTask,
    recentInspectionDate,
  } = data;

  const inspectionReport: InspectionReport = {
    blockId: blockId as string,
    blockName: blockName as string,
    shipName: shipName as string,
    inOut: inOut as BlockInOut,
    recentInspectionTask: recentInspectionTask as TaskStep,
    recentInspectionDate: dateUtil.parseDate(recentInspectionDate as string),
  };

  return inspectionReport;
};

const deserializeReportHistory = (data: { [key: string]: unknown }) => {
  const { taskId, taskStep, completionDate, result } = data;

  const inspectionReport: InspectionReportHistory = {
    taskId: taskId as string,
    taskStep: taskStep as TaskStep,
    completionDate: dateUtil.parseDate(completionDate as string),
    result: result as InspectionReportResult,
  };

  return inspectionReport;
};

export default {
  getMyInspectionReports,
  getInspectionReportHistories,
  getSurfacePreparationReport,
  getFinalCoatingReport,
  getCoatingInspectionReport,
};
