import { useEffect, useState } from 'react';

const useSelectWidth = ({
  selectWrapperRef,
  selectRef,
  fontWidth,
  iconSpacing,
}: {
  selectWrapperRef: React.RefObject<HTMLElement>;
  selectRef: React.RefObject<HTMLSelectElement>;
  fontWidth: number;
  iconSpacing: number;
}) => {
  const [calculatedWidth, setCalculatedWidth] = useState<number>();

  useEffect(() => {
    if (!selectRef.current || !selectWrapperRef.current) return;

    const { selectedIndex, options } = selectRef.current;

    if (!options[selectedIndex]) return;

    const textLength = options[selectedIndex].text.length;
    const textWidth = textLength * fontWidth;
    const responsiveWidth = textWidth + iconSpacing;

    setCalculatedWidth(responsiveWidth);
  }, [
    selectWrapperRef,
    selectRef.current?.selectedIndex,
    fontWidth,
    iconSpacing,
    setCalculatedWidth,
  ]);

  return { calculatedWidth };
};

export default useSelectWidth;
