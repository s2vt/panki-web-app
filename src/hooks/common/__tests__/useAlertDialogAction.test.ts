import { act, renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import { OpenAlertDialogArg, actions } from '@src/reducers/alertDialog';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import useAlertDialogAction from '../useAlertDialogAction';

describe('useAlertDialogAction hook', () => {
  const setup = (initialState: Pick<RootState, 'alertDialog'>) => {
    const { wrapper, store } = prepareMockWrapper(initialState as RootState);
    const { result } = renderHook(() => useAlertDialogAction(), { wrapper });

    return { wrapper, store, result };
  };

  it('should success call open dialog pending action when isOpen state is false', () => {
    // given
    const initialState: Pick<RootState, 'alertDialog'> = {
      alertDialog: {
        isOpen: false,
        text: faker.datatype.string(),
        color: undefined,
        position: 'center',
      },
    };

    const sampleOpenDialogArg: OpenAlertDialogArg = {
      text: faker.datatype.string(),
      openDialogTime: 0,
      position: 'center',
      color: faker.internet.color(),
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.openAlertDialog(sampleOpenDialogArg);
    });

    // then
    const isPendingActionCalled = !!store
      .getActions()
      .find(
        (action) =>
          action.type === actions.openAlertDialogWithDelay.pending.type,
      );

    expect(isPendingActionCalled).toEqual(true);
  });

  it('should call open dialog fulfilled action when after opening a dialog and openDialogTime elapsed', async () => {
    // given
    const initialState: Pick<RootState, 'alertDialog'> = {
      alertDialog: {
        isOpen: false,
        text: faker.datatype.string(),
        color: undefined,
        position: 'center',
      },
    };

    const sampleOpenDialogTime = 1;

    const sampleOpenDialogArg: OpenAlertDialogArg = {
      text: faker.datatype.string(),
      openDialogTime: sampleOpenDialogTime,
      position: 'center',
      color: faker.internet.color(),
    };

    // when
    const { store, result } = setup(initialState);
    await act(async () => {
      result.current.openAlertDialog(sampleOpenDialogArg);
      await new Promise((resolve) => setTimeout(resolve, sampleOpenDialogTime));
    });

    // then
    const isOpenAlertDialogActionCalled = !!store
      .getActions()
      .find((action) => action.type === actions.openAlertDialog.type);

    expect(isOpenAlertDialogActionCalled).toEqual(true);
  });

  it('should not call open dialog action when isOpen state is true', () => {
    // given
    const initialState: Pick<RootState, 'alertDialog'> = {
      alertDialog: {
        isOpen: true,
        text: faker.datatype.string(),
        color: undefined,
        position: 'center',
      },
    };

    const sampleOpenDialogArg: OpenAlertDialogArg = {
      text: faker.datatype.string(),
      openDialogTime: 0,
      position: 'center',
      color: faker.internet.color(),
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.openAlertDialog(sampleOpenDialogArg);
    });

    // then
    const isOpenAlertDialogActionCalled = !!store
      .getActions()
      .find((action) => action.type === actions.openAlertDialog.type);

    expect(isOpenAlertDialogActionCalled).toEqual(false);
  });

  it('should success call close dialog action', () => {
    // given
    const initialState: Pick<RootState, 'alertDialog'> = {
      alertDialog: {
        isOpen: true,
        text: faker.datatype.string(),
        color: undefined,
        position: 'center',
      },
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.closeAlertDialog();
    });

    // then
    expect(store.getActions()).toEqual([actions.closeAlertDialog()]);
  });
});
