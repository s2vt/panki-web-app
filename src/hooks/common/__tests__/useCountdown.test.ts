import { act, renderHook } from '@testing-library/react-hooks/dom';
import useCountdown from '../useCountdown';

describe('useCountdown hook', () => {
  const setup = (initialCount: number) =>
    renderHook(() => useCountdown(initialCount));

  it('should count is started when use useCountdown hook', () => {
    // given
    const initialCount = 10;
    jest.useFakeTimers();

    // when
    const { result } = setup(initialCount);

    // then
    expect(result.current.count).toEqual(initialCount);
    expect(result.current.isCountOver).toEqual(false);
  });

  it('should count is over when after initialCount time passed', () => {
    // given
    const initialCount = 10;
    jest.useFakeTimers();

    // when
    const { result } = setup(initialCount);

    act(() => {
      jest.runAllTimers();
    });

    // then
    expect(result.current.count).toEqual(0);
    expect(result.current.isCountOver).toEqual(true);
  });

  it('should restart count when use resetCount event', () => {
    // given
    const initialCount = 10;
    jest.useFakeTimers();

    // when
    const { result } = setup(initialCount);

    act(() => {
      jest.runAllTimers();
      result.current.resetCount();
    });

    // then
    expect(result.current.count).toEqual(initialCount);
    expect(result.current.isCountOver).toEqual(false);
  });
});
