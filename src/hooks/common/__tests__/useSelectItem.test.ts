import { renderHook, act } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import { SelectItem } from '@src/types/common.types';
import useSelectItems from '../useSelectItems';

describe('useSelectItems hook', () => {
  const setup = () => renderHook(() => useSelectItems());

  it('should change selected items state when use setSelectedItems', () => {
    // given
    const sampleSelectItem: SelectItem = {
      id: faker.datatype.number().toString(),
      label: faker.datatype.string(),
    };

    // when
    const { result } = setup();

    act(() => {
      result.current.setSelectedItems((prevSelectedItems) => [
        ...prevSelectedItems,
        sampleSelectItem,
      ]);
    });

    // then
    expect(result.current.selectedItems.length).toBeGreaterThan(0);
    expect(result.current.selectedItems[0]).toEqual(sampleSelectItem);
  });

  it('should isSelected function return true when select item is includes into selected items state', () => {
    // given
    const sampleSelectItem: SelectItem = {
      id: faker.datatype.number().toString(),
      label: faker.datatype.string(),
    };

    // when
    const { result } = setup();

    act(() => {
      result.current.setSelectedItems((prevSelectedItems) => [
        ...prevSelectedItems,
        sampleSelectItem,
      ]);
    });

    // then
    expect(result.current.isSelected(sampleSelectItem)).toEqual(true);
  });

  it('should add item to selected items state when input select item is not selected', () => {
    // given
    const sampleSelectItem: SelectItem = {
      id: faker.datatype.number().toString(),
      label: faker.datatype.string(),
    };

    // when
    const { result } = setup();

    act(() => {
      result.current.toggleItem(sampleSelectItem);
    });

    // then
    expect(result.current.selectedItems.length).toEqual(1);
    expect(result.current.selectedItems[0]).toEqual(sampleSelectItem);
  });

  it('should remove item to selected items state when input select item is selected', () => {
    // given
    const sampleSelectItem: SelectItem = {
      id: faker.datatype.number().toString(),
      label: faker.datatype.string(),
    };

    // when
    const { result } = setup();

    act(() => {
      result.current.toggleItem(sampleSelectItem);
    });
    act(() => {
      result.current.toggleItem(sampleSelectItem);
    });

    // then
    expect(result.current.selectedItems.length).toEqual(0);
  });
});
