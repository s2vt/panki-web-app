import { act, renderHook } from '@testing-library/react-hooks/dom';
import useWindowSize from '../useWindowSize';

describe('useWindowSize hook', () => {
  const setup = () => renderHook(() => useWindowSize());

  beforeAll(() => {
    window.resizeTo = function resizeTo(width, height) {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'));
    };
  });

  it('should get window size', () => {
    const { result } = setup();

    expect(result.current.width).not.toBeUndefined();
    expect(result.current.height).not.toBeUndefined();
  });

  it('should change dimensions when resize window', () => {
    const { result } = setup();

    act(() => {
      window.resizeTo(100, 100);
    });

    expect(result.current.width).toEqual(100);
    expect(result.current.height).toEqual(100);
  });
});
