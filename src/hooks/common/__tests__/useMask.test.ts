import { renderHook } from '@testing-library/react-hooks/dom';
import useMask from '../useMask';

describe('usePhoneNumberMask hook', () => {
  const setup = () => renderHook(() => useMask());

  it('should masking event target value telephone number', () => {
    // given
    const phoneNumber = '0101234567';

    // when
    const { result } = setup();

    const { maskPhoneNumber } = result.current;

    const maskedValue = maskPhoneNumber(phoneNumber);

    // then
    expect(maskedValue).toEqual('010-123-4567');
  });

  it('should masking event target value cell phone number', () => {
    // given
    const phoneNumber = '01012345678';

    // when
    const { result } = setup();

    const { maskPhoneNumber } = result.current;

    const maskedValue = maskPhoneNumber(phoneNumber);

    // then
    expect(maskedValue).toEqual('010-1234-5678');
  });

  it('should masking event target value to Korean', () => {
    // given
    const value = '테스트test';

    // when
    const { result } = setup();

    const { maskKorean } = result.current;

    const maskedValue = maskKorean(value);

    // then
    expect(maskedValue).toEqual('테스트');
  });

  it('should masking event target value to English', () => {
    // given
    const value = '테스트test';

    // when
    const { result } = setup();

    const { maskEnglish } = result.current;

    const maskedValue = maskEnglish(value);

    // then
    expect(maskedValue).toEqual('test');
  });

  it('should masking event target value to number', () => {
    // given
    const value = '1a2b3c4d';

    // when
    const { result } = setup();

    const { maskNumber } = result.current;

    const maskedValue = maskNumber(value);

    // then
    expect(maskedValue).toEqual('1234');
  });

  it('should masking event target value to English and number', () => {
    // given
    const value = '000111aaa아아아@(#)*@!)555';

    // when
    const { result } = setup();

    const { maskEnglishNumber } = result.current;

    const maskedValue = maskEnglishNumber(value);

    // then
    expect(maskedValue).toEqual('000111aaa555');
  });
});
