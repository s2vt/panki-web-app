import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { renderHook } from '@testing-library/react-hooks/dom';
import useSidebarSelector from '../useSidebarSelector';

describe('useSidebarSelector hook', () => {
  const setup = (initialState: Pick<RootState, 'sidebar'>) => {
    const { wrapper } = prepareMockWrapper(initialState as RootState);

    const { result } = renderHook(() => useSidebarSelector(), { wrapper });

    return { result };
  };

  it("return store's sidebar state", () => {
    // given
    const initialState: Pick<RootState, 'sidebar'> = {
      sidebar: { isOpen: true },
    };

    const { result } = setup(initialState);

    // when
    const { isOpen } = result.current;

    // then
    expect(isOpen).toEqual(true);
  });
});
