import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import usePagination from '../usePagination';

describe('usePagination hook', () => {
  const setup = (items: unknown[], criteria: number) =>
    renderHook(() => usePagination(items, criteria));

  describe('pageItems', () => {
    it('should split items by criteria', () => {
      // given
      const items = Array.from(Array(20).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      // then
      expect(result.current.lastPage).toEqual(2);
      expect(result.current.currentPageItems.length).toEqual(10);
    });

    it('should last page items to be the remainder of dividing by the criteria', () => {
      // given
      const items = Array.from(Array(15).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(result.current.lastPage));

      // then
      expect(result.current.currentPageItems.length).toEqual(5);
    });
  });

  describe('setCurrentPage', () => {
    it('should success change current page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(5));

      // then
      expect(result.current.currentPage).toEqual(5);
    });

    it('should set current page as the first page when input smaller than first page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(-10));

      // then
      expect(result.current.currentPage).toEqual(1);
    });

    it('should set current page as the last page when input greater than last page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(1000));

      // then
      expect(result.current.currentPage).toEqual(result.current.lastPage);
    });
  });

  describe('previousPage', () => {
    it('should success change to previous page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(2));

      act(() => result.current.previousPage());

      // then
      expect(result.current.currentPage).toEqual(1);
    });

    it('should not change when previous page smaller then first page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(2));

      act(() => result.current.previousPage());
      act(() => result.current.previousPage());
      act(() => result.current.previousPage());

      // then
      expect(result.current.currentPage).toEqual(1);
    });
  });

  describe('nextPage', () => {
    it('should success change to next page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(2));

      act(() => result.current.nextPage());

      // then
      expect(result.current.currentPage).toEqual(3);
    });

    it('should not change when next page greater then last page', () => {
      // given
      const items = Array.from(Array(80).keys());
      const criteria = 10;

      // when
      const { result } = setup(items, criteria);

      act(() => result.current.setCurrentPage(result.current.lastPage));

      act(() => result.current.nextPage());
      act(() => result.current.nextPage());
      act(() => result.current.nextPage());

      // then
      expect(result.current.currentPage).toEqual(result.current.lastPage);
    });
  });
});
