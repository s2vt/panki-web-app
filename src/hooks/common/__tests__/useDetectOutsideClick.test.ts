import React, { RefObject } from 'react';
import { act, renderHook } from '@testing-library/react-hooks/dom';

import useDetectOutsideClick from '../useDetectOutsideClick';

describe('useDetectOutsideClick hook', () => {
  const setup = (
    ref: RefObject<HTMLElement> = React.createRef<HTMLElement>(),
    initialState = false,
  ) => renderHook(() => useDetectOutsideClick(ref, initialState));

  it('should change isOpen state when use setIsOpen function', () => {
    // given
    const initialState = false;

    // when
    const { result } = setup(undefined, initialState);

    act(() => result.current[1](true));

    // then
    expect(result.current[0]).toEqual(true);
  });
});
