import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import { actions } from '@src/reducers/sidebar';
import useSidebarAction from '../useSidebarAction';

describe('useSidebarAction hook', () => {
  const setup = () => {
    const { wrapper, store } = prepareMockWrapper();

    const { result } = renderHook(() => useSidebarAction(), { wrapper });

    return { result, store };
  };

  it('success call setIsOpen action', () => {
    // given
    const isOpen = true;

    const { result, store } = setup();

    // when
    act(() => {
      result.current.setIsOpen(isOpen);
    });

    // then
    expect(store.getActions()[0]).toEqual(actions.setIsOpen(isOpen));
  });
});
