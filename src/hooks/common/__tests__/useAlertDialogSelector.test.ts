import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import { AlertDialogState } from '@src/reducers/alertDialog';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import useAlertDialogSelector from '../useAlertDialogSelector';

describe('useAlertDialogSelector Hook', () => {
  const setup = (initialState: Pick<RootState, 'alertDialog'>) => {
    const { wrapper, store } = prepareMockWrapper(initialState as RootState);
    const { result } = renderHook(() => useAlertDialogSelector(), { wrapper });

    return { wrapper, store, result };
  };

  it("should return store's alertDialog state", () => {
    // given
    const sampleAlertDialogState: AlertDialogState = {
      isOpen: true,
      color: undefined,
      text: faker.datatype.string(),
      position: undefined,
    };

    const initialState: Pick<RootState, 'alertDialog'> = {
      alertDialog: sampleAlertDialogState,
    };

    // when
    const { result } = setup(initialState);

    // then
    expect(result.current.alertDialog).toEqual(sampleAlertDialogState);
  });
});
