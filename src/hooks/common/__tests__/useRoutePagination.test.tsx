import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import useRoutePagination from '../useRoutePagination';

describe('useRoutePagination hook', () => {
  const setup = ({
    pageParam,
    lastPage,
    basePath,
    setCurrentPage,
  }: {
    pageParam: string | undefined;
    lastPage: number;
    basePath: ROUTE_PATHS;
    setCurrentPage: (page: number) => void;
  }) => {
    const history = createMemoryHistory();

    const wrapper = ({ children }: { children: React.ReactNode }) => (
      <Router history={history}>{children}</Router>
    );

    const result = renderHook(
      () =>
        useRoutePagination({
          pageParam,
          lastPage,
          basePath,
          setCurrentPage,
        }),
      { wrapper },
    );

    return { ...result, history };
  };

  it('should current page equal when page param less than last page', () => {
    const { result } = setup({
      pageParam: '2',
      lastPage: 10,
      basePath: ROUTE_PATHS.user,
      setCurrentPage: jest.fn(),
    });

    expect(result.current.currentPage).toEqual(2);
  });

  it('should current page is 1 when page param more than last page', () => {
    const { result } = setup({
      pageParam: '2',
      lastPage: 1,
      basePath: ROUTE_PATHS.user,
      setCurrentPage: jest.fn(),
    });

    expect(result.current.currentPage).toEqual(1);
  });

  it('should route push with base path and page', () => {
    const basePath = ROUTE_PATHS.user;

    const { result, history } = setup({
      pageParam: '2',
      lastPage: 10,
      basePath,
      setCurrentPage: jest.fn(),
    });

    act(() => result.current.handlePageClick(5));

    expect(history.location.pathname).toEqual(`${basePath}/5`);
  });
});
