import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
  AlertDialogPosition,
  OpenAlertDialogArg,
  actions,
} from '@src/reducers/alertDialog';

const useAlertDialogAction = () => {
  const dispatch = useDispatch();

  const openAlertDialog = useCallback(
    ({
      openDialogTime = 1.5 * 1000,
      position = 'center',
      ...rest
    }: Omit<OpenAlertDialogArg, 'openDialogTime' | 'position'> & {
      openDialogTime?: number;
      position?: AlertDialogPosition;
    }) => {
      dispatch(
        actions.openAlertDialogWithDelay({ openDialogTime, position, ...rest }),
      );
    },
    [dispatch],
  );

  const closeAlertDialog = useCallback(
    () => dispatch(actions.closeAlertDialog()),
    [dispatch],
  );

  return { openAlertDialog, closeAlertDialog };
};

export default useAlertDialogAction;
