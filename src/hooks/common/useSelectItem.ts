import { useCallback, useState } from 'react';

const useSelectItem = <T>(initialState: T) => {
  const [selectedItem, setSelectedItem] = useState<T>(initialState);

  const handleChange = useCallback(
    (selectItem: T) => setSelectedItem(selectItem),
    [setSelectedItem],
  );

  return {
    selectedItem,
    setSelectedItem,
    handleChange,
  };
};

export default useSelectItem;
