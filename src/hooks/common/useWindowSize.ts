import { useEffect, useState } from 'react';

const useWindowSize = () => {
  function getSize() {
    return {
      width: window.innerWidth ?? undefined,
      height: window.innerHeight ?? undefined,
    };
  }

  const [windowSize, setWindowSize] = useState(getSize);

  useEffect(() => {
    function handleResize() {
      setWindowSize(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowSize;
};

export default useWindowSize;
