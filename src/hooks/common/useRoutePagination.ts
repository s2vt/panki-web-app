import { useCallback, useEffect, useMemo } from 'react';
import { useHistory } from 'react-router';
import { ROUTE_PATHS } from '@src/routes/routePaths';

const useRoutePagination = ({
  pageParam,
  lastPage,
  basePath,
  setCurrentPage,
}: {
  pageParam: string | undefined;
  lastPage: number;
  basePath: ROUTE_PATHS;
  setCurrentPage: (page: number) => void;
}) => {
  const history = useHistory();

  const currentPage = useMemo(() => {
    if (pageParam === undefined || Number(pageParam) > lastPage) {
      return 1;
    }

    return Number(pageParam);
  }, [pageParam, lastPage]);

  useEffect(() => {
    setCurrentPage(currentPage);
  }, [currentPage, setCurrentPage]);

  const handlePageClick = useCallback(
    (page: number) => {
      history.push(`${basePath}/${page}`);
    },
    [history],
  );

  return { currentPage, handlePageClick };
};

export default useRoutePagination;
