import { useCallback } from 'react';

const NUMBER_REG_EXP = /[^0-9]/g;
const ENGLISH_REG_EXP = /[^A-Za-z]/gi;
const KOREAN_REG_EXP = /[a-z0-9]|[ [\]{}()<>?|`~!@#$%^&*-_+=,.;:"'\\]/g;
const PHONE_REG_EXP = /(^02.{0}|^01.{1}|[0-9]{3,4})([0-9]+)([0-9]{4})/;
const ENGLISH_NUMBER_REG_EXP = /[^A-Za-z0-9]/g;

const useMask = () => {
  const maskPhoneNumber = useCallback((inputValue: string) => {
    let maskedValue = inputValue.replace(NUMBER_REG_EXP, '');

    maskedValue = maskedValue.replace(PHONE_REG_EXP, '$1-$2-$3');

    return maskedValue;
  }, []);

  const maskNumber = useCallback(
    (inputValue: string) => maskingWithRegExp(inputValue, NUMBER_REG_EXP),
    [],
  );

  const maskEnglish = useCallback(
    (inputValue: string) => maskingWithRegExp(inputValue, ENGLISH_REG_EXP),
    [],
  );

  const maskKorean = useCallback(
    (inputValue: string) => maskingWithRegExp(inputValue, KOREAN_REG_EXP),
    [],
  );

  const maskEnglishNumber = useCallback(
    (inputValue: string) =>
      maskingWithRegExp(inputValue, ENGLISH_NUMBER_REG_EXP),
    [],
  );

  const maskingWithRegExp = useCallback(
    (inputValue: string, regExp: RegExp) => {
      const maskedValue = inputValue.replace(regExp, '');

      return maskedValue;
    },
    [],
  );

  return {
    maskPhoneNumber,
    maskNumber,
    maskEnglish,
    maskKorean,
    maskEnglishNumber,
  };
};

export default useMask;
