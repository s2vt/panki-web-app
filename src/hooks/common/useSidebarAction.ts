import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { actions } from '../../reducers/sidebar';

const useSidebarAction = () => {
  const dispatch = useDispatch();

  const setIsOpen = useCallback(
    (isOpen: boolean) => {
      dispatch(actions.setIsOpen(isOpen));
    },
    [dispatch, actions],
  );

  return { setIsOpen };
};

export default useSidebarAction;
