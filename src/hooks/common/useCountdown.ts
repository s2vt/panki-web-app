import { useEffect, useRef, useState } from 'react';

const useCountdown = (initialCount: number) => {
  const [count, setCount] = useState(initialCount);
  const countdownRef = useRef<NodeJS.Timeout>();

  const isCountOver = count <= 0;

  const clearCountdownInterval = () => {
    if (countdownRef.current !== undefined) {
      clearInterval(countdownRef.current);
    }
  };

  const startCount = () => {
    countdownRef.current = setInterval(
      () => setCount((prevCount) => prevCount - 1),
      1000,
    );
  };

  const resetCount = () => {
    clearCountdownInterval();
    setCount(initialCount);
    startCount();
  };

  useEffect(() => {
    if (count > 0) {
      startCount();
    }

    return () => {
      clearCountdownInterval();
    };
  }, [count]);

  return { count, setCount, resetCount, isCountOver };
};

export default useCountdown;
