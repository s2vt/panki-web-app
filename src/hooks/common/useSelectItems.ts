import { useCallback, useMemo, useState } from 'react';
import { SelectItem } from '@src/types/common.types';

const useSelectItems = <T>(initialState: (SelectItem & T)[] = []) => {
  const [selectedItems, setSelectedItems] =
    useState<(SelectItem & T)[]>(initialState);

  const selectedItemsIds = useMemo(
    () => selectedItems?.map((selectedItem) => selectedItem.id),
    [selectedItems],
  );

  const isSelected = useCallback(
    (selectItem: SelectItem & T) => selectedItemsIds.includes(selectItem.id),
    [selectedItemsIds],
  );

  const toggleItem = useCallback(
    (selectItem: SelectItem & T) => {
      if (isSelected(selectItem)) {
        setSelectedItems((prevSelectedItems) =>
          prevSelectedItems.filter(
            (selectedItem) => selectedItem.id !== selectItem.id,
          ),
        );
      } else {
        setSelectedItems((prevSelectedItems) => [
          ...prevSelectedItems,
          selectItem,
        ]);
      }
    },

    [setSelectedItems, isSelected],
  );

  return {
    selectedItems,
    setSelectedItems,
    toggleItem,
    isSelected,
  };
};

export default useSelectItems;
