import { useTypedSelector } from '../../reducers/rootReducer';

const useSidebarSelector = () => {
  const isOpen = useTypedSelector((state) => state.sidebar.isOpen);

  return { isOpen };
};

export default useSidebarSelector;
