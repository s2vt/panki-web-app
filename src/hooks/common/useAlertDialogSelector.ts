import { useTypedSelector } from '@src/reducers/rootReducer';

const useAlertDialogSelector = () => {
  const alertDialog = useTypedSelector((state) => state.alertDialog);

  return { alertDialog };
};

export default useAlertDialogSelector;
