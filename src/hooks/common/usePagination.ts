import { useCallback, useMemo, useState } from 'react';

const usePagination = <T>(items: T[], criteria: number) => {
  const [currentPage, setCurrentPage] = useState(1);

  const lastPage = useMemo(
    () => (items.length > 0 ? Math.ceil(items.length / criteria) : 1),
    [items],
  );

  const hasPreviousPage = useMemo(() => currentPage > 1, [items]);
  const hasNextPage = useMemo(() => currentPage < lastPage, [items]);

  const pageItems = useMemo(() => {
    const copiedItems = [...items];

    return Array.from({ length: lastPage }).map((_) =>
      copiedItems.splice(0, criteria),
    );
  }, [items, criteria, lastPage]);

  const currentPageItems = useMemo(
    () => pageItems[currentPage - 1] ?? [],
    [currentPage, pageItems],
  );

  const handleCurrentPage = useCallback(
    (page: number) => {
      if (page < 1) {
        setCurrentPage(1);
        return;
      }

      if (page > lastPage) {
        setCurrentPage(lastPage);
        return;
      }

      setCurrentPage(page);
    },
    [lastPage, setCurrentPage],
  );

  const previousPage = useCallback(() => {
    if (currentPage > 1) {
      setCurrentPage((prevCurrentPage) => prevCurrentPage - 1);
    }
  }, [currentPage, setCurrentPage]);

  const nextPage = useCallback(() => {
    if (currentPage < lastPage) {
      setCurrentPage((prevCurrentPage) => prevCurrentPage + 1);
    }
  }, [currentPage, setCurrentPage, lastPage]);

  return {
    currentPage,
    setCurrentPage: handleCurrentPage,
    currentPageItems,
    previousPage,
    nextPage,
    lastPage,
    hasPreviousPage,
    hasNextPage,
  };
};

export default usePagination;
