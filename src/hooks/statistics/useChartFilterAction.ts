import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { ChartFilterParams } from '@src/types/statistics.types';
import { ChartFilterPeriod, actions } from '@src/reducers/chartFilter';
import { SubcontractorCompany } from '@src/types/company.types';

const useChartFilterAction = () => {
  const dispatch = useDispatch();

  const setChartFilterParams = useCallback(
    (chartFilterParams: ChartFilterParams) => {
      dispatch(actions.setChartFilterParams(chartFilterParams));
    },
    [dispatch],
  );

  const setPeriod = useCallback(
    (period: ChartFilterPeriod) => {
      dispatch(actions.setPeriod(period));
    },
    [dispatch],
  );

  const setSearchQuery = useCallback(
    (query: string) => {
      dispatch(actions.setSearchQuery(query));
    },
    [dispatch],
  );

  const setSubcontractorCompany = useCallback(
    (subcontractorCompany: SubcontractorCompany) => {
      dispatch(actions.setSubcontractorCompany(subcontractorCompany));
    },
    [dispatch],
  );

  return {
    setChartFilterParams,
    setPeriod,
    setSearchQuery,
    setSubcontractorCompany,
  };
};

export default useChartFilterAction;
