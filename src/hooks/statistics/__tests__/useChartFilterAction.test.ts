import { act, renderHook } from '@testing-library/react-hooks/dom';
import rootState from '@src/test/fixtures/reducers/rootState';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { ChartFilterPeriod, actions } from '@src/reducers/chartFilter';
import { RootState } from '@src/reducers/rootReducer';
import { SubcontractorCompany } from '@src/types/company.types';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterParams,
  ChartFilterType,
  CoatingFilterTask,
} from '@src/types/statistics.types';
import useChartFilterAction from '../useChartFilterAction';

describe('useChartFilterAction hook', () => {
  const setup = (initialState: RootState) => {
    const { wrapper, store } = prepareMockWrapper(initialState);
    const { result } = renderHook(() => useChartFilterAction(), { wrapper });

    return { store, result };
  };

  it('should success call set chart filter action', () => {
    // given
    const initialState = rootState;
    const chartFilterParams: ChartFilterParams = {
      searchType: ChartFilterType.all,
      searchDateType: ChartFilterDateType.year,
      blastingType: BlastingFilterTask.inPrimary,
      ctgType: CoatingFilterTask.final,
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.setChartFilterParams(chartFilterParams);
    });

    // then
    expect(store.getActions()).toEqual([
      actions.setChartFilterParams(chartFilterParams),
    ]);
  });

  it('should success call set period action', () => {
    // given
    const initialState = rootState;
    const period: ChartFilterPeriod = 'currentMonth';

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.setPeriod(period);
    });

    // then
    expect(store.getActions()).toEqual([actions.setPeriod(period)]);
  });

  it('should success call set search query action', () => {
    // given
    const initialState = rootState;
    const searchQuery = 'searchQuery';

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.setSearchQuery(searchQuery);
    });

    // then
    expect(store.getActions()).toEqual([actions.setSearchQuery(searchQuery)]);
  });

  it('should success call set subcontractor company action', () => {
    // given
    const initialState = rootState;
    const subcontractorCompany: SubcontractorCompany = {
      id: '1',
      companyName: 'subcontractorCompany1',
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.setSubcontractorCompany(subcontractorCompany);
    });

    // then
    expect(store.getActions()).toEqual([
      actions.setSubcontractorCompany(subcontractorCompany),
    ]);
  });
});
