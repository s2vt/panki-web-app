import { renderHook } from '@testing-library/react-hooks/dom';
import rootState from '@src/test/fixtures/reducers/rootState';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { ChartFilterState } from '@src/reducers/chartFilter';
import { RootState } from '@src/reducers/rootReducer';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterType,
  CoatingFilterTask,
} from '@src/types/statistics.types';
import useChartFilterSelector from '../useChartFilterSelector';

describe('useChartFilterSelector hook', () => {
  const setup = (initialState: RootState) => {
    const { wrapper, store } = prepareMockWrapper(initialState);
    const { result } = renderHook(() => useChartFilterSelector(), { wrapper });

    return { store, result };
  };

  it("should return store's chart filter state", () => {
    // given
    const chartFilterState: ChartFilterState = {
      chartFilterParams: {
        searchType: ChartFilterType.all,
        searchDateType: ChartFilterDateType.year,
        blastingType: BlastingFilterTask.inPrimary,
        ctgType: CoatingFilterTask.final,
      },
      period: 'currentMonth',
      searchQuery: 'searchQuery',
      subcontractorCompany: {
        id: '1',
        companyName: 'subcontractorCompany1',
      },
    };

    const initialState: RootState = {
      ...rootState,
      chartFilter: chartFilterState,
    };

    // when
    const { result } = setup(initialState);

    // then
    expect(result.current.chartFilterParams).toEqual(
      chartFilterState.chartFilterParams,
    );
    expect(result.current.period).toEqual(chartFilterState.period);
    expect(result.current.searchQuery).toEqual(chartFilterState.searchQuery);
    expect(result.current.subcontractorCompany).toEqual(
      chartFilterState.subcontractorCompany,
    );
  });
});
