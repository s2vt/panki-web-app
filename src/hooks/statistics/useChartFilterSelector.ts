import { useTypedSelector } from '@src/reducers/rootReducer';

const useChartFilterSelector = () => {
  const chartFilterParams = useTypedSelector(
    (state) => state.chartFilter.chartFilterParams,
  );

  const period = useTypedSelector((state) => state.chartFilter.period);

  const searchQuery = useTypedSelector(
    (state) => state.chartFilter.searchQuery,
  );

  const subcontractorCompany = useTypedSelector(
    (state) => state.chartFilter.subcontractorCompany,
  );

  return { chartFilterParams, period, searchQuery, subcontractorCompany };
};

export default useChartFilterSelector;
