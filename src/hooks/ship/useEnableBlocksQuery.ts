import { useQuery, UseQueryOptions } from 'react-query';
import blockApi from '@src/api/blockApi';
import HttpError from '@src/api/model/HttpError';
import { Block } from '@src/types/block.types';

const useEnableBlocksQuery = (
  shipId: string,
  options?: UseQueryOptions<Block[], HttpError>,
) =>
  useQuery(createKey(shipId), () => blockApi.getEnableBlocks(shipId), options);

const baseKey = ['shipEnableBlocks'];
const createKey = (shipId: string) => [...baseKey, shipId];

useEnableBlocksQuery.baseKey = baseKey;
useEnableBlocksQuery.createKey = createKey;

export default useEnableBlocksQuery;
