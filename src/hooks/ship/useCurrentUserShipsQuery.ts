import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import { Ship, ShipFilter } from '@src/types/ship.types';

const useCurrentUserShipsQuery = (
  options?: UseQueryOptions<Ship[], HttpError> & {
    shipFilter?: ShipFilter;
  },
) =>
  useQuery(createKey(), shipApi.getCurrentUserShips, {
    ...options,
    select: (ships: Ship[]) => filterShips(ships, options?.shipFilter),
  });

const filterShips = (ships: Ship[], shipFilter?: ShipFilter) => {
  const filteredShips = ships.filter((ship) => {
    if (
      shipFilter?.shipName !== undefined &&
      !ship.shipName.includes(shipFilter.shipName)
    ) {
      return false;
    }

    if (shipFilter?.state !== undefined && shipFilter.state !== ship.state) {
      return false;
    }

    return true;
  });

  return filteredShips;
};

const baseKey = ['currentUserShips'];
const createKey = () => [...baseKey];

useCurrentUserShipsQuery.filterShips = filterShips;
useCurrentUserShipsQuery.baseKey = baseKey;
useCurrentUserShipsQuery.createKey = createKey;

export default useCurrentUserShipsQuery;
