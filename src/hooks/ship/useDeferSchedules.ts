import HttpError from '@src/api/model/HttpError';
import scheduleApi from '@src/api/scheduleApi';
import { SelectStringItem } from '@src/types/common.types';
import { useCallback, useState } from 'react';
import { QueryKey, useMutation, useQueryClient } from 'react-query';
import useAlertDialogAction from '../common/useAlertDialogAction';
import useSelectItems from '../common/useSelectItems';

type UseDeferSchedulesProps = {
  invalidateQueryKey: QueryKey;
  notSelectedErrorMessage: string;
};

const useDeferSchedules = ({
  invalidateQueryKey,
  notSelectedErrorMessage,
}: UseDeferSchedulesProps) => {
  const [isModifyMode, setIsModifyMode] = useState(false);
  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);

  const queryClient = useQueryClient();
  const { openAlertDialog } = useAlertDialogAction();

  const {
    selectedItems: selectedTaskItems,
    setSelectedItems: setSelectedTaskItems,
    isSelected,
    toggleItem,
  } = useSelectItems<SelectStringItem>();

  const handleDeferScheduleSuccess = () => {
    setSelectedTaskItems([]);
    queryClient.invalidateQueries(invalidateQueryKey);
  };

  const handleDeferScheduleError = (error: HttpError) =>
    openAlertDialog({ text: error.message, position: 'rightTop' });

  const { mutate } = useMutation(
    (taskIds: string[]) => scheduleApi.deferInspectionSchedules(taskIds),
    {
      onSuccess: handleDeferScheduleSuccess,
      onError: handleDeferScheduleError,
    },
  );

  const handleModifyClick = useCallback(
    () => setIsModifyMode(true),
    [setIsModifyMode],
  );

  const handleCancelClick = useCallback(() => {
    setSelectedTaskItems([]);
    setIsModifyMode(false);
  }, [setSelectedTaskItems, setIsModifyMode]);

  const closeConfirmModal = useCallback(
    () => setIsConfirmModalOpen(false),
    [setIsConfirmModalOpen],
  );

  const handleDeferClick = useCallback(() => {
    if (selectedTaskItems.length === 0) {
      openAlertDialog({
        text: notSelectedErrorMessage,
        position: 'rightTop',
      });
      return;
    }

    setIsConfirmModalOpen(true);
  }, [selectedTaskItems, openAlertDialog, setIsConfirmModalOpen]);

  const handleDeferConfirm = useCallback(() => {
    setIsModifyMode(false);
    setIsConfirmModalOpen(false);
    mutate(selectedTaskItems.map((item) => item.id));
  }, [selectedTaskItems, setIsModifyMode, setIsConfirmModalOpen, mutate]);

  return {
    isModifyMode,
    isConfirmModalOpen,
    selectedTaskItems,
    isSelected,
    toggleItem,
    handleModifyClick,
    handleCancelClick,
    closeConfirmModal,
    handleDeferClick,
    handleDeferConfirm,
  };
};

export default useDeferSchedules;
