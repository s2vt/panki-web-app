import { renderHook } from '@testing-library/react-hooks/dom';
import shipApi from '@src/api/shipApi';
import { Ship, ShipState } from '@src/types/ship.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useCurrentUserShipsQuery from '../useCurrentUserShipsQuery';

describe('useCurrentUserShipsQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useCurrentUserShipsQuery(), { wrapper });
  };

  it('should cache data when succeed fetch parent company ships', async () => {
    // given
    const sampleShips: Ship[] = [
      {
        id: '1',
        ownerCompany: 'ownerCompany',
        shipName: '0001',
        qmManager: [{ id: '1', userName: 'test' }],
        state: 'FINISHED',
      },
      {
        id: '2',
        ownerCompany: 'ownerCompany',
        shipName: '0002',
        qmManager: [{ id: '2', userName: 'test' }],
        state: 'FINISHED',
      },
    ];

    shipApi.getCurrentUserShips = jest.fn().mockResolvedValue(sampleShips);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleShips);
  });

  it('should set error when failed fetch parent company ships', async () => {
    // given
    shipApi.getCurrentUserShips = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });

  describe('filterShips', () => {
    it('should success filter with ship name', () => {
      // given
      const ships: Ship[] = [
        {
          id: '1',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '1111',
          state: ShipState.FINISHED,
        },
        {
          id: '2',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '2222',
          state: ShipState.FINISHED,
        },
      ];

      const shipName = '1111';

      // when
      const filteredShips = useCurrentUserShipsQuery.filterShips(ships, {
        shipName,
      });

      // then
      expect(filteredShips.length).toEqual(1);
    });

    it('should success filter with ship state', () => {
      // given
      const ships: Ship[] = [
        {
          id: '1',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '1111',
          state: ShipState.WORKING,
        },
        {
          id: '2',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '2222',
          state: ShipState.FINISHED,
        },
      ];

      const state = ShipState.WORKING;

      // when
      const filteredShips = useCurrentUserShipsQuery.filterShips(ships, {
        state,
      });

      // then
      expect(filteredShips.length).toEqual(1);
    });

    it('should success filter with ship name and ship state', () => {
      // given
      const ships: Ship[] = [
        {
          id: '1',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '1111',
          state: ShipState.WORKING,
        },
        {
          id: '2',
          ownerCompany: 'owner',
          qmManager: [{ id: '1', userName: 'qm' }],
          shipName: '2222',
          state: ShipState.WORKING,
        },
      ];

      const shipName = '1111';
      const state = ShipState.WORKING;

      // when
      const filteredShips = useCurrentUserShipsQuery.filterShips(ships, {
        shipName,
        state,
      });

      // then
      expect(filteredShips.length).toEqual(1);
    });
  });
});
