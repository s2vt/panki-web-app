import scheduleApi from '@src/api/scheduleApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { SelectStringItem } from '@src/types/common.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import { actions as alertDialogActions } from '@src/reducers/alertDialog';
import { QueryKey } from 'react-query';
import rootState from '@src/test/fixtures/reducers/rootState';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import HttpError from '@src/api/model/HttpError';
import { waitFor } from '@testing-library/react';
import useDeferSchedules from '../useDeferSchedules';

describe('useDeferSchedules', () => {
  const setup = ({
    invalidateQueryKey = 'queryKey',
    notSelectedErrorMessage = 'error',
  }: {
    invalidateQueryKey?: QueryKey;
    notSelectedErrorMessage?: string;
  }) => {
    const { wrapper, store, queryClient } = prepareMockWrapper(rootState);

    const deferInspectionSchedules = jest.spyOn(
      scheduleApi,
      'deferInspectionSchedules',
    );

    const { result, waitFor } = renderHook(
      () => useDeferSchedules({ invalidateQueryKey, notSelectedErrorMessage }),
      { wrapper },
    );

    return {
      store,
      queryClient,
      result,
      waitFor,
      deferInspectionSchedules,
      notSelectedErrorMessage,
    };
  };

  describe('handleModifyClick', () => {
    it('should change isModifyMode to true', () => {
      // given
      const { result } = setup({});

      // when
      act(result.current.handleModifyClick);

      // then
      expect(result.current.isModifyMode).toEqual(true);
    });
  });

  describe('handleCancelClick', () => {
    it('should change isModifyMode to false', () => {
      // given
      const { result } = setup({});

      // when
      act(result.current.handleModifyClick);
      act(result.current.handleCancelClick);

      // then
      expect(result.current.isModifyMode).toEqual(false);
    });

    it('should change selectedTaskItems to empty', () => {
      // given
      const selectedTask: SelectStringItem = {
        id: '1',
        value: '1',
      };
      const { result } = setup({});

      // when
      act(() => result.current.toggleItem(selectedTask));
      act(result.current.handleCancelClick);

      // then
      expect(result.current.selectedTaskItems.length).toEqual(0);
    });
  });

  describe('closeConfirmModal', () => {
    it('should change isConfirmModalOpen to true', () => {
      // given
      const { result } = setup({});

      // when
      act(result.current.closeConfirmModal);

      // then
      expect(result.current.isConfirmModalOpen).toEqual(false);
    });
  });

  describe('handleDeferClick', () => {
    it('should call open alert dialog action with notSelectedErrorMessage when has not task selected', () => {
      // given
      const notSelectedErrorMessage = 'task를 선택하세요';

      const { result, store } = setup({ notSelectedErrorMessage });

      // when
      act(result.current.handleDeferClick);

      // then
      expect(store.getActions()).toContainEqual(
        alertDialogActions.openAlertDialog({
          text: notSelectedErrorMessage,
          position: 'rightTop',
        }),
      );
    });

    it('should change isConfirmModalOpen to true when has task selected', () => {
      // given
      const selectedTask: SelectStringItem = {
        id: '1',
        value: '1',
      };

      const { result } = setup({});

      // when
      act(() => result.current.toggleItem(selectedTask));
      act(result.current.handleDeferClick);

      // then
      expect(result.current.isConfirmModalOpen).toEqual(true);
    });
  });

  describe('handleDeferConfirm', () => {
    it('should call deferInspectionSchedules', async () => {
      // given
      const selectedTask1: SelectStringItem = {
        id: '1',
        value: '1',
      };
      const selectedTask2: SelectStringItem = {
        id: '2',
        value: '2',
      };

      const { result, waitFor, deferInspectionSchedules } = setup({});

      deferInspectionSchedules.mockResolvedValue({
        msg: 'success',
        resultCode: ResultCode.success,
      });

      // when
      act(() => result.current.toggleItem(selectedTask1));
      act(() => result.current.toggleItem(selectedTask2));
      act(result.current.handleDeferConfirm);

      // then
      const taskIds = ['1', '2'];

      await waitFor(() =>
        expect(deferInspectionSchedules).toBeCalledWith(taskIds),
      );
    });

    it('should call open alert dialog action with error when failed defer schedules', async () => {
      // given
      const selectedTask: SelectStringItem = {
        id: '1',
        value: '1',
      };

      const { result, store, deferInspectionSchedules } = setup({});

      deferInspectionSchedules.mockRejectedValue(
        new HttpError(HttpErrorStatus.BadRequest, ResultCode.invalidRequest),
      );

      // when
      act(() => result.current.toggleItem(selectedTask));
      act(result.current.handleDeferConfirm);

      // then
      await waitFor(() =>
        expect(store.getActions()).toContainEqual(
          alertDialogActions.openAlertDialog({
            text: '잘못된 요청입니다',
            position: 'rightTop',
          }),
        ),
      );
    });
  });
});
