import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import blockApi from '@src/api/blockApi';
import { Block, BlockState } from '@src/types/block.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useEnableBlocksQuery from '../useEnableBlocksQuery';

describe('useShipEnableBlocksQuery hook', () => {
  const setup = (shipId: string) => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useEnableBlocksQuery(shipId), { wrapper });
  };

  it('should cache data when succeed fetch enable blocks', async () => {
    // given
    const shipId = faker.datatype.number().toString();

    const blocks: Block[] = [
      {
        id: '1',
        blockName: '1B11',
        blockDivision: 'PORT',
        inOut: 'IN',
        disable: false,
        state: BlockState.PREPARATION,
      },
      {
        id: '2',
        blockName: '1B11',
        blockDivision: 'PORT',
        inOut: 'OUT',
        disable: false,
        state: BlockState.PREPARATION,
      },
    ];

    blockApi.getEnableBlocks = jest.fn().mockResolvedValue(blocks);

    // when
    const { result, waitFor } = setup(shipId);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(blocks);
  });

  it('should set error when failed fetch enable ship blocks', async () => {
    // given
    const companyId = faker.datatype.number().toString();

    blockApi.getEnableBlocks = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(companyId);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
