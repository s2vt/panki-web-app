import HttpError from '@src/api/model/HttpError';
import reportApi from '@src/api/reportApi';
import inspectionReports from '@src/test/fixtures/report/inspectionReports';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { BlockInOut } from '@src/types/block.types';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { ReportFilterPayload } from '@src/types/report.types';
import { renderHook } from '@testing-library/react-hooks/dom';
import useMyInspectionReportsQuery from '../useMyInspectionReportsQuery';

describe('useMyInspectionReportsQuery hook', () => {
  const setup = (reportFilterPayload: ReportFilterPayload) => {
    const { wrapper } = prepareWrapper();

    const { result, waitFor } = renderHook(
      () => useMyInspectionReportsQuery(reportFilterPayload),
      { wrapper },
    );

    return { result, waitFor };
  };

  it('cache data when succeed fetch my inspection reports', async () => {
    // given
    const reportFilterPayload: ReportFilterPayload = {
      inOut: BlockInOut.IN,
    };

    const getMyInspectionReports = jest
      .spyOn(reportApi, 'getMyInspectionReports')
      .mockResolvedValue(inspectionReports);

    const { result, waitFor } = setup(reportFilterPayload);

    // when
    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(inspectionReports);
    expect(getMyInspectionReports).toBeCalledWith({
      inOut: BlockInOut.IN,
    });
  });

  it('remove empty values on params', async () => {
    // given
    const reportFilterPayload: ReportFilterPayload = {
      inOut: BlockInOut.IN,
      endDate: undefined,
      shipName: undefined,
      recentTaskStep: undefined,
      startDate: undefined,
    };

    const getMyInspectionReports = jest
      .spyOn(reportApi, 'getMyInspectionReports')
      .mockResolvedValue(inspectionReports);

    const { result, waitFor } = setup(reportFilterPayload);

    // when
    await waitFor(() => result.current.isSuccess);

    // then
    expect(getMyInspectionReports).toBeCalledWith({
      inOut: BlockInOut.IN,
    });
  });

  it('should set error when failed fetch my inspection reports', async () => {
    // given
    const reportFilterPayload: ReportFilterPayload = {
      inOut: BlockInOut.IN,
    };

    reportApi.getMyInspectionReports = jest
      .fn()
      .mockRejectedValue(
        new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
      );

    const { result, waitFor } = setup(reportFilterPayload);

    // when
    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
