import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import reportApi from '@src/api/reportApi';
import { InspectionReport, ReportFilterPayload } from '@src/types/report.types';
import * as commonUtil from '@src/utils/commonUtil';

const useMyInspectionReportsQuery = (
  reportFilterPayload: ReportFilterPayload,
  options?: UseQueryOptions<InspectionReport[], HttpError>,
) => {
  const params = commonUtil.removeEmpty(
    reportFilterPayload,
  ) as ReportFilterPayload;

  return useQuery(
    createKey(params),
    () => reportApi.getMyInspectionReports(params),
    options,
  );
};

const baseKey = ['myInspectionReports'];
const createKey = (reportFilterPayload: ReportFilterPayload) => [
  ...baseKey,
  reportFilterPayload,
];

useMyInspectionReportsQuery.baseKey = baseKey;
useMyInspectionReportsQuery.createKey = createKey;

export default useMyInspectionReportsQuery;
