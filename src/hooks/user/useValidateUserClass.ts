import { User, UserClass } from '@src/types/user.types';

const useValidateUserClass = (
  validationUserClasses: UserClass[],
  user: User | undefined,
) => {
  if (user === undefined) {
    return false;
  }

  if (validationUserClasses.length === 0) {
    return true;
  }

  return validationUserClasses.includes(user.className);
};

export default useValidateUserClass;
