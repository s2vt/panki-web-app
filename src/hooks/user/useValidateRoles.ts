import { User } from '@src/types/user.types';

const useValidateRoles = (
  validationRoles: string[],
  user: User | undefined,
) => {
  const userRoles = user?.roles?.map((role) => role.role) ?? [];

  const intersectionRoles = validationRoles.filter((validationRole) =>
    userRoles.some((userRole) => validationRole === userRole),
  );

  const isValidRoles = intersectionRoles.length === validationRoles.length;

  return isValidRoles;
};

export default useValidateRoles;
