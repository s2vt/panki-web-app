import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
  actions,
  SaveUserModalPage,
  SaveUserModalMode,
} from '@src/reducers/saveUserModal';
import { Role, SaveUserPayload, User } from '@src/types/user.types';

const useSaveUserModalAction = () => {
  const dispatch = useDispatch();

  const reset = useCallback(() => {
    dispatch(actions.reset());
  }, [dispatch]);

  const setIsModifying = useCallback(
    (isModifying: boolean) => {
      dispatch(actions.setIsModifying(isModifying));
    },
    [dispatch],
  );

  const setHasCompleted = useCallback(
    (isModifying: boolean) => {
      dispatch(actions.setHasCompleted(isModifying));
    },
    [dispatch],
  );

  const setSelectedUser = useCallback(
    (user: User) => {
      dispatch(actions.setSelectedUser(user));
    },
    [dispatch],
  );

  const setPage = useCallback(
    (page: SaveUserModalPage) => {
      dispatch(actions.setPage(page));
    },
    [dispatch],
  );

  const setMode = useCallback(
    (mode: SaveUserModalMode) => {
      dispatch(actions.setMode(mode));
    },
    [dispatch],
  );

  const setPayloadFromSelectedUser = useCallback(() => {
    dispatch(actions.setPayloadFromSelectedUser());
  }, [dispatch]);

  const setPayloadRoles = useCallback(
    (roles: Role[]) => {
      dispatch(actions.setPayloadRoles(roles));
    },
    [dispatch],
  );

  const handleSaveUserFormSubmit = useCallback(
    (saveUserPayload: SaveUserPayload) => {
      dispatch(actions.handleSaveUserFormSubmit(saveUserPayload));
    },
    [dispatch],
  );

  const setPayloadCheckSendPassword = useCallback(
    (checkSendPassword: boolean) => {
      dispatch(actions.setPayloadCheckSendPassword(checkSendPassword));
    },
    [dispatch],
  );

  return {
    reset,
    setIsModifying,
    setHasCompleted,
    setSelectedUser,
    setPayloadFromSelectedUser,
    setPayloadRoles,
    handleSaveUserFormSubmit,
    setPayloadCheckSendPassword,
    setPage,
    setMode,
  };
};

export default useSaveUserModalAction;
