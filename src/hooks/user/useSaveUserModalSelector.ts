import { useTypedSelector } from '@src/reducers/rootReducer';

const useSaveUserModalSelector = () => {
  const isModifying = useTypedSelector(
    (state) => state.saveUserModal.isModifying,
  );

  const hasCompleted = useTypedSelector(
    (state) => state.saveUserModal.hasCompleted,
  );

  const selectedUser = useTypedSelector(
    (state) => state.saveUserModal.selectedUser,
  );

  const saveUserPayload = useTypedSelector(
    (state) => state.saveUserModal.saveUserPayload,
  );

  const page = useTypedSelector((state) => state.saveUserModal.page);

  const mode = useTypedSelector((state) => state.saveUserModal.mode);

  return {
    isModifying,
    hasCompleted,
    selectedUser,
    saveUserPayload,
    page,
    mode,
  };
};

export default useSaveUserModalSelector;
