import { useEffect, useState } from 'react';
import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import tokenStorage from '@src/storage/tokenStorage';
import { HttpErrorStatus } from '@src/types/http.types';
import useUserAction from './useUserAction';
import useUserSelector from './useUserSelector';

const useCheckUserEffect = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { setUser, clearUser } = useUserAction();
  const { isLoggedIn } = useUserSelector();

  const token = tokenStorage.getToken();

  useEffect(() => {
    if (!isLoggedIn || token === null) {
      clearUser();
      return;
    }

    signIn();
  }, []);

  const signIn = async () => {
    try {
      setIsLoading(true);

      const user = await userApi.getCurrentUser();

      setUser(user);
    } catch (e) {
      if (e instanceof HttpError && e.status === HttpErrorStatus.Unauthorized) {
        clearUser();
      }
    } finally {
      setIsLoading(false);
    }
  };

  return { isLoading };
};

export default useCheckUserEffect;
