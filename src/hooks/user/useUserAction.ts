import { useCallback } from 'react';
import { useQueryClient } from 'react-query';
import { useDispatch } from 'react-redux';
import { actions } from '@src/reducers/user';
import tokenStorage from '@src/storage/tokenStorage';
import { User } from '@src/types/user.types';

const useUserAction = () => {
  const queryClient = useQueryClient();
  const dispatch = useDispatch();

  const setUser = useCallback(
    (user: User) => {
      dispatch(actions.setUser(user));
    },
    [dispatch],
  );

  const clearUser = useCallback(() => {
    dispatch(actions.clearUser());
    tokenStorage.clearToken();
    queryClient.clear();
  }, [dispatch]);

  return {
    setUser,
    clearUser,
  };
};

export default useUserAction;
