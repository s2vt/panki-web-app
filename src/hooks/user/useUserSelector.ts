import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { RootState, useTypedSelector } from '@src/reducers/rootReducer';
import { LoginState, UserClass } from '@src/types/user.types';

const useUserSelector = () => {
  const user = useTypedSelector((state) => state.user.user);
  const loginState = useTypedSelector((state) => state.user.loginState);

  const isLoggedIn = useTypedSelector(
    (state) => state.user.loginState === LoginState.loggedIn,
  );

  const companyId = useTypedSelector(
    (state) => state.user.user?.company.id ?? '',
  );

  const companyName = useTypedSelector(
    (state) => state.user.user?.company.companyName ?? '',
  );

  const userClassSelector = (state: RootState) => state.user.user?.className;

  const isAdminUser = useSelector(
    createSelector(
      userClassSelector,
      (className) => className === UserClass.admin,
    ),
  );

  const isManagerUser = useSelector(
    createSelector(
      userClassSelector,
      (className) => className === UserClass.manager,
    ),
  );

  const isEmployeeUser = useSelector(
    createSelector(
      userClassSelector,
      (className) => className === UserClass.employee,
    ),
  );

  const isAdminOrManagerUser = useSelector(
    createSelector(
      userClassSelector,
      (className) =>
        className === UserClass.admin || className === UserClass.manager,
    ),
  );

  return {
    user,
    loginState,
    isLoggedIn,
    companyId,
    companyName,
    isAdminUser,
    isManagerUser,
    isEmployeeUser,
    isAdminOrManagerUser,
  };
};

export default useUserSelector;
