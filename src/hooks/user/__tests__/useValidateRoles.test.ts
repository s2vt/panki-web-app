import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import { Role, User } from '@src/types/user.types';
import useValidateRoles from '../useValidateRoles';

describe('useValidateRoles hook', () => {
  const setup = (validationRoles: string[] = [], user: User) =>
    renderHook(() => useValidateRoles(validationRoles, user));

  it('should pass validate when validation roles is empty', () => {
    // given
    const sampleValidationRoles: string[] = [];
    const sampleUserRoles: Role[] = [
      {
        id: '1',
        role: 'role1',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
      {
        id: '2',
        role: 'role2',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
    ];

    const sampleUser: User = { roles: sampleUserRoles } as User;

    // when
    const { result } = setup(sampleValidationRoles, sampleUser);

    // then
    expect(result.current).toEqual(true);
  });

  it('should pass validate when equal validation roles and user roles', () => {
    // given
    const sampleValidationRoles = ['role1', 'role2'];
    const sampleUserRoles: Role[] = [
      {
        id: '1',
        role: 'role1',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
      {
        id: '2',
        role: 'role2',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
    ];

    const sampleUser: User = { roles: sampleUserRoles } as User;

    // when
    const { result } = setup(sampleValidationRoles, sampleUser);

    // then
    expect(result.current).toEqual(true);
  });

  it('should pass validate when included user roles in validation roles', () => {
    // given
    const sampleValidationRoles = ['role1'];
    const sampleUserRoles: Role[] = [
      {
        id: '1',
        role: 'role1',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
      {
        id: '2',
        role: 'role2',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
    ];

    const sampleUser: User = { roles: sampleUserRoles } as User;

    // when
    const { result } = setup(sampleValidationRoles, sampleUser);

    // then
    expect(result.current).toEqual(true);
  });

  it('should failed validate when not included user roles in validation roles', () => {
    // given
    const sampleValidationRoles = ['role1', 'role2'];
    const sampleUserRoles: Role[] = [
      {
        id: '1',
        role: 'role1',
        roleKR: faker.datatype.string(),
        description: faker.datatype.string(),
      },
    ];

    const sampleUser: User = { roles: sampleUserRoles } as User;

    // when
    const { result } = setup(sampleValidationRoles, sampleUser);

    // then
    expect(result.current).toEqual(false);
  });
});
