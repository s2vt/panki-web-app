import { renderHook } from '@testing-library/react-hooks/dom';
import { RootState } from '@src/reducers/rootReducer';
import { LoginState } from '@src/types/user.types';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import user from '@src/test/fixtures/user/user';
import useUserSelector from '../useUserSelector';

describe('useUserSelector hook', () => {
  const setup = (initialState: Pick<RootState, 'user'>) => {
    const { wrapper, store } = prepareMockWrapper(initialState as RootState);
    const { result } = renderHook(() => useUserSelector(), { wrapper });

    return { wrapper, store, result };
  };

  it("should return store's user state", () => {
    // given
    const initialState: Pick<RootState, 'user'> = {
      user: {
        user,
        loginState: LoginState.loggedIn,
      },
    };

    // when
    const { result } = setup(initialState);

    // then
    expect(result.current.user).toEqual(user);
    expect(result.current.isLoggedIn).toEqual(true);
  });
});
