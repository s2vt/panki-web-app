import { renderHook } from '@testing-library/react-hooks/dom';
import { User, UserClass } from '@src/types/user.types';
import useValidateUserClass from '../useValidateUserClass';

describe('useValidateUserClass hook', () => {
  const setup = (validationUserClasses: UserClass[], user: User | undefined) =>
    renderHook(() => useValidateUserClass(validationUserClasses, user));

  it('should return false when user is undefined', () => {
    // given
    const user = undefined;

    // when
    const { result } = setup([], user);

    // then
    expect(result.current).toEqual(false);
  });

  it('should pass validate when validation user classes is empty', () => {
    // given
    const sampleValidationUserClasses: UserClass[] = [];
    const sampleUserClass: UserClass = 'Admin';

    const sampleUser: User = { className: sampleUserClass } as User;

    // when
    const { result } = setup(sampleValidationUserClasses, sampleUser);

    // then
    expect(result.current).toEqual(true);
  });

  it('should pass validate when included user class in validation classes', () => {
    // given
    const sampleValidationUserClasses: UserClass[] = ['Admin', 'Employee'];
    const sampleUserClass: UserClass = 'Admin';

    const sampleUser: User = { className: sampleUserClass } as User;

    // when
    const { result } = setup(sampleValidationUserClasses, sampleUser);

    // then
    expect(result.current).toEqual(true);
  });

  it('should failed validate when not included user class in validation classes', () => {
    // given
    const sampleValidationUserClasses: UserClass[] = ['Admin'];
    const sampleUserClass: UserClass = 'Employee';

    const sampleUser: User = { className: sampleUserClass } as User;

    // when
    const { result } = setup(sampleValidationUserClasses, sampleUser);

    // then
    expect(result.current).toEqual(false);
  });
});
