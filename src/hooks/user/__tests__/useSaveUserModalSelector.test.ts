import { RootState } from '@src/reducers/rootReducer';
import {
  SaveUserModalMode,
  SaveUserModalPage,
  SaveUserModalState,
} from '@src/reducers/saveUserModal';
import rootState from '@src/test/fixtures/reducers/rootState';
import saveUserModalState from '@src/test/fixtures/reducers/saveUserModalState';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import user from '@src/test/fixtures/user/user';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { renderHook } from '@testing-library/react-hooks/dom';
import useSaveUserModalSelector from '../useSaveUserModalSelector';

describe('useSaveUserModalSelector hook', () => {
  const setup = (saveUserModal: SaveUserModalState = saveUserModalState) => {
    const initialState: RootState = {
      ...rootState,
      saveUserModal,
    };

    const { wrapper } = prepareMockWrapper(initialState);
    const { result } = renderHook(() => useSaveUserModalSelector(), {
      wrapper,
    });

    return { result };
  };

  it("return store's isModifying state", () => {
    // given
    const isModifying = true;

    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      isModifying,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.isModifying).toEqual(isModifying);
  });

  it("return store's hasCompleted state", () => {
    // given
    const hasCompleted = true;

    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      hasCompleted,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.hasCompleted).toEqual(hasCompleted);
  });

  it("return store's selectedUser state", () => {
    // given
    const selectedUser = user;

    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      selectedUser,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.selectedUser).toEqual(selectedUser);
  });

  it("return store's saveUserPayload state", () => {
    // given
    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      saveUserPayload,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.saveUserPayload).toEqual(saveUserPayload);
  });

  it("return store's page state", () => {
    // given
    const page: SaveUserModalPage = 'review';

    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      page,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.page).toEqual(page);
  });

  it("return store's mode state", () => {
    // given
    const mode: SaveUserModalMode = 'add';

    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      mode,
    };

    const { result } = setup(initialState);

    // when

    // then
    expect(result.current.mode).toEqual(mode);
  });
});
