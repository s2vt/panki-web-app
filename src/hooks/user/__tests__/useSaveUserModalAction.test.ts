import { RootState } from '@src/reducers/rootReducer';
import {
  actions,
  SaveUserModalMode,
  SaveUserModalPage,
  SaveUserModalState,
} from '@src/reducers/saveUserModal';
import rootState from '@src/test/fixtures/reducers/rootState';
import saveUserModalState from '@src/test/fixtures/reducers/saveUserModalState';
import roles from '@src/test/fixtures/user/roles';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import user from '@src/test/fixtures/user/user';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import useSaveUserModalAction from '../useSaveUserModalAction';

describe('useSaveUserModalAction hook', () => {
  const setup = (saveUserModal: SaveUserModalState = saveUserModalState) => {
    const initialState: RootState = {
      ...rootState,
      saveUserModal,
    };

    const { wrapper, store } = prepareMockWrapper(initialState);

    const { result } = renderHook(() => useSaveUserModalAction(), { wrapper });

    return { result, store };
  };

  it('success call reset action', () => {
    // given
    const { store, result } = setup();

    // when
    act(() => {
      result.current.reset();
    });

    // then
    expect(store.getActions()).toEqual([actions.reset()]);
  });

  it('success call setIsModifying action', () => {
    // given
    const { store, result } = setup();

    const isModifying = true;

    // when
    act(() => {
      result.current.setIsModifying(isModifying);
    });

    // then
    expect(store.getActions()).toEqual([actions.setIsModifying(isModifying)]);
  });

  it('success call setHasCompleted action', () => {
    // given
    const { store, result } = setup();

    const hasCompleted = true;

    // when
    act(() => {
      result.current.setHasCompleted(hasCompleted);
    });

    // then
    expect(store.getActions()).toEqual([actions.setHasCompleted(hasCompleted)]);
  });

  it('success call setSelectedUser action', () => {
    // given
    const { store, result } = setup();

    // when
    act(() => {
      result.current.setSelectedUser(user);
    });

    // then
    expect(store.getActions()).toEqual([actions.setSelectedUser(user)]);
  });

  it('success call setPage action', () => {
    // given
    const { store, result } = setup();

    const page: SaveUserModalPage = 'assignRoles';

    // when
    act(() => {
      result.current.setPage(page);
    });

    // then
    expect(store.getActions()).toEqual([actions.setPage(page)]);
  });

  it('success call setMode action', () => {
    // given
    const { store, result } = setup();

    const mode: SaveUserModalMode = 'add';

    // when
    act(() => {
      result.current.setMode(mode);
    });

    // then
    expect(store.getActions()).toEqual([actions.setMode(mode)]);
  });

  it('success call setPayloadFromSelectedUser action', () => {
    // given
    const { store, result } = setup();

    // when
    act(() => {
      result.current.setPayloadFromSelectedUser();
    });

    // then
    expect(store.getActions()).toEqual([actions.setPayloadFromSelectedUser()]);
  });

  it('success call setPayloadRoles action', () => {
    // given
    const { store, result } = setup();

    // when
    act(() => {
      result.current.setPayloadRoles(roles);
    });

    // then
    expect(store.getActions()).toEqual([actions.setPayloadRoles(roles)]);
  });

  it('success call handleSaveUserFormSubmit action', () => {
    // given
    const { store, result } = setup();

    // when
    act(() => {
      result.current.handleSaveUserFormSubmit(saveUserPayload);
    });

    // then
    expect(store.getActions()).toEqual([
      actions.handleSaveUserFormSubmit(saveUserPayload),
    ]);
  });

  it('success call setPayloadCheckSendPassword action', () => {
    // given
    const { store, result } = setup();

    const checkSendPassword = true;

    // when
    act(() => {
      result.current.setPayloadCheckSendPassword(checkSendPassword);
    });

    // then
    expect(store.getActions()).toEqual([
      actions.setPayloadCheckSendPassword(checkSendPassword),
    ]);
  });
});
