import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import userApi from '@src/api/userApi';
import { RootState } from '@src/reducers/rootReducer';
import tokenStorage from '@src/storage/tokenStorage';
import { LoginState } from '@src/types/user.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';

import rootState from '@src/test/fixtures/reducers/rootState';
import HttpError from '@src/api/model/HttpError';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import useCheckUserEffect from '../useCheckUserEffect';

describe('useCheckUserEffect Hook', () => {
  const setup = (initialState?: Pick<RootState, 'user'>) => {
    const { wrapper, store } = prepareWrapper(initialState as RootState);
    const { result, waitForNextUpdate, waitFor } = renderHook(
      () => useCheckUserEffect(),
      { wrapper },
    );

    return { wrapper, store, result, waitForNextUpdate, waitFor };
  };

  it('should break function when token is not exists', () => {
    // given
    const initialState: Pick<RootState, 'user'> = {
      user: {
        user: undefined,
        loginState: LoginState.loggedIn,
      },
    };

    tokenStorage.getToken = jest.fn().mockReturnValue(null);
    userApi.getCurrentUser = jest.fn();

    // when
    setup(initialState);

    // then
    expect(userApi.getCurrentUser).not.toBeCalled();
  });

  it('should call setUser action when logged in', async () => {
    // given
    const initialState: Pick<RootState, 'user'> = {
      user: {
        user: undefined,
        loginState: LoginState.loggedIn,
      },
    };

    tokenStorage.getToken = jest.fn().mockReturnValue(faker.internet.password);

    // when
    const { waitForNextUpdate, store } = setup(initialState);
    await waitForNextUpdate();

    // then
    const { user, loginState } = store.getState().user;

    expect(user).toEqual(user);
    expect(loginState).toEqual(LoginState.loggedIn);
  });

  it('should call clearUser action when catch 401 exception', async () => {
    // given
    userApi.getCurrentUser = jest
      .fn()
      .mockRejectedValue(
        new HttpError(HttpErrorStatus.Unauthorized, ResultCode.error),
      );

    tokenStorage.getToken = jest.fn().mockReturnValue(faker.internet.password);

    // when
    const { waitForNextUpdate, store } = setup(rootState);
    await waitForNextUpdate();

    // then
    const { user, loginState } = store.getState().user;

    expect(user).toEqual(undefined);
    expect(loginState).toEqual(LoginState.loggedOut);
  });
});
