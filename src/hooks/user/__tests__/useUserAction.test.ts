import { act, renderHook } from '@testing-library/react-hooks/dom';
import { LoginState } from '@src/types/user.types';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { actions } from '@src/reducers/user';
import { RootState } from '@src/reducers/rootReducer';
import tokenStorage from '@src/storage/tokenStorage';
import user from '@src/test/fixtures/user/user';
import useUserAction from '../useUserAction';

describe('useUserAction hook', () => {
  const setup = (initialState: Pick<RootState, 'user'>) => {
    const { wrapper, store } = prepareMockWrapper(initialState as RootState);
    const { result } = renderHook(() => useUserAction(), { wrapper });

    return { wrapper, store, result };
  };

  it('should success call login action', () => {
    // given
    const initialState: Pick<RootState, 'user'> = {
      user: {
        user: undefined,
        loginState: LoginState.loggedOut,
      },
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.setUser(user);
    });

    // then
    expect(store.getActions()).toEqual([actions.setUser(user)]);
  });

  it('should success call logout action', () => {
    // given
    const initialState: Pick<RootState, 'user'> = {
      user: {
        user: undefined,
        loginState: LoginState.loggedIn,
      },
    };

    // when
    const { store, result } = setup(initialState);
    act(() => {
      result.current.clearUser();
    });

    // then
    expect(store.getActions()).toEqual([actions.clearUser()]);
    expect(tokenStorage.getToken()).toEqual(null);
  });
});
