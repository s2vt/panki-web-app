import { useQuery, UseQueryOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';
import { Company } from '@src/types/user.types';

const useOrganizationChartQuery = (
  options?: UseQueryOptions<Company, HttpError>,
) => useQuery(createKey(), companyApi.getOrganizationChart, options);

const baseKey = ['organizationChart'];
const createKey = () => [...baseKey];

useOrganizationChartQuery.baseKey = baseKey;
useOrganizationChartQuery.createKey = createKey;

export default useOrganizationChartQuery;
