import { renderHook } from '@testing-library/react-hooks/dom';
import companyApi from '@src/api/companyApi';
import { SubcontractorCompany } from '@src/types/company.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useSubcontractorCompanies from '../useSubcontractorCompanies';

describe('useSubcontractorCompanies hook', () => {
  const setup = (companyId: string) => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useSubcontractorCompanies(companyId), { wrapper });
  };

  it('should cache data when succeed fetch subcontractor companies', async () => {
    // given
    const companyId = '1';

    const sampleSubcontractorCompany: SubcontractorCompany[] = [
      { id: '1', companyName: 'company1' },
      { id: '2', companyName: 'company2' },
    ];

    companyApi.getSubcontractorCompanies = jest
      .fn()
      .mockResolvedValue(sampleSubcontractorCompany);

    // when
    const { result, waitFor } = setup(companyId);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleSubcontractorCompany);
  });

  it('should set error when failed fetch subcontractor companies', async () => {
    // given
    const companyId = '1';

    companyApi.getSubcontractorCompanies = jest
      .fn()
      .mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(companyId);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
