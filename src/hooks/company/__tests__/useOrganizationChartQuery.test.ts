import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import companyApi from '@src/api/companyApi';
import { Company } from '@src/types/user.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useOrganizationChartQuery from '../useOrganizationChartQuery';

describe('useOrganizationChartQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();
    return renderHook(() => useOrganizationChartQuery(), {
      wrapper,
    });
  };

  it('should cache data when succeed fetch company with subcontractor data', async () => {
    // given
    const sampleCompany: Company = {
      id: faker.datatype.number().toString(),
      adminCount: faker.datatype.number(),
      companyEmail: faker.internet.email(),
      companyName: faker.company.companyName(),
      orgName: faker.company.companyName(),
    };

    companyApi.getOrganizationChart = jest
      .fn()
      .mockResolvedValue(sampleCompany);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleCompany);
  });

  it('should set error when failed fetch company with subcontractor data', async () => {
    // given
    companyApi.getOrganizationChart = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
