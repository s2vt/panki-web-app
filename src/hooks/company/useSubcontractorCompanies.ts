import { useQuery, UseQueryOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';
import { SubcontractorCompany } from '@src/types/company.types';

const useSubcontractorCompanies = (
  companyId: string,
  options?: UseQueryOptions<SubcontractorCompany[], HttpError>,
) =>
  useQuery(
    createKey(companyId),
    () => companyApi.getSubcontractorCompanies(companyId),
    options,
  );

const baseKey = ['subcontractorCompanies'];
const createKey = (companyId: string) => [...baseKey, companyId];

useSubcontractorCompanies.baseKey = baseKey;
useSubcontractorCompanies.createKey = createKey;

export default useSubcontractorCompanies;
