import { DotProps } from 'recharts';

declare module 'recharts' {
  export interface BarOnMouseOverParams {
    tooltipPayload: BarTooltipPayload[];
  }

  export interface BarTooltipPayload {
    dataKey: string;
    value: string;
    index: number;
  }

  export interface LineOnMouseOverParams extends DotProps {
    dataKey: string;
    value: number;
    payload: {
      legend: string;
    };
  }

  export interface ChartMouseMoveParams {
    isTooltipActive: boolean;
    activeLabel?: string;
  }
}
