import '@mui/material/styles';

declare module '@mui/material/styles' {
  interface Theme {
    sizes: Sizes;
  }
  interface ThemeOptions {
    sizes?: Sizes;
  }

  interface Sizes {
    headerHeight: string;
    pageTitleHeight: string;
    authPageFooterHeight: string;
    centerLayoutWidth: string;
    sideBoxWidth: string;
    sidebarWidth: string;
    containerMinWidth: string;
    containerMaxWidth: string;
  }

  interface BreakpointOverrides {
    xs: false;
    sm: false;
    xl: false;
  }
}
