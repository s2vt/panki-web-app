import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: Colors;
    zIndexes: ZIndexes;
    layout: Layout;
    sizes: Sizes;
    borders: Borders;
    transition: Transition;
    breakPoint: BreakPoint;
  }

  export interface Colors {
    primary: string;
    secondary: string;
    white: string;
    pageBackground: string;
    modalBackground: string;
    inactive: string;
    red: string;
    border: string;
    tableBackground: string;
    tableHoverColor: string;
    gray: string;
    lightBlue: string;
    lightGray: string;
    lightPink: string;
    lightGreen: string;
    lightYellow: string;
    lightOrange: string;
    brightBlue: string;
    lightSky: string;
    redAccent: string;
    black: string;
    lightBlack: string;
    error: string;
    reportLineColor: string;
    disabled: string;
    disabledText: string;
    reportInspectionBadgeColor: string;
    reportInspectionDateColor: string;
    reportItemBadgeLineColor: string;
    reportItemActiveColor: string;
    blockStateColors: {
      blasting: string;
      coating: string;
      finished: string;
    };
  }

  export interface ZIndexes {
    header: number;
    description: number;
    modal: number;
    dropdown: number;
    dialog: number;
  }

  export interface Layout {
    flexCenterLayout: FlattenSimpleInterpolation;
  }

  export interface Sizes {
    headerHeight: string;
    pageTitleHeight: string;
    authPageFooterHeight: string;
    centerMaxHeight: string;
    sideBoxWidth: string;
  }

  export interface Borders {
    blockTableBorder: string | FlattenSimpleInterpolation;
  }

  export interface Transition {
    fadeIn: Keyframes;
    fadeOut: Keyframes;
    spin: Keyframes;
  }

  export interface BreakPoint {
    large: string;
  }
}
