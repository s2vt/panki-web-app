import { SaveUserPayload, UserClass } from '@src/types/user.types';

const saveUserPayload: SaveUserPayload = {
  id: '1',
  userId: 'userId',
  userName: '유저명',
  engName: 'userName',
  email: 'test@test.co.kr',
  company: 'company',
  roles: [],
  className: UserClass.employee,
  phone: '010-1234-5678',
  checkAutoGeneratePassword: false,
  checkSendPassword: false,
};

export default saveUserPayload;
