import { User, UserClass } from '@src/types/user.types';

const users: User[] = [
  {
    id: '1',
    userId: 'userId1',
    userName: '유저명1',
    engName: 'userName1',
    email: 'test1@test.co.kr',
    company: {
      id: '1',
      adminCount: 1,
      companyEmail: 'test.co.kr',
      companyName: 'company',
      orgName: 'ShipYardOrg',
    },
    orgName: 'ShipYardOrg',
    roles: [],
    className: UserClass.admin,
    phone: '010-1234-5678',
  },
  {
    id: '2',
    userId: 'userId2',
    userName: '유저명2',
    engName: 'userName2',
    email: 'test2@test.co.kr',
    company: {
      id: '1',
      adminCount: 1,
      companyEmail: 'test.co.kr',
      companyName: 'company',
      orgName: 'ShipYardOrg',
    },
    orgName: 'ShipYardOrg',
    roles: [],
    className: UserClass.admin,
    phone: '010-1234-5678',
  },
];

export default users;
