import { Role } from '@src/types/user.types';

const roles: Role[] = [
  {
    id: '1',
    role: 'role1',
    roleKR: '역할1',
    description: '역할1 설명',
  },
  {
    id: '2',
    role: 'role2',
    roleKR: '역할2',
    description: '역할2 설명',
  },
];

export default roles;
