import { Company } from '@src/types/user.types';

const company: Company = {
  id: '1',
  adminCount: 1,
  companyEmail: 'test.co.kr',
  companyName: 'company',
  orgName: 'ShipYardOrg',
};

export default company;
