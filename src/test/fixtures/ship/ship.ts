import { Ship, ShipState } from '@src/types/ship.types';

const ship: Ship = {
  id: '1',
  ownerCompany: 'company',
  qmManager: [
    {
      id: '1',
      userName: 'userName',
    },
  ],
  shipName: 'shipName',
  state: ShipState.WORKING,
};

export default ship;
