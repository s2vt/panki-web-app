import { BlockInOut } from '@src/types/block.types';
import { InspectionReport } from '@src/types/report.types';
import { TaskStep } from '@src/types/task.types';

const inspectionReports: InspectionReport[] = [
  {
    blockId: '1',
    blockName: 'block1',
    inOut: BlockInOut.IN,
    recentInspectionDate: new Date('2020-01-01'),
    recentInspectionTask: TaskStep.firstSprayTask,
    shipName: 'ship',
  },
  {
    blockId: '2',
    blockName: 'block2',
    inOut: BlockInOut.IN,
    recentInspectionDate: new Date('2020-01-01'),
    recentInspectionTask: TaskStep.firstSprayTask,
    shipName: 'ship',
  },
];

export default inspectionReports;
