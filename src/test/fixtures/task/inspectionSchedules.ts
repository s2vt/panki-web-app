import { BlockInOut } from '@src/types/block.types';
import { InspectionSchedule, TaskStep } from '@src/types/task.types';

const inspectionSchedules: InspectionSchedule[] = [
  {
    taskId: '1',
    taskStep: TaskStep.blastingTask,
    blockName: 'block',
    inOut: BlockInOut.IN,
    scheduledAt: new Date('2020-01-01'),
    shipName: 'ship',
  },
  {
    taskId: '2',
    taskStep: TaskStep.blastingTask,
    blockName: 'block',
    inOut: BlockInOut.OUT,
    scheduledAt: new Date('2020-01-01'),
    shipName: 'ship',
  },
];

export default inspectionSchedules;
