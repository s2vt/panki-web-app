import { ChartFilterState } from '@src/reducers/chartFilter';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterType,
  CoatingFilterTask,
} from '@src/types/statistics.types';
import * as dateUtil from '@src/utils/dateUtil';

const chartFilterState: ChartFilterState = {
  chartFilterParams: {
    searchType: ChartFilterType.all,
    searchDateType: ChartFilterDateType.year,
    blastingType: BlastingFilterTask.inPrimary,
    ctgType: CoatingFilterTask.final,
    from: dateUtil.formatToCommon(dateUtil.getFirstDateOfYear(new Date())),
    to: dateUtil.formatToCommon(new Date()),
  },
  period: 'lastMonth',
  searchQuery: '',
  subcontractorCompany: {
    id: '1',
    companyName: 'subcontractorCompany1',
  },
};

export default chartFilterState;
