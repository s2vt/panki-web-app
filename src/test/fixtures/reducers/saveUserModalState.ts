import { SaveUserModalState } from '@src/reducers/saveUserModal';

const saveUserModalState: SaveUserModalState = {
  hasCompleted: false,
  isModifying: false,
  saveUserPayload: undefined,
  selectedUser: undefined,
  mode: 'add',
  page: 'userInfo',
};

export default saveUserModalState;
