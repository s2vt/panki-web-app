import { UserState } from '@src/reducers/user';
import { LoginState, UserClass } from '@src/types/user.types';

const userState: UserState = {
  loginState: LoginState.loggedIn,
  user: {
    id: '1',
    userId: 'userId',
    userName: '유저명',
    engName: 'userName',
    email: 'test@test.co.kr',
    company: {
      id: '1',
      adminCount: 1,
      companyEmail: 'test.co.kr',
      companyName: 'company',
      orgName: 'ShipYardOrg',
    },
    orgName: 'ShipYardOrg',
    roles: [],
    className: UserClass.admin,
    phone: '010-1234-5678',
  },
};

export default userState;
