import { RootState } from '@src/reducers/rootReducer';
import alertDialogState from './alertDialogState';
import chartFilterState from './chartFilterState';
import saveUserModalState from './saveUserModalState';
import sidebarState from './sidebarState';
import userState from './userState';

const rootState: RootState = {
  user: userState,
  alertDialog: alertDialogState,
  chartFilter: chartFilterState,
  saveUserModal: saveUserModalState,
  sidebar: sidebarState,
};

export default rootState;
