import { SidebarState } from '@src/reducers/sidebar';

const sidebarState: SidebarState = {
  isOpen: false,
};

export default sidebarState;
