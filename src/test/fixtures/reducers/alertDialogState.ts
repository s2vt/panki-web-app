import { AlertDialogState } from '@src/reducers/alertDialog';

const alertDialogState: AlertDialogState = {
  isOpen: false,
  color: undefined,
  text: undefined,
  position: 'rightTop',
};

export default alertDialogState;
