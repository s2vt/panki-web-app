import { createStore } from '@reduxjs/toolkit';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router';
import { HelmetProvider } from 'react-helmet-async';
import rootReducer, { RootState } from '@src/reducers/rootReducer';
import theme from '@src/styles/theme';
import {
  StyledEngineProvider,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material';
import muiTheme from '@src/styles/muiTheme';

const prepareWrapper = (initialState?: RootState) => {
  const store = createStore(rootReducer, initialState);
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false }, mutations: { retry: false } },
  });
  const history = createMemoryHistory();

  const wrapper = ({ children }: { children: React.ReactNode }) => (
    <HelmetProvider>
      <Provider store={store}>
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <MuiThemeProvider theme={muiTheme}>
              <QueryClientProvider client={queryClient}>
                <Router history={history}>{children}</Router>
              </QueryClientProvider>
            </MuiThemeProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </Provider>
    </HelmetProvider>
  );

  return { wrapper, store, queryClient, history };
};

export default prepareWrapper;
