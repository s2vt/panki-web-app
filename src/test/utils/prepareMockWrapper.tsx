import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThemeProvider } from 'styled-components';
import { createMemoryHistory } from 'history';
import { ThemeProvider as MuiThemeProvider } from '@mui/material';
import { Router } from 'react-router';
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import { HelmetProvider } from 'react-helmet-async';
import { RootState } from '@src/reducers/rootReducer';
import theme from '@src/styles/theme';
import { StyledEngineProvider } from '@mui/styled-engine';
import muiTheme from '@src/styles/muiTheme';

const prepareMockWrapper = (initialState?: RootState) => {
  const middleWares = getDefaultMiddleware();
  const store = configureMockStore(middleWares)(initialState);
  const queryClient = new QueryClient();
  const history = createMemoryHistory();

  const wrapper = ({ children }: { children: React.ReactNode }) => (
    <HelmetProvider>
      <Provider store={store}>
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <MuiThemeProvider theme={muiTheme}>
              <QueryClientProvider client={queryClient}>
                <Router history={history}>{children}</Router>
              </QueryClientProvider>
            </MuiThemeProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </Provider>
    </HelmetProvider>
  );

  return { wrapper, store, queryClient, history };
};

export default prepareMockWrapper;
