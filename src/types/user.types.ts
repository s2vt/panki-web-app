import { ADMIN, EMPLOYEE, MANAGER } from '@src/constants/constantString';
import { debugAssert } from '@src/utils/commonUtil';
import { EnumLiteralsOf } from './common.types';

export type User = {
  id: string;
  userId: string;
  userName: string;
  engName: string;
  email: string;
  company: Company;
  orgName: string;
  roles: Role[];
  className: UserClass;
  phone?: string | null;
};

export type UserFilter = Partial<
  Pick<User, 'userName' | 'className' | 'company'>
>;

export type SaveUserPayload = {
  id?: string;
  userId?: string;
  userName?: string;
  engName?: string;
  email?: string;
  company?: string;
  roles?: Role[];
  className?: UserClass;
  phone?: string | null;
  checkAutoGeneratePassword?: boolean;
  checkSendPassword?: boolean;
};

export type SavedUser = User & {
  temporaryPassword?: string;
};

export type Company = {
  id: string;
  companyName: string;
  companyEmail: string;
  orgName: string;
  adminCount?: number;
  children?: Company[];
};

export type CompanyWithUsers = Company & {
  users?: CompanyUser[];
};

export type CompanyUser = Pick<User, 'id' | 'userName'>;

export type Role = {
  id: string;
  role: string;
  roleKR: string;
  description: string;
};

export const LoginState = {
  loggedIn: 'loggedIn',
  loggedOut: 'loggedOut',
} as const;
export type LoginState = EnumLiteralsOf<typeof LoginState>;

export const UserClass = {
  admin: 'Admin',
  manager: 'Manager',
  employee: 'Employee',
} as const;
export type UserClass = EnumLiteralsOf<typeof UserClass>;

export const UserRole = {
  adminRole: 'AdminRole',
  getShipRole: 'GenShipRole',
  submitDataRole: 'SubmitDataRole',
  scheduleManageRole: 'ScheduleManageRole',
  confirmInspectionReportRole: 'ConfirmInspectionReportRole',
  requestDataCheckRole: 'RequestDataCheckRole',
} as const;
export type UserRole = EnumLiteralsOf<typeof UserRole>;

export const serializeSaveUserPayload = (saveUserPayload: SaveUserPayload) => {
  const { roles, ...rest } = saveUserPayload;

  const serializedSaveUserPayload = {
    ...rest,
    userRoleIds: roles?.map((role) => role.id),
  };

  return serializedSaveUserPayload;
};

export const convertUserClassText = (userClass: UserClass | undefined) => {
  switch (userClass) {
    case UserClass.admin:
      return ADMIN;
    case UserClass.manager:
      return MANAGER;
    case UserClass.employee:
      return EMPLOYEE;
    default:
      debugAssert(false, 'User class not includes this user class');
      return '';
  }
};

export const getUserClassLowerThanTargetClass = (targetClass: UserClass) => {
  const userClasses = Object.values(UserClass);
  const foundIndex = userClasses.lastIndexOf(targetClass);

  return userClasses.filter((_, index) => index > foundIndex);
};

export const isLowerThenInputUserClass = (
  targetClass: UserClass,
  inputClass: UserClass,
) => {
  const userClasses = Object.values(UserClass);
  const inputClassIndex = userClasses.lastIndexOf(targetClass);
  const currentClassIndex = userClasses.lastIndexOf(inputClass);

  return currentClassIndex < inputClassIndex;
};
