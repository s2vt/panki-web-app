import { EnumLiteralsOf } from './common.types';

export type Block = {
  id: string;
  blockName: string;
  blockDivision: BlockDivision;
  inOut: BlockInOut;
  state: BlockState;
  disable?: boolean;
};

export type BlockTemplate = {
  id: string;
  blockName: string;
  blockDivision: BlockDivision;
};

export const BlockPrefix = {
  A: 'A',
  B: 'B',
  D: 'D',
  E: 'E',
  F: 'F',
  H: 'H',
  S: 'S',
  V: 'V',
  W: 'W',
  L: 'L',
  T: 'T',
  R: 'R',
  MST: 'MST',
} as const;
export type BlockPrefix = EnumLiteralsOf<typeof BlockPrefix>;

export const BlockDivision = {
  PORT_STBD: 'PORT_STBD',
  PORT: 'PORT',
  STARBOARD: 'STARBOARD',
  CENTER: 'CENTER',
} as const;
export type BlockDivision = EnumLiteralsOf<typeof BlockDivision>;

export const BlockInOut = {
  IN: 'IN',
  OUT: 'OUT',
};
export type BlockInOut = EnumLiteralsOf<typeof BlockInOut>;

export type BlockTableItem = {
  blockPrefix: string;
  divisionGroupedBlocks: DivisionGroupedBlock[];
};

export type DivisionGroupedBlock = {
  division: BlockDivision;
  divisionBlocks: DivisionBlock[];
};

export type DivisionBlock = {
  blockName: string;
  blocks: Block[];
};

export const BlockState = {
  CREATE: 'CREATE',
  PREPARATION: 'PREPARATION',
  BLASTING_INSPECTION_REQUEST: 'BLASTING_INSPECTION_REQUEST',
  BLASTING_INSPECTION_FINISHED: 'BLASTING_INSPECTION_FINISHED',
  CTG_1ST_INSPECTION_REQUEST: 'CTG_1ST_INSPECTION_REQUEST',
  CTG_1ST_INSPECTION_REJECTED: 'CTG_1ST_INSPECTION_REJECTED',
  CTG_1ST_INSPECTION_FINISHED: 'CTG_1ST_INSPECTION_FINISHED',
  CTG_2ND_INSPECTION_REQUEST: 'CTG_2ND_INSPECTION_REQUEST',
  CTG_2ND_INSPECTION_REJECTED: 'CTG_2ND_INSPECTION_REJECTED',
  CTG_2ND_INSPECTION_FINISHED: 'CTG_2ND_INSPECTION_FINISHED',
  CTG_3RD_INSPECTION_REQUEST: 'CTG_3RD_INSPECTION_REQUEST',
  CTG_3RD_INSPECTION_REJECTED: 'CTG_3RD_INSPECTION_REJECTED',
  CTG_3RD_INSPECTION_FINISHED: 'CTG_3RD_INSPECTION_FINISHED',
  CTG_4TH_INSPECTION_REQUEST: 'CTG_4TH_INSPECTION_REQUEST',
  CTG_4TH_INSPECTION_REJECTED: 'CTG_4TH_INSPECTION_REJECTED',
  CTG_4TH_INSPECTION_FINISHED: 'CTG_4TH_INSPECTION_FINISHED',
  CTG_5TH_INSPECTION_REQUEST: 'CTG_5TH_INSPECTION_REQUEST',
  CTG_5TH_INSPECTION_REJECTED: 'CTG_5TH_INSPECTION_REJECTED',
  CTG_5TH_INSPECTION_FINISHED: 'CTG_5TH_INSPECTION_FINISHED',
  CTG_INSPECTION_REQUEST: 'CTG_INSPECTION_REQUEST',
  CTG_INSPECTION_ING: 'CTG_INSPECTION_ING',
  INSPECTION_FINISH: 'INSPECTION_FINISH',
} as const;
export type BlockState = EnumLiteralsOf<typeof BlockState>;
