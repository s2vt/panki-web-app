import {
  convertUserClassText,
  getUserClassLowerThanTargetClass,
  isLowerThenInputUserClass,
  SaveUserPayload,
  serializeSaveUserPayload,
  UserClass,
} from '../user.types';

describe('user types', () => {
  describe('serializeSaveUserPayload', () => {
    it('should success serialize save user form data', () => {
      // given
      const saveUserPayload: SaveUserPayload = {
        id: 'test',
        userId: 'test',
        phone: 'test',
        userName: '테스트',
        engName: 'test',
        email: 'test@email.com',
        checkAutoGeneratePassword: false,
        checkSendPassword: false,
        className: 'Admin',
        company: 'test',
        roles: [
          {
            id: '1',
            description: 'description1',
            role: 'role1',
            roleKR: '역할1',
          },
          {
            id: '2',
            description: 'description2',
            role: 'role2',
            roleKR: '역할2',
          },
        ],
      };

      // when
      const result = serializeSaveUserPayload(saveUserPayload);

      const expectedData = {
        id: 'test',
        userId: 'test',
        phone: 'test',
        userName: '테스트',
        engName: 'test',
        email: 'test@email.com',
        checkAutoGeneratePassword: false,
        checkSendPassword: false,
        className: 'Admin',
        company: 'test',
        userRoleIds: ['1', '2'],
      };

      // then
      expect(result).toEqual(expectedData);
    });
  });

  describe('convertUserClassText', () => {
    it('should return admin korean text', () => {
      // given
      const userClass = UserClass.admin;

      // when
      const result = convertUserClassText(userClass);

      // then
      expect(result).toEqual('Admin');
    });

    it('should return manager korean text', () => {
      // given
      const userClass = UserClass.manager;

      // when
      const result = convertUserClassText(userClass);

      // then
      expect(result).toEqual('QM');
    });

    it('should return employee korean text', () => {
      // given
      const userClass = UserClass.employee;

      // when
      const result = convertUserClassText(userClass);

      // then
      expect(result).toEqual('작업업체');
    });

    it('should throw error when use not included user class', () => {
      const userClass = undefined;

      expect(() => convertUserClassText(userClass)).toThrowError(
        'User class not includes this user class',
      );
    });
  });

  describe('getUserClassLowerThanTargetClass', () => {
    it('should success get lower class then admin', () => {
      // given
      const userClass: UserClass = UserClass.admin;

      // when
      const result = getUserClassLowerThanTargetClass(userClass);

      const expectedData: UserClass[] = [UserClass.manager, UserClass.employee];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should success get lower class then manager', () => {
      // given
      const userClass: UserClass = UserClass.manager;

      // when
      const result = getUserClassLowerThanTargetClass(userClass);

      const expectedData: UserClass[] = [UserClass.employee];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should fail get lower class then employee', () => {
      // given
      const userClass: UserClass = UserClass.employee;

      // when
      const result = getUserClassLowerThanTargetClass(userClass);

      const expectedData: UserClass[] = [];

      // then
      expect(result).toEqual(expectedData);
    });
  });

  describe('isLowerThenInputUserClass', () => {
    it('should return true when target class lower then input user class', () => {
      // given
      const inputUserClass: UserClass = UserClass.admin;

      // when
      const result = isLowerThenInputUserClass(
        UserClass.manager,
        inputUserClass,
      );

      // then
      expect(result).toEqual(true);
    });

    it('should return false when target class upper then input user class', () => {
      // given
      const inputUserClass: UserClass = UserClass.manager;

      // when
      const result = isLowerThenInputUserClass(UserClass.admin, inputUserClass);

      // then
      expect(result).toEqual(false);
    });

    it('should return false when target class equal input user class', () => {
      // given
      const inputUserClass: UserClass = UserClass.admin;

      // when
      const result = isLowerThenInputUserClass(UserClass.admin, inputUserClass);

      // then
      expect(result).toEqual(false);
    });
  });
});
