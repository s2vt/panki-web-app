import {
  BlastingFilterTask,
  calculateYardStatistic,
  ChartData,
  CoatingFilterTask,
  CoatingPatternData,
  CompanyRateChartData,
  CompanyStatisticTableData,
  convertBlastingFilterTaskText,
  convertCoatingFilterTaskText,
  createCoatingPatternChartData,
  createCompanyOriginCoatingRateChartData,
  createCompanyPassRateChartData,
  createInspectionStatisticTableData,
  createRecentStatisticsBarChartData,
  createPassRateLineChartData,
  getChartDataKeys,
  getChartDataMaximumValue,
  getChartDataMinimumValue,
  getInspectionStatisticsLegends,
  InspectionStatisticData,
  YardStatistic,
  isBlastingFilterTask,
  isCoatingFilterTask,
  RecentStatistic,
} from '../statistics.types';

describe('statistics types', () => {
  const SAMPLE_DATA: YardStatistic[] = [
    {
      companyName: '작업업체1',
      companyId: '1',
      data: [
        {
          columnName: '2021년 1월',
          blasting: {
            inspectionCount: 1,
            passRate: 1,
          },
          coating: {
            inspectionCount: 1,
            measureCount: 1,
            passRate: 1,
            average: 1,
            standardDeviation: 1,
            lowerRate: 1,
            upperRate: 1,
            originRate: 1,
          },
        },
        {
          columnName: '2021년 2월',
          blasting: {
            inspectionCount: 2,
            passRate: 2,
          },
          coating: {
            inspectionCount: 2,
            measureCount: 2,
            passRate: 2,
            average: 2,
            standardDeviation: 2,
            lowerRate: 2,
            upperRate: 2,
            originRate: 2,
          },
        },
        {
          columnName: '2021년 3월',
          blasting: {
            inspectionCount: 3,
            passRate: 3,
          },
          coating: {
            inspectionCount: 3,
            measureCount: 3,
            passRate: 3,
            average: 3,
            standardDeviation: 3,
            lowerRate: 3,
            upperRate: 3,
            originRate: 3,
          },
        },
      ],
    },
    {
      companyName: '작업업체2',
      companyId: '2',
      data: [
        {
          columnName: '2021년 1월',
          blasting: {
            inspectionCount: 4,
            passRate: 4,
          },
          coating: {
            inspectionCount: 4,
            measureCount: 4,
            passRate: 4,
            average: 4,
            standardDeviation: 4,
            lowerRate: 4,
            upperRate: 4,
            originRate: 4,
          },
        },
        {
          columnName: '2021년 2월',
          blasting: {
            inspectionCount: 5,
            passRate: 5,
          },
          coating: {
            inspectionCount: 5,

            measureCount: 5,
            passRate: 5,
            average: 5,
            standardDeviation: 5,
            lowerRate: 5,
            upperRate: 5,
            originRate: 5,
          },
        },
        {
          columnName: '2021년 3월',
          blasting: {
            inspectionCount: 6,
            passRate: 6,
          },
          coating: {
            inspectionCount: 6,
            measureCount: 6,
            passRate: 6,
            average: 6,
            standardDeviation: 6,
            lowerRate: 6,
            upperRate: 6,
            originRate: 6,
          },
        },
      ],
    },
  ];

  describe('isBlastingFilterTask', () => {
    it('should return true when use with blasting filter task', () => {
      // given
      const blastingFilterTask = BlastingFilterTask.inAll;

      // when
      const result = isBlastingFilterTask(blastingFilterTask);

      // then
      expect(result).toEqual(true);
    });

    it('should return false when use not included blasting task', () => {
      // given
      const blastingFilterTask = 'notIncludedTask';

      // when
      const result = isBlastingFilterTask(blastingFilterTask);

      // then
      expect(result).toEqual(false);
    });
  });

  describe('isCoatingFilterTask', () => {
    it('should return true when use with coating filter task', () => {
      // given
      const coatingFilterTask = CoatingFilterTask.all;

      // when
      const result = isCoatingFilterTask(coatingFilterTask);

      // then
      expect(result).toEqual(true);
    });

    it('should return false when use not included coating task', () => {
      // given
      const coatingFilterTask = 'notIncludedTask';

      // when
      const result = isCoatingFilterTask(coatingFilterTask);

      // then
      expect(result).toEqual(false);
    });
  });

  describe('convertBlastingFilterTaskText', () => {
    it('should get in primary blasting task text', () => {
      // given
      const blastingInPrimaryTask = BlastingFilterTask.inPrimary;

      // when
      const result = convertBlastingFilterTaskText(blastingInPrimaryTask);

      // then
      expect(result).toEqual('블라스팅 PSP (IN)');
    });

    it('should get in secondary blasting task text', () => {
      // given
      const blastingInSecondaryTask = BlastingFilterTask.inSecondary;

      // when
      const result = convertBlastingFilterTaskText(blastingInSecondaryTask);

      // then
      expect(result).toEqual('블라스팅 SSP (IN)');
    });

    it('should get in all task text', () => {
      // given
      const blastingInAllTask = BlastingFilterTask.inAll;

      // when
      const result = convertBlastingFilterTaskText(blastingInAllTask);

      // then
      expect(result).toEqual('블라스팅 총계 (IN)');
    });

    it('should get out primary blasting task text', () => {
      // given
      const blastingOutPrimaryTask = BlastingFilterTask.outPrimary;

      // when
      const result = convertBlastingFilterTaskText(blastingOutPrimaryTask);

      // then
      expect(result).toEqual('블라스팅 PSP (OUT)');
    });

    it('should get out secondary blasting task text', () => {
      // given
      const blastingOutSecondaryTask = BlastingFilterTask.outSecondary;

      // when
      const result = convertBlastingFilterTaskText(blastingOutSecondaryTask);

      // then
      expect(result).toEqual('블라스팅 SSP (OUT)');
    });

    it('should get out all blasting task text', () => {
      // given
      const blastingOutAllTask = BlastingFilterTask.outAll;

      // when
      const result = convertBlastingFilterTaskText(blastingOutAllTask);

      // then
      expect(result).toEqual('블라스팅 총계 (OUT)');
    });

    it('should throw error when use not included task', () => {
      const notIncludedTask = 'notIncludedTask' as BlastingFilterTask;

      expect(() => convertBlastingFilterTaskText(notIncludedTask)).toThrowError(
        `Filter blasting task is not includes ${notIncludedTask}`,
      );
    });
  });

  describe('convertCoatingFilterTaskText', () => {
    it('should get first coating task text', () => {
      // given
      const coatingFirstTask = CoatingFilterTask.first;

      // when
      const result = convertCoatingFilterTaskText(coatingFirstTask);

      // then
      expect(result).toEqual('도장 (W.B.TK) 1st');
    });

    it('should get final coating task text', () => {
      // given
      const coatingFirstTask = CoatingFilterTask.final;

      // when
      const result = convertCoatingFilterTaskText(coatingFirstTask);

      // then
      expect(result).toEqual('도장 (W.B.TK) FINAL');
    });

    it('should get all coating task text', () => {
      // given
      const coatingFirstTask = CoatingFilterTask.all;

      // when
      const result = convertCoatingFilterTaskText(coatingFirstTask);

      // then
      expect(result).toEqual('도장 (W.B.TK) 총계');
    });

    it('should throw error when use not included task', () => {
      const notIncludedTask = 'notIncludedTask' as CoatingFilterTask;

      expect(() => convertCoatingFilterTaskText(notIncludedTask)).toThrowError(
        `Filter coating task is not includes ${notIncludedTask}`,
      );
    });
  });

  describe('createRecentStatisticsBarChartData', () => {
    it('should create recent bar chart data', () => {
      // given
      const recentStatistics: RecentStatistic[] = [
        {
          year: 2020,
          month: 1,
          blasting: {
            improvementRate: 0,
            passRate: 10,
          },
          coating: {
            improvementRate: 0,
            passRate: 10,
          },
          originCoating: {
            improvementRate: 10,
            passRate: 10,
          },
        },
      ];

      // when
      const result = createRecentStatisticsBarChartData(recentStatistics);

      const expectedData = [
        {
          legend: '2020년 1월',
          '블라스팅 합격률': 10,
          '도장 합격률': 10,
          정도막률: 10,
          '정도막 개선율 (전월대비)': 10,
        },
      ];

      // then
      expect(result).toEqual(expectedData);
    });
  });

  describe('getInspectionStatisticsLegends', () => {
    it('should success get legends without duplicate key', () => {
      // given
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            { columnName: '2021년 2월', blasting: null, coating: null },
          ],
        },
        {
          companyName: '작업업체2',
          companyId: '2',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            { columnName: '2021년 2월', blasting: null, coating: null },
          ],
        },
      ];

      // when
      const result = getInspectionStatisticsLegends(sampleData);

      const expectedData = ['2021년 1월', '2021년 2월'];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should success get legends with all exists column name', () => {
      // given
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            { columnName: '2021년 2월', blasting: null, coating: null },
          ],
        },
        {
          companyName: '작업업체2',
          companyId: '2',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            { columnName: '2021년 2월', blasting: null, coating: null },
            { columnName: '2021년 3월', blasting: null, coating: null },
          ],
        },
      ];

      // when
      const result = getInspectionStatisticsLegends(sampleData);

      const expectedData = ['2021년 1월', '2021년 2월', '2021년 3월'];

      // then
      expect(result).toEqual(expectedData);
    });
  });

  describe('createPassRateLineChartData', () => {
    it('should success create blasting line chart data', () => {
      const result = createPassRateLineChartData(
        SAMPLE_DATA,
        'blastingPassRate',
      );

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: 1, 작업업체2: 4 },
        { legend: '2021년 2월', 작업업체1: 2, 작업업체2: 5 },
        { legend: '2021년 3월', 작업업체1: 3, 작업업체2: 6 },
      ];

      expect(result).toEqual(expectedData);
    });

    it('should legend data is null when blasting inspection data is null', () => {
      // given
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            {
              columnName: '2021년 2월',
              blasting: { inspectionCount: 80, passRate: 80 },
              coating: null,
            },
          ],
        },
      ];

      // when
      const result = createPassRateLineChartData(
        sampleData,
        'blastingPassRate',
      );

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: null },
        { legend: '2021년 2월', 작업업체1: 80 },
      ];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should success create coating line chart data', () => {
      const result = createPassRateLineChartData(
        SAMPLE_DATA,
        'coatingPassRage',
      );

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: 1, 작업업체2: 4 },
        { legend: '2021년 2월', 작업업체1: 2, 작업업체2: 5 },
        { legend: '2021년 3월', 작업업체1: 3, 작업업체2: 6 },
      ];

      expect(result).toEqual(expectedData);
    });

    it('should legend data is null when coating inspection data is null', () => {
      // given
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            {
              columnName: '2021년 2월',
              blasting: null,
              coating: {
                inspectionCount: 80,
                measureCount: 80,
                passRate: 80,
                average: 6,
                standardDeviation: 6,
                lowerRate: 6,
                upperRate: 6,
                originRate: 6,
              },
            },
          ],
        },
      ];

      // when
      const result = createPassRateLineChartData(sampleData, 'coatingPassRage');

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: null },
        { legend: '2021년 2월', 작업업체1: 80 },
      ];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should success create origin coating line chart data', () => {
      const result = createPassRateLineChartData(
        SAMPLE_DATA,
        'coatingOriginRate',
      );

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: 1, 작업업체2: 4 },
        { legend: '2021년 2월', 작업업체1: 2, 작업업체2: 5 },
        { legend: '2021년 3월', 작업업체1: 3, 작업업체2: 6 },
      ];

      expect(result).toEqual(expectedData);
    });

    it('should legend data is null when origin coating inspection data is null', () => {
      // given
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            { columnName: '2021년 1월', blasting: null, coating: null },
            {
              columnName: '2021년 2월',
              blasting: null,
              coating: {
                inspectionCount: 80,
                measureCount: 80,
                passRate: 80,
                average: 6,
                standardDeviation: 6,
                lowerRate: 6,
                upperRate: 6,
                originRate: 6,
              },
            },
          ],
        },
      ];

      // when
      const result = createPassRateLineChartData(
        sampleData,
        'coatingOriginRate',
      );

      const expectedData = [
        { legend: '2021년 1월', 작업업체1: null },
        { legend: '2021년 2월', 작업업체1: 6 },
      ];

      // then
      expect(result).toEqual(expectedData);
    });

    it('should throw error when use not included field params', () => {
      const sampleData: YardStatistic[] = [
        {
          companyName: '작업업체1',
          companyId: '1',
          data: [
            {
              columnName: 'column',
              blasting: null,
              coating: null,
            },
          ],
        },
      ];

      const notIncludedField = 'test' as
        | 'blastingPassRate'
        | 'coatingPassRage'
        | 'coatingOriginRate';

      expect(() =>
        createPassRateLineChartData(sampleData, notIncludedField),
      ).toThrowError();
    });
  });

  describe('createCompanyOriginCoatingRateChartData', () => {
    it('should create company origin coating rate chart data', () => {
      // given
      const rateCharts: CompanyRateChartData[] = [
        {
          columnName: '업체1',
          blastingRate: 10,
          ctgRate: 10,
          originDftRate: 10,
        },
        {
          columnName: '업체2',
          blastingRate: 20,
          ctgRate: 20,
          originDftRate: 20,
        },
      ];

      // when
      const result = createCompanyOriginCoatingRateChartData(rateCharts);

      // then
      expect(result).toEqual([
        {
          legend: '업체1',
          정도막률: '10.0',
        },
        {
          legend: '업체2',
          정도막률: '20.0',
        },
      ]);
    });
  });

  describe('createCompanyPassRateChartData', () => {
    it('should create company pass rate chart data', () => {
      // given
      const rateCharts: CompanyRateChartData[] = [
        {
          columnName: '업체1',
          blastingRate: 10,
          ctgRate: 10,
          originDftRate: 10,
        },
        {
          columnName: '업체2',
          blastingRate: 20,
          ctgRate: 20,
          originDftRate: 20,
        },
      ];

      const blastingTask = BlastingFilterTask.inAll;
      const coatingTask = CoatingFilterTask.all;

      // when
      const result = createCompanyPassRateChartData({
        rateCharts,
        blastingTask,
        coatingTask,
      });

      // then
      expect(result).toEqual([
        {
          legend: '업체1',
          '블라스팅 총계 (IN)': '10.0',
          '도장 (W.B.TK) 총계': '10.0',
        },
        {
          legend: '업체2',
          '블라스팅 총계 (IN)': '20.0',
          '도장 (W.B.TK) 총계': '20.0',
        },
      ]);
    });
  });

  describe('createCoatingPatternChartData', () => {
    it('should create coating pattern chart data', () => {
      // given
      const coatingPatternData: CoatingPatternData[] = [
        {
          legend: '1구간',
          first: 10,
          second: 10,
        },
        {
          legend: '2구간',
          first: 20,
          second: 20,
        },
      ];

      // when
      const result = createCoatingPatternChartData(coatingPatternData);

      // then
      expect(result).toEqual([
        {
          legend: '1구간',
          '도장 (W.B.TK) 1st': 10,
          '도장 (W.B.TK) FINAL': 10,
        },
        {
          legend: '2구간',
          '도장 (W.B.TK) 1st': 20,
          '도장 (W.B.TK) FINAL': 20,
        },
      ]);
    });
  });

  describe('calculateYardStatistic', () => {
    it('should success calculate company statistics', () => {
      // given
      const sampleCompanyData = {
        companyName: '작업업체1',
        companyId: '1',
        data: [
          {
            columnName: '2021년 1월',
            blasting: {
              inspectionCount: 1,
              passRate: 1,
            },
            coating: {
              inspectionCount: 1,
              measureCount: 1,
              passRate: 1,
              average: 1,
              standardDeviation: 1,
              lowerRate: 1,
              upperRate: 1,
              originRate: 1,
            },
          },
          {
            columnName: '2021년 2월',
            blasting: {
              inspectionCount: 1,
              passRate: 1,
            },
            coating: {
              inspectionCount: 1,
              measureCount: 1,
              passRate: 1,
              average: 1,
              standardDeviation: 1,
              lowerRate: 1,
              upperRate: 1,
              originRate: 1,
            },
          },
          {
            columnName: '2021년 3월',
            blasting: {
              inspectionCount: 1,
              passRate: 1,
            },
            coating: {
              inspectionCount: 1,
              measureCount: 1,
              passRate: 1,
              average: 1,
              standardDeviation: 1,
              lowerRate: 1,
              upperRate: 1,
              originRate: 1,
            },
          },
        ],
      };

      // when
      const result = calculateYardStatistic(sampleCompanyData);

      const expectedData: CompanyStatisticTableData = {
        companyName: '작업업체1',
        companyId: '1',
        blasting: { inspectionCount: 3, passRate: 1 },
        coating: {
          inspectionCount: 3,
          measureCount: 3,
          passRate: 1,
          average: 1,
          standardDeviation: 1,
          lowerRate: 1,
          upperRate: 1,
          originRate: 1,
        },
      };

      // then
      expect(result).toEqual(expectedData);
    });

    it('should return null when inspection data is null', () => {
      // given
      const sampleCompanyData = {
        companyName: '작업업체1',
        companyId: '1',
        data: [
          {
            columnName: '2021년 1월',
            blasting: null,
            coating: null,
          },
        ],
      };

      // when
      const result = calculateYardStatistic(sampleCompanyData);

      // then
      expect(result).toEqual({
        companyName: '작업업체1',
        companyId: '1',
        blasting: null,
        coating: null,
      });
    });
  });

  describe('createInspectionStatisticTableData', () => {
    it('should create inspection statistic table data', () => {
      // given
      const inspectionStatisticData: InspectionStatisticData = {
        blasting: {
          inspectionCount: 1000,
          passRate: 10,
        },
        coating: {
          inspectionCount: 1000,
          passRate: 10,
          measureCount: 1000,
          average: 10,
          standardDeviation: 10,
          lowerRate: 10,
          upperRate: 10,
          originRate: 10,
        },
      };

      // when
      const result = createInspectionStatisticTableData(
        inspectionStatisticData,
      );

      // then
      expect(result).toEqual({
        blastingStatistics: ['1,000', '10.0'],
        coatingStatistics: [
          '1,000',
          '10.0',
          '1,000',
          '10.0',
          '10.0',
          '10.0',
          '10.0',
          '10.0',
        ],
      });
    });
  });

  describe('getChartDataKeys', () => {
    it('should get chart data keys', () => {
      // given
      const chartData: ChartData[] = [
        { legend: 'legend1', dataKey1: 0, dataKey2: 0 },
        { legend: 'legend2', dataKey3: 0 },
      ];

      // when
      const dataKeys = getChartDataKeys(chartData);

      // then
      expect(dataKeys).toEqual(['dataKey1', 'dataKey2', 'dataKey3']);
    });
  });

  describe('getChartDataMinimumValue', () => {
    it('should get chart data minimum value', () => {
      // given
      const chartData: ChartData[] = [
        { legend: 'legend1', dataKey1: 100, dataKey2: 500 },
        { legend: 'legend2', dataKey1: 1000, dataKey2: 1500 },
      ];

      // when
      const minimumValue = getChartDataMinimumValue(chartData);

      // then
      expect(minimumValue).toEqual(100);
    });
  });

  describe('getChartDataMaximumValue', () => {
    it('should get chart data maximum value', () => {
      // given
      const chartData: ChartData[] = [
        { legend: 'legend1', dataKey1: 100, dataKey2: 500 },
        { legend: 'legend2', dataKey1: 1000, dataKey2: 1500 },
      ];

      // when
      const minimumValue = getChartDataMaximumValue(chartData);

      // then
      expect(minimumValue).toEqual(1500);
    });
  });
});
