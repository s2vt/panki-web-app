export type SupervisorCompany = {
  companyName: string;
};

export type SubcontractorCompany = {
  id: string;
  companyName: string;
};

export type CreateCompanyPayload = {
  companyName: string;
  parentId: string;
};
