import * as dateUtil from '@src/utils/dateUtil';
import { BlockInOut } from './block.types';
import { EnumLiteralsOf, UnknownObject } from './common.types';

export const TaskStep = {
  blastingTask: 'blasting',
  firstSprayTask: 'spray_1',
  secondSprayTask: 'spray_2',
  firstStripCoat: '1st_stripe_coat',
  secondStripCoat: '2nd_stripe_coat',
  dftInspection: 'dft_inspection',
  respray: 'respray',
} as const;
export type TaskStep = EnumLiteralsOf<typeof TaskStep>;

export type InspectionSchedule = {
  taskId: string;
  blockName: string;
  shipName: string;
  inOut: BlockInOut;
  scheduledAt: Date;
  taskStep: TaskStep;
};

export type SentInspectionRequests = {
  blastingApplications: InspectionSchedule[];
  ctgApplications: InspectionSchedule[];
};

export const deserializeInspectionSchedule = (data: UnknownObject) => {
  const { taskId, blockName, inOut, shipName, scheduledAt, taskStep } = data;

  const inspectionSchedule: InspectionSchedule = {
    taskId: taskId as string,
    blockName: blockName as string,
    inOut: inOut as BlockInOut,
    shipName: shipName as string,
    scheduledAt: dateUtil.parseDate(scheduledAt as string),
    taskStep: taskStep as TaskStep,
  };

  return inspectionSchedule;
};
