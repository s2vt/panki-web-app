import { EnumLiteralsOf } from './common.types';

export const AuthInputName = {
  userId: 'userId',
  email: 'email',
  password: 'password',
  passwordCheck: 'passwordCheck',
  userName: 'userName',
  engName: 'engName',
  userType: 'userType',
  verifyKey: 'verifyKey',
  privacyPolicy: 'privacyPolicy',
  termsAndConditions: 'termsAndConditions',
  company: 'company',
  phone: 'phone',
  className: 'className',
  orgName: 'orgName',
  checkSendPassword: 'checkSendPassword',
} as const;
export type AuthInputName = EnumLiteralsOf<typeof AuthInputName>;

export type SignInPayload = {
  userId: string;
  password: string;
};

export type UpdatePasswordPayload = {
  password: string;
  newPassword: string;
};

export type SendSmsPayload = {
  phone: string;
};

export type SmsVerifyPayload = {
  phone: string;
  verifyKey: string;
};

export type PasswordResetPayload = {
  phone: string;
  token: string;
  password: string;
  passwordCheck: string;
};
