import { EnumLiteralsOf } from './common.types';

export type ApiResponse = {
  resultCode: ResultCode;
  msg: string;
  [key: string]: unknown;
};

export const HttpErrorStatus = {
  BadRequest: 400,
  Unauthorized: 401,
  Forbidden: 403,
  NotFound: 404,
  RequestTimeout: 408,
  InternalServerError: 500,
  BadGateway: 502,
} as const;
export type HttpErrorStatus = EnumLiteralsOf<typeof HttpErrorStatus>;

export const ResultCode = {
  success: 'SUCCESS',
  fail: 'FAIL',
  error: 'ERROR',
  invalidRequest: 'INVALID_REQUEST',
  duplicateId: 'DUPLICATE_ID',
  mismatch: 'MISMATCH',
  loginFail: 'LOGIN_FAIL',
  notFound: 'NOT_FOUND',
  failCountOver: 'FAIL_COUNT_OVER',
  timeOut: 'TIME_OUT',
  tooManyRequests: 'TOO_MANY_REQUESTS',
} as const;
export type ResultCode = EnumLiteralsOf<typeof ResultCode>;
