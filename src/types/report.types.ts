import { BlockInOut } from './block.types';
import { TaskStep } from './task.types';
import { EnumLiteralsOf } from './common.types';
import { debugAssert } from '../utils/commonUtil';

export const InspectionReportResult = {
  denied: 'DENIED',
  accepted: 'ACCEPTED',
} as const;
export type InspectionReportResult = EnumLiteralsOf<
  typeof InspectionReportResult
>;

export type InspectionReport = {
  blockId: string;
  blockName: string;
  shipName: string;
  inOut: BlockInOut;
  recentInspectionTask: TaskStep;
  recentInspectionDate: Date;
};

export type InspectionReportHistory = {
  taskId: string;
  taskStep: TaskStep;
  completionDate: Date;
  result: InspectionReportResult;
};

export type ReportFilterPayload = {
  shipName?: string;
  inOut: BlockInOut;
  recentTaskStep?: TaskStep;
  startDate?: string;
  endDate?: string;
};

export const convertTaskStepText = (taskStep: TaskStep) => {
  switch (taskStep) {
    case TaskStep.blastingTask:
      return 'Surface Preparation';
    case TaskStep.firstSprayTask:
      return 'Before 2nd coat';
    case TaskStep.secondSprayTask:
      return 'Final Inspection';
    case TaskStep.firstStripCoat:
      return '1st stripe coat';
    case TaskStep.secondStripCoat:
      return '2nd stripe coat';
    case TaskStep.respray:
      return 'Respray';
    case TaskStep.dftInspection:
      return 'DFT Inspection';
    default:
      debugAssert(false, `Task step is not includes ${taskStep}`);
      return '';
  }
};
