import { TaskStep } from './task.types';
import { EnumLiteralsOf } from './common.types';
import { CompanyUser } from './user.types';
import { debugAssert } from '../utils/commonUtil';

export type Ship = {
  id: string;
  shipName: string;
  ownerCompany: string;
  qmManager: CompanyUser[];
  state: ShipState;
};

export type ShipFilter = Partial<Pick<Ship, 'shipName' | 'state'>>;

export const ShipState = {
  WORKING: 'WORKING',
  FINISHED: 'FINISHED',
  NEED_REQUEST_INSPECTION: 'NEED_REQUEST_INSPECTION',
} as const;
export type ShipState = EnumLiteralsOf<typeof ShipState>;

export type SaveShipPayload = {
  shipName: string;
  ownerCompanyName: string;
  managerIds: string[];
};

export type CreateShipPayload = SaveShipPayload & {
  disabledNames: string[];
};

export type UpdateShipPayload = SaveShipPayload & {
  shipId: string;
};

export type BlastingInspectionRequestPayload = {
  scheduledAt?: string;
  place: string;
  blockIds?: string[];
};

export const CoatingInspectionType = {
  coatingInspection: 'coating_inspection',
  firstStripCoat: TaskStep.firstStripCoat,
  secondStripCoat: TaskStep.secondStripCoat,
  respray: TaskStep.respray,
  dftInspection: TaskStep.dftInspection,
} as const;
export type CoatingInspectionType = EnumLiteralsOf<
  typeof CoatingInspectionType
>;

export const convertCoatingInspectionTypeText = (
  taskStep: CoatingInspectionType,
) => {
  switch (taskStep) {
    case CoatingInspectionType.coatingInspection:
      return 'Coating Inspection';
    case CoatingInspectionType.firstStripCoat:
      return '1st stripe coat';
    case CoatingInspectionType.secondStripCoat:
      return '2nd stripe coat';
    case CoatingInspectionType.respray:
      return 'Respray';
    case CoatingInspectionType.dftInspection:
      return 'DFT Inspection';
    default:
      debugAssert(false, `Task step is not includes ${taskStep}`);
      return '';
  }
};

export type CoatingInspectionRequestPayload = {
  scheduledAt?: string;
  place?: string;
  blockIds?: string[];
  reqTaskStep: CoatingInspectionType;
};
