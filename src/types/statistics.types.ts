import { debugAssert } from '@src/utils/commonUtil';
import { compareAsc } from 'date-fns';
import { ORIGIN_COATING_RATE } from '@src/constants/constantString';
import { EnumLiteralsOf } from './common.types';

export type RecentStatistic = {
  year: number;
  month: number;
  blasting: RecentStatisticPassRate;
  coating: RecentStatisticPassRate;
  originCoating: RecentStatisticPassRate;
};

export type RecentStatisticPassRate = {
  passRate: number;
  improvementRate: number;
};

export type YardStatistic = {
  companyName: string;
  companyId: string;
  data: StatisticByDate[];
};

export type InspectionStatisticData = {
  blasting: {
    inspectionCount: number;
    passRate: number;
  } | null;
  coating: {
    inspectionCount: number;
    measureCount: number;
    passRate: number;
    average: number;
    standardDeviation: number;
    lowerRate: number;
    upperRate: number;
    originRate: number;
  } | null;
};

export type StatisticByDate = InspectionStatisticData & {
  columnName: string;
};

export type CompanyStatisticTableData = InspectionStatisticData & {
  companyName: string;
  companyId: string;
};

export type CompanyStatistic = {
  statisticInfos: BlockStatistic[];
  rateCharts: CompanyRateChartData[];
  pattern: CoatingPatternData[];
  distribution: CoatingDistributionData;
};

export type CompanyRateChartData = {
  columnName: string;
  blastingRate: number | null;
  ctgRate: number | null;
  originDftRate: number | null;
};

export type CoatingPatternData = {
  legend: string;
  first: number;
  second: number;
};

export type CoatingDistributionData = {
  average: number;
  data: ChartData[];
};

export type BlockStatistic = InspectionStatisticData & {
  shipName: string;
  blockName: string;
};

export type ChartData = {
  legend: string | number;
  [key: string]: unknown;
};

export type ChartFilterParams = {
  searchType: ChartFilterType;
  searchDateType: ChartFilterDateType;
  blastingType: BlastingFilterTask;
  ctgType: CoatingFilterTask;
  from?: string;
  to?: string;
};

export type CompanyChartFilterParams = ChartFilterParams & {
  searchCompanyName: string;
};

export type ShipChartFilterParams = ChartFilterParams & {
  searchShipName: string;
};

export const ChartFilterType = {
  all: 'ALL',
  my: 'MY',
} as const;

export type ChartFilterType = EnumLiteralsOf<typeof ChartFilterType>;

export const ChartFilterDateType = {
  year: 'YEAR',
  period: 'PERIOD',
} as const;
export type ChartFilterDateType = EnumLiteralsOf<typeof ChartFilterDateType>;

export const BlastingFilterTask = {
  inPrimary: 'PSP_IN',
  inSecondary: 'SSP_IN',
  inAll: 'ALL_IN',
  outPrimary: 'PSP_OUT',
  outSecondary: 'SSP_OUT',
  outAll: 'ALL_OUT',
} as const;
export type BlastingFilterTask = EnumLiteralsOf<typeof BlastingFilterTask>;

export const CoatingFilterTask = {
  first: 'spray_1',
  final: 'spray_2',
  all: 'ALL',
} as const;
export type CoatingFilterTask = EnumLiteralsOf<typeof CoatingFilterTask>;

export const isBlastingFilterTask = (
  value: unknown,
): value is BlastingFilterTask =>
  typeof value === 'string' &&
  Object.values(BlastingFilterTask).includes(value as BlastingFilterTask);

export const isCoatingFilterTask = (
  value: unknown,
): value is CoatingFilterTask =>
  typeof value === 'string' &&
  Object.values(CoatingFilterTask).includes(value as CoatingFilterTask);

export const convertBlastingFilterTaskText = (
  filterBlastingTask: BlastingFilterTask,
) => {
  switch (filterBlastingTask) {
    case BlastingFilterTask.inPrimary:
      return '블라스팅 PSP (IN)';
    case BlastingFilterTask.inSecondary:
      return '블라스팅 SSP (IN)';
    case BlastingFilterTask.inAll:
      return '블라스팅 총계 (IN)';
    case BlastingFilterTask.outPrimary:
      return '블라스팅 PSP (OUT)';
    case BlastingFilterTask.outSecondary:
      return '블라스팅 SSP (OUT)';
    case BlastingFilterTask.outAll:
      return '블라스팅 총계 (OUT)';
    default:
      debugAssert(
        false,
        `Filter blasting task is not includes ${filterBlastingTask}`,
      );
      return '';
  }
};

export const convertCoatingFilterTaskText = (
  filterCoatingTask: CoatingFilterTask,
) => {
  switch (filterCoatingTask) {
    case CoatingFilterTask.first:
      return '도장 (W.B.TK) 1st';
    case CoatingFilterTask.final:
      return '도장 (W.B.TK) FINAL';
    case CoatingFilterTask.all:
      return '도장 (W.B.TK) 총계';
    default:
      debugAssert(
        false,
        `Filter coating task is not includes ${filterCoatingTask}`,
      );
      return '';
  }
};

export const createRecentStatisticsBarChartData = (data: RecentStatistic[]) => {
  const copiedData = [...data];

  copiedData.sort((a, b) => {
    const dateA = new Date(`${a.year}-${a.month}`);
    const dateB = new Date(`${b.year}-${b.month}`);

    return compareAsc(dateA, dateB);
  });

  const result = copiedData.map((datum) => {
    const { year, month, blasting, coating, originCoating } = datum;

    return {
      legend: `${year}년 ${month}월`,
      '블라스팅 합격률': blasting.passRate,
      '도장 합격률': coating.passRate,
      정도막률: originCoating.passRate,
      '정도막 개선율 (전월대비)': originCoating.improvementRate,
    };
  });

  return result;
};

export const getInspectionStatisticsLegends = (
  inspectionStatistics: YardStatistic[],
) => {
  const result = inspectionStatistics.map((statistic) =>
    statistic.data.map((datum) => datum.columnName),
  );

  const legends = Array.from(new Set(result.flat()));

  return legends;
};

export const createPassRateLineChartData = (
  inspectionStatistics: YardStatistic[],
  field: 'blastingPassRate' | 'coatingPassRage' | 'coatingOriginRate',
) => {
  const legends = getInspectionStatisticsLegends(inspectionStatistics);

  const result = legends.map((legend) => {
    const initialValue: { [key: string]: unknown } = {};

    const companyData = inspectionStatistics.reduce(
      (accumulator, statistic) => {
        const { companyName, data } = statistic;

        data.forEach((datum) => {
          if (datum.columnName === legend) {
            switch (field) {
              case 'blastingPassRate':
                accumulator[companyName] = !datum.blasting?.passRate
                  ? null
                  : Number(datum.blasting?.passRate.toFixed(1));
                break;
              case 'coatingPassRage':
                accumulator[companyName] = !datum.coating?.passRate
                  ? null
                  : Number(datum.coating?.passRate.toFixed(1));
                break;
              case 'coatingOriginRate':
                accumulator[companyName] = !datum.coating?.originRate
                  ? null
                  : Number(datum.coating?.originRate.toFixed(1));
                break;
              default:
                debugAssert(
                  false,
                  `Field params is not includes this ${field}`,
                );
            }
          }
        });
        return accumulator;
      },
      initialValue,
    );

    return { legend, ...companyData } as ChartData;
  });

  return result;
};

export const createCompanyOriginCoatingRateChartData = (
  rateCharts: CompanyRateChartData[],
) =>
  rateCharts.map((rateChart) => {
    const { columnName, originDftRate } = rateChart;

    const chartData: ChartData = {
      legend: columnName,
    };

    chartData[ORIGIN_COATING_RATE] = originDftRate?.toFixed(1);

    return chartData;
  });

export const createCompanyPassRateChartData = ({
  rateCharts,
  blastingTask,
  coatingTask,
}: {
  rateCharts: CompanyRateChartData[];
  blastingTask: BlastingFilterTask;
  coatingTask: CoatingFilterTask;
}) =>
  rateCharts.map((rateChart) => {
    const { columnName, blastingRate, ctgRate } = rateChart;

    const chartData: ChartData = {
      legend: columnName,
    };

    chartData[convertBlastingFilterTaskText(blastingTask)] =
      blastingRate?.toFixed(1);
    chartData[convertCoatingFilterTaskText(coatingTask)] = ctgRate?.toFixed(1);

    return chartData;
  });

export const createCoatingPatternChartData = (
  coatingPatternData: CoatingPatternData[],
) => {
  const result = coatingPatternData.map((datum) => {
    const { legend, first, second } = datum;

    const chartData: ChartData = { legend };

    const firstTask = convertCoatingFilterTaskText(CoatingFilterTask.first);
    const secondTask = convertCoatingFilterTaskText(CoatingFilterTask.final);

    chartData[firstTask] = first;
    chartData[secondTask] = second;

    return chartData;
  });

  return result;
};

export const calculateYardStatistic = (
  yardStatistic: YardStatistic,
): CompanyStatisticTableData => {
  const { companyId, companyName, data } = yardStatistic;

  let blastingCalculatedCount = 0;
  let coatingCalculatedCount = 0;

  let blastingInspectionCount = 0;
  let blastingPassRate = 0;
  let coatingMeasureCount = 0;
  let coatingPassRate = 0;
  let coatingAverage = 0;
  let coatingStandardDeviation = 0;
  let coatingLowerRate = 0;
  let coatingUpperRate = 0;
  let coatingOriginRate = 0;
  let coatingInspectionCount = 0;

  data.forEach((datum) => {
    if (datum.blasting !== null) {
      blastingCalculatedCount += 1;
      blastingInspectionCount += datum.blasting.inspectionCount;
      blastingPassRate += datum.blasting.passRate;
    }

    if (datum.coating !== null) {
      coatingCalculatedCount += 1;
      coatingMeasureCount += datum.coating.measureCount;
      coatingPassRate += datum.coating.passRate;
      coatingAverage += datum.coating.average;
      coatingStandardDeviation += datum.coating.standardDeviation;
      coatingLowerRate += datum.coating.lowerRate;
      coatingUpperRate += datum.coating.upperRate;
      coatingOriginRate += datum.coating.originRate;
      coatingInspectionCount += datum.coating.inspectionCount;
    }

    return datum;
  });

  return {
    companyId,
    companyName,
    blasting:
      blastingCalculatedCount > 0
        ? {
            inspectionCount: blastingInspectionCount,
            passRate: blastingPassRate / blastingCalculatedCount,
          }
        : null,
    coating:
      coatingCalculatedCount > 0
        ? {
            inspectionCount: coatingInspectionCount,
            measureCount: coatingMeasureCount,
            passRate: coatingPassRate / coatingCalculatedCount,
            average: coatingAverage / coatingCalculatedCount,
            lowerRate: coatingLowerRate / coatingCalculatedCount,
            originRate: coatingOriginRate / coatingCalculatedCount,
            standardDeviation:
              coatingStandardDeviation / coatingCalculatedCount,
            upperRate: coatingUpperRate / coatingCalculatedCount,
          }
        : null,
  };
};

export const createInspectionStatisticTableData = (
  inspectionStatisticData: InspectionStatisticData,
) => {
  const { blasting, coating } = inspectionStatisticData;

  const blastingStatistics = [
    blasting?.inspectionCount?.toLocaleString(),
    blasting?.passRate?.toFixed(1),
  ];

  const coatingStatistics = [
    coating?.inspectionCount?.toLocaleString(),
    coating?.passRate?.toFixed(1),
    coating?.measureCount?.toLocaleString(),
    coating?.average?.toFixed(1),
    coating?.standardDeviation?.toFixed(1),
    coating?.lowerRate?.toFixed(1),
    coating?.upperRate?.toFixed(1),
    coating?.originRate?.toFixed(1),
  ];

  return { blastingStatistics, coatingStatistics };
};

export const getChartDataKeys = (data: ChartData[]) => {
  const result = data.map((datum) => {
    const { legend, ...rest } = datum;

    return Object.keys(rest);
  });

  const dataKeys = Array.from(new Set(result.flat()));

  return dataKeys;
};

export const getFlattenChartValues = (data: ChartData[]) => {
  const result = data.map((datum) => {
    const { legend, ...rest } = datum;

    const values = Object.values(rest)
      .filter((value) => value !== undefined && value !== null)
      .map((value) => Number(value));

    return values;
  });

  return result.flat();
};

export const getChartDataMinimumValue = (data: ChartData[]) => {
  const flattenChartValues = getFlattenChartValues(data);

  return Math.min(...flattenChartValues);
};

export const getChartDataMaximumValue = (data: ChartData[]) => {
  const flattenChartValues = getFlattenChartValues(data);

  return Math.max(...flattenChartValues);
};
