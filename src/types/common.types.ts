export type SelectItem = {
  id: string;
  [key: string]: unknown;
};

export type SelectStringItem = {
  id: string;
  value: string;
};

export type DropdownItem = SelectItem & {
  label: string;
};

export type FilterSelectItem<T> = DropdownItem & {
  value?: T;
};

export type FilterDropdownItem<T> = DropdownItem & {
  key?: keyof T;
  value?: T[keyof T];
};

export type EnumLiteralsOf<T extends object> = T[keyof T];

export type UnknownObject = { [key: string]: unknown };
