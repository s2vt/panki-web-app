export const addWhiteSpacesBetweenCharacters = (text: string) =>
  text.split('').join(' ');
