export const removeEmpty = (object: { [key: string]: unknown }) => {
  const newObject: { [key: string]: unknown } = {};

  Object.keys(object).forEach((key) => {
    if (object[key] !== undefined && object[key] !== null) {
      newObject[key] = object[key];
    }
  });

  return newObject;
};

export const debugAssert = (expression: boolean, message: string) => {
  const isProduction = process.env.NODE_ENV === 'production';

  if (expression) {
    return;
  }

  console.error(message);

  if (!isProduction) {
    throw new Error(message);
  }
};
