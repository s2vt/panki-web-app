import {
  BlockTableItem,
  Block,
  DivisionBlock,
  DivisionGroupedBlock,
  BlockDivision,
  BlockPrefix,
} from '@src/types/block.types';

export const daesunShipmentBlocksFilterCondition = (
  blockPrefix: string,
  blockName: string,
) =>
  (blockPrefix === BlockPrefix.MST && blockName.includes(blockPrefix)) ||
  (!blockName.includes(BlockPrefix.MST) && blockName.startsWith(blockPrefix));

export const basicShipmentBlocksFilterCondition = (
  blockPrefix: string,
  blockName: string,
) => blockName.includes(blockPrefix);

export const getBlockTableItemsByBlocks = (
  blocks: Block[],
  blockPrefixes: string[],
  filterCondition: (blockPrefix: string, blockName: string) => boolean,
) => {
  const copiedBlocks = [...blocks];

  const blockTableData: BlockTableItem[] = blockPrefixes.map((blockPrefix) => {
    const filteredBlocks = copiedBlocks.reduce((total, block, index) => {
      const { blockName } = block;

      if (filterCondition(blockPrefix, blockName)) {
        total.push(block);
        delete copiedBlocks[index];
      }

      return total;
    }, [] as Block[]);

    const divisionBlocks = groupBlocksByBlockName(filteredBlocks);

    const divisionGroupedBlocks = groupBlocksByDivision(divisionBlocks);

    return { blockPrefix, divisionGroupedBlocks };
  });

  /**
   *  블록 구분의 PORT_STBD 경우는 PORT와 STARBOARD 에 모두 포함되는 블록을
   *  나타내는 타입이기 때문에 PORT 와 STARBOARD 그룹에 데이터를 옮겨줌
   */
  blockTableData.forEach((data, index) => {
    const { divisionGroupedBlocks } = data;

    const portStarboardDivision = divisionGroupedBlocks.find(
      (divisionGroupedBlock, divisionIndex) => {
        const isPortStarboardBlocks =
          divisionGroupedBlock.division === BlockDivision.PORT_STBD;
        if (isPortStarboardBlocks) {
          blockTableData[divisionIndex].divisionGroupedBlocks.splice(
            divisionIndex,
            1,
          );
        }

        return isPortStarboardBlocks;
      },
    );

    if (portStarboardDivision !== undefined) {
      const { divisionBlocks } = portStarboardDivision;

      blockTableData[index].divisionGroupedBlocks.forEach(
        (divisionGroupedBlock, divisionIndex) => {
          if (
            divisionGroupedBlock.division === BlockDivision.PORT ||
            divisionGroupedBlock.division === BlockDivision.STARBOARD
          ) {
            blockTableData[index].divisionGroupedBlocks[
              divisionIndex
            ].divisionBlocks.unshift(divisionBlocks[0]);
            if (divisionBlocks.length > 1) {
              blockTableData[index].divisionGroupedBlocks[
                divisionIndex
              ].divisionBlocks.push(divisionBlocks[1]);
            }
          }
        },
      );
    }
  });

  const emptyFilteredBlockTableData = blockTableData.filter(
    (data) => data.divisionGroupedBlocks.length > 0,
  );

  return emptyFilteredBlockTableData;
};

export const groupBlocksByBlockName = (blocks: Block[]) => {
  const blockNames: Set<string> = new Set(
    blocks.map((block) => block.blockName),
  );

  const divisionBlocks: DivisionBlock[] = [];

  blockNames.forEach((blockName) => {
    const groupedBlocks: Block[] = [];

    blocks.forEach((block) => {
      if (blockName === block.blockName) {
        groupedBlocks.push(block);
      }
    });

    divisionBlocks.push({ blockName, blocks: groupedBlocks });
  });

  return divisionBlocks;
};

export const groupBlocksByDivision = (divisionBlocks: DivisionBlock[]) => {
  const divisionGroupedBlock: DivisionGroupedBlock[] = [];

  Object.values(BlockDivision).forEach((division) => {
    const groupedBlocks: DivisionBlock[] = [];

    divisionBlocks.forEach((divisionBlock) => {
      const { blockDivision } = divisionBlock.blocks[0];

      if (division === blockDivision) {
        groupedBlocks.push(divisionBlock);
      }
    });

    if (groupedBlocks.length !== 0) {
      divisionGroupedBlock.push({ division, divisionBlocks: groupedBlocks });
    }
  });

  return divisionGroupedBlock;
};

export const getMaximumBlockCount = (blockTableItems: BlockTableItem[]) =>
  Math.max(
    ...blockTableItems
      .map((item) =>
        item.divisionGroupedBlocks.map(
          (divisionGroupedBlock) => divisionGroupedBlock.divisionBlocks.length,
        ),
      )
      .flat(),
  );
