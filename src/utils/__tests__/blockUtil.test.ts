import {
  Block,
  BlockDivision,
  BlockPrefix,
  BlockState,
  DivisionBlock,
} from '@src/types/block.types';

import {
  basicShipmentBlocksFilterCondition,
  getBlockTableItemsByBlocks,
  groupBlocksByBlockName,
  groupBlocksByDivision,
} from '../blockUtil';

const BLOCK_PREFIXES = ['B', 'S', 'F', 'E', 'N'];

describe('block types', () => {
  describe('groupBlocksByBlockName', () => {
    it('should get grouped blocks by blockName', () => {
      // given
      const blockName = '1B11';

      const blocks: Block[] = [
        {
          id: '1',
          blockName,
          blockDivision: 'PORT',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '2',
          blockName,
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '3',
          blockName: '1S11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '4',
          blockName: '1F11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
      ];

      // when
      const divisionBlocks = groupBlocksByBlockName(blocks);

      const filteredBlock = divisionBlocks.find(
        (divisionBlock) => divisionBlock.blockName === blockName,
      );

      // then
      expect(divisionBlocks.length).toEqual(3);
      expect(filteredBlock?.blocks.length).toEqual(2);
    });
  });

  describe('groupBlocksByDivision', () => {
    it('should get grouped blocks by division', () => {
      // given
      const divisionBlocks: DivisionBlock[] = [
        {
          blockName: '1B11',
          blocks: [
            {
              id: '1',
              blockName: '1B11',
              blockDivision: 'PORT',
              inOut: 'IN',
              state: BlockState.PREPARATION,
            },
            {
              id: '2',
              blockName: '1B11',
              blockDivision: 'PORT',
              inOut: 'OUT',
              state: BlockState.PREPARATION,
            },
          ],
        },
        {
          blockName: '5B11',
          blocks: [
            {
              id: '3',
              blockName: '5B11',
              blockDivision: 'STARBOARD',
              inOut: 'IN',
              state: BlockState.PREPARATION,
            },
            {
              id: '4',
              blockName: '5B11',
              blockDivision: 'STARBOARD',
              inOut: 'OUT',
              state: BlockState.PREPARATION,
            },
          ],
        },
      ];

      // when
      const divisionGroupedBlocks = groupBlocksByDivision(divisionBlocks);

      const divisions = divisionGroupedBlocks.map(
        (divisionGroupedBlock) => divisionGroupedBlock.division,
      );

      // then
      expect(divisions).toContain(BlockDivision.PORT);
      expect(divisions).toContain(BlockDivision.STARBOARD);
      expect(divisionGroupedBlocks[0].divisionBlocks[0]).toEqual(
        divisionBlocks[0],
      );
    });
  });

  describe('getBlockTableItemsByBlocks', () => {
    it('should get block table data with custom filter', () => {
      const blocks: Block[] = [
        {
          id: '1',
          blockName: 'A11',
          blockDivision: 'CENTER',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '2',
          blockName: 'B11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '3',
          blockName: 'D33',
          blockDivision: 'CENTER',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '4',
          blockName: 'E11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '5',
          blockName: 'F11',
          blockDivision: 'CENTER',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '6',
          blockName: 'F/MST',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '7',
          blockName: 'R/MST',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '8',
          blockName: 'H14',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '9',
          blockName: 'S31',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '10',
          blockName: 'U11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '11',
          blockName: 'W11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '12',
          blockName: 'L11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '13',
          blockName: 'T11',
          blockDivision: 'CENTER',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
      ];

      const blockPrefixes = [
        'A',
        'B',
        'D',
        'E',
        'F',
        'H',
        'S',
        'W',
        'L',
        'T',
        'MST',
      ];

      const filterCondition = (blockPrefix: string, blockName: string) =>
        (blockPrefix === 'MST' && blockName.includes(blockPrefix)) ||
        (!blockName.includes('MST') && blockName.startsWith(blockPrefix));

      // when
      const blockTableData = getBlockTableItemsByBlocks(
        blocks,
        blockPrefixes,
        filterCondition,
      );

      const resultBlockPrefixes = blockTableData.map(
        (datum) => datum.blockPrefix,
      );

      // then
      expect(resultBlockPrefixes).toEqual(blockPrefixes);
    });

    it('should get block table data by blocks', () => {
      // given
      const blocks: Block[] = [
        {
          id: '1',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '2',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '3',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '4',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '5',
          blockName: '1S11',
          blockDivision: 'PORT',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '6',
          blockName: '1S11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
      ];

      // when
      const blockTableData = getBlockTableItemsByBlocks(
        blocks,
        BLOCK_PREFIXES,
        basicShipmentBlocksFilterCondition,
      );

      const bPrefixBlocks = blockTableData.find(
        (blockTableDatum) => blockTableDatum.blockPrefix === BlockPrefix.B,
      );

      const sPrefixBlocks = blockTableData.find(
        (blockTableDatum) => blockTableDatum.blockPrefix === BlockPrefix.S,
      );

      // then
      expect(blockTableData.length).toEqual(2);
      expect(bPrefixBlocks?.divisionGroupedBlocks.length).toEqual(2);
      expect(sPrefixBlocks?.divisionGroupedBlocks.length).toEqual(1);
    });

    it('should PORT_STBD blocks insert into PORT and STARBOARD division blocks', () => {
      // given
      const blocks: Block[] = [
        {
          id: '1',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '2',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '3',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '4',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '5',
          blockName: 'B10C',
          blockDivision: 'PORT_STBD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '6',
          blockName: 'B10C',
          blockDivision: 'PORT_STBD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
      ];

      // when
      const blockTableData = getBlockTableItemsByBlocks(
        blocks,
        BLOCK_PREFIXES,
        basicShipmentBlocksFilterCondition,
      );

      const bPrefixBlocks = blockTableData.find(
        (blockTableDatum) => blockTableDatum.blockPrefix === BlockPrefix.B,
      );

      const divisions = bPrefixBlocks?.divisionGroupedBlocks.map(
        (divisionGroupedBlock) => divisionGroupedBlock.division,
      );

      // then
      expect(divisions).toContain(BlockDivision.PORT);
      expect(divisions).toContain(BlockDivision.STARBOARD);
      expect(divisions).not.toContain(BlockDivision.PORT_STBD);
    });

    it('should PORT_STBD blocks insert into PORT and STARBOARD division block list first and last', () => {
      // given
      const blocks: Block[] = [
        {
          id: '1',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '2',
          blockName: '1B11',
          blockDivision: 'PORT',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '3',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '4',
          blockName: '5B11',
          blockDivision: 'STARBOARD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '5',
          blockName: 'B10C',
          blockDivision: 'PORT_STBD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '6',
          blockName: 'B10C',
          blockDivision: 'PORT_STBD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
        {
          id: '7',
          blockName: 'B20C',
          blockDivision: 'PORT_STBD',
          inOut: 'IN',
          state: BlockState.PREPARATION,
        },
        {
          id: '8',
          blockName: 'B20C',
          blockDivision: 'PORT_STBD',
          inOut: 'OUT',
          state: BlockState.PREPARATION,
        },
      ];

      // when
      const blockTableData = getBlockTableItemsByBlocks(
        blocks,
        BLOCK_PREFIXES,
        basicShipmentBlocksFilterCondition,
      );

      const bPrefixBlocks = blockTableData.find(
        (blockTableDatum) => blockTableDatum.blockPrefix === BlockPrefix.B,
      );

      const portBlocks = bPrefixBlocks?.divisionGroupedBlocks.find(
        (divisionGroupedBlock) =>
          divisionGroupedBlock.division === BlockDivision.PORT,
      );

      // then
      const firstItem = portBlocks?.divisionBlocks[0];
      const lastItem =
        portBlocks?.divisionBlocks[portBlocks?.divisionBlocks.length - 1];

      expect(firstItem?.blockName).toEqual('B10C');
      expect(lastItem?.blockName).toEqual('B20C');
    });
  });
});
