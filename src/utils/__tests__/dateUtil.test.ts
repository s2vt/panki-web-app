import { isThursday, isFriday, isMonday, isSaturday, isSunday } from 'date-fns';
import {
  filterWeekend,
  getFirstDateOfMonth,
  getFirstDateOfYear,
  formatToCommon,
  formatToDateTime,
  getLastDateOfMonth,
  getLastDateOfYear,
  getNextWeekday,
  formatToDateTimeDay,
  changeTimeToStartOfDay,
  changeTimeToEndOfDay,
  isSameDate,
} from '../dateUtil';

describe('dateUtil', () => {
  describe('formatToCommon', () => {
    it('should get formatted common date', () => {
      // given
      const date = new Date('2000-01-01 23:59');

      // when
      const formattedDate = formatToCommon(date);

      // then
      expect(formattedDate).toEqual('2000-01-01');
    });

    it('should get formatted common date with symbol', () => {
      // given
      const date = new Date('2000-01-01 23:59');
      const symbol = '/';

      // when
      const formattedDate = formatToCommon(date, symbol);

      // then
      expect(formattedDate).toEqual('2000/01/01');
    });
  });

  describe('formatToDateTime', () => {
    it('should get formatted date time', () => {
      // given
      const date = new Date('2000-01-01 23:59');

      // when
      const formattedDate = formatToDateTime(date);

      // then
      expect(formattedDate).toEqual('2000-01-01 23:59');
    });

    it('should get formatted date time with symbol', () => {
      // given
      const date = new Date('2000-01-01 23:59');
      const symbol = '/';

      // when
      const formattedDate = formatToDateTime(date, symbol);

      // then
      expect(formattedDate).toEqual('2000/01/01 23:59');
    });
  });

  describe('formatToDateTimeDay', () => {
    it('should get formatted date time day', () => {
      // given
      const date = new Date('2000-01-01 23:59');

      // when
      const formattedDate = formatToDateTimeDay(date);

      // then
      expect(formattedDate).toEqual('2000-01-01(토) 23:59:00');
    });

    it('should get formatted date time day with symbol', () => {
      // given
      const date = new Date('2000-01-01 23:59');
      const symbol = '/';

      // when
      const formattedDate = formatToDateTimeDay(date, symbol);

      // then
      expect(formattedDate).toEqual('2000/01/01(토) 23:59:00');
    });
  });

  describe('changeTimeToStartOfDay', () => {
    it('should change time to start of day', () => {
      // given
      const date = new Date('2000-01-01 10:00');

      // when
      const changedDate = changeTimeToStartOfDay(date);

      // then
      expect(changedDate.getHours()).toEqual(0);
      expect(changedDate.getMinutes()).toEqual(0);
      expect(changedDate.getSeconds()).toEqual(0);
    });
  });

  describe('changeTimeToEndOfDay', () => {
    it('should change time to end of day', () => {
      // given
      const date = new Date('2000-01-01 10:00');

      // when
      const changedDate = changeTimeToEndOfDay(date);

      // then
      expect(changedDate.getHours()).toEqual(23);
      expect(changedDate.getMinutes()).toEqual(59);
      expect(changedDate.getSeconds()).toEqual(59);
    });
  });

  describe('getNextWeekday', () => {
    it('should get next day when input date is before friday', () => {
      // given
      const thursday = new Date('2000-01-06');

      // when
      const result = getNextWeekday(thursday);

      // then
      expect(isThursday(thursday)).toEqual(true);
      expect(isFriday(result)).toEqual(true);
    });

    it('should get next week monday when input date is after friday', () => {
      // given
      const friday = new Date('2000-01-07');

      // when
      const result = getNextWeekday(friday);

      // then
      expect(isFriday(friday)).toEqual(true);
      expect(isMonday(result)).toEqual(true);
    });
  });

  describe('filterWeekend', () => {
    it('should get false when input date is saturday', () => {
      // given
      const saturday = new Date('2000-01-01');

      // when
      const result = filterWeekend(saturday);

      // then
      expect(isSaturday(saturday)).toEqual(true);
      expect(result).toEqual(false);
    });

    it('should get false when input date is sunday', () => {
      // given
      const sunday = new Date('2000-01-02');

      // when
      const result = filterWeekend(sunday);

      // then
      expect(isSunday(sunday)).toEqual(true);
      expect(result).toEqual(false);
    });

    it('should get true when input date is week day', () => {
      // given
      const monday = new Date('2000-01-03');

      // when
      const result = filterWeekend(monday);

      // then
      expect(isMonday(monday)).toEqual(true);
      expect(result).toEqual(true);
    });
  });

  describe('getFirstDateOfMonth', () => {
    it('should get first date of month', () => {
      // given
      const middleDateOfMonth = new Date('2020-01-15');

      // when
      const firstDateOfMonth = getFirstDateOfMonth(middleDateOfMonth);

      // then
      expect(firstDateOfMonth.toDateString()).toEqual(
        new Date('2020-01-01').toDateString(),
      );
    });
  });

  describe('getLastDateOfMonth', () => {
    it('should get last date of month', () => {
      // given
      const middleDateOfMonth = new Date('2020-01-15');

      // when
      const lastDateOfMonth = getLastDateOfMonth(middleDateOfMonth);

      // then
      expect(lastDateOfMonth.toDateString()).toEqual(
        new Date('2020-01-31').toDateString(),
      );
    });
  });

  describe('getFirstDateOfYear', () => {
    it('should get first date of year', () => {
      // given
      const middleDateOfYear = new Date('2020-07-01');

      // when
      const lastDateOfMonth = getFirstDateOfYear(middleDateOfYear);
      // then
      expect(lastDateOfMonth.toDateString()).toEqual(
        new Date('2020-01-01').toDateString(),
      );
    });
  });

  describe('getLastDateOfYear', () => {
    it('should get first date of year', () => {
      // given
      const middleDateOfYear = new Date('2020-07-01');

      // when
      const lastDateOfMonth = getLastDateOfYear(middleDateOfYear);
      // then
      expect(lastDateOfMonth.toDateString()).toEqual(
        new Date('2020-12-31').toDateString(),
      );
    });
  });

  describe('isSameDate', () => {
    it('should return true when compare dates is same', () => {
      // given
      const dateA = new Date('2020-01-01 13:00');
      const dateB = new Date('2020-01-01 00:00');

      // when
      const result = isSameDate(dateA, dateB);

      // then
      expect(result).toEqual(true);
    });

    it('should return false when compare dates is not same', () => {
      // given
      const dateA = new Date('2020-01-02 00:00');
      const dateB = new Date('2020-01-01 00:00');

      // when
      const result = isSameDate(dateA, dateB);

      // then
      expect(result).toEqual(false);
    });
  });
});
