import { addWhiteSpacesBetweenCharacters } from '../textUtil';

describe('textUtil', () => {
  describe('addWhiteSpacesBetweenCharacters', () => {
    it('should get white spaces added text', () => {
      // given
      const targetText = '123';

      // when
      const result = addWhiteSpacesBetweenCharacters(targetText);

      // then
      expect(result).toEqual('1 2 3');
    });

    it('should return input when characters length is 1', () => {
      // given
      const targetText = '1';

      // when
      const result = addWhiteSpacesBetweenCharacters(targetText);

      // then
      expect(result).toEqual(targetText);
    });
  });
});
