import * as commonUtil from '../commonUtil';

describe('commonUtil', () => {
  describe('removeEmpty', () => {
    it('should success remove keys when value is empty', () => {
      // given
      const object = {
        a: '1',
        b: '2',
        c: undefined,
      };

      // when
      const removed = commonUtil.removeEmpty(object);

      // then
      expect(removed).toEqual({
        a: '1',
        b: '2',
      });
    });
  });
});
