import { addDays, format, isSameYear, isSaturday, isSunday } from 'date-fns';
import isSameDay from 'date-fns/isSameDay';
import isSameMonth from 'date-fns/isSameMonth';
import ko from 'date-fns/locale/ko';

export type DateFormatOptions = {
  locale?: Locale;
  weekStartsOn?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  firstWeekContainsDate?: number;
  useAdditionalWeekYearTokens?: boolean;
  useAdditionalDayOfYearTokens?: boolean;
};

export const parseDate = (value: string) => new Date(value.replace(/-/g, '/'));

export const formatToCommon = (
  date: Date,
  symbol = '-',
  options: DateFormatOptions = { locale: ko },
) => format(date, `yyyy${symbol}MM${symbol}dd`, options);

export const formatToDateTime = (
  date: Date,
  symbol = '-',
  options: DateFormatOptions = { locale: ko },
) => format(date, `yyyy${symbol}MM${symbol}dd HH:mm`, options);

export const formatToDateTimeWithoutWhiteSpace = (
  date: Date,
  symbol = '-',
  options: DateFormatOptions = { locale: ko },
) => format(date, `yyyy${symbol}MM${symbol}dd${symbol}HH:mm`, options);

export const formatToDateTimeWithSeconds = (
  date: Date,
  symbol = '-',
  options: DateFormatOptions = { locale: ko },
) => format(date, `yyyy${symbol}MM${symbol}dd HH:mm:ss`, options);

export const formatToDateTimeDay = (
  date: Date,
  symbol = '-',
  options: DateFormatOptions = { locale: ko },
) => format(date, `yyyy${symbol}MM${symbol}dd(eee) HH:mm:ss`, options);

export const changeTimeToStartOfDay = (date: Date) => {
  const copied = new Date(date);
  copied.setHours(0);
  copied.setMinutes(0);
  copied.setSeconds(0);

  return copied;
};

export const changeTimeToEndOfDay = (date: Date) => {
  const copied = new Date(date);
  copied.setHours(23);
  copied.setMinutes(59);
  copied.setSeconds(59);

  return copied;
};

export const getNextWeekday = (date: Date): Date => {
  const nextDay = addDays(date, 1);

  if (!isSunday(nextDay) && !isSaturday(nextDay)) {
    return nextDay;
  }

  return getNextWeekday(nextDay);
};

export const filterWeekend = (date: Date) => {
  if (isSunday(date) || isSaturday(date)) {
    return false;
  }

  return true;
};

export const getFirstDateOfMonth = (date: Date) => {
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = 1;

  const firstDateOfMonth = new Date(year, month, day, 0, 0, 0);

  return firstDateOfMonth;
};

export const getLastDateOfMonth = (date: Date) => {
  const year = date.getFullYear();
  const nextMonth = date.getMonth() + 1;
  const day = 0;

  const firstDateOfMonth = new Date(year, nextMonth, day, 23, 59, 59);

  return firstDateOfMonth;
};

export const getFirstDateOfYear = (date: Date) => {
  const year = date.getFullYear();
  const month = 0;
  const day = 1;

  const firstDateOfYear = new Date(year, month, day, 0, 0, 0);

  return firstDateOfYear;
};

export const getLastDateOfYear = (date: Date) => {
  const year = date.getFullYear();
  const firstMonthOfNextYear = 12;
  const day = 0;

  const lastDateOfYear = new Date(year, firstMonthOfNextYear, day, 23, 59, 59);

  return lastDateOfYear;
};

export const isSameDate = (dateLeft: Date, dateRight: Date) =>
  isSameYear(dateLeft, dateRight) &&
  isSameMonth(dateLeft, dateRight) &&
  isSameDay(dateLeft, dateRight);
