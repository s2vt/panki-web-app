const token = 'ACCESS_TOKEN';

const getToken = () => {
  try {
    const accessToken = localStorage.getItem(token);
    if (!accessToken) {
      return null;
    }
    return accessToken;
  } catch (e) {
    localStorage.removeItem(token);
    return null;
  }
};

const setToken = (accessToken: string) => {
  localStorage.setItem(token, accessToken);
};

const clearToken = () => {
  localStorage.removeItem(token);
};

const isTokenExists = () => {
  const accessToken = getToken();
  return accessToken !== null;
};

export default { getToken, setToken, clearToken, isTokenExists };
