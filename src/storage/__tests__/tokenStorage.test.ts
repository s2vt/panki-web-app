import faker from 'faker';
import tokenStorage from '../tokenStorage';

describe('tokenStorage', () => {
  const getItem = jest.spyOn(Storage.prototype, 'getItem');
  const setItem = jest.spyOn(Storage.prototype, 'setItem');
  const removeItem = jest.spyOn(Storage.prototype, 'removeItem');

  it('should get token when token storage is not empty', () => {
    // given
    getItem.mockReturnValue(faker.internet.password());

    // when
    const token = tokenStorage.getToken();

    // then
    expect(token).not.toBeNull();
  });

  it('should return null when token storage is empty', () => {
    // given
    getItem.mockReturnValue(null);

    // when
    const token = tokenStorage.getToken();

    // then
    expect(token).toBeNull();
  });

  it('should return null when occurs error', () => {
    // given
    getItem.mockImplementation(() => {
      throw new Error();
    });

    // when
    const token = tokenStorage.getToken();

    // then
    expect(token).toBeNull();
    expect(removeItem).toHaveBeenCalledWith('ACCESS_TOKEN');
  });

  it('should success set token', () => {
    // given
    const token = 'token';

    // when
    tokenStorage.setToken(token);

    // then
    expect(setItem).toBeCalledWith('ACCESS_TOKEN', token);
  });

  it('should clear token', () => {
    tokenStorage.clearToken();

    expect(removeItem).toBeCalledWith('ACCESS_TOKEN');
  });

  it('should return true when token exists', () => {
    // given
    getItem.mockReturnValue(faker.internet.password());

    // when
    const result = tokenStorage.isTokenExists();

    // then
    expect(result).toEqual(true);
  });

  it('should return false when token exists', () => {
    // given
    getItem.mockReturnValue(null);

    // when
    const result = tokenStorage.isTokenExists();

    // then
    expect(result).toEqual(false);
  });
});
