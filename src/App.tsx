import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import useCheckUserEffect from './hooks/user/useCheckUserEffect';
import NotFoundPage from './pages/NotFoundPage';
import { ROUTE_PATHS } from './routes/routePaths';
import MainRouter from './routes/MainRouter';
import AlertDialog from './components/base/AlertDialog';
import ForbiddenPage from './pages/ForbiddenPage';
import AuthPagesRouter from './routes/AuthPagesRouter';
import PrivateRoute from './components/routes/PrivateRoute';
import PublicRoute from './components/routes/PublicRoute';
import LoadingSpinner from './components/common/LoadingSpinner';

const App = () => {
  const { isLoading } = useCheckUserEffect();

  return (
    <>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <Router>
          <Switch>
            <PublicRoute path={ROUTE_PATHS.auth} component={AuthPagesRouter} />
            <PrivateRoute path={ROUTE_PATHS.root} component={MainRouter} />
            <Route
              exact
              path={ROUTE_PATHS.forbidden}
              component={ForbiddenPage}
            />
            <Route component={NotFoundPage} />
          </Switch>
        </Router>
      )}
      <AlertDialog />
    </>
  );
};

export default App;
