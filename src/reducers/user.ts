import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LoginState, User } from '@src/types/user.types';

export type UserState = { user: User | undefined; loginState: LoginState };

const initialState: UserState = {
  user: undefined,
  loginState: LoginState.loggedOut,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<User>) => {
      state.user = action.payload;
      state.loginState = LoginState.loggedIn;
    },
    clearUser: (state) => {
      state.user = undefined;
      state.loginState = LoginState.loggedOut;
    },
  },
});

export const { actions } = userSlice;
export default userSlice.reducer;
