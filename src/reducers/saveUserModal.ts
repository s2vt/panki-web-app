import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Role, SaveUserPayload, User } from '../types/user.types';

export type SaveUserModalPage = 'userInfo' | 'assignRoles' | 'review';
export type SaveUserModalMode = 'add' | 'modify';

export type SaveUserModalState = {
  selectedUser: User | undefined;
  isModifying: boolean;
  hasCompleted: boolean;
  saveUserPayload: SaveUserPayload | undefined;
  page: SaveUserModalPage;
  mode: SaveUserModalMode;
};

const initialState: SaveUserModalState = {
  selectedUser: undefined,
  isModifying: false,
  hasCompleted: false,
  saveUserPayload: undefined,
  page: 'userInfo',
  mode: 'add',
};

export const saveUserModalSlice = createSlice({
  name: 'saveUserModal',
  initialState,
  reducers: {
    reset: () => initialState,
    setIsModifying: (state, { payload }: PayloadAction<boolean>) => {
      state.isModifying = payload;
    },
    setHasCompleted: (state, { payload }: PayloadAction<boolean>) => {
      state.hasCompleted = payload;
    },
    setSelectedUser: (state, { payload }: PayloadAction<User>) => {
      state.selectedUser = payload;
    },
    setPage: (state, { payload }: PayloadAction<SaveUserModalPage>) => {
      state.page = payload;
    },
    setMode: (state, { payload }: PayloadAction<SaveUserModalMode>) => {
      state.mode = payload;
    },
    setPayloadFromSelectedUser: (state) => {
      const { selectedUser } = state;

      if (!selectedUser) {
        return;
      }

      const {
        id,
        userId,
        userName,
        className,
        email,
        engName,
        roles,
        phone,
        company: { companyName },
      } = selectedUser;

      state.saveUserPayload = {
        id,
        userId,
        userName,
        className,
        email,
        engName,
        roles,
        phone,
        company: companyName,
      };
    },
    setPayloadRoles: (state, { payload: roles }: PayloadAction<Role[]>) => {
      const { saveUserPayload } = state;

      state.saveUserPayload = { ...saveUserPayload, roles };
    },
    setPayloadCheckSendPassword: (
      state,
      { payload }: PayloadAction<boolean>,
    ) => {
      if (state.saveUserPayload === undefined) {
        return;
      }

      state.saveUserPayload.checkSendPassword = payload;
    },
    handleSaveUserFormSubmit: (
      state,
      { payload }: PayloadAction<SaveUserPayload>,
    ) => {
      const { saveUserPayload, isModifying } = state;

      const {
        userId,
        userName,
        engName,
        email,
        phone,
        className,
        checkAutoGeneratePassword,
        company,
      } = payload;

      state.saveUserPayload = {
        ...saveUserPayload,
        userId,
        userName,
        engName,
        email,
        phone,
        className,
        checkAutoGeneratePassword,
        company,
      };

      if (isModifying) {
        state.page = 'review';
      } else {
        state.page = 'assignRoles';
      }
    },
  },
});

export const { actions } = saveUserModalSlice;
export default saveUserModalSlice.reducer;
