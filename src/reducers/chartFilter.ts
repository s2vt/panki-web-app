import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SubcontractorCompany } from '@src/types/company.types';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterParams,
  ChartFilterType,
  CoatingFilterTask,
} from '../types/statistics.types';
import { getFirstDateOfYear, formatToCommon } from '../utils/dateUtil';

export type ChartFilterPeriod = 'lastMonth' | 'currentMonth';

export type ChartFilterState = {
  chartFilterParams: ChartFilterParams;
  period: ChartFilterPeriod;
  searchQuery: string;
  subcontractorCompany?: SubcontractorCompany;
};

export const initialState: ChartFilterState = {
  chartFilterParams: {
    searchType: ChartFilterType.all,
    searchDateType: ChartFilterDateType.year,
    blastingType: BlastingFilterTask.inPrimary,
    ctgType: CoatingFilterTask.final,
    from: formatToCommon(getFirstDateOfYear(new Date())),
    to: formatToCommon(new Date()),
  },
  period: 'lastMonth',
  searchQuery: '',
};

const chartFilterSlice = createSlice({
  name: 'chartFilter',
  initialState,
  reducers: {
    setChartFilterParams: (state, action: PayloadAction<ChartFilterParams>) => {
      state.chartFilterParams = action.payload;
    },
    setPeriod: (state, action: PayloadAction<ChartFilterPeriod>) => {
      state.period = action.payload;
    },
    setSearchQuery: (state, action: PayloadAction<string>) => {
      state.searchQuery = action.payload;
    },
    setSubcontractorCompany: (
      state,
      action: PayloadAction<SubcontractorCompany>,
    ) => {
      state.subcontractorCompany = action.payload;
    },
  },
});

export const { actions } = chartFilterSlice;
export default chartFilterSlice.reducer;
