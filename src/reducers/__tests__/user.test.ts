import faker from 'faker';
import { LoginState, User, UserClass } from '@src/types/user.types';
import user, { actions, UserState } from '../user';

describe('User reducer', () => {
  it('has initial state', () => {
    // given
    const initialState = undefined;

    // when
    const state = user(initialState, { type: '@@INIT' });

    // then
    expect(state.user).toEqual(undefined);
    expect(state.loginState).toEqual(LoginState.loggedOut);
  });

  it('should success set user action', () => {
    // given
    const sampleUser: User = {
      id: faker.datatype.number().toString(),
      userId: faker.internet.userName(),
      userName: faker.internet.userName(),
      engName: faker.internet.userName(),
      email: faker.internet.email(),
      company: {
        id: faker.datatype.number().toString(),
        adminCount: faker.datatype.number(),
        companyEmail: faker.internet.email(),
        companyName: faker.company.companyName(),
        orgName: faker.company.companyName(),
      },
      orgName: faker.company.companyName(),
      roles: [],
      className: UserClass.admin,
      phone: faker.phone.phoneNumber(),
    };

    // when
    const state = user(undefined, actions.setUser(sampleUser));

    // then
    expect(state.user).toEqual(sampleUser);
    expect(state.loginState).toEqual(LoginState.loggedIn);
  });

  it('should success clear user action', () => {
    // given
    const initialState: UserState = {
      user: {
        id: faker.datatype.number().toString(),
        userId: faker.internet.userName(),
        userName: faker.internet.userName(),
        engName: faker.internet.userName(),
        email: faker.internet.email(),
        company: {
          id: faker.datatype.number().toString(),
          adminCount: faker.datatype.number(),
          companyEmail: faker.internet.email(),
          companyName: faker.company.companyName(),
          orgName: faker.company.companyName(),
        },
        orgName: faker.company.companyName(),
        roles: [],
        className: UserClass.admin,
      },
      loginState: LoginState.loggedIn,
    };

    // when
    const state = user(initialState, actions.clearUser());

    // then
    expect(state.user).toEqual(undefined);
    expect(state.loginState).toEqual(LoginState.loggedOut);
  });
});
