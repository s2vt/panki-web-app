import rootState from '@src/test/fixtures/reducers/rootState';
import { SubcontractorCompany } from '@src/types/company.types';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterParams,
  ChartFilterType,
  CoatingFilterTask,
} from '@src/types/statistics.types';
import chartFilter, { actions } from '../chartFilter';

describe('ChartFilter reducer', () => {
  it('has initial state', () => {
    // given
    const initialState = rootState.chartFilter;

    // when
    const state = chartFilter(initialState, { type: '@@INIT' });

    // then
    expect(state).toEqual(initialState);
  });

  it('should success set chart filter params action', () => {
    // given
    const initialState = rootState.chartFilter;

    const chartFilterParams: ChartFilterParams = {
      blastingType: BlastingFilterTask.inAll,
      ctgType: CoatingFilterTask.all,
      searchDateType: ChartFilterDateType.period,
      searchType: ChartFilterType.all,
    };

    // when
    const state = chartFilter(
      initialState,
      actions.setChartFilterParams(chartFilterParams),
    );

    // then
    expect(state.chartFilterParams).toEqual(chartFilterParams);
  });

  it('should success set period action', () => {
    // given
    const initialState = rootState.chartFilter;

    // when
    const state = chartFilter(initialState, actions.setPeriod('currentMonth'));

    // then
    expect(state.period).toEqual('currentMonth');
  });

  it('should success set search query action', () => {
    // given
    const initialState = rootState.chartFilter;

    // when
    const state = chartFilter(initialState, actions.setSearchQuery('query'));

    // then
    expect(state.searchQuery).toEqual('query');
  });

  it('should success set subcontractor company action', () => {
    // given
    const initialState = rootState.chartFilter;

    const subcontractorCompany: SubcontractorCompany = {
      id: '1',
      companyName: 'subcontractorCompany1',
    };

    // when
    const state = chartFilter(
      initialState,
      actions.setSubcontractorCompany(subcontractorCompany),
    );

    // then
    expect(state.subcontractorCompany).toEqual(subcontractorCompany);
  });
});
