import saveUserModalState from '@src/test/fixtures/reducers/saveUserModalState';
import roles from '@src/test/fixtures/user/roles';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import user from '@src/test/fixtures/user/user';
import saveUserModal, { SaveUserModalState, actions } from '../saveUserModal';

describe('SaveUserModal reducer', () => {
  it('has initial state', () => {
    // given
    const initialState = undefined;

    // when
    const state = saveUserModal(initialState, { type: '@INIT' });

    // then
    expect(state.hasCompleted).toEqual(false);
    expect(state.isModifying).toEqual(false);
  });

  describe('setter actions', () => {
    it('change state to initialState when using reset action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        hasCompleted: true,
      };

      // when
      const state = saveUserModal(initialState, actions.reset());

      // then
      expect(state.hasCompleted).toEqual(false);
      expect(state.isModifying).toEqual(false);
    });

    it('should set isModifying when using setIsModifying action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        isModifying: false,
      };

      // when
      const state = saveUserModal(initialState, actions.setIsModifying(true));

      // then
      expect(state.isModifying).toEqual(true);
    });

    it('should set isModifying when using setHasCompleted action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        hasCompleted: false,
      };

      // when
      const state = saveUserModal(initialState, actions.setHasCompleted(true));

      // then
      expect(state.hasCompleted).toEqual(true);
    });

    it('should set selectedUser state when using setSelectedUser action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        selectedUser: undefined,
      };

      // when
      const state = saveUserModal(initialState, actions.setSelectedUser(user));

      // then
      expect(state.selectedUser).toEqual(user);
    });

    it('should set page state when using setPage action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        page: 'assignRoles',
      };

      // when
      const state = saveUserModal(initialState, actions.setPage('review'));

      // then
      expect(state.page).toEqual('review');
    });

    it('should set mode state when using setMode action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        mode: 'add',
      };

      // when
      const state = saveUserModal(initialState, actions.setMode('modify'));

      // then
      expect(state.mode).toEqual('modify');
    });

    it('change nothing when using setPayloadFromSelectedUser action caused selectedUser is undefined', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        selectedUser: undefined,
      };

      // when
      const state = saveUserModal(
        initialState,
        actions.setPayloadFromSelectedUser(),
      );

      // then
      expect(state.saveUserPayload).toEqual(saveUserModalState.saveUserPayload);
    });

    it('should set saveUserPayload from selectedUser when using setPayloadFromSelectedUser action', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        selectedUser: user,
      };

      const {
        id,
        userId,
        userName,
        className,
        email,
        engName,
        roles,
        phone,
        company: { companyName },
      } = user;

      const changedSaveUserPayload = {
        id,
        userId,
        userName,
        className,
        email,
        engName,
        roles,
        phone,
        company: companyName,
      };

      // when
      const state = saveUserModal(
        initialState,
        actions.setPayloadFromSelectedUser(),
      );

      // then
      expect(state.saveUserPayload).toEqual(changedSaveUserPayload);
    });

    it("set saveUserPayload's roles field when using setPayloadRoles action", () => {
      // given
      const initialState: SaveUserModalState = saveUserModalState;

      // when
      const state = saveUserModal(initialState, actions.setPayloadRoles(roles));

      // then
      expect(state.saveUserPayload?.roles).toEqual(roles);
    });

    it('change nothing when using setPayloadCheckSendPassword action caused saveUserPayload is undefined', () => {
      // given
      const initialState: SaveUserModalState = {
        ...saveUserModalState,
        saveUserPayload: undefined,
      };

      // when
      const state = saveUserModal(
        initialState,
        actions.setPayloadCheckSendPassword(true),
      );

      // then
      expect(state.saveUserPayload?.checkSendPassword).toBeUndefined();
    });
  });

  it("set saveUserPayloads's checkSendPassword field when using setPayloadCheckSendPassword action", () => {
    // given
    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      saveUserPayload: { checkSendPassword: false },
    };

    // when
    const state = saveUserModal(
      initialState,
      actions.setPayloadCheckSendPassword(true),
    );

    // then
    expect(state.saveUserPayload?.checkSendPassword).toEqual(true);
  });
});

describe('handleSaveUserFormSubmit action', () => {
  it('should set saveUserPayload when using handleSaveUserFormSubmit action', () => {
    // given
    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      saveUserPayload: undefined,
    };

    const {
      userId,
      userName,
      engName,
      email,
      phone,
      className,
      checkAutoGeneratePassword,
      company,
    } = saveUserPayload;

    const changedSaveUserPayload = {
      userId,
      userName,
      engName,
      email,
      phone,
      className,
      checkAutoGeneratePassword,
      company,
    };

    // when
    const state = saveUserModal(
      initialState,
      actions.handleSaveUserFormSubmit(saveUserPayload),
    );

    // then
    expect(state.saveUserPayload).toEqual(changedSaveUserPayload);
  });

  it('should set page to review when using handleSaveUserFormSubmit action and isModifying is true', () => {
    // given
    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      isModifying: true,
    };

    // when
    const state = saveUserModal(
      initialState,
      actions.handleSaveUserFormSubmit(saveUserPayload),
    );

    // then
    expect(state.page).toEqual('review');
  });

  it('should set page to assignRoles when using handleSaveUserFormSubmit action and isModifying is false', () => {
    // given
    const initialState: SaveUserModalState = {
      ...saveUserModalState,
      isModifying: false,
    };

    // when
    const state = saveUserModal(
      initialState,
      actions.handleSaveUserFormSubmit(saveUserPayload),
    );

    // then
    expect(state.page).toEqual('assignRoles');
  });
});
