import sidebar, { actions } from '../sidebar';

describe('Sidebar reducer', () => {
  it('has initial state', () => {
    // given
    const initialState = undefined;

    // when
    const state = sidebar(initialState, { type: '@@INIT' });

    // then
    expect(state.isOpen).toEqual(true);
  });

  it('success setIsOpen action', () => {
    // given
    const initialState = { isOpen: false };
    const isOpen = true;

    // when
    const state = sidebar(initialState, actions.setIsOpen(isOpen));

    // then
    expect(state.isOpen).toEqual(true);
  });
});
