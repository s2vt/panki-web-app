import { AnyAction } from '@reduxjs/toolkit';
import faker from 'faker';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import alertDialog, { AlertDialogState, actions } from '../alertDialog';
import { RootState } from '../rootReducer';

describe('Dialog reducer', () => {
  const setup = (initialState?: RootState) => {
    const { store } = prepareMockWrapper(initialState);

    return { store };
  };

  it('has initial state', () => {
    // given
    const initialState = undefined;

    // when
    const state = alertDialog(initialState, { type: '@@INIT' });

    // then
    expect(state.isOpen).toEqual(false);
    expect(state.color).toEqual(undefined);
    expect(state.text).toEqual(undefined);
  });

  it('should success open dialog when used open dialog with delay action', async () => {
    // given
    const initialState = {
      alertDialog: {
        isOpen: false,
        text: undefined,
      },
    } as RootState;

    const { store } = setup(initialState);

    // when
    await store.dispatch(
      actions.openAlertDialogWithDelay({
        text: 'test',
        openDialogTime: 1,
      }) as unknown as AnyAction,
    );

    // then
    expect(store.getActions()[0].type).toEqual(
      actions.openAlertDialogWithDelay.pending.type,
    );
    expect(store.getActions()[1]).toEqual({
      payload: {
        color: undefined,
        position: undefined,
        text: 'test',
      },
      type: actions.openAlertDialog.type,
    });
    expect(store.getActions()[2]).toEqual({
      type: actions.closeAlertDialog.type,
    });
  });

  it('should success open dialog when called open dialog action', () => {
    // given
    const initialState: AlertDialogState = {
      isOpen: false,
      color: undefined,
      text: undefined,
      position: undefined,
    };

    // when
    const state = alertDialog(
      initialState,
      actions.openAlertDialog({
        text: 'test',
        color: 'black',
        position: 'center',
      }),
    );

    // then
    expect(state.isOpen).toEqual(true);
    expect(state.text).toEqual('test');
    expect(state.color).toEqual('black');
    expect(state.position).toEqual('center');
  });

  it('should success close dialog when called close dialog action', () => {
    // given
    const initialState: AlertDialogState = {
      isOpen: true,
      color: undefined,
      text: faker.datatype.string(),
      position: undefined,
    };

    // when
    const state = alertDialog(initialState, actions.closeAlertDialog());

    // then
    expect(state.isOpen).toEqual(false);
    expect(state.color).toEqual(undefined);
    expect(state.text).toEqual(undefined);
  });
});
