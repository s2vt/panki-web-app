import { combineReducers } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useSelector } from 'react-redux';

import user from './user';
import alertDialog from './alertDialog';
import chartFilter from './chartFilter';
import saveUserModal from './saveUserModal';
import sidebar from './sidebar';

const rootReducer = combineReducers({
  user,
  alertDialog,
  chartFilter,
  saveUserModal,
  sidebar,
});

export type RootState = ReturnType<typeof rootReducer>;
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export default rootReducer;
