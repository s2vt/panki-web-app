import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type SidebarState = {
  isOpen: boolean;
};

const initialState: SidebarState = {
  isOpen: true,
};

const sidebarSlice = createSlice({
  name: 'sideBar',
  initialState,
  reducers: {
    setIsOpen: (state, action: PayloadAction<boolean>) => {
      state.isOpen = action.payload;
    },
  },
});

export const { actions } = sidebarSlice;
export default sidebarSlice.reducer;
