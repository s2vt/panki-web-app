import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './rootReducer';

export type AlertDialogPosition = 'center' | 'rightTop' | undefined;

export type AlertDialogState = {
  isOpen: boolean;
  text: string | undefined;
  color: string | undefined;
  position: AlertDialogPosition;
};

const initialState: AlertDialogState = {
  isOpen: false,
  text: undefined,
  color: undefined,
  position: undefined,
};

export type OpenAlertDialogArg = {
  text: string;
  color?: string | undefined;
  openDialogTime: number;
  position?: AlertDialogPosition;
};

const delayDialog = async (openDialogTime: number) =>
  new Promise<void>((resolve) => setTimeout(resolve, openDialogTime));

const openAlertDialogWithDelay = createAsyncThunk<void, OpenAlertDialogArg>(
  'alertDialog/openAlertDialogWithDelay',
  async (arg, { getState, dispatch }) => {
    const { alertDialog } = getState() as RootState;
    const { openDialogTime, text, color, position } = arg;

    if (alertDialog.isOpen) {
      return;
    }

    dispatch(actions.openAlertDialog({ text, color, position }));

    await delayDialog(openDialogTime);

    dispatch(actions.closeAlertDialog());
  },
);

const alertDialogSlice = createSlice({
  name: 'alertDialog',
  initialState,
  reducers: {
    openAlertDialog: (
      state,
      action: PayloadAction<{
        text: string;
        color?: string | undefined;
        position?: AlertDialogPosition;
      }>,
    ) => {
      const { text, color, position } = action.payload;

      state.isOpen = true;
      state.text = text;
      state.color = color;
      state.position = position;
    },
    closeAlertDialog: (state) => {
      state.isOpen = false;
      state.text = undefined;
      state.color = undefined;
      state.position = undefined;
    },
  },
});

export const actions = {
  ...alertDialogSlice.actions,
  openAlertDialogWithDelay,
};

export default alertDialogSlice.reducer;
