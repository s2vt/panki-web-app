// common
export const SEND_SMS = '문자 보내기';
export const VERIFY_KEY = '인증번호';
export const TIME_EXTENSION = '시간연장';
export const RESEND = '재전송';
export const ADD = '추가';
export const CLOSE = '닫기';
export const NEXT = '다음';
export const PREVIOUS = '이전';
export const MODIFY = '수정';
export const SAVE = '저장';
export const CANCEL = '취소';
export const CONFIRM = '확인';
export const REFRESH = '새로고침';
export const FILTERING = '필터링';
export const CREATE = '만들기';
export const DELETE = '삭제';
export const MAIN = 'MAIN';
export const SELECT = '선택';
export const STATE = '상태';
export const ALL_SELECT = '전체선택';
export const UNSELECT = '선택해제';
export const DONE = '최종 완료';
export const REQUEST = '신청하기';
export const CALENDAR = '달력';
export const PROGRESS = '진행';
export const FINISHED = '완료';
export const REJECTED = '반려';
export const WAITING = '대기';
export const AVERAGE = '평균';
export const TARGET = '목표';
export const LAST_MONTH = '지난달';
export const CURRENT_MONTH = '이번달';
export const CURRENT_YEAR = '이번해';
export const DOWNLOAD = '다운로드';
export const RESET = '초기화';
export const EDIT = '편집';
export const INQUIRY = '조회';
export const TO_PRINT = '인쇄하기';
export const FAIL_DATA_FETCH = '데이터를 불러오는데 실패하였습니다';
export const REFETCH = '다시 불러오기';
export const OPEN_IN_NEW_WINDOW = '새창에서 열기';

// user
export const EMAIL = '이메일';
export const SEND = '전송';
export const LOGIN = '로그인';
export const LOGOUT = '로그아웃';
export const ID = '아이디';
export const SHORT_EMPLOYEE_ID = '사번';
export const FULL_EMPLOYEE_ID = '사원번호';
export const NAME = '이름';
export const KOR_NAME = '이름(국문)';
export const ENG_NAME = '이름(영문)';
export const PASSWORD = '비밀번호';
export const NEW_PASSWORD = '새로운 비밀번호';
export const TEMPORARY_PASSWORD = '임시 비밀번호';
export const PASSWORD_CHECK = '비밀번호 확인';
export const FIND_PASSWORD = '비밀번호 찾기';
export const RESET_PASSWORD = '비밀번호 변경하기';
export const AUTO_GENERATE = '자동 생성';
export const AUTO_GENERATE_PASSWORD = '자동 암호 생성';
export const CUSTOMER_INQUIRES = '고객문의';
export const USER_ACCOUNT = '사용자 계정';
export const USERNAME = '사용자 이름';
export const ADD_USER = '사용자추가';
export const DELETE_USER = '사용자삭제';
export const USER_INFORMATION = '사용자 정보';
export const EDIT_PROFILE = '프로필 편집';
export const ACCOUNT_SETTING = '계정 설정';
export const DELETE_ACCOUNT = '계정 삭제';
export const DELETE_ACCOUNT_AND_SIGN_OUT = '계정 삭제 및 탈퇴';
export const PROFILE_IMAGE = '프로필 이미지';
export const USER_BASIC_INFORMATION = '사용자 기본 정보';
export const RESULT_SAVE_USER = '사용자 정보 저장 결과';
export const CATEGORY = '유형';
export const COMPANY_NAME = '회사명';
export const ORGANIZATION = '소속';
export const ADD_ORGANIZATION = '조직추가';
export const ADMIN = 'Admin';
export const MANAGER = 'QM';
export const EMPLOYEE = '작업업체';
export const EDIT_NAME = '이름편집';
export const CLASS = '등급';
export const USER_CLASS = '등급';
export const PHONE_NUMBER = '전화번호';
export const CELL_PHONE_NUMBER = '휴대폰 번호';
export const ROLE = '역할';
export const REVIEW_AND_CONFIRM = '검토 후 완료';
export const TERMS_AND_CONDITIONS = '이용약관';
export const PRIVACY_POLICY = '개인정보보호정책';
export const PERMISSION_DENIED = '권한이 없습니다';
export const SEARCH_EMPLOYEE = '사원 검색';

// messages
export const DELETE_COMPANY_CONFIRM_MESSAGE =
  '정말로 해당 조직을 삭제하시겠습니까?';
export const INSPECTION_REQUEST_EMPTY_MESSAGE =
  '검사를 신청한 내역이 없습니다.';

// errors
export const SERVER_ERROR = '서버 에러';
export const SIGN_IN_MISMATCH_ERROR = '비밀번호가 일치하지 않습니다.';
export const USER_NOT_FOUND_ERROR = '사용자가 존재하지 않습니다.';
export const SIGN_IN_LOCK_ERROR = '계정이 잠겼습니다';
export const UNAUTHORIZED_ERROR = '인증이 만료되었습니다';
export const BAD_REQUEST_ERROR = '잘못된 요청입니다';
export const FORBIDDEN_ERROR = '권한이 없습니다';
export const REQUEST_TIMEOUT_ERROR = '요청 시간이 만료되었습니다';
export const NOT_FOUND_ERROR = '데이터가 없습니다';
export const REQUIRED_CHECK_ERROR = '필수 항목을 체크해주세요';
export const REQUIRED_INPUT_ERROR = '필수 입력 항목을 입력하세요';
export const SELECT_BLOCKS_ERROR = '블록을 체크하세요';
export const SELECT_ROLES_ERROR = '역할을 체크하세요';
export const ADD_ORGANIZATION_ERROR = '조직명을 입력하세요';
export const DELETE_ORGANIZATION_ERROR = '삭제할 조직을 체크하세요';
export const DUPLICATE_SHIP_NAME_ERROR = '이미 존재하는 호선입니다';

// placeholders
export const PHONE_NUMBER_PLACEHOLDER = '010-0000-0000';
export const SIX_DIGITS_NUMBER_PLACEHOLDER = '숫자 6자리 입력';
export const USER_SEARCH_PLACEHOLDER = '사용자 검색 후 엔터';
export const SHIP_NUMBER_SEARCH_PLACEHOLDER = '호선 번호 검색';
export const FOUR_DIGITS_SHIP_NUMBER_PLACEHOLDER = '네 자리 호선 번호 입력';
export const KOREAN_NAME_PLACEHOLDER = '성 이름';
export const ENGLISH_NAME_PLACEHOLDER = '이름 성';

// descriptions
export const INSPECTION_PASS_RATE_DESCRIPTION =
  '블라스팅과 도장 검사에 대한 공정별 검사 성공률 및 지표를 확인할 수 있습니다.';
export const STATISTICS_BY_COMPANY_DESCRIPTION =
  '업체별 검사 통계와 구간별 도장 패턴, 기간내 도장 분포도를 확인할 수 있습니다.';
export const SEMANTIC_ANALYSIS_DESCRIPTION =
  '작업업체의 구간별 도장 패턴을 통하여 검사 결과를 평가할 수 있습니다.';
export const SEMANTIC_ANALYSIS_PAGE_DESCRIPTION =
  '통계 분석은 블라스팅과 검사 통계를 분석하여 품질과 원가를 관리하는 데에 참고할 수 있는 정보를 제공하고 있습니다.';
export const COATING_PATTERN_ANALYSIS_DESCRIPTION =
  '구간별 도장 패턴을 기반으로 페인트 사용 패턴을 분석합니다.';
export const RELEVANT_STATISTICAL_STATUS_DESCRIPTION =
  '작업업체의 호선/블록별 검사 통계, 정도막률/합격률, 구간별 도장 패턴, 도장 분포도를 구체적으로 확인할 수 있습니다.';

// user management
export const ASSIGN_ROLE = '역할 할당';
export const STATISTICS = '통계';
export const ORGANIZATION_AND_ROLE = '조직/권한';
export const ORGANIZATION_CHART = '조직도';
export const MANAGE_ORGANIZATION_AND_ROLE = '조직/권한관리';

// ship
export const SHIP = '호선';
export const SHIP_AND_BLOCK = '호선/블록';
export const SHIP_LIST = '호선 목록';
export const INSPECTION_REQUEST_LIST = '검사 신청 내역';
export const INSPECTION_REQUEST = '검사 신청';
export const INSPECTION_RESULT = '검사 결과';
export const BLASTING = '블라스팅';
export const COATING = '도장';
export const BLASTING_INSPECTION_REQUEST = '블라스팅 검사 신청';
export const COATING_INSPECTION_REQUEST = '도장 검사 신청';
export const REQUEST_BLASTING_BLASTING = '블라스팅 검사 신청하기';
export const REQUEST_COATING_INSPECTION = '도장 검사 신청하기';
export const NEED_REQUEST_INSPECTION = '검사 신청 필요';
export const SHIP_NUMBER = '호선 번호';
export const SHIP_OWNER = '선주사';
export const ASSIGNED_QUALITY_MANAGER = '담당 QM';
export const CREATE_BLOCK = '블록 생성';
export const BLOCK_STATE = '블록 상태';
export const BLOCK_NAME = '블록명';
export const CREATE_SHIP = '호선 생성';
export const REQUEST_BLOCK = '신청 블록';
export const INSPECTION_DUE_DATE = '검사 예정일';
export const INSPECTION_SCHEDULE = '검사 스케쥴';
export const TODAY_INSPECTION_SCHEDULE = '금일 검사 스케쥴';
export const HOLD = '보류';
export const APPLICANT = '신청자';
export const INSPECTION_PLACE = '검사 장소';
export const INSPECTION_TYPE = '검사 유형';
export const ORDINAL_NUMBER_FIRST = '1ST';
export const ORDINAL_NUMBER_SECOND = '2ND';
export const ORDINAL_NUMBER_THIRD = '3RD';
export const ORDINAL_NUMBER_FOURTH = '4TH';
export const ORDINAL_NUMBER_FIFTH = '5TH';

// statistics
export const PASS_RATE = '합격률';
export const INSPECTION_PASS_RATE = '검사 합격률';
export const BLASTING_PASS_RATE = '블라스팅 합격률';
export const BLASTING_INSPECTION_PASS_RATE = '블라스팅 검사 합격률';
export const COATING_PASS_RATE = '도장 합격률';
export const BLASTING_AND_COATING_PASS_RATE = '블라스팅/도장 합격률';
export const COATING_INSPECTION_PASS_RATE = '도장 검사 합격률';
export const ORIGIN_COATING_RATE = '정도막률';
export const ORIGIN_COATING_RATE_AND_PASS_RATE = '정도막률/합격률';
export const INSPECTION_AVERAGE = '검사 평균';
export const INSPECTION_RESULT_AVERAGE = '검사 결과 평균';
export const STATISTICS_BY_COMPANY = '작업업체별 통계';
export const STATISTICS_BY_WORKER = '작업자별 통계';
export const STATISTICS_BY_SHOP_AND_BLOCK = '호선/블록별 검사 통계';
export const COATING_DISTRIBUTION_IN_PERIOD = '기간 내 도장 분포도';
export const COATING_PATTERN_BY_SECTION = '구간별 도장 패턴';
export const MEASUREMENT_SECTION = '측정구간';
export const SEARCH_COMPANY = '작업업체명 검색';
export const SEMANTIC_ANALYSIS = '의미 분석';
export const TURN_OFF_ALL_LEGENDS = '범례 전체 해제';
export const CHART_DOWNLOAD = '차트 다운로드';
export const TASK_MANAGER = '작업담당';
export const NUMBER_OF_TIMES = '횟수';
export const TOTAL_MEASURE_COUNT = '총 측정 수';
export const TOTAL_INSPECTION_COUNT = '총 검사 수';
export const STANDARD_DEVIATION = '표준편차';
export const MICROMETER = 'μm';
export const LOWER_COATING = '저도막';
export const UPPER_COATING = '과도막';
export const ORIGIN_COATING = '정도막';
export const REGULATION_COATING = '규정도막';
export const AVERAGE_COATING = '평균도막';
export const MINIMUM_VALUE = '최소값';
export const MAXIMUM_VALUE = '최대값';
export const REFERENCE_CONTRAST = '기준대비';
export const THICKNESS = '두께';
export const COATING_THICKNESS = '도막 두께';
export const COATING_THICKNESS_REFERENCE = '도막 두께 기준';
export const HEAT_MAP = 'heatMap';
export const OVERALL_STATISTICS = '전체통계';
export const MY_STATISTICS = '나의통계';
export const INQUIRY_BY_PERIOD = '기간별 조회';
export const PERIOD = '기간';
export const INQUIRY_PERIOD = '조회 기간';
export const INQUIRY_TARGET = '조회 대상';
export const GO_TO_SEMANTIC_ANALYSIS = '검사결과 의미 분석 바로가기';
export const GO_TO_STATISTICS_BY_COMPANY = '작업업체별 검사 통계 바로가기';
export const COATING_PATTERN_ANALYSIS = '도장 패턴 분석';
export const PATTERN = '패턴';
export const QUALITY_MANAGEMENT = '품질 관리';
export const COST_MANAGEMENT = '원가 관리';
export const MAIN_FEATURES = '주요 특징';
export const RELEVANT_STATISTICAL_STATUS = '관련 통계 현황';

// report
export const INSPECTION_REPORT = '검사 레포트';
export const INSPECTION_REPORT_CURRENT_STATUS = '검사 레포트 현황';
export const ASSIGNED_SHIPS = '담당 호선';
export const ALL_SHIPS = '전체 호선';
export const RECENT = '최신';
export const COATING_INSPECTION_REPORT = 'Coating Inspection Report';
