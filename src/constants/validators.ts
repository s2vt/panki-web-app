import { RegisterOptions } from 'react-hook-form';

export const ID_VALIDATOR: RegisterOptions = {
  required: 'Id를 입력해 주세요',
};

export const USER_NAME_VALIDATOR: RegisterOptions = {
  required: '이름을 입력해 주세요',
};

export const VERIFY_KEY_VALIDATOR: RegisterOptions = {
  required: '인증번호를 입력해 주세요',
};

export const EMAIL_VALIDATOR: RegisterOptions = {
  required: '이메일을 입력해 주세요',
  pattern: {
    value:
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    message: '올바르지 않은 이메일 형식입니다',
  },
};

export const REQUIRED_VALIDATOR: RegisterOptions = {
  required: '필수 항목입니다.',
};

export const EMPLOYEE_ID_VALIDATOR: RegisterOptions = {
  required: '사원번호를 입력해 주세요',
};

export const PASSWORD_VALIDATOR: RegisterOptions = {
  required: '비밀번호를 입력해 주세요',
  pattern: {
    value: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]/,
    message: '특수문자,숫자,문자를 넣어주세요',
  },
};

export const PHONE_VALIDATOR: RegisterOptions = {
  required: '휴대폰 번호를 입력해 주세요',
  pattern: {
    message: '휴대폰 번호 양식에 맞게 입력해 주세요',
    value: /^[0-9]{2,3}[-]+[0-9]{3,4}[-]+[0-9]{4}$/,
  },
};

export const SHIP_NAME_VALIDATOR: RegisterOptions = {
  required: '호선번호를 입력해 주세요',
};

export const CHECKBOX_VALIDATOR: RegisterOptions = {
  required: '필수 항목을 체크하세요',
  validate: (value: boolean) => value === true,
};
