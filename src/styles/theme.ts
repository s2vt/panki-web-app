import { css, DefaultTheme, keyframes } from 'styled-components';

export const colors = {
  primary: 'rgba(24, 32, 97, 1)',
  secondary: 'rgba(254, 185, 48, 1)',
  pageBackground: 'rgba(196, 196, 196, 0.2)',
  white: 'rgba(255,255,255,1)',
  modalBackground: 'rgba(24, 32, 97, 0.8)',
  inactive: 'rgba(183, 181, 181, 1)',
  red: 'rgba(255, 0, 0, 1)',
  border: 'rgba(196, 196, 196, 1)',
  tableBackground: 'rgba(238, 239, 255, 1)',
  tableHoverColor: 'rgba(254, 185, 48, 0.26)',
  gray: 'rgba(93, 94, 97, 1)',
  lightBlue: 'rgba(119, 126, 196, 1)',
  lightGray: 'rgba(237, 237, 237, 1)',
  lightPink: 'rgba(249, 136, 255, 1)',
  lightGreen: 'rgba(54, 186, 7, 1)',
  lightYellow: 'rgba(225, 227, 96, 1)',
  lightOrange: 'rgba(250, 105, 0, 1)',
  brightBlue: 'rgba(0, 0, 255, 1)',
  lightSky: 'rgba(14, 187, 225, 1)',
  redAccent: 'rgba(203, 80, 80, 1)',
  black: 'rgba(0, 0, 0, 1)',
  lightBlack: 'rgba(38, 38, 38, 1)',
  error: 'rgba(231, 59, 59, 1)',
  reportLineColor: 'rgba(173, 173, 173, 1)',
  disabled: 'rgba(220, 220, 220, 1)',
  disabledText: 'rgba(113, 113, 113, 1)',
  reportInspectionBadgeColor: 'rgba(47, 47, 47, 1)',
  reportInspectionDateColor: 'rgba(157, 157, 157, 1)',
  reportItemBadgeLineColor: 'rgba(39, 54, 182, 0.3)',
  reportItemActiveColor: 'rgba(0, 41, 255, 0.05)',
  blockStateColors: {
    blasting: 'rgba(253, 229, 168, 1)',
    coating: 'rgba(255, 208, 173, 1)',
    finished: 'rgba(171, 240, 199, 1)',
  },
};

const zIndexes = {
  header: 1100,
  description: 1100,
  modal: 1300,
  dropdown: 1200,
  dialog: 1500,
};

const sizes = {
  headerHeight: '8rem',
  pageTitleHeight: '9.6rem',
  authPageFooterHeight: '8.2rem',
  centerMaxHeight: '73rem',
  sideBoxWidth: '31.7rem',
};

const layout = {
  flexCenterLayout: css`
    display: flex;
    justify-content: center;
    align-items: center;
  `,
};

const borders = {
  blockTableBorder: `1px solid ${colors.primary}`,
};

const transition = {
  fadeIn: keyframes`
    0% {
      opacity: 0;
    }
    
    100% {
      opacity: 1;
    }
  `,
  fadeOut: keyframes`
    0% {
      opacity: 1;
    }

    100% {
      opacity: 0;
    }
  `,
  spin: keyframes`
    from {
        transform: rotate(0deg);
    }

    to {
      transform: rotate(360deg);
    }
  `,
};

const breakPoint = {
  large: '@media screen and (min-width: 1440px)',
};

const theme: DefaultTheme = {
  colors,
  zIndexes,
  layout,
  sizes,
  borders,
  transition,
  breakPoint,
};

export default theme;
