import { colors } from './theme';

export const RECENT_STATISTICS_BAR_CHART_COLORS = [
  colors.primary,
  colors.secondary,
  colors.lightPink,
  colors.lightGreen,
];

export const COATING_PATTERN_BAR_CHART_COLORS = [
  'rgba(204, 0, 0, 1)',
  'rgba(196, 196, 196, 1)',
];

export const COATING_DISTRIBUTION_BAR_COLOR = 'rgba(158, 180, 255, 1)';

export const LINE_CHART_COLORS = [
  'rgba(203, 80, 80, 1)',
  'rgba(83, 80, 203, 1)',
  'rgba(54, 186, 7, 1)',
  'rgba(80, 80, 80, 1)',
  'rgba(31, 31, 31, 1)',
  'rgba(24, 32, 97, 1)',
  'rgba(254, 185, 48, 1)',
  'rgba(249, 136, 255, 1)',
  'rgba(0, 169, 118, 1)',
  'rgba(225, 227, 96, 1)',
  'rgba(126, 87, 194, 1)',
  'rgba(255, 213, 79, 1)',
  'rgba(255, 213, 79, 1)',
  'rgba(128, 216, 255, 1)',
  'rgba(187, 222, 251, 1)',
  'rgba(239, 154, 154, 1)',
  'rgba(221, 44, 0, 1)',
  'rgba(225, 190, 231, 1)',
  'rgba(158, 158, 158, 1)',
  'rgba(255, 138, 128, 1)',
  'rgba(56, 142, 60, 1)',
  'rgba(78, 52, 46, 1)',
  'rgba(0, 137, 123, 1)',
  'rgba(255, 128, 171, 1)',
  'rgba(255, 171, 145, 1)',
  'rgba(212, 225, 87, 1)',
  'rgba(1, 87, 155, 1)',
  'rgba(239, 108, 0, 1)',
  'rgba(178, 223, 219, 1)',
  'rgba(121, 85, 72, 1)',
  'rgba(200, 230, 201, 1)',
  'rgba(117, 117, 117, 1)',
  'rgba(55, 71, 79, 1)',
  'rgba(111, 116, 221, 1)',
  'rgba(255, 208, 176, 1)',
  'rgba(255, 236, 179, 1)',
  'rgba(73, 73, 73, 1)',
  'rgba(255, 110, 64, 1)',
  'rgba(74, 20, 140, 1)',
  'rgba(255, 238, 88, 1)',
  'rgba(174, 213, 129, 1)',
  'rgba(130, 177, 255, 1)',
  'rgba(255, 61, 0, 1)',
] as const;
