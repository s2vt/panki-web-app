import {
  BreakpointsOptions,
  createTheme,
  PaletteOptions,
  Sizes,
} from '@mui/material';
import colors from './colors';

const breakpoints: BreakpointsOptions = {
  values: { md: 900, lg: 1440 },
};

const sizes: Sizes = {
  headerHeight: '7.2rem',
  pageTitleHeight: '9.6rem',
  authPageFooterHeight: '8.2rem',
  centerLayoutWidth: '1054px',
  sideBoxWidth: '31.7rem',
  sidebarWidth: '26rem',
  containerMinWidth: '960px',
  containerMaxWidth: '1280px',
};

const palette: PaletteOptions = {
  mode: 'light',
  common: {
    black: colors.darkPaper,
  },
  primary: {
    light: colors.primaryLight,
    main: colors.primaryMain,
    dark: colors.primaryDark,
  },
  secondary: {
    light: colors.secondaryLight,
    main: colors.secondaryMain,
    dark: colors.secondaryDark,
  },
  error: {
    light: colors.errorLight,
    main: colors.errorMain,
    dark: colors.errorDark,
  },
  warning: {
    light: colors.warningLight,
    main: colors.warningMain,
    dark: colors.warningDark,
  },
  success: {
    light: colors.successLight,
    200: colors.success200,
    main: colors.successMain,
    dark: colors.successDark,
  },
  grey: {
    50: colors.grey50,
    100: colors.grey100,
    500: colors.darkTextSecondary,
    600: colors.warningMain,
    700: colors.darkTextPrimary,
    900: colors.darkLevel1,
  },
  text: {
    primary: colors.primaryMain,
    secondary: colors.secondaryMain,
  },
  background: {
    paper: colors.paper,
    default: colors.lightBackground,
  },
};

const muiTheme = createTheme({
  palette,
  breakpoints,
  sizes,
  typography: {
    body1: {
      fontSize: '1.4rem',
    },
    body2: {
      fontSize: '1.6rem',
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          width: '100%',
          height: '100%',
          boxSizing: 'border-box',
          fontSize: '62.5% !important',
        },
        body: {
          width: 'inherit',
          height: 'inherit',
        },
        '#root': {
          width: 'inherit',
          height: 'inherit',
        },
      },
    },
  },
});

export default muiTheme;
