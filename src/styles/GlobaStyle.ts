import { createGlobalStyle } from 'styled-components';
import reset from 'styled-reset';

const GlobalStyle = createGlobalStyle`
  ${reset}
  html {
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    font-size: 62.5%;
    
    body, #root {
      width: inherit;
      height: inherit;
    }
    
    * {
        font-family: 'Arial', 'Noto Sans Korean', "Sans Serif";
        box-sizing: inherit;
    }
  }
`;

export default GlobalStyle;
