import ReactDOM from 'react-dom';
import { HelmetProvider } from 'react-helmet-async';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import {
  ThemeProvider as MuiThemeProvider,
  StyledEngineProvider,
} from '@mui/material';
import { ThemeProvider } from 'styled-components';
import muiTheme from './styles/muiTheme';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { store, persistor } from './store';
import theme from './styles/theme';
import './styles/fonts.css';
import * as sentryUtil from './utils/sentryUtil';
import GlobalStyle from './styles/GlobaStyle';

const isProduction = process.env.NODE_ENV === 'production';

if (isProduction) {
  sentryUtil.init();
}

const queryClient = new QueryClient({
  defaultOptions: { queries: { refetchOnWindowFocus: false } },
});

const rootElement = document.getElementById('root');

ReactDOM.render(
  <HelmetProvider>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <QueryClientProvider client={queryClient}>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
              <MuiThemeProvider theme={muiTheme}>
                <>
                  <GlobalStyle />
                  <App />
                  <ReactQueryDevtools initialIsOpen={false} />
                </>
              </MuiThemeProvider>
            </ThemeProvider>
          </StyledEngineProvider>
        </QueryClientProvider>
      </PersistGate>
    </Provider>
  </HelmetProvider>,
  rootElement,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
