import { EnumLiteralsOf } from '@src/types/common.types';

export const ROUTE_PATHS = {
  root: '/',
  forbidden: '/forbidden',
  auth: '/auth',
  signIn: '/auth/sign-in',
  forgotPassword: '/auth/forgot-password',
  customerInquires: '/customer-inquires',
  user: '/user',
  userManagement: '/user/management',
  ship: '/ship',
  shipAssigned: '/ship/assigned',
  shipManagement: '/ship/management',
  createShip: '/ship/create',
  shipStatus: '/ship/status',
  inspectionRequest: '/ship/inspection-request',
  blastingInspectionRequest: '/ship/inspection-request/blasting',
  coatingInspectionRequest: '/ship/inspection-request/coating',
  inspectionResult: '/inspection-result',
  statistics: '/statistics',
  statisticsPassRate: '/statistics/pass-rate',
  statisticsCompany: '/statistics/company',
  statisticsSemanticAnalysis: '/statistics/semantic-analysis',
} as const;
export type ROUTE_PATHS = EnumLiteralsOf<typeof ROUTE_PATHS>;
