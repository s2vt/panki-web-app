import { Switch, Route } from 'react-router';
import { Redirect } from 'react-router-dom';
import StatisticsPassRatePage from '@src/pages/StatisticsPassRatePage';
import StatisticsPage from '@src/pages/StatisticsPage';
import SemanticAnalysisPage from '@src/pages/SemanticAnalysisPage';
import StatisticsByCompanyPage from '@src/pages/StatisticsByCompanyPage/StatisticsByCompanyPage';
import { ROUTE_PATHS } from '../routePaths';

const StatisticsPagesRouter = () => (
  <Switch>
    <Route exact path={ROUTE_PATHS.statistics} component={StatisticsPage} />
    <Route
      exact
      path={ROUTE_PATHS.statisticsPassRate}
      component={StatisticsPassRatePage}
    />
    <Route
      exact
      path={ROUTE_PATHS.statisticsCompany}
      component={StatisticsByCompanyPage}
    />
    <Route
      exact
      path={ROUTE_PATHS.statisticsSemanticAnalysis}
      component={SemanticAnalysisPage}
    />
    <Redirect to={ROUTE_PATHS.statistics} />
  </Switch>
);

export default StatisticsPagesRouter;
