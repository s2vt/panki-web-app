import { render } from '@testing-library/react';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import StatisticsPagesRouter from '.';

describe('<StatisticsPagesRouter />', () => {
  const setup = (initialState?: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    const result = render(
      <Wrapper>
        <StatisticsPagesRouter />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup();

    expect(container).toBeInTheDocument();
  });
});
