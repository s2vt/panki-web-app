import { Meta, Story } from '@storybook/react';
import StatisticsPagesRouter from './StatisticsPagesRouter';

export default {
  title: 'Routes/StatisticsPagesRouter',
  component: StatisticsPagesRouter,
} as Meta;

const Template: Story = (args) => <StatisticsPagesRouter {...args} />;

export const Basic = Template.bind({});
