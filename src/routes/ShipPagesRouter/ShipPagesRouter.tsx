import { Redirect, Route, Switch } from 'react-router-dom';
import PermissionRoute from '@src/components/routes/PermissionRoute';
import useUserSelector from '@src/hooks/user/useUserSelector';
import CreateShipPage from '@src/pages/CreateShipPage';
import ShipManagementPage from '@src/pages/ShipManagementPage';
import ShipPage from '@src/pages/AssignedShipsPage';
import { UserClass, UserRole } from '@src/types/user.types';
import BlastingInspectionRequestPage from '@src/pages/BlastingInspectionRequestPage';
import InspectionRequestPage from '@src/pages/InspectionRequestPage';
import CoatingInspectionRequestPage from '@src/pages/CoatingInspectionRequestPage';
import ShipStatusPage from '@src/pages/ShipStatusPage';
import useValidateRoles from '@src/hooks/user/useValidateRoles';
import { ROUTE_PATHS } from '../routePaths';

const ShipPagesRouter = () => {
  const { user, isEmployeeUser } = useUserSelector();

  const hasRequestDataCheckRole = useValidateRoles(
    [UserRole.requestDataCheckRole],
    user,
  );

  return (
    <Switch>
      {hasRequestDataCheckRole && (
        <Route
          exact
          path={[
            ROUTE_PATHS.shipAssigned,
            `${ROUTE_PATHS.shipAssigned}/:page(\\d+)`,
          ]}
          component={ShipPage}
        />
      )}
      <Route
        exact
        path={[
          ROUTE_PATHS.shipManagement,
          `${ROUTE_PATHS.shipManagement}/:page(\\d+)`,
        ]}
        component={ShipManagementPage}
      />
      <PermissionRoute
        exact
        path={ROUTE_PATHS.createShip}
        component={CreateShipPage}
        validationUserClasses={[UserClass.admin, UserClass.manager]}
      />
      <PermissionRoute
        exact
        path={ROUTE_PATHS.shipStatus}
        component={ShipStatusPage}
        validationUserClasses={[UserClass.admin, UserClass.manager]}
      />
      <PermissionRoute
        exact
        path={ROUTE_PATHS.inspectionRequest}
        component={InspectionRequestPage}
        validationRoles={[UserRole.requestDataCheckRole]}
      />
      <PermissionRoute
        exact
        path={ROUTE_PATHS.blastingInspectionRequest}
        component={BlastingInspectionRequestPage}
        validationRoles={[UserRole.requestDataCheckRole]}
      />
      <PermissionRoute
        exact
        path={ROUTE_PATHS.coatingInspectionRequest}
        component={CoatingInspectionRequestPage}
        validationRoles={[UserRole.requestDataCheckRole]}
      />
      {isEmployeeUser ? (
        <Redirect to={ROUTE_PATHS.shipAssigned} />
      ) : (
        <Redirect to={ROUTE_PATHS.shipManagement} />
      )}
    </Switch>
  );
};

export default ShipPagesRouter;
