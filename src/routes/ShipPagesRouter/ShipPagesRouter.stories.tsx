import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import ShipPagesRouter from './ShipPagesRouter';

export default {
  title: 'Routes/ShipPagesRouter',
  component: ShipPagesRouter,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <ShipPagesRouter {...args} />;

export const Basic = Template.bind({});
