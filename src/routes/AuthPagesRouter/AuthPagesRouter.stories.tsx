import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import AuthPagesRouter from './AuthPagesRouter';

export default {
  title: 'Routes/AuthPagesRouter',
  component: AuthPagesRouter,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <AuthPagesRouter {...args} />;

export const Basic = Template.bind({});
