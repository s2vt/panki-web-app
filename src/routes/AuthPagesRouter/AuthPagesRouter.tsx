import AuthPageHeader from '@src/components/auth/AuthPageHeader';
import { Redirect, Route, Switch } from 'react-router-dom';
import ForgotPasswordPage from '../../pages/ForgotPasswordPage';
import SignInPage from '../../pages/SignInPage';
import { ROUTE_PATHS } from '../routePaths';

const AuthPagesRouter = () => (
  <>
    <AuthPageHeader />
    <Switch>
      <Route exact path={ROUTE_PATHS.signIn} component={SignInPage} />
      <Route
        exact
        path={ROUTE_PATHS.forgotPassword}
        component={ForgotPasswordPage}
      />
      <Redirect to={ROUTE_PATHS.root} />
    </Switch>
  </>
);

export default AuthPagesRouter;
