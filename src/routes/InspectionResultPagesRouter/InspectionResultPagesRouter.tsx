import { Redirect, Route, Switch } from 'react-router-dom';
import InspectionResultReportPage from '@src/pages/InspectionResultReportPage';
import { ROUTE_PATHS } from '../routePaths';

const InspectionResultPagesRouter = () => (
  <Switch>
    <Route
      exact
      path={ROUTE_PATHS.inspectionResult}
      component={InspectionResultReportPage}
    />
    <Redirect to={ROUTE_PATHS.inspectionResult} />
  </Switch>
);

export default InspectionResultPagesRouter;
