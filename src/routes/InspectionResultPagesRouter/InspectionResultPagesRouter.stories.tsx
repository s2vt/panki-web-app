import { Meta, Story } from '@storybook/react';
import InspectionResultPagesRouter from '.';

export default {
  title: 'Routes/InspectionResultPagesRouter',
  component: InspectionResultPagesRouter,
} as Meta;

const Template: Story = (args) => <InspectionResultPagesRouter {...args} />;

export const Basic = Template.bind({});
