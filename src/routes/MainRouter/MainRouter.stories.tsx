import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import MainRouter from './MainRouter';

export default {
  title: 'Routes/MainRouter',
  component: MainRouter,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <MainRouter {...args} />;

export const Basic = Template.bind({});
