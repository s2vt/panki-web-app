import { Redirect, Route, Switch } from 'react-router-dom';
import { useMemo } from 'react';
import MainLayout from '@src/components/base/MainLayout';
import ForbiddenPage from '@src/pages/ForbiddenPage';
import PermissionRoute from '@src/components/routes/PermissionRoute';
import { UserClass } from '@src/types/user.types';
import useUserSelector from '@src/hooks/user/useUserSelector';
import Sidebar from '@src/components/base/Sidebar';
import { ROUTE_PATHS } from '../routePaths';
import UserPagesRouter from '../UserPagesRouter';
import ShipPagesRouter from '../ShipPagesRouter';
import InspectionResultPagesRouter from '../InspectionResultPagesRouter';

/** 로그인 후 보여줄 페이지 컴포넌트입니다. */
const MainRouter = () => {
  const { isAdminUser } = useUserSelector();
  const rootRouter = useMemo(
    () => (isAdminUser ? UserPagesRouter : ShipPagesRouter),
    [isAdminUser],
  );

  return (
    <>
      <Sidebar />
      <MainLayout>
        <Switch>
          <Route exact path={ROUTE_PATHS.root} component={rootRouter} />
          <Route path={ROUTE_PATHS.ship} component={ShipPagesRouter} />
          <Route path={ROUTE_PATHS.user} component={UserPagesRouter} />
          <PermissionRoute
            path={ROUTE_PATHS.inspectionResult}
            component={InspectionResultPagesRouter}
            validationUserClasses={[UserClass.admin, UserClass.manager]}
          />
          <Route path={ROUTE_PATHS.forbidden} component={ForbiddenPage} />
          <Redirect to={ROUTE_PATHS.root} />
        </Switch>
      </MainLayout>
    </>
  );
};

export default MainRouter;
