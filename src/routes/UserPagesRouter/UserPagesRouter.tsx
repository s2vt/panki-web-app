import { Redirect, Route, Switch } from 'react-router-dom';
import UserManagementPage from '@src/pages/UserManagementPage';
import { ROUTE_PATHS } from '../routePaths';

const UserPagesRoute = () => (
  <Switch>
    <Route
      exact
      path={[
        ROUTE_PATHS.userManagement,
        `${ROUTE_PATHS.userManagement}/:page(\\d+)`,
      ]}
      component={UserManagementPage}
    />
    <Redirect to={ROUTE_PATHS.userManagement} />
  </Switch>
);

export default UserPagesRoute;
