import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import UserPagesRoute from './UserPagesRouter';

export default {
  title: 'Routes/UserPagesRouter',
  component: UserPagesRoute,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <UserPagesRoute {...args} />;

export const Basic = Template.bind({});
