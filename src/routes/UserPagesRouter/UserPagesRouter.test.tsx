import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import userApi from '@src/api/userApi';
import { RootState } from '@src/reducers/rootReducer';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import UserPagesRouter from '.';

describe('<UserPagesRouter />', () => {
  const setup = (initialState?: RootState) => {
    const { wrapper: Wrapper } = prepareMockWrapper(initialState);

    userApi.getCompanyUsers = jest.fn();

    const result = render(
      <Wrapper>
        <UserPagesRouter />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup(rootState);

    expect(container).toBeInTheDocument();
  });
});
