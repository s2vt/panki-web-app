import { Meta, Story } from '@storybook/react';
import InspectionRequestsBox from './InspectionRequestsBox';

export default {
  title: 'Components/ship/InspectionRequestsBox',
  component: InspectionRequestsBox,
} as Meta;

const Template: Story = (args) => <InspectionRequestsBox {...args} />;

export const Basic = Template.bind({});
