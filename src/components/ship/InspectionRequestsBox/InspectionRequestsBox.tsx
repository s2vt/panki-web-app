import { memo, useCallback } from 'react';
import styled from 'styled-components';
import {
  CANCEL,
  CLOSE,
  INSPECTION_REQUEST_LIST,
  MODIFY,
} from '@src/constants/constantString';
import ConfirmModal from '@src/components/common/ConfirmModal';
import Divider from '@src/components/common/Divider';
import ModifyButton from '@src/components/common/ModifyButton';
import RoundButton from '@src/components/common/RoundButton';
import SideBox from '@src/components/common/SideBox';
import ModalWrapper from '@src/components/base/ModalWrapper';
import useDeferSchedules from '@src/hooks/ship/useDeferSchedules';
import useSentInspectionRequestsQuery from './hooks/useSentInspectionRequestsQuery';
import InspectionRequests from './InspectionRequests';

const InspectionRequestsBox = () => {
  const {
    isModifyMode,
    isConfirmModalOpen,
    selectedTaskItems,
    isSelected,
    toggleItem,
    handleModifyClick,
    handleCancelClick,
    closeConfirmModal,
    handleDeferClick,
    handleDeferConfirm,
  } = useDeferSchedules({
    invalidateQueryKey: useSentInspectionRequestsQuery.baseKey,
    notSelectedErrorMessage: '취소할 검사를 클릭해주세요',
  });

  const renderModifyButton = useCallback(() => {
    if (isModifyMode) {
      return null;
    }

    return <ModifyButton label={MODIFY} onClick={handleModifyClick} />;
  }, [isModifyMode]);

  return (
    <>
      <StyledSideBox
        title={INSPECTION_REQUEST_LIST}
        renderButton={renderModifyButton}
      >
        {isModifyMode ? (
          <ButtonSection>
            <RoundButton label={CANCEL} size="xs" onClick={handleDeferClick} />
            <RoundButton label={CLOSE} size="xs" onClick={handleCancelClick} />
          </ButtonSection>
        ) : (
          <Divider orientation="horizontal" />
        )}
        <ContentSection>
          <InspectionRequests
            isModifyMode={isModifyMode}
            onItemClick={toggleItem}
            isSelected={isSelected}
          />
        </ContentSection>
      </StyledSideBox>
      <ModalWrapper isOpen={isConfirmModalOpen}>
        <ConfirmModal
          text={`해당 검사 요청을 취소하시겠습니까? (${selectedTaskItems.length}건)`}
          onClose={closeConfirmModal}
          onConfirm={handleDeferConfirm}
        />
      </ModalWrapper>
    </>
  );
};

const StyledSideBox = styled(SideBox)`
  display: flex;
  flex-direction: column;
`;

const ButtonSection = styled.div`
  display: flex;
  align-items: center;
  gap: 0.8rem;
`;

const ContentSection = styled.div`
  flex: 1;
  margin-top: 0.8rem;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
`;

export default memo(InspectionRequestsBox);
