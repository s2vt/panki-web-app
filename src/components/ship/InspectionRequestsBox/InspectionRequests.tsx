import styled from 'styled-components';
import {
  INSPECTION_REQUEST_EMPTY_MESSAGE,
  SERVER_ERROR,
} from '@src/constants/constantString';
import Divider from '@src/components/common/Divider';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import { SelectStringItem } from '@src/types/common.types';
import InspectionScheduleItem from '../InspectionScheduleBox/InspectionScheduleItem';
import useSentInspectionRequestsQuery from './hooks/useSentInspectionRequestsQuery';

export type InspectionRequestsProps = {
  isModifyMode: boolean;
  onItemClick: (selectItem: SelectStringItem) => void;
  isSelected: (selectItem: SelectStringItem) => boolean;
};

const InspectionRequests = ({
  isModifyMode,
  onItemClick,
  isSelected,
}: InspectionRequestsProps) => {
  const {
    data: sentInspectionRequests,
    isError,
    isFetching,
    error,
    refetch,
  } = useSentInspectionRequestsQuery();

  if (isFetching) {
    return <LoadingSpinner />;
  }

  if (isError) {
    return (
      <ErrorBox
        error={error?.message ?? SERVER_ERROR}
        onRefetchClick={refetch}
      />
    );
  }

  if (
    !sentInspectionRequests ||
    (sentInspectionRequests.blastingApplications.length === 0 &&
      sentInspectionRequests.ctgApplications.length === 0)
  ) {
    return <EmptyMessage>{INSPECTION_REQUEST_EMPTY_MESSAGE}</EmptyMessage>;
  }

  const { blastingApplications, ctgApplications } = sentInspectionRequests;

  return (
    <>
      <ScrollableSection>
        {blastingApplications.map((inspectionSchedule) => (
          <InspectionScheduleItem
            key={inspectionSchedule.taskId}
            inspectionSchedule={inspectionSchedule}
            isModifyMode={isModifyMode}
            onClick={onItemClick}
            isSelected={isSelected}
          />
        ))}
      </ScrollableSection>
      {ctgApplications.length > 0 && <StyledDivider orientation="horizontal" />}
      <ScrollableSection>
        {ctgApplications.map((inspectionSchedule) => (
          <InspectionScheduleItem
            key={inspectionSchedule.taskId}
            inspectionSchedule={inspectionSchedule}
            isModifyMode={isModifyMode}
            onClick={onItemClick}
            isSelected={isSelected}
          />
        ))}
      </ScrollableSection>
    </>
  );
};

const EmptyMessage = styled.div`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.primary};
`;

const ScrollableSection = styled.div`
  flex: 1;

  ${({ theme }) => theme.breakPoint.large} {
    overflow-y: auto;
  }
`;

const StyledDivider = styled(Divider)`
  display: none;
  margin: 0.8rem 0;
  min-height: 1px;

  ${({ theme }) => theme.breakPoint.large} {
    display: block;
  }
`;

export default InspectionRequests;
