import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import taskApi from '@src/api/taskApi';
import { SentInspectionRequests } from '@src/types/task.types';

const useSentInspectionRequestsQuery = (
  options?: UseQueryOptions<SentInspectionRequests, HttpError>,
) => useQuery(createKey(), taskApi.getSentInspectionRequests, options);

const baseKey = ['sentInspectionRequests'];
const createKey = () => [...baseKey];

useSentInspectionRequestsQuery.baseKey = baseKey;
useSentInspectionRequestsQuery.createKey = createKey;

export default useSentInspectionRequestsQuery;
