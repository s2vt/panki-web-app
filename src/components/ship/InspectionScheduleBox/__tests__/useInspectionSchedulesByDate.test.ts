import { BlockInOut } from '@src/types/block.types';
import { InspectionSchedule, TaskStep } from '@src/types/task.types';
import { renderHook } from '@testing-library/react-hooks/dom';
import { addDays, subDays } from 'date-fns';
import useInspectionSchedulesByDate from '../hooks/useInspectionSchedulesByDate';

describe('useInspectionSchedulesByDate hook', () => {
  const setup = (inspectionSchedules: InspectionSchedule[]) =>
    renderHook(() => useInspectionSchedulesByDate(inspectionSchedules));

  it('should return grouped schedules by date', () => {
    // given
    const currentDate = new Date();

    const currentDateSchedule: InspectionSchedule = {
      taskId: '1',
      blockName: 'block1',
      inOut: BlockInOut.IN,
      scheduledAt: currentDate,
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const prevDateSchedule: InspectionSchedule = {
      taskId: '2',
      blockName: 'block2',
      inOut: BlockInOut.IN,
      scheduledAt: subDays(currentDate, 1),
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const nextDateSchedule: InspectionSchedule = {
      taskId: '3',
      blockName: 'block3',
      inOut: BlockInOut.IN,
      scheduledAt: addDays(currentDate, 1),
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const inspectionSchedules: InspectionSchedule[] = [
      prevDateSchedule,
      nextDateSchedule,
      currentDateSchedule,
    ];

    const { result } = setup(inspectionSchedules);

    // then
    const expected = {
      currentDateSchedules: [currentDateSchedule],
      otherSchedules: [prevDateSchedule, nextDateSchedule],
    };

    expect(result.current).toEqual(expected);
  });
});
