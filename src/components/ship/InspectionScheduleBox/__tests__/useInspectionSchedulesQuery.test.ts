import HttpError from '@src/api/model/HttpError';
import scheduleApi from '@src/api/scheduleApi';
import inspectionSchedules from '@src/test/fixtures/task/inspectionSchedules';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { BlockInOut } from '@src/types/block.types';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { InspectionSchedule, TaskStep } from '@src/types/task.types';
import { renderHook } from '@testing-library/react-hooks/dom';
import { addDays, subDays } from 'date-fns';
import useInspectionSchedulesQuery from '../hooks/useInspectionSchedulesQuery';

describe('useInspectionSchedulesQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    const { result, waitFor } = renderHook(
      () => useInspectionSchedulesQuery(),
      { wrapper },
    );

    return { result, waitFor };
  };

  it('should cache data when succeed fetch', async () => {
    // given
    scheduleApi.getInspectionSchedules = jest
      .fn()
      .mockResolvedValue(inspectionSchedules);

    const { result, waitFor } = setup();

    // when
    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(inspectionSchedules);
  });

  it('should sort by current date', async () => {
    // given
    const currentDate = new Date();

    const currentDateSchedule: InspectionSchedule = {
      taskId: '1',
      blockName: 'block1',
      inOut: BlockInOut.IN,
      scheduledAt: currentDate,
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const prevDateSchedule: InspectionSchedule = {
      taskId: '2',
      blockName: 'block2',
      inOut: BlockInOut.IN,
      scheduledAt: subDays(currentDate, 1),
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const nextDateSchedule: InspectionSchedule = {
      taskId: '3',
      blockName: 'block3',
      inOut: BlockInOut.IN,
      scheduledAt: addDays(currentDate, 1),
      shipName: 'ship',
      taskStep: TaskStep.blastingTask,
    };

    const inspectionSchedules: InspectionSchedule[] = [
      prevDateSchedule,
      nextDateSchedule,
      currentDateSchedule,
    ];

    scheduleApi.getInspectionSchedules = jest
      .fn()
      .mockResolvedValue(inspectionSchedules);

    const { result, waitFor } = setup();

    // when
    await waitFor(() => result.current.isSuccess);

    // then
    const expected = [currentDateSchedule, nextDateSchedule, prevDateSchedule];

    expect(result.current.data).toEqual(expected);
  });

  it('should set error when failed fetch', async () => {
    // given
    scheduleApi.getInspectionSchedules = jest
      .fn()
      .mockRejectedValue(
        new HttpError(HttpErrorStatus.BadRequest, ResultCode.invalidRequest),
      );

    const { result, waitFor } = setup();

    // when
    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
