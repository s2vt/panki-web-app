import { memo } from 'react';
import styled from 'styled-components';
import { SERVER_ERROR } from '@src/constants/constantString';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import { SelectStringItem } from '@src/types/common.types';
import Divider from '@src/components/common/Divider';
import useInspectionSchedulesQuery from './hooks/useInspectionSchedulesQuery';
import InspectionScheduleItem from './InspectionScheduleItem';
import useInspectionSchedulesByDate from './hooks/useInspectionSchedulesByDate';

export type InspectionSchedulesProps = {
  isModifyMode: boolean;
  onItemClick: (selectItem: SelectStringItem) => void;
  isSelected: (selectItem: SelectStringItem) => boolean;
};

const InspectionSchedules = ({
  isModifyMode,
  onItemClick,
  isSelected,
}: InspectionSchedulesProps) => {
  const {
    data: inspectionSchedules,
    isError,
    isFetching,
    refetch,
  } = useInspectionSchedulesQuery();

  const { currentDateSchedules, otherSchedules } = useInspectionSchedulesByDate(
    inspectionSchedules ?? [],
  );

  if (isFetching) {
    return <LoadingSpinner />;
  }

  if (isError) {
    return <ErrorBox error={SERVER_ERROR} onRefetchClick={refetch} />;
  }

  if (inspectionSchedules === undefined || inspectionSchedules?.length === 0) {
    return <EmptyMessage>검사 스케쥴이 없습니다.</EmptyMessage>;
  }

  return (
    <Box>
      {currentDateSchedules.map((inspectionSchedule) => (
        <InspectionScheduleItem
          key={inspectionSchedule.taskId}
          inspectionSchedule={inspectionSchedule}
          isModifyMode={isModifyMode}
          onClick={onItemClick}
          isSelected={isSelected}
        />
      ))}
      {currentDateSchedules.length > 0 && <Divider orientation="horizontal" />}
      {otherSchedules.map((inspectionSchedule) => (
        <InspectionScheduleItem
          key={inspectionSchedule.taskId}
          inspectionSchedule={inspectionSchedule}
          isModifyMode={isModifyMode}
          onClick={onItemClick}
          isSelected={isSelected}
        />
      ))}
    </Box>
  );
};

const Box = styled.div`
  margin-top: 0.8rem;
`;

const EmptyMessage = styled.div`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default memo(InspectionSchedules);
