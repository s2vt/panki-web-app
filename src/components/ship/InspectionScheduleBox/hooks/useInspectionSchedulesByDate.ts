import { InspectionSchedule } from '@src/types/task.types';
import { isSameDate } from '@src/utils/dateUtil';
import { useMemo } from 'react';

type SchedulesByDate = {
  currentDateSchedules: InspectionSchedule[];
  otherSchedules: InspectionSchedule[];
};

const useInspectionSchedulesByDate = (
  inspectionSchedules: InspectionSchedule[],
) => {
  const currentDate = useMemo(() => new Date(), []);

  const { currentDateSchedules, otherSchedules } = inspectionSchedules.reduce(
    (prev, schedule) => {
      const { scheduledAt } = schedule;

      if (isSameDate(currentDate, scheduledAt)) {
        prev.currentDateSchedules.push(schedule);
      } else {
        prev.otherSchedules.push(schedule);
      }

      return prev;
    },
    { currentDateSchedules: [], otherSchedules: [] } as SchedulesByDate,
  );

  return { currentDateSchedules, otherSchedules };
};

export default useInspectionSchedulesByDate;
