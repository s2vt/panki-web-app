import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import scheduleApi from '@src/api/scheduleApi';
import { InspectionSchedule } from '@src/types/task.types';
import _ from 'lodash';
import * as dateUtil from '@src/utils/dateUtil';

const useInspectionSchedulesQuery = (
  options?: UseQueryOptions<InspectionSchedule[], HttpError>,
) =>
  useQuery(createKey(), scheduleApi.getInspectionSchedules, {
    ...options,
    select: sortSchedulesByCurrentDateAndDescending,
  });

const sortSchedulesByCurrentDateAndDescending = (
  inspectionSchedules: InspectionSchedule[],
) => {
  const copied = _.cloneDeep(inspectionSchedules);
  const currentDate = new Date();

  copied.sort((a, b) => {
    if (dateUtil.isSameDate(currentDate, a.scheduledAt)) {
      return -1;
    }

    if (dateUtil.isSameDate(currentDate, b.scheduledAt)) {
      return 1;
    }

    return b.scheduledAt.getTime() - a.scheduledAt.getTime();
  });

  return copied;
};

const baseKey = ['inspectionSchedulesQuery'];
const createKey = () => [...baseKey];

useInspectionSchedulesQuery.baseKey = baseKey;
useInspectionSchedulesQuery.createKey = createKey;

export default useInspectionSchedulesQuery;
