import { Meta, Story } from '@storybook/react';
import InspectionScheduleBox from './InspectionScheduleBox';

export default {
  title: 'Components/InspectionScheduleBox',
  component: InspectionScheduleBox,
} as Meta;

const Template: Story = (args) => <InspectionScheduleBox {...args} />;

export const Basic = Template.bind({});
