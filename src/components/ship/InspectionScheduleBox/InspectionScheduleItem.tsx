import { memo } from 'react';
import styled, { css } from 'styled-components';
import { SHIP } from '@src/constants/constantString';
import { convertTaskStepText } from '@src/types/report.types';
import { InspectionSchedule } from '@src/types/task.types';
import * as dateUtil from '@src/utils/dateUtil';
import { SelectStringItem } from '@src/types/common.types';

export type InspectionScheduleItemProps = {
  inspectionSchedule: InspectionSchedule;
  onClick: (selectItem: SelectStringItem) => void;
  isSelected: (selectItem: SelectStringItem) => boolean;
  isModifyMode: boolean;
};

const InspectionScheduleItem = ({
  inspectionSchedule,
  onClick,
  isSelected,
  isModifyMode,
}: InspectionScheduleItemProps) => {
  const { blockName, shipName, taskStep, taskId, scheduledAt, inOut } =
    inspectionSchedule;

  const taskItem: SelectStringItem = { id: taskId, value: taskId };

  return (
    <Box
      isSelected={isSelected(taskItem)}
      onClick={() => isModifyMode && onClick({ id: taskId, value: taskId })}
      isModifyMode={isModifyMode}
    >
      <InfoSection>
        <strong>{`${shipName}${SHIP}`}</strong>
        <strong>{blockName}</strong>
        <p>{inOut}</p>
      </InfoSection>
      <InfoSection>
        <p>{convertTaskStepText(taskStep)}</p>
      </InfoSection>
      <p>{dateUtil.formatToDateTime(scheduledAt)}</p>
    </Box>
  );
};

const Box = styled.div<{ isSelected: boolean; isModifyMode: boolean }>`
  font-size: 1.4rem;
  padding: 0.8rem 0;
  color: ${({ isSelected, theme }) =>
    isSelected ? theme.colors.black : theme.colors.primary};
  ${({ isModifyMode }) =>
    isModifyMode &&
    css`
      cursor: pointer;
    `}

  strong {
    margin-right: 0.4rem;
    font-weight: 700;
  }

  p {
    ${({ isSelected }) =>
      isSelected &&
      css`
        font-weight: 700;
      `}
  }
`;

const InfoSection = styled.div`
  display: flex;
  margin-bottom: 0.4rem;
`;

export default memo(InspectionScheduleItem);
