import { Ship } from '@src/types/ship.types';
import ShipTableItem from '../ShipTableItem';

export type ShipTableBodyProps = {
  ships: Ship[];
  onClick: (ship: Ship) => void;
  onCheckboxChange?: (ship: Ship) => void;
  isSelected?: (ship: Ship) => boolean;
};

const ShipTableBody = ({
  ships,
  onClick,
  onCheckboxChange,
  isSelected,
}: ShipTableBodyProps) => (
  <tbody>
    {ships.map((ship, index) => (
      <ShipTableItem
        key={ship.id}
        ship={ship}
        onClick={onClick}
        onCheckboxChange={onCheckboxChange}
        checked={isSelected && isSelected(ship)}
        isOddItem={index % 2 === 0}
      />
    ))}
  </tbody>
);

export default ShipTableBody;
