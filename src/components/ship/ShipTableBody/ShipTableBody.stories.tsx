import { Meta, Story } from '@storybook/react';
import ShipTableBody, { ShipTableBodyProps } from './ShipTableBody';

export default {
  title: 'Components/ship/ShipTableBody',
  component: ShipTableBody,
} as Meta;

const Template: Story<ShipTableBodyProps> = (args) => (
  <table style={{ width: '100%' }}>
    <ShipTableBody {...args} />
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  ships: [
    {
      id: '1',
      ownerCompany: '선주사1',
      shipName: '0001',
      qmManager: [{ id: '1', userName: 'test' }],
      state: 'FINISHED',
    },
    {
      id: '2',
      ownerCompany: '선주사2',
      shipName: '0002',
      qmManager: [{ id: '1', userName: 'test' }],
      state: 'FINISHED',
    },
  ],
};
