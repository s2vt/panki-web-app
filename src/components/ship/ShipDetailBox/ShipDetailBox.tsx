/* eslint-disable react/no-array-index-key */
import styled from 'styled-components';
import {
  ASSIGNED_QUALITY_MANAGER,
  SHIP_NUMBER,
  SHIP_OWNER,
} from '@src/constants/constantString';
import { Ship } from '@src/types/ship.types';
import Input from '@src/components/form/Input';
import RowFormInput from '@src/components/form/RowFormInput';
import ShipFormBase from '../ShipFormBase';

export type ShipDetailBoxProps = {
  ship: Ship;
};

const ShipDetailBox = ({ ship }: ShipDetailBoxProps) => {
  const { shipName, ownerCompany, qmManager } = ship;

  return (
    <ShipFormBase.InputSection height="19rem">
      <ShipFormBase.Row>
        <RowFormInput
          label={SHIP_NUMBER}
          enableShadow
          readOnly
          labelFontSize="1.6rem"
          defaultValue={shipName}
          labelPadding={0}
        />
      </ShipFormBase.Row>
      <ShipFormBase.Row>
        <RowFormInput
          label={SHIP_OWNER}
          enableShadow
          readOnly
          labelFontSize="1.6rem"
          defaultValue={ownerCompany}
          labelPadding={0}
        />
      </ShipFormBase.Row>
      <ShipFormBase.Row>
        <RowFormInput
          label={ASSIGNED_QUALITY_MANAGER}
          enableShadow
          readOnly
          labelFontSize="1.6rem"
          defaultValue={qmManager[0].userName}
          labelPadding={0}
        />
      </ShipFormBase.Row>
      {qmManager.map(
        (manager, index) =>
          index > 0 && (
            <ShipFormBase.BlankRow key={index}>
              <StyledInput
                inputWidth="52.3rem"
                enableShadow
                readOnly
                defaultValue={manager.userName}
              />
            </ShipFormBase.BlankRow>
          ),
      )}
    </ShipFormBase.InputSection>
  );
};

const StyledInput = styled(Input)`
  height: 4.6rem;
  border: none;

  :read-only {
    font-weight: 400;
    color: ${({ theme }) => theme.colors.primary};
  }
`;

export default ShipDetailBox;
