import { Meta, Story } from '@storybook/react';
import ShipDetailBox, { ShipDetailBoxProps } from './ShipDetailBox';

export default {
  title: 'Components/Ship/ShipDetailBox',
  component: ShipDetailBox,
} as Meta;

const Template: Story<ShipDetailBoxProps> = (args) => (
  <ShipDetailBox {...args} />
);

export const Basic = Template.bind({});
