import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { SaveShipPayload } from '@src/types/ship.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import useUpdateShip from '../hooks/useUpdateShip';

describe('useUpdateShip hook', () => {
  const setup = (shipId: string) => {
    const { wrapper } = prepareWrapper();

    const handleUpdateShipSuccess = jest.fn();
    const handleUpdateShipError = jest.fn();
    const updateShip = jest.spyOn(shipApi, 'updateShip');

    const { result, waitFor } = renderHook(
      () =>
        useUpdateShip({
          shipId,
          onUpdateShipSuccess: handleUpdateShipSuccess,
          onUpdateShipError: handleUpdateShipError,
        }),
      { wrapper },
    );

    return {
      result,
      waitFor,
      handleUpdateShipSuccess,
      handleUpdateShipError,
      updateShip,
    };
  };

  it('should call handleUpdateShipError when api throw error', async () => {
    // given
    const shipId = '1';
    const saveShipPayload: SaveShipPayload = {
      managerIds: [],
      ownerCompanyName: 'company',
      shipName: '1234',
    };

    const { result, waitFor, handleUpdateShipError, updateShip } =
      setup(shipId);

    updateShip.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleSaveShipSubmit(saveShipPayload);
    });

    // then
    await waitFor(() => expect(handleUpdateShipError).toBeCalled());
  });

  it('should call handleUpdateShipSuccess when success api request', async () => {
    // given
    const shipId = '1';
    const saveShipPayload: SaveShipPayload = {
      managerIds: [],
      ownerCompanyName: 'company',
      shipName: '1234',
    };

    const { result, waitFor, handleUpdateShipSuccess, updateShip } =
      setup(shipId);

    updateShip.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleSaveShipSubmit(saveShipPayload);
    });

    // then
    await waitFor(() => expect(handleUpdateShipSuccess).toBeCalled());
    expect(updateShip).toBeCalledWith({
      ...saveShipPayload,
      shipId,
    });
  });
});
