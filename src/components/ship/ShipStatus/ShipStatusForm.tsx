import { useState } from 'react';
import { useHistory } from 'react-router';
import HttpError from '@src/api/model/HttpError';
import {
  DUPLICATE_SHIP_NAME_ERROR,
  MODIFY,
} from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { ResultCode } from '@src/types/http.types';
import { Ship } from '@src/types/ship.types';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import AlertModal from '@src/components/common/AlertModal';
import MainButton from '@src/components/common/MainButton';
import ModalWrapper from '@src/components/base/ModalWrapper';
import SaveShipForm from '../SaveShipForm';
import ShipDetailBox from '../ShipDetailBox';
import ShipFormBase from '../ShipFormBase';
import useUpdateShip from './hooks/useUpdateShip';

export type ShipStatusFormProps = {
  ship: Ship;
};

const ShipStatusForm = ({ ship }: ShipStatusFormProps) => {
  const [isModifyMode, setIsModifyMode] = useState(false);
  const { openAlertDialog } = useAlertDialogAction();
  const history = useHistory();

  const [isDuplicateShipAlertOpen, setIsDuplicateShipAlertOpen] =
    useState(false);

  const handleUpdateShipSuccess = () => {
    history.push(ROUTE_PATHS.ship);
  };

  const handleUpdateShipError = (error: HttpError) => {
    if (error.resultCode === ResultCode.duplicateId) {
      setIsDuplicateShipAlertOpen(true);
      return;
    }

    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { handleSaveShipSubmit, isUpdateShipLoading } = useUpdateShip({
    shipId: ship.id,
    onUpdateShipSuccess: handleUpdateShipSuccess,
    onUpdateShipError: handleUpdateShipError,
  });

  return (
    <>
      {isModifyMode ? (
        <SaveShipForm
          onSubmit={handleSaveShipSubmit}
          onClickClose={() => setIsModifyMode(false)}
          disabledSubmit={isUpdateShipLoading}
          ship={ship}
        />
      ) : (
        <>
          <ShipDetailBox ship={ship} />
          <ShipFormBase.ButtonSection>
            <MainButton
              label={addWhiteSpacesBetweenCharacters(MODIFY)}
              backgroundColor="white"
              labelColor="primary"
              onClick={() => setIsModifyMode(true)}
            />
          </ShipFormBase.ButtonSection>
        </>
      )}
      <ModalWrapper isOpen={isDuplicateShipAlertOpen}>
        <AlertModal
          text={DUPLICATE_SHIP_NAME_ERROR}
          onClose={() => setIsDuplicateShipAlertOpen(false)}
        />
      </ModalWrapper>
    </>
  );
};

export default ShipStatusForm;
