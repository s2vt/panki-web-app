import { Meta, Story } from '@storybook/react';
import ShipStatus from '../ShipStatus';

export default {
  title: 'Components/Ship/ShipStatus',
  component: ShipStatus,
} as Meta;

const Template: Story = (args) => <ShipStatus {...args} />;

export const Basic = Template.bind({});
