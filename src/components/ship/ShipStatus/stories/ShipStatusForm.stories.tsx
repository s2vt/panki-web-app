import { Meta, Story } from '@storybook/react';
import ShipStatusForm, { ShipStatusFormProps } from '../ShipStatusForm';

export default {
  title: 'Components/Ship/ShipStatus/ShipStatusForm',
  component: ShipStatusForm,
} as Meta;

const Template: Story<ShipStatusFormProps> = (args) => (
  <ShipStatusForm {...args} />
);

export const Basic = Template.bind({});
