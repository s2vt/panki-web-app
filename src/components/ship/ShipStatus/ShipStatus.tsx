import { useEffect, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router';
import { SHIP_AND_BLOCK } from '@src/constants/constantString';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Ship } from '@src/types/ship.types';
import PageTitle from '@src/components/base/PageTitle';
import { Box } from '@mui/material';
import ShipStatusForm from './ShipStatusForm';
import BlockStateTable from '../BlockStateTable';

const ShipStatus = () => {
  const location = useLocation<{ ship?: Ship }>();
  const history = useHistory<{ ship?: Ship }>();

  useEffect(() => {
    if (location.state?.ship === undefined) {
      history.push(ROUTE_PATHS.ship);
    }
  }, [history]);

  const ship = useMemo(() => location.state?.ship, [location.state?.ship]);

  return (
    <Box>
      <PageTitle title={SHIP_AND_BLOCK} />
      {ship !== undefined && (
        <>
          <ShipStatusForm ship={ship} />
          <BlockStateTable shipId={ship.id} />
        </>
      )}
    </Box>
  );
};

export default ShipStatus;
