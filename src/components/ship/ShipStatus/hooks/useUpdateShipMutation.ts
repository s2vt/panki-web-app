import { useMutation, UseMutationOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import { UpdateShipPayload } from '@src/types/ship.types';

const useUpdateShipMutation = (
  options?: UseMutationOptions<unknown, HttpError, UpdateShipPayload>,
) =>
  useMutation(
    (updateShipPayload: UpdateShipPayload) =>
      shipApi.updateShip(updateShipPayload),
    options,
  );

export default useUpdateShipMutation;
