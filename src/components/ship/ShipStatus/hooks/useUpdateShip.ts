import HttpError from '@src/api/model/HttpError';
import { SaveShipPayload } from '@src/types/ship.types';
import useUpdateShipMutation from './useUpdateShipMutation';

export type UseUpdateShipProps = {
  shipId: string;
  onUpdateShipSuccess: () => void;
  onUpdateShipError: (error: HttpError) => void;
};

const useUpdateShip = ({
  shipId,
  onUpdateShipSuccess,
  onUpdateShipError,
}: UseUpdateShipProps) => {
  const { mutate: updateShipMutate, isLoading: isUpdateShipLoading } =
    useUpdateShipMutation({
      onSuccess: onUpdateShipSuccess,
      onError: onUpdateShipError,
    });

  const handleSaveShipSubmit = (saveShipPayload: SaveShipPayload) => {
    updateShipMutate({ ...saveShipPayload, shipId });
  };

  return { handleSaveShipSubmit, isUpdateShipLoading };
};

export default useUpdateShip;
