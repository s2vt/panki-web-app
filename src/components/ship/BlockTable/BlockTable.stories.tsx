import { Meta, Story } from '@storybook/react';
import { Block, BlockState } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import BlockTable, { BlockTableProps } from './BlockTable';

export default {
  title: 'Components/ship/BlockTable',
  component: BlockTable,
} as Meta;

const Template: Story<BlockTableProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <BlockTable {...args} />
  </div>
);

const blocks: Block[] = [
  {
    id: '1',
    blockDivision: 'PORT',
    blockName: '1B11',
    inOut: 'IN',
    state: BlockState.BLASTING_INSPECTION_FINISHED,
  },
  {
    id: '2',
    blockDivision: 'PORT',
    blockName: '1B11',
    inOut: 'OUT',
    state: BlockState.BLASTING_INSPECTION_REQUEST,
  },
  {
    id: '1',
    blockDivision: 'STARBOARD',
    blockName: '5B11',
    inOut: 'IN',
    state: BlockState.INSPECTION_FINISH,
  },
  {
    id: '2',
    blockDivision: 'STARBOARD',
    blockName: '5B11',
    inOut: 'OUT',
    state: BlockState.CTG_1ST_INSPECTION_FINISHED,
  },
  {
    id: '3',
    blockDivision: 'PORT',
    blockName: '1S11',
    inOut: 'IN',
    state: BlockState.CTG_2ND_INSPECTION_FINISHED,
  },
  {
    id: '4',
    blockDivision: 'PORT',
    blockName: '1S11',
    inOut: 'OUT',
    state: BlockState.CTG_3RD_INSPECTION_REJECTED,
  },
  {
    id: '3',
    blockDivision: 'STARBOARD',
    blockName: '5S11',
    inOut: 'IN',
    state: BlockState.CTG_1ST_INSPECTION_REJECTED,
  },
  {
    id: '4',
    blockDivision: 'STARBOARD',
    blockName: '5S11',
    inOut: 'OUT',
    state: BlockState.CTG_5TH_INSPECTION_REJECTED,
  },
  {
    id: '5',
    blockDivision: 'STARBOARD',
    blockName: '1S12',
    inOut: 'IN',
    state: BlockState.CTG_2ND_INSPECTION_REQUEST,
  },
  {
    id: '6',
    blockDivision: 'STARBOARD',
    blockName: '1S12',
    inOut: 'OUT',
    state: BlockState.CTG_5TH_INSPECTION_FINISHED,
  },
  {
    id: '7',
    blockDivision: 'CENTER',
    blockName: 'F11C',
    inOut: 'IN',
    state: BlockState.CREATE,
  },
  {
    id: '8',
    blockDivision: 'CENTER',
    blockName: 'F11C',
    inOut: 'OUT',
    state: BlockState.CREATE,
  },
  {
    id: '9',
    blockDivision: 'CENTER',
    blockName: 'E11C',
    inOut: 'IN',
    state: BlockState.CREATE,
  },
  {
    id: '10',
    blockDivision: 'CENTER',
    blockName: 'E11C',
    inOut: 'OUT',
    state: BlockState.CREATE,
  },
  {
    id: '11',
    blockDivision: 'CENTER',
    blockName: 'N11C',
    inOut: 'IN',
    state: BlockState.CREATE,
  },
  {
    id: '12',
    blockDivision: 'CENTER',
    blockName: 'N11C',
    inOut: 'OUT',
    state: BlockState.CREATE,
  },
];

export const SelectionMode = Template.bind({});
SelectionMode.args = {
  blocks,
  isSelected: (_: SelectItem & Block) => true,
  mode: 'selection',
  selectionType: 'select',
  onCheckboxChange: () => console.log(),
  selectableStates: [
    BlockState.PREPARATION,
    BlockState.BLASTING_INSPECTION_FINISHED,
    BlockState.CTG_1ST_INSPECTION_FINISHED,
    BlockState.CTG_1ST_INSPECTION_REJECTED,
    BlockState.CTG_2ND_INSPECTION_FINISHED,
    BlockState.CTG_2ND_INSPECTION_REJECTED,
    BlockState.CTG_3RD_INSPECTION_FINISHED,
    BlockState.CTG_3RD_INSPECTION_REJECTED,
    BlockState.CTG_4TH_INSPECTION_FINISHED,
    BlockState.CTG_4TH_INSPECTION_REJECTED,
    BlockState.CTG_5TH_INSPECTION_REJECTED,
  ],
};

export const ViewMode = Template.bind({});
ViewMode.args = {
  blocks,
  isSelected: (_: SelectItem & Block) => true,
  mode: 'view',
};
