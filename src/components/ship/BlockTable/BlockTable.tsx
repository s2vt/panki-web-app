/* eslint-disable array-callback-return */
import { useMemo } from 'react';
import styled from 'styled-components';
import {
  Block,
  BlockInOut,
  BlockPrefix,
  BlockState,
  BlockTableItem,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import {
  daesunShipmentBlocksFilterCondition,
  getBlockTableItemsByBlocks,
  getMaximumBlockCount,
} from '@src/utils/blockUtil';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import BlockTableRow from '../BlockTableRow';

export type BlockTableMode = 'selection' | 'view';

export type BlockTableSelectionType = 'select' | 'disable';

export type BlockTableProps = {
  blocks: Block[];
  /**
   * selection 모드의 경우 isSelected, onCheckboxChange, selectionType props 가 필수
   */
  mode: BlockTableMode;
  isSelected?: (selectItem: SelectItem & Block) => boolean;
  enableMultipleSelection?: boolean;
  onCheckboxChange?: (selectItem: SelectItem & Block) => void;
  /**
   * select의 경우 기본적인 선택 모드이고,
   * disable의 경우 default로 모든 블록 아이템이 체크 되어있고 해제한 아이템들이 리스트에 쌓이는 방식
   */
  selectionType?: BlockTableSelectionType;
  /**
   * selection 모드에서 해당 state에 해당하는 블록들만 선택 가능하게끔 값을 지정
   */
  selectableStates?: BlockState[];
  selectablePosition?: BlockInOut[];
};

const BlockTable = ({
  blocks,
  mode,
  isSelected,
  enableMultipleSelection = false,
  onCheckboxChange,
  selectionType,
  selectableStates,
  selectablePosition,
}: BlockTableProps) => {
  const blockTableItems: BlockTableItem[] = useMemo(
    () =>
      getBlockTableItemsByBlocks(
        blocks,
        Object.values(BlockPrefix),
        daesunShipmentBlocksFilterCondition,
      ),
    [blocks],
  );

  const maximumBlockCount = useMemo(
    () => getMaximumBlockCount(blockTableItems),
    [blockTableItems],
  );

  return (
    <Box>
      <Header>
        <BlockHeaderColumn>BLOCK</BlockHeaderColumn>
        <NumberHeaderColumn>No.</NumberHeaderColumn>
      </Header>
      <Body>
        {blockTableItems.map((blockTableItem) => (
          <BlockTableRow
            key={blockTableItem.blockPrefix}
            blockTableItem={blockTableItem}
            maximumBlockCount={maximumBlockCount}
            isSelected={isSelected}
            onCheckboxChange={onCheckboxChange}
            mode={mode}
            selectionType={selectionType}
            selectableStates={selectableStates}
            selectablePosition={selectablePosition}
            enableMultipleSelection={enableMultipleSelection}
          />
        ))}
      </Body>
    </Box>
  );
};

const Loader = () => (
  <LoadingWrapper>
    <LoadingSpinner />
  </LoadingWrapper>
);

const Box = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  border: ${({ theme }) => theme.borders.blockTableBorder};
  background: ${({ theme }) => theme.colors.white};
  margin-bottom: 8rem;
  font-size: 1.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const Header = styled.div`
  height: 3.273rem;
  display: flex;
  background: ${({ theme }) => theme.colors.tableBackground};
  border-bottom: ${({ theme }) => theme.borders.blockTableBorder};
`;

const Body = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const HeaderItem = styled.b`
  ${({ theme }) => theme.layout.flexCenterLayout}
`;

const BlockHeaderColumn = styled(HeaderItem)`
  width: 5.95rem;
  border-right: ${({ theme }) => theme.borders.blockTableBorder};
`;

const NumberHeaderColumn = styled(HeaderItem)`
  flex: 1;
`;

const LoadingWrapper = styled.div`
  height: 54.4rem;
`;

const BlockTableTitleSection = styled.div`
  padding: 1.7rem 0 1.4rem 3.5rem;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 2.2rem;
  font-weight: 700;
  display: flex;
`;

BlockTable.Loader = Loader;
BlockTable.LoadingWrapper = LoadingWrapper;
BlockTable.TitleSection = BlockTableTitleSection;

export default BlockTable;
