import { Meta, Story } from '@storybook/react';
import BlockStatusBox, { BlockStatusBoxProps } from './BlockStatusBox';

export default {
  title: 'Components/Ship/BlockStatusBox',
  component: BlockStatusBox,
} as Meta;

const Template: Story<BlockStatusBoxProps> = (args) => (
  <BlockStatusBox {...args} />
);

export const Basic = Template.bind({});
