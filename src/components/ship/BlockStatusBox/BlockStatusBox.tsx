import styled from 'styled-components';
import {
  BLASTING,
  BLOCK_STATE,
  COATING,
  DONE,
} from '@src/constants/constantString';
import { colors } from '@src/styles/theme';
import BlockTable from '../BlockTable';

export type BlockStatusBoxProps = {};

const BlockStatusBox = () => (
  <BlockTable.TitleSection>
    {BLOCK_STATE}
    <Box color="blasting" text={BLASTING} />
    <Box color="coating" text={COATING} />
    <Box color="finished" text={DONE} />
  </BlockTable.TitleSection>
);

const Box = ({
  color,
  text,
}: {
  color: keyof typeof colors.blockStateColors;
  text: string;
}) => (
  <StateWrapper>
    <StateColorBox color={color} />
    <p>{text}</p>
  </StateWrapper>
);

const StateWrapper = styled.div`
  margin-left: 3.6rem;
  font-size: 1.6rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
  display: flex;
  align-items: center;
  gap: 0.8rem;
`;

const StateColorBox = styled.div<{
  color: keyof typeof colors.blockStateColors;
}>`
  width: 5.4rem;
  height: 100%;
  background: ${({ color, theme }) => theme.colors.blockStateColors[color]};
`;

export default BlockStatusBox;
