import { Meta, Story } from '@storybook/react';
import ShipTableHeader, { ShipTableHeaderProps } from './ShipTableHeader';

export default {
  title: 'Components/Ship/ShipTableHeader',
  component: ShipTableHeader,
} as Meta;

const Template: Story<ShipTableHeaderProps> = (args) => (
  <ShipTableHeader {...args} />
);

export const Basic = Template.bind({});
