import {
  ASSIGNED_QUALITY_MANAGER,
  SHIP,
  SHIP_OWNER,
  STATE,
} from '@src/constants/constantString';
import TableHeader from '@src/components/table/TableHeader';

export type ShipTableHeaderProps = {
  onCheckboxClick?: () => void;
  checked?: boolean;
};

const SHIP_TABLE_COLUMNS = [SHIP, SHIP_OWNER, ASSIGNED_QUALITY_MANAGER, STATE];

const ShipTableHeader = ({
  onCheckboxClick,
  checked,
}: ShipTableHeaderProps) => (
  <TableHeader
    columns={SHIP_TABLE_COLUMNS}
    checked={checked}
    onCheckboxClick={onCheckboxClick}
  />
);

export default ShipTableHeader;
