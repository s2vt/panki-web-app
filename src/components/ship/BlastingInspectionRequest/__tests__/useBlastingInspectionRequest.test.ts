import HttpError from '@src/api/model/HttpError';
import taskApi from '@src/api/taskApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import {
  Block,
  BlockDivision,
  BlockInOut,
  BlockState,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { BlastingInspectionRequestPayload } from '@src/types/ship.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import useBlastingInspectionRequest from '../hooks/useBlastingInspectionRequest';

describe('useBlastingInspectionRequest hook', () => {
  const setup = (selectedBlocks: (SelectItem & Block)[] = []) => {
    const { wrapper, store } = prepareMockWrapper();

    const onBlastingInspectionRequestSuccess = jest.fn();
    const onBlastingInspectionRequestError = jest.fn();
    const requestBlastingInspection = jest.spyOn(
      taskApi,
      'requestBlastingInspection',
    );

    const { result, waitFor } = renderHook(
      () =>
        useBlastingInspectionRequest({
          selectedBlocks,
          onBlastingInspectionRequestSuccess,
          onBlastingInspectionRequestError,
        }),
      { wrapper },
    );

    return {
      result,
      store,
      waitFor,
      onBlastingInspectionRequestSuccess,
      onBlastingInspectionRequestError,
      requestBlastingInspection,
    };
  };

  it('should call open alert dialog action when has not block selected', () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [];
    const inspectionRequestPayload: BlastingInspectionRequestPayload = {
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const { result, store } = setup(selectedBlocks);

    // when
    act(() => {
      result.current.handleSubmit(inspectionRequestPayload);
    });

    // then
    expect(store.getActions()[0].meta.arg.text).toBe('블록을 체크하세요');
  });

  it('should call onBlastingInspectionRequestError when request failed', async () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [
      {
        id: '1',
        blockName: 'block1',
        blockDivision: BlockDivision.CENTER,
        inOut: BlockInOut.IN,
        state: BlockState.PREPARATION,
      },
    ];

    const inspectionRequestPayload: BlastingInspectionRequestPayload = {
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const {
      result,
      onBlastingInspectionRequestError,
      waitFor,
      requestBlastingInspection,
    } = setup(selectedBlocks);

    requestBlastingInspection.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleSubmit(inspectionRequestPayload);
    });

    // then
    await waitFor(() => expect(onBlastingInspectionRequestError).toBeCalled());
  });

  it('should call onBlastingInspectionRequestSuccess when request succeed', async () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [
      {
        id: '1',
        blockName: 'block1',
        blockDivision: BlockDivision.CENTER,
        inOut: BlockInOut.IN,
        state: BlockState.PREPARATION,
      },
    ];

    const inspectionRequestPayload: BlastingInspectionRequestPayload = {
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const {
      result,
      onBlastingInspectionRequestSuccess,
      waitFor,
      requestBlastingInspection,
    } = setup(selectedBlocks);

    requestBlastingInspection.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleSubmit(inspectionRequestPayload);
    });

    // then
    await waitFor(() =>
      expect(onBlastingInspectionRequestSuccess).toBeCalled(),
    );
    expect(requestBlastingInspection).toBeCalledWith({
      ...inspectionRequestPayload,
      blockIds: ['1'],
    });
  });
});
