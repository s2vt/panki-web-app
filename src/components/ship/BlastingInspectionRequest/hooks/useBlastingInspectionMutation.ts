import { useMutation, UseMutationOptions } from 'react-query';
import taskApi from '@src/api/taskApi';
import HttpError from '@src/api/model/HttpError';
import { BlastingInspectionRequestPayload } from '@src/types/ship.types';

const useBlastingInspectionMutation = (
  options?: UseMutationOptions<
    unknown,
    HttpError,
    BlastingInspectionRequestPayload
  >,
) =>
  useMutation(
    (blastingInspectionRequestPayload: BlastingInspectionRequestPayload) =>
      taskApi.requestBlastingInspection(blastingInspectionRequestPayload),
    options,
  );

export default useBlastingInspectionMutation;
