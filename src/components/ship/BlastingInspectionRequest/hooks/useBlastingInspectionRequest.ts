import HttpError from '@src/api/model/HttpError';
import { SELECT_BLOCKS_ERROR } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { Block } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { BlastingInspectionRequestPayload } from '@src/types/ship.types';
import { useCallback } from 'react';
import useBlastingInspectionMutation from './useBlastingInspectionMutation';

type UseBlastingInspectionRequestProps = {
  selectedBlocks: (SelectItem & Block)[];
  onBlastingInspectionRequestSuccess: () => void;
  onBlastingInspectionRequestError: (error: HttpError) => void;
};

const useBlastingInspectionRequest = ({
  selectedBlocks,
  onBlastingInspectionRequestSuccess,
  onBlastingInspectionRequestError,
}: UseBlastingInspectionRequestProps) => {
  const { openAlertDialog } = useAlertDialogAction();

  const { mutate, isLoading: isBlastingInspectionRequestLoading } =
    useBlastingInspectionMutation({
      onSuccess: onBlastingInspectionRequestSuccess,
      onError: onBlastingInspectionRequestError,
    });

  const handleSubmit = useCallback(
    (inspectionRequestPayload: BlastingInspectionRequestPayload) => {
      if (selectedBlocks.length === 0) {
        openAlertDialog({ text: SELECT_BLOCKS_ERROR, position: 'rightTop' });
        return;
      }

      const blockIds = selectedBlocks.map((block) => block.id);

      mutate({ ...inspectionRequestPayload, blockIds });
    },
    [selectedBlocks, openAlertDialog],
  );

  return { handleSubmit, isBlastingInspectionRequestLoading };
};

export default useBlastingInspectionRequest;
