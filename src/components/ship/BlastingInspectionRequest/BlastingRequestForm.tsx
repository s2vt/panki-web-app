import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { useMemo } from 'react';
import {
  APPLICANT,
  CALENDAR,
  INSPECTION_DUE_DATE,
  INSPECTION_PLACE,
  REQUEST,
  SHIP_NUMBER,
} from '@src/constants/constantString';
import { BlastingInspectionRequestPayload } from '@src/types/ship.types';
import { User } from '@src/types/user.types';
import DatePickerWrapper from '@src/components/common/DatePickerWrapper';
import MainButton from '@src/components/common/MainButton';
import RowFormInput from '@src/components/form/RowFormInput';
import {
  getNextWeekday,
  filterWeekend,
  formatToDateTime,
} from '@src/utils/dateUtil';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import { REQUIRED_VALIDATOR } from '@src/constants/validators';
import ShipFormBase from '../ShipFormBase';

export type BlastingRequestFormProps = {
  currentUser: User;
  onSubmit: (payload: BlastingInspectionRequestPayload) => void;
  disabledSubmit: boolean;
  shipName: string;
};

const BlastingRequestForm = ({
  currentUser,
  onSubmit,
  disabledSubmit,
  shipName,
}: BlastingRequestFormProps) => {
  const { handleSubmit, register, setValue } =
    useForm<BlastingInspectionRequestPayload>({
      defaultValues: {
        scheduledAt: '',
      },
    });

  const currentDate = useMemo(() => {
    const date = new Date();
    date.setHours(9);
    date.setMinutes(0);
    date.setSeconds(0);

    return date;
  }, []);

  const nextWeekday = useMemo(() => getNextWeekday(currentDate), []);

  const handleDateChange = (date: Date) => {
    setValue('scheduledAt', formatToDateTime(date));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <ShipFormBase.InputSection height="19rem">
        <ShipFormBase.Row>
          <RowFormInput
            label={APPLICANT}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            defaultValue={currentUser.userName}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={SHIP_NUMBER}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            defaultValue={shipName}
            labelPadding={0}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={INSPECTION_PLACE}
            enableShadow
            labelFontSize="1.6rem"
            {...register('place', REQUIRED_VALIDATOR)}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={INSPECTION_DUE_DATE}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            {...register('scheduledAt')}
          />
          <DatePickerWrapper
            onChange={handleDateChange}
            startDate={nextWeekday}
            showTimeSelect
            timeIntervals={30}
            popperPlacement="bottom-start"
            filterDate={filterWeekend}
            timeCaption="시간"
          >
            <CalendarButton label={addWhiteSpacesBetweenCharacters(CALENDAR)} />
          </DatePickerWrapper>
        </ShipFormBase.Row>
      </ShipFormBase.InputSection>
      <ShipFormBase.ButtonSection>
        <MainButton label={REQUEST} type="submit" disabled={disabledSubmit} />
      </ShipFormBase.ButtonSection>
    </form>
  );
};

const CalendarButton = styled(MainButton)`
  width: 20.7rem;
  height: 4.6rem;
  font-size: 1.6rem;
  font-weight: 400;
`;

export default BlastingRequestForm;
