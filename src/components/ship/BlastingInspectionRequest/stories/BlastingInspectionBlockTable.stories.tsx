import { Meta, Story } from '@storybook/react';
import BlastingInspectionBlockTable, {
  BlastingInspectionBlockTableProps,
} from '../BlastingInspectionBlockTable';

export default {
  title: 'Components/BlastingInspectionBlockTable',
  component: BlastingInspectionBlockTable,
} as Meta;

const Template: Story<BlastingInspectionBlockTableProps> = (args) => (
  <BlastingInspectionBlockTable {...args} />
);

export const Basic = Template.bind({});
