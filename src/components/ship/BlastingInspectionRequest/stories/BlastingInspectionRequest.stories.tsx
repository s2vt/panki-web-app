import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import BlastingInspectionRequest from '../BlastingInspectionRequest';

export default {
  title: 'Components/ship/BlastingInspectionRequest',
  component: BlastingInspectionRequest,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <BlastingInspectionRequest {...args} />;

export const Basic = Template.bind({});
