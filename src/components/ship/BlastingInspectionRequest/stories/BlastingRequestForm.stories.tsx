import { Meta, Story } from '@storybook/react';
import BlastingRequestForm, {
  BlastingRequestFormProps,
} from '../BlastingRequestForm';

export default {
  title: 'Components/ship/BlastingRequestForm',
  component: BlastingRequestForm,
} as Meta;

const Template: Story<BlastingRequestFormProps> = (args) => (
  <BlastingRequestForm {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  currentUser: {
    id: '1',
    className: 'Employee',
    email: 'test@email.com',
    engName: 'test',
    userName: '테스트',
    orgName: 'ShipYardOrg',
    userId: 'test',
    roles: [],
    company: {
      id: '1',
      orgName: 'ShipYardOrg',
      companyEmail: 'email.com',
      companyName: 'company',
    },
  },
};
