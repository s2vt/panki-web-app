import { useEffect, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import HttpError from '@src/api/model/HttpError';
import { BLASTING_INSPECTION_REQUEST } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useSelectItems from '@src/hooks/common/useSelectItems';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Block } from '@src/types/block.types';
import { Ship } from '@src/types/ship.types';
import PageTitle from '@src/components/base/PageTitle';
import { Box } from '@mui/material';
import BlastingRequestForm from './BlastingRequestForm';
import BlastingInspectionBlockTable from './BlastingInspectionBlockTable';
import useBlastingInspectionRequest from './hooks/useBlastingInspectionRequest';

const BlastingInspectionRequest = () => {
  const location = useLocation<{ ship?: Ship }>();
  const history = useHistory();

  const { openAlertDialog } = useAlertDialogAction();
  const { user } = useUserSelector();

  const {
    selectedItems: selectedBlocks,
    isSelected,
    toggleItem: handleCheckboxChange,
  } = useSelectItems<Block>();

  useEffect(() => {
    if (location.state?.ship === undefined) {
      history.push(ROUTE_PATHS.ship);
    }
  }, [history]);

  const ship = useMemo(() => location.state?.ship, [location.state?.ship]);

  const handleBlastingInspectionRequestSuccess = () => {
    history.push(ROUTE_PATHS.inspectionRequest, { ship: location.state.ship });
  };

  const handleBlastingInspectionRequestError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { handleSubmit, isBlastingInspectionRequestLoading } =
    useBlastingInspectionRequest({
      selectedBlocks,
      onBlastingInspectionRequestSuccess:
        handleBlastingInspectionRequestSuccess,
      onBlastingInspectionRequestError: handleBlastingInspectionRequestError,
    });

  return (
    <Box>
      <PageTitle title={BLASTING_INSPECTION_REQUEST} />
      {user !== undefined && ship !== undefined && (
        <>
          <BlastingRequestForm
            currentUser={user}
            onSubmit={handleSubmit}
            disabledSubmit={isBlastingInspectionRequestLoading}
            shipName={ship.shipName}
          />
          <BlastingInspectionBlockTable
            isSelected={isSelected}
            onCheckboxChange={handleCheckboxChange}
            shipId={ship.id}
          />
        </>
      )}
    </Box>
  );
};

export default BlastingInspectionRequest;
