import ErrorBox from '@src/components/common/ErrorBox';
import { FAIL_DATA_FETCH, REQUEST_BLOCK } from '@src/constants/constantString';
import useEnableBlocksQuery from '@src/hooks/ship/useEnableBlocksQuery';
import { Block, BlockState } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import BlockTable from '../BlockTable';

const SELECTABLE_BLOCK_STATES: BlockState[] = [BlockState.PREPARATION];

export type BlastingInspectionBlockTableProps = {
  shipId: string;
  isSelected: (selectItem: SelectItem & Block) => boolean;
  onCheckboxChange: (selectItem: SelectItem & Block) => void;
};

const BlastingInspectionBlockTable = ({
  shipId,
  isSelected,
  onCheckboxChange,
}: BlastingInspectionBlockTableProps) => {
  const {
    data: blocks,
    isFetching,
    isError,
    refetch,
  } = useEnableBlocksQuery(shipId);

  if (isFetching) {
    return <BlockTable.Loader />;
  }

  if (isError) {
    return (
      <BlockTable.LoadingWrapper>
        <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
      </BlockTable.LoadingWrapper>
    );
  }

  return (
    <>
      <BlockTable.TitleSection>{REQUEST_BLOCK}</BlockTable.TitleSection>;
      {blocks !== undefined && (
        <BlockTable
          blocks={blocks}
          mode="selection"
          isSelected={isSelected}
          onCheckboxChange={onCheckboxChange}
          selectionType="select"
          selectableStates={SELECTABLE_BLOCK_STATES}
          enableMultipleSelection
        />
      )}
    </>
  );
};

export default BlastingInspectionBlockTable;
