import { Grid } from '@mui/material';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import Pagination from '@src/components/common/Pagination';
import TableBase from '@src/components/table/TableBase';
import { FAIL_DATA_FETCH } from '@src/constants/constantString';
import usePagination from '@src/hooks/common/usePagination';
import useRoutePagination from '@src/hooks/common/useRoutePagination';
import useCurrentUserShipsQuery from '@src/hooks/ship/useCurrentUserShipsQuery';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { SelectItem } from '@src/types/common.types';
import { Ship, ShipFilter } from '@src/types/ship.types';
import { useParams } from 'react-router-dom';
import ShipTableBody from '../ShipTableBody';
import ShipTableHeader from '../ShipTableHeader';

export type ShipManagementTableProps = {
  shipFilter: ShipFilter | undefined;
  onHeaderCheckboxClick: () => void;
  onShipClick: (ship: Ship) => void;
  onCheckboxChange: (selectItem: SelectItem & Ship) => void;
  isShipSelected: (selectItem: SelectItem & Ship) => boolean;
  selectedShips: (SelectItem & Ship)[];
};

const ShipManagementTable = ({
  shipFilter,
  onHeaderCheckboxClick,
  onShipClick,
  onCheckboxChange,
  isShipSelected,
  selectedShips,
}: ShipManagementTableProps) => {
  const { page } = useParams<{ page?: string }>();

  const {
    data: ships,
    isFetching,
    isError,
    refetch,
  } = useCurrentUserShipsQuery({
    shipFilter,
    enabled: false,
  });

  const {
    setCurrentPage,
    lastPage,
    currentPageItems: currentPageShips,
  } = usePagination<Ship>(ships ?? [], 10);

  const { currentPage, handlePageClick } = useRoutePagination({
    pageParam: page,
    basePath: ROUTE_PATHS.shipManagement,
    lastPage,
    setCurrentPage,
  });

  if (isFetching) {
    return (
      <TableBase.LoadingWrapper>
        <LoadingSpinner />
      </TableBase.LoadingWrapper>
    );
  }

  if (isError) {
    return (
      <TableBase.LoadingWrapper>
        <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
      </TableBase.LoadingWrapper>
    );
  }

  return (
    <>
      <Grid item xs={6}>
        <TableBase.TableWrapper>
          <TableBase.Table>
            <ShipTableHeader
              onCheckboxClick={onHeaderCheckboxClick}
              checked={selectedShips.length === ships?.length}
            />
            {currentPageShips && (
              <ShipTableBody
                ships={currentPageShips}
                onClick={onShipClick}
                onCheckboxChange={onCheckboxChange}
                isSelected={isShipSelected}
              />
            )}
          </TableBase.Table>
        </TableBase.TableWrapper>
      </Grid>
      <TableBase.PaginationSection>
        <Pagination
          currentPage={currentPage}
          lastPage={lastPage}
          onPageClick={handlePageClick}
        />
      </TableBase.PaginationSection>
    </>
  );
};

export default ShipManagementTable;
