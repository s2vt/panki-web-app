import { Meta, Story } from '@storybook/react';
import ShipManagement from '../ShipManagement';

export default {
  title: 'Components/ship/ShipManagement',
  component: ShipManagement,
} as Meta;

const Template: Story = (args) => <ShipManagement {...args} />;

export const Basic = Template.bind({});
