import { Meta, Story } from '@storybook/react';
import ShipManagementTableAction, {
  ShipManagementTableActionProps,
} from '../ShipManagementTableAction';

export default {
  title: 'Components/Ship/ShipManagement/ShipManagementTableAction',
  component: ShipManagementTableAction,
} as Meta;

const Template: Story<ShipManagementTableActionProps> = (args) => (
  <ShipManagementTableAction {...args} />
);

export const Basic = Template.bind({});
