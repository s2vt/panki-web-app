import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { Ship } from '@src/types/ship.types';
import { useCallback, useState } from 'react';
import { useMutation } from 'react-query';

type UseDeleteShipsProps = {
  selectedShips: Ship[] | undefined;
  onDeleteShipsSuccess: () => void;
  onDeleteShipsError: (error: HttpError) => void;
};

const useDeleteShips = ({
  selectedShips,
  onDeleteShipsSuccess,
  onDeleteShipsError,
}: UseDeleteShipsProps) => {
  const [isDeleteShipModalOpen, setIsDeleteShipModalOpen] = useState(false);
  const { openAlertDialog } = useAlertDialogAction();

  const { mutate: deleteShipsMutate } = useMutation(
    (shipIds: string[]) => shipApi.deleteShips(shipIds),
    {
      onSuccess: onDeleteShipsSuccess,
      onError: onDeleteShipsError,
    },
  );

  const closeDeleteShipsModal = useCallback(() => {
    setIsDeleteShipModalOpen(false);
  }, [setIsDeleteShipModalOpen]);

  const handleDeleteShipClick = useCallback(() => {
    if (selectedShips === undefined) {
      return;
    }

    if (selectedShips.length === 0) {
      openAlertDialog({
        text: '삭제할 호선을 선택하세요',
        position: 'rightTop',
      });
      return;
    }

    setIsDeleteShipModalOpen(true);
  }, [selectedShips, openAlertDialog, setIsDeleteShipModalOpen]);

  const handleDeleteShipSubmit = useCallback(() => {
    if (selectedShips === undefined) {
      return;
    }

    deleteShipsMutate(selectedShips.map((selectedShip) => selectedShip.id));
  }, [deleteShipsMutate, selectedShips]);

  return {
    isDeleteShipModalOpen,
    closeDeleteShipsModal,
    handleDeleteShipClick,
    handleDeleteShipSubmit,
  };
};

export default useDeleteShips;
