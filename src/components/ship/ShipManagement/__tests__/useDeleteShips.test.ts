import shipApi from '@src/api/shipApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { Ship } from '@src/types/ship.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import ship from '@src/test/fixtures/ship/ship';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import HttpError from '@src/api/model/HttpError';
import useDeleteShips from '../hooks/useDeleteShips';

describe('useDeleteShips hook', () => {
  const setup = (selectedShips: Ship[] | undefined) => {
    const { wrapper, store } = prepareMockWrapper();

    const onDeleteShipsError = jest.fn();
    const onDeleteShipsSuccess = jest.fn();
    const deleteShips = jest.spyOn(shipApi, 'deleteShips');

    const { result, waitFor } = renderHook(
      () =>
        useDeleteShips({
          onDeleteShipsError,
          onDeleteShipsSuccess,
          selectedShips,
        }),
      {
        wrapper,
      },
    );

    return {
      store,
      result,
      waitFor,
      onDeleteShipsError,
      onDeleteShipsSuccess,
      deleteShips,
    };
  };

  it('should call open alert dialog action when has not ship selected', () => {
    // given
    const selectedShips: Ship[] = [];

    const { result, store } = setup(selectedShips);

    // when
    act(() => {
      result.current.handleDeleteShipClick();
    });

    // then
    expect(store.getActions()[0].meta.arg.text).toEqual(
      '삭제할 호선을 선택하세요',
    );
    expect(result.current.isDeleteShipModalOpen).toEqual(false);
  });

  it('should set isDeleteShipModalOpen to true when ship selected', () => {
    // given
    const selectedShips: Ship[] = [ship];

    const { result } = setup(selectedShips);

    // when
    act(() => {
      result.current.handleDeleteShipClick();
    });

    // then
    expect(result.current.isDeleteShipModalOpen).toEqual(true);
  });

  it('should call onDeleteShipsError when api request failed', async () => {
    // given
    const selectedShips: Ship[] = [ship];

    const { result, waitFor, onDeleteShipsError, deleteShips } =
      setup(selectedShips);

    deleteShips.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleDeleteShipSubmit();
    });

    // then
    await waitFor(() => expect(onDeleteShipsError).toBeCalled());
  });

  it('should call onDeleteShipsSuccess when api request succeed', async () => {
    // given
    const selectedShips: Ship[] = [ship];

    const { result, waitFor, onDeleteShipsSuccess, deleteShips } =
      setup(selectedShips);

    deleteShips.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleDeleteShipSubmit();
    });

    // then
    await waitFor(() => expect(onDeleteShipsSuccess).toBeCalled());
  });
});
