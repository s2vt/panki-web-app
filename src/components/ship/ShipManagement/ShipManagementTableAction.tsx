import { memo, useCallback, useRef } from 'react';
import { useHistory } from 'react-router';
import {
  CREATE,
  DELETE,
  FILTERING,
  REFRESH,
  SHIP_NUMBER_SEARCH_PLACEHOLDER,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import DropdownMultiSelectWrapper from '@src/components/common/DropdownMultiSelectWrapper';
import SearchInput from '@src/components/common/SearchInput';
import RoundButton from '@src/components/common/RoundButton';
import TableActionWrapper from '@src/components/table/TableActionWrapper';
import useMask from '@src/hooks/common/useMask';
import { DropdownItem, FilterDropdownItem } from '@src/types/common.types';
import { Ship, ShipState } from '@src/types/ship.types';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import ConfirmModal from '@src/components/common/ConfirmModal';
import HttpError from '@src/api/model/HttpError';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import ModalWrapper from '@src/components/base/ModalWrapper';
import useDeleteShips from './hooks/useDeleteShips';

export type ShipManagementTableActionProps = {
  onSearchEnter: (value: string) => void;
  onFilterChange: (state?: ShipState) => void;
  selectedShips: Ship[];
  onRefetchShips: () => void;
};

const FILTER_DROPDOWN_ITEMS: FilterDropdownItem<Ship>[] = [
  { id: '1', label: '진행', value: ShipState.WORKING },
  {
    id: '2',
    label: '검사 신청 필요',
    value: ShipState.NEED_REQUEST_INSPECTION,
  },
  { id: '3', label: '완료', value: ShipState.FINISHED },
];

const ShipManagementTableAction = ({
  onSearchEnter,
  onFilterChange,
  selectedShips,
  onRefetchShips,
}: ShipManagementTableActionProps) => {
  const { selectedItems, setSelectedItems, toggleItem, isSelected } =
    useSelectItems<DropdownItem>();
  const searchInputRef = useRef<HTMLInputElement>(null);

  const history = useHistory();
  const { openAlertDialog } = useAlertDialogAction();
  const { maskEnglishNumber } = useMask();

  const handleDropdownChange = useCallback(
    (selectItem: FilterDropdownItem<Ship>) => {
      setSelectedItems([]);
      toggleItem(selectItem);

      if (selectedItems.some((item) => item.id === selectItem.id)) {
        onFilterChange(undefined);
      } else {
        onFilterChange(selectItem.value as ShipState);
      }
    },
    [selectedItems, setSelectedItems, toggleItem],
  );

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const maskedValue = maskEnglishNumber(e.target.value);
      e.target.value = maskedValue;
    },
    [maskEnglishNumber],
  );

  const handleRefreshClick = useCallback(() => {
    if (searchInputRef.current !== null) {
      searchInputRef.current.value = '';
      onSearchEnter('');
    }

    onRefetchShips();
  }, [onRefetchShips, searchInputRef, onSearchEnter]);

  const handleCreateShipClick = useCallback(() => {
    history.push(ROUTE_PATHS.createShip);
  }, []);

  const handleDeleteShipsSuccess = () => {
    closeDeleteShipsModal();
    onRefetchShips();
  };

  const handleDeleteShipsError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const {
    closeDeleteShipsModal,
    handleDeleteShipClick,
    handleDeleteShipSubmit,
    isDeleteShipModalOpen,
  } = useDeleteShips({
    selectedShips,
    onDeleteShipsError: handleDeleteShipsError,
    onDeleteShipsSuccess: handleDeleteShipsSuccess,
  });

  return (
    <>
      <TableActionWrapper>
        <RoundButton label={CREATE} onClick={handleCreateShipClick} />
        <RoundButton label={DELETE} onClick={handleDeleteShipClick} />
        <RoundButton label={REFRESH} onClick={handleRefreshClick} />
        <DropdownMultiSelectWrapper
          dropdownItems={FILTER_DROPDOWN_ITEMS}
          isSelected={isSelected}
          onChange={handleDropdownChange}
          checkboxSize="xs"
        >
          <RoundButton label={FILTERING} />
        </DropdownMultiSelectWrapper>
        <SearchInput
          placeholder={SHIP_NUMBER_SEARCH_PLACEHOLDER}
          onSearchEnter={onSearchEnter}
          onChange={handleInputChange}
          ref={searchInputRef}
        />
      </TableActionWrapper>
      <ModalWrapper isOpen={isDeleteShipModalOpen}>
        <ConfirmModal
          text="정말 해당 호선을 삭제하시겠습니까?"
          onConfirm={handleDeleteShipSubmit}
          onClose={closeDeleteShipsModal}
        />
      </ModalWrapper>
    </>
  );
};

export default memo(ShipManagementTableAction);
