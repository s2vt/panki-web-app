import { useCallback, useState } from 'react';
import { useHistory } from 'react-router-dom';
import useSelectItems from '@src/hooks/common/useSelectItems';
import useCurrentUserShipsQuery from '@src/hooks/ship/useCurrentUserShipsQuery';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Ship, ShipFilter, ShipState } from '@src/types/ship.types';
import PageTitle from '@src/components/base/PageTitle';
import TableBase from '@src/components/table/TableBase';
import { SHIP_LIST } from '@src/constants/constantString';
import SideLayout from '@src/components/base/SideLayout';
import InspectionScheduleBox from '../InspectionScheduleBox';
import ShipManagementTableAction from './ShipManagementTableAction';
import ShipManagementTable from './ShipManagementTable';

const ShipManagement = () => {
  const history = useHistory();

  const [shipFilter, setShipFilter] = useState<ShipFilter>();

  const { data: ships, refetch } = useCurrentUserShipsQuery({ shipFilter });

  const {
    selectedItems: selectedShips,
    setSelectedItems: setSelectedShips,
    toggleItem: handleCheckboxChange,
    isSelected: isShipSelected,
  } = useSelectItems<Ship>();

  const handleRefetchShips = useCallback(() => {
    setSelectedShips([]);
    history.replace(ROUTE_PATHS.shipManagement);
    refetch();
  }, [setSelectedShips, history, refetch]);

  const handleShipClick = useCallback(
    (ship: Ship) => {
      history.push(ROUTE_PATHS.shipStatus, { ship });
    },
    [history],
  );

  const handleSearchEnter = useCallback(
    (value: string) => {
      setShipFilter((prevFilter) => ({ ...prevFilter, shipName: value }));
    },
    [setShipFilter],
  );

  const handleFilterChange = useCallback(
    (state?: ShipState) => {
      setShipFilter((prevFilter) => ({ ...prevFilter, state }));
    },
    [setShipFilter],
  );

  const handleHeaderCheckboxClick = useCallback(() => {
    if (selectedShips.length === ships?.length) {
      setSelectedShips([]);
      return;
    }

    if (ships !== undefined) {
      setSelectedShips(ships);
    }
  }, [ships, selectedShips, setSelectedShips]);

  return (
    <>
      <PageTitle title={SHIP_LIST} />
      <SideLayout.Container>
        <SideLayout.Side>
          <InspectionScheduleBox />
        </SideLayout.Side>
        <SideLayout.Main>
          <TableBase.TableSection>
            <ShipManagementTableAction
              onSearchEnter={handleSearchEnter}
              onFilterChange={handleFilterChange}
              selectedShips={selectedShips}
              onRefetchShips={handleRefetchShips}
            />
            <ShipManagementTable
              shipFilter={shipFilter}
              onHeaderCheckboxClick={handleHeaderCheckboxClick}
              onShipClick={handleShipClick}
              onCheckboxChange={handleCheckboxChange}
              isShipSelected={isShipSelected}
              selectedShips={selectedShips}
            />
          </TableBase.TableSection>
        </SideLayout.Main>
      </SideLayout.Container>
    </>
  );
};

export default ShipManagement;
