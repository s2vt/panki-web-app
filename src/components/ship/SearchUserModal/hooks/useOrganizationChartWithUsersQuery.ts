import { useQuery, UseQueryOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';
import { CompanyWithUsers } from '@src/types/user.types';

const useOrganizationChartWithUsersQuery = (
  options?: UseQueryOptions<CompanyWithUsers, HttpError>,
) => useQuery(createKey(), companyApi.getOrganizationChartWithUsers, options);

const baseKey = ['organizationChartWithUsers'];
const createKey = () => [...baseKey];

useOrganizationChartWithUsersQuery.baseKey = baseKey;
useOrganizationChartWithUsersQuery.createKey = createKey;

export default useOrganizationChartWithUsersQuery;
