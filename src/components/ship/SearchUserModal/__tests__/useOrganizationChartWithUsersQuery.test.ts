import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import companyApi from '@src/api/companyApi';
import { CompanyWithUsers } from '@src/types/user.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useOrganizationChartWithUsersQuery from '../hooks/useOrganizationChartWithUsersQuery';

describe('useOrganizationChartWithUsersQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useOrganizationChartWithUsersQuery(), {
      wrapper,
    });
  };

  it('should cache data when succeed fetch company tree with users', async () => {
    // given
    const sampleResponse: CompanyWithUsers = {
      id: faker.datatype.number().toString(),
      orgName: faker.company.companyName(),
      companyEmail: faker.internet.email(),
      companyName: faker.company.companyName(),
      children: [],
      users: [],
      adminCount: 0,
    };

    companyApi.getOrganizationChartWithUsers = jest
      .fn()
      .mockResolvedValue(sampleResponse);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleResponse);
  });

  it('should set error when field fetch company tree with users', async () => {
    // given
    companyApi.getOrganizationChartWithUsers = jest
      .fn()
      .mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
