import { Meta, Story } from '@storybook/react';
import SearchUserModal, { SearchUserModalProps } from './SearchUserModal';

export default {
  title: 'Components/ship/SearchUserModal',
  component: SearchUserModal,
} as Meta;

const Template: Story<SearchUserModalProps> = (args) => (
  <SearchUserModal {...args} />
);

export const Basic = Template.bind({});
