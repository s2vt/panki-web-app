import { useState } from 'react';
import styled from 'styled-components';
import {
  FAIL_DATA_FETCH,
  ORGANIZATION_CHART,
  SELECT,
} from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { CompanyUser } from '@src/types/user.types';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import ModalTitle from '@src/components/base/ModalTitle';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import MainButton from '@src/components/common/MainButton';
import OrganizationUserChart from '@src/components/user/OrganizationUserChart';
import useOrganizationChartWithUsersQuery from './hooks/useOrganizationChartWithUsersQuery';

export type SearchUserModalProps = {
  onClose: () => void;
  onSubmit: (companyUser: CompanyUser) => void;
};

const SearchUserModal = ({ onClose, onSubmit }: SearchUserModalProps) => {
  const {
    data: company,
    isFetching: isOrganizationChartFetching,
    isError: isOrganizationChartQueryError,
    isSuccess: isOrganizationChartQuerySuccess,
    refetch: refetchOrganizationChart,
  } = useOrganizationChartWithUsersQuery();
  const [selectedUser, setSelectedUser] = useState<CompanyUser>();
  const { openAlertDialog } = useAlertDialogAction();

  const handleUserClick = (companyUser: CompanyUser) => {
    setSelectedUser(companyUser);
  };

  const handleSubmit = () => {
    if (selectedUser === undefined) {
      openAlertDialog({ text: '유저를 선택하세요', position: 'rightTop' });
      return;
    }

    onSubmit(selectedUser);
    onClose();
  };

  return (
    <>
      <ModalTitle title={ORGANIZATION_CHART} onClose={onClose} />
      <Box>
        {isOrganizationChartFetching && <LoadingSpinner />}
        {!isOrganizationChartFetching && isOrganizationChartQueryError && (
          <ErrorBox
            error={FAIL_DATA_FETCH}
            onRefetchClick={refetchOrganizationChart}
          />
        )}
        {company !== undefined &&
          !isOrganizationChartFetching &&
          isOrganizationChartQuerySuccess && (
            <ChartSection>
              <ChartWrapper>
                <OrganizationUserChart
                  company={company}
                  level={0}
                  selectedUser={selectedUser}
                  onUserClick={handleUserClick}
                  onUserDoubleClick={handleSubmit}
                />
              </ChartWrapper>
              <SelectButton
                label={addWhiteSpacesBetweenCharacters(SELECT)}
                onClick={handleSubmit}
              />
            </ChartSection>
          )}
      </Box>
    </>
  );
};

const Box = styled.div`
  width: 38.7rem;
  height: 41rem;
  max-height: 41rem;
  overflow-y: auto;
  position: relative;
`;

const ChartSection = styled.div`
  min-height: 100%;
  padding: 3.4rem 2.8rem 2.2rem;
  display: flex;
  flex-direction: column;
`;

const ChartWrapper = styled.div`
  margin-bottom: 2.4rem;
`;

const SelectButton = styled(MainButton)`
  width: 100%;
  height: 5.6rem;
  margin-top: auto;
  font-size: 2rem;
`;

export default SearchUserModal;
