import ErrorBox from '@src/components/common/ErrorBox';
import { FAIL_DATA_FETCH } from '@src/constants/constantString';
import useEnableBlocksQuery from '@src/hooks/ship/useEnableBlocksQuery';
import BlockStatusBox from '../BlockStatusBox';
import BlockTable from '../BlockTable';

export type BlockStateTableProps = {
  shipId: string;
};

const BlockStateTable = ({ shipId }: BlockStateTableProps) => {
  const {
    data: blocks,
    isFetching,
    isError,
    refetch,
  } = useEnableBlocksQuery(shipId);

  if (isFetching) {
    return <BlockTable.Loader />;
  }

  if (isError) {
    <BlockTable.LoadingWrapper>
      <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
    </BlockTable.LoadingWrapper>;
  }

  return (
    <>
      <BlockStatusBox />
      {blocks !== undefined && <BlockTable blocks={blocks} mode="view" />}
    </>
  );
};

export default BlockStateTable;
