import { Meta, Story } from '@storybook/react';
import BlockStateTable, { BlockStateTableProps } from './BlockStateTable';

export default {
  title: 'Components/BlockStateTable',
  component: BlockStateTable,
} as Meta;

const Template: Story<BlockStateTableProps> = (args) => (
  <BlockStateTable {...args} />
);

export const Basic = Template.bind({});
