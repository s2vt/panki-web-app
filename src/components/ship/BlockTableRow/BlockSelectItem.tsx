import { memo } from 'react';
import {
  Block,
  BlockInOut,
  BlockState,
  DivisionBlock,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import Checkbox from '@src/components/common/Checkbox';
import {
  BlockItemWrapper,
  BlockNameSection,
  HorizontalSplitSection,
  InOutSection,
  VerticalSplitSection,
} from './styles';
import { BlockTableSelectionType } from '../BlockTable';

export type BlockSelectItemProps = {
  divisionBlock: DivisionBlock;
  onCheckboxChange: (selectItem: SelectItem & Block) => void;
  isSelected: (selectItem: SelectItem & Block) => boolean;
  selectionType: BlockTableSelectionType;
  enableMultipleSelection: boolean;
  selectableStates?: BlockState[];
  selectablePosition?: BlockInOut[];
};

const IN_BLOCK_SELECTABLE_STATES: BlockState[] = [
  BlockState.CREATE,
  BlockState.PREPARATION,
  BlockState.BLASTING_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_REJECTED,
  BlockState.CTG_2ND_INSPECTION_REJECTED,
];

const OUT_BLOCK_SELECTABLE_STATES: BlockState[] = [
  BlockState.CREATE,
  BlockState.PREPARATION,
  BlockState.BLASTING_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_REJECTED,
  BlockState.CTG_2ND_INSPECTION_FINISHED,
  BlockState.CTG_2ND_INSPECTION_REJECTED,
  BlockState.CTG_3RD_INSPECTION_FINISHED,
  BlockState.CTG_3RD_INSPECTION_REJECTED,
  BlockState.CTG_4TH_INSPECTION_FINISHED,
  BlockState.CTG_4TH_INSPECTION_REJECTED,
  BlockState.CTG_5TH_INSPECTION_REJECTED,
];

const BlockSelectItem = ({
  divisionBlock,
  onCheckboxChange,
  isSelected,
  selectionType,
  selectableStates,
  selectablePosition,
  enableMultipleSelection,
}: BlockSelectItemProps) => {
  const { blockName, blocks } = divisionBlock;

  const isSelectable = (block: Block) => {
    const isInOutIncludesState =
      block.inOut === BlockInOut.IN
        ? IN_BLOCK_SELECTABLE_STATES.includes(block.state)
        : OUT_BLOCK_SELECTABLE_STATES.includes(block.state);

    return selectableStates?.includes(block.state) && isInOutIncludesState;
  };

  const isDisabled = (block: Block) =>
    !selectablePosition?.includes(block.inOut);

  const handleSimultaneousCheckboxChange = () => {
    const selectableBlocks: Block[] = [];

    if (selectableStates === undefined) {
      selectableBlocks.push(...blocks);
    } else {
      selectableBlocks.push(
        ...blocks.filter((block) => selectableStates.includes(block.state)),
      );
    }

    selectableBlocks.forEach((block) => onCheckboxChange(block));
  };

  return (
    <BlockItemWrapper>
      <BlockNameSection>{blockName}</BlockNameSection>
      <InOutSection>
        {blocks.map((block, index, blocks) => {
          const { id, inOut } = block;

          return (
            <HorizontalSplitSection
              key={id + inOut}
              enableBorder={index !== blocks.length - 1}
            >
              <VerticalSplitSection>{block.inOut}</VerticalSplitSection>
              <VerticalSplitSection>
                {isSelectable(block) && (
                  <Checkbox
                    onChange={
                      enableMultipleSelection
                        ? handleSimultaneousCheckboxChange
                        : () => onCheckboxChange(block)
                    }
                    name={id}
                    disabled={isDisabled(block)}
                    checked={
                      selectionType === 'select'
                        ? isSelected(block)
                        : !isSelected(block)
                    }
                    checkboxSize="xs"
                    checkboxIcon={
                      isDisabled(block) ? 'unselectedCheckbox' : 'blankCheckbox'
                    }
                  />
                )}
              </VerticalSplitSection>
            </HorizontalSplitSection>
          );
        })}
      </InOutSection>
    </BlockItemWrapper>
  );
};

export default memo(BlockSelectItem);
