import styled, { css } from 'styled-components';

export const Box = styled.div<{ divisionAmount: number }>`
  display: flex;
  height: ${({ divisionAmount }) => `${divisionAmount * 7.32}rem`};
  border-bottom: ${({ theme }) => theme.borders.blockTableBorder};

  :last-child {
    border-bottom: none;
  }
`;

export const BlockColumnSection = styled.div`
  min-width: 5.95rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  border-right: ${({ theme }) => theme.borders.blockTableBorder};
`;

export const Row = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const RowItem = styled.div`
  flex: 1;
  display: flex;
`;

export const DivisionSection = styled.div`
  width: 10.1rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  border-right: ${({ theme }) => theme.borders.blockTableBorder};
`;

export const BlockItemWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  ${({ theme }) => theme.layout.flexCenterLayout}
  border-right: ${({ theme }) => theme.borders.blockTableBorder};

  :last-child {
    border-right: none;
  }
`;

export const VerticalSplitSection = styled.div`
  flex: 1;
  width: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
  border-top: ${({ theme }) => theme.borders.blockTableBorder};
`;

export const HorizontalSplitSection = styled.div<{ enableBorder: boolean }>`
  flex: 1;
  width: 100%;
  display: flex;
  flex-direction: column;
  ${({ theme }) => theme.layout.flexCenterLayout}
  ${({ enableBorder, theme }) =>
    enableBorder &&
    css`
      border-right: ${theme.borders.blockTableBorder};
    `};
`;

export const InOutSection = styled.div`
  flex: 2;
  width: 100%;
  display: flex;
`;

export const BlockNameSection = styled.div`
  flex: 1;
  width: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
  background:${({ theme }) => theme.colors.tableBackground};
`;
