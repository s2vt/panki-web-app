/* eslint-disable react/no-array-index-key */
import React, { memo, useMemo } from 'react';
import {
  Block,
  BlockInOut,
  BlockState,
  BlockTableItem,
  DivisionBlock,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { debugAssert } from '@src/utils/commonUtil';
import { BlockTableMode, BlockTableSelectionType } from '../BlockTable';
import BlockSelectItem from './BlockSelectItem';
import BlockItemSkeleton from './BlockItemSkeleton';
import { BlockColumnSection, Box, Row, RowItem } from './styles';
import BlockStateItem from './BlockStateItem';

export type BlockTableRowProps = {
  blockTableItem: BlockTableItem;
  /**
   * selection 모드의 경우 isSelected, onCheckboxChange, selectionType props 가 필수
   */
  mode: BlockTableMode;
  maximumBlockCount: number;
  isSelected?: (selectItem: SelectItem & Block) => boolean;
  enableMultipleSelection: boolean;
  onCheckboxChange?: (selectItem: SelectItem & Block) => void;
  selectionType?: BlockTableSelectionType;
  className?: string;
  selectableStates?: BlockState[];
  selectablePosition?: BlockInOut[];
};

const MINIMUM_BLOCKS_COUNT = 11;

const BlockTableRow = ({
  blockTableItem,
  mode,
  maximumBlockCount,
  className,
  isSelected,
  enableMultipleSelection,
  onCheckboxChange,
  selectionType,
  selectableStates,
  selectablePosition = [BlockInOut.IN, BlockInOut.OUT],
}: BlockTableRowProps) => {
  const { blockPrefix, divisionGroupedBlocks } = blockTableItem;

  const blockCount = useMemo(
    () => Math.max(maximumBlockCount, MINIMUM_BLOCKS_COUNT),
    [maximumBlockCount],
  );

  const isValidSelectionMode =
    mode === 'selection' &&
    onCheckboxChange !== undefined &&
    isSelected !== undefined &&
    selectionType !== undefined;

  const renderBlockItem = (divisionBlock: DivisionBlock | undefined) => {
    if (divisionBlock === undefined) {
      return <BlockItemSkeleton />;
    }

    if (mode === 'view') {
      return <BlockStateItem divisionBlock={divisionBlock} />;
    }

    if (isValidSelectionMode) {
      return (
        <BlockSelectItem
          divisionBlock={divisionBlock}
          isSelected={isSelected}
          onCheckboxChange={onCheckboxChange}
          selectionType={selectionType}
          selectableStates={selectableStates}
          enableMultipleSelection={enableMultipleSelection}
          selectablePosition={selectablePosition}
        />
      );
    }

    debugAssert(
      false,
      'selectionType, onCheckboxChange and isSelected props should exists when selection mode',
    );

    return null;
  };

  return (
    <Box className={className} divisionAmount={divisionGroupedBlocks.length}>
      <BlockColumnSection>{blockPrefix}</BlockColumnSection>
      <Row>
        {divisionGroupedBlocks.map((divisionGroupedBlock) => {
          const { division, divisionBlocks } = divisionGroupedBlock;

          return (
            <RowItem key={division}>
              {Array.from({ length: blockCount }).map((_, index) => (
                <React.Fragment key={index}>
                  {renderBlockItem(divisionBlocks[index])}
                </React.Fragment>
              ))}
            </RowItem>
          );
        })}
      </Row>
    </Box>
  );
};

export default memo(BlockTableRow);
