/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import {
  BlockItemWrapper,
  BlockNameSection,
  HorizontalSplitSection,
  InOutSection,
  VerticalSplitSection,
} from './styles';

export type BlockItemSkeletonProps = {};

const BlockItemSkeleton = () => (
  <BlockItemWrapper>
    <BlockNameSection />
    <InOutSection>
      {Array.from({ length: 2 }).map((_, index, array) => (
        <HorizontalSplitSection
          key={index}
          enableBorder={index !== array.length - 1}
        >
          <VerticalSplitSection />
          <VerticalSplitSection />
        </HorizontalSplitSection>
      ))}
    </InOutSection>
  </BlockItemWrapper>
);

export default memo(BlockItemSkeleton);
