import { Meta, Story } from '@storybook/react';
import BlockItemSkeleton, {
  BlockItemSkeletonProps,
} from '../BlockItemSkeleton';

export default {
  title: 'Components/ship/BlockTableRow/BlockItemSkeleton',
  component: BlockItemSkeleton,
} as Meta;

const Template: Story<BlockItemSkeletonProps> = (args) => (
  <div style={{ width: '100px', height: '100px', display: 'flex' }}>
    <BlockItemSkeleton {...args} />
  </div>
);

export const Basic = Template.bind({});
