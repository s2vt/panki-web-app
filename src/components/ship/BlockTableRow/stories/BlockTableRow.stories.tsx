import { Meta, Story } from '@storybook/react';
import { Block, BlockState, BlockTableItem } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import BlockTableRow, { BlockTableRowProps } from '../BlockTableRow';

export default {
  title: 'Components/ship/BlockTableRow',
  component: BlockTableRow,
} as Meta;

const Template: Story<BlockTableRowProps> = (args) => (
  <div style={{ height: '100px', display: 'flex' }}>
    <BlockTableRow {...args} />
  </div>
);

const blockTableItem: BlockTableItem = {
  blockPrefix: 'B',
  divisionGroupedBlocks: [
    {
      division: 'PORT',
      divisionBlocks: [
        {
          blockName: '1B11',
          blocks: [
            {
              id: '1',
              blockName: '1B11',
              inOut: 'IN',
              blockDivision: 'PORT',
              state: BlockState.CREATE,
            },
            {
              id: '2',
              blockName: '1B11',
              inOut: 'OUT',
              blockDivision: 'PORT',
              state: BlockState.CREATE,
            },
          ],
        },
      ],
    },
  ],
};

export const SelectionMode = Template.bind({});
SelectionMode.args = {
  blockTableItem,
  mode: 'selection',
  isSelected: (_: SelectItem & Block) => true,
  selectionType: 'select',
  onCheckboxChange: () => console.log(),
};

export const ViewMode = Template.bind({});
ViewMode.args = {
  blockTableItem,
  mode: 'view',
};
