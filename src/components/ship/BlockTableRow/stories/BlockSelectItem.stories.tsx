import { Meta, Story } from '@storybook/react';
import { BlockState } from '@src/types/block.types';
import BlockSelectItem, { BlockSelectItemProps } from '../BlockSelectItem';

export default {
  title: 'Components/ship/BlockTableRow/BlockSelectItem',
  component: BlockSelectItem,
} as Meta;

const Template: Story<BlockSelectItemProps> = (args) => (
  <div style={{ width: '100px', height: '100px', display: 'flex' }}>
    <BlockSelectItem {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  divisionBlock: {
    blockName: 'B10C',
    blocks: [
      {
        id: '1',
        blockDivision: 'PORT',
        blockName: 'B10C',
        inOut: 'IN',
        state: BlockState.CREATE,
      },
      {
        id: '2',
        blockDivision: 'PORT',
        blockName: 'B10C',
        inOut: 'OUT',
        state: BlockState.CREATE,
      },
    ],
  },
  isSelected: () => true,
};
