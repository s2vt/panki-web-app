import { Meta, Story } from '@storybook/react';
import { BlockState } from '@src/types/block.types';
import BlockStateItem, { BlockStateItemProps } from '../BlockStateItem';

export default {
  title: 'Components/ship/BlockTableRow/BlockStateItem',
  component: BlockStateItem,
} as Meta;

const Template: Story<BlockStateItemProps> = (args) => (
  <BlockStateItem {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  divisionBlock: {
    blockName: 'B10C',
    blocks: [
      {
        id: '1',
        blockDivision: 'PORT',
        blockName: 'B10C',
        inOut: 'IN',
        state: BlockState.CREATE,
      },
      {
        id: '2',
        blockDivision: 'PORT',
        blockName: 'B10C',
        inOut: 'OUT',
        state: BlockState.CREATE,
      },
    ],
  },
};
