import { memo, useCallback } from 'react';
import styled, { DefaultTheme } from 'styled-components';
import { FINISHED, PROGRESS, REJECTED } from '@src/constants/constantString';
import { BlockState, DivisionBlock } from '@src/types/block.types';
import {
  BlockNameSection,
  HorizontalSplitSection,
  VerticalSplitSection,
  InOutSection,
  BlockItemWrapper,
} from './styles';

export type BlockStateItemProps = {
  divisionBlock: DivisionBlock;
};

const BLASTING_TASK = 'BLASTING';
const COATING_TASK = 'CTG';
const REQUEST_STATE = 'REQUEST';
const REJECTED_STATE = 'REJECTED';
const FINISHED_STATE = 'FINISH';
const STEP_NUMBER_REG_EXP = new RegExp(/(?=CTG_)*[\d]/g);

const BlockStateItem = ({ divisionBlock }: BlockStateItemProps) => {
  const { blockName, blocks } = divisionBlock;

  const getTextByState = useCallback((state: BlockState | undefined) => {
    if (state?.includes(REQUEST_STATE)) {
      return PROGRESS;
    }

    if (state?.includes(FINISHED_STATE)) {
      return FINISHED;
    }

    if (state?.includes(REJECTED_STATE)) {
      return REJECTED;
    }

    return '';
  }, []);

  const getStepNumber = useCallback((state: BlockState | undefined) => {
    if (!state?.includes(COATING_TASK)) {
      return null;
    }

    const stepNumberMatch = state.match(STEP_NUMBER_REG_EXP);

    if (stepNumberMatch === null) {
      return null;
    }

    const stepNumber = stepNumberMatch[0];

    return stepNumber;
  }, []);

  return (
    <BlockItemWrapper>
      <BlockNameSection>{blockName}</BlockNameSection>
      <InOutSection>
        {blocks.map((block, index, blocks) => {
          const { id, inOut, state } = block;
          const stepNumber = getStepNumber(state);

          return (
            <HorizontalSplitSection
              key={id + inOut}
              enableBorder={index !== blocks.length - 1}
            >
              <VerticalSplitSection>{inOut}</VerticalSplitSection>
              <VerticalSplitSection>
                <StateSection state={state}>
                  {stepNumber && <StepNumberCircle stepNumber={stepNumber} />}
                  <p>{getTextByState(state)}</p>
                </StateSection>
              </VerticalSplitSection>
            </HorizontalSplitSection>
          );
        })}
      </InOutSection>
    </BlockItemWrapper>
  );
};

const StepNumberCircle = memo(({ stepNumber }: { stepNumber: string }) => (
  <CircleBox>
    <p>{stepNumber}</p>
  </CircleBox>
));

const getBackgroundColorByState = (
  state: BlockState | undefined,
  theme: DefaultTheme,
) => {
  if (state?.includes(BLASTING_TASK)) {
    return theme.colors.blockStateColors.blasting;
  }

  if (state?.includes(COATING_TASK)) {
    return theme.colors.blockStateColors.coating;
  }

  if (state === BlockState.INSPECTION_FINISH) {
    return theme.colors.blockStateColors.finished;
  }

  return theme.colors.white;
};

const StateSection = styled.div<{ state: BlockState | undefined }>`
  width: 100%;
  height: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
  background: ${({ state, theme }) => getBackgroundColorByState(state, theme)};
  font-size: 1.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const CircleBox = styled.div`
  width: 1.1rem;
  height: 1.1rem;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 8px;
  text-align: center;
  font-size: 0.8rem;
  font-weight: 700;
`;

export default BlockStateItem;
