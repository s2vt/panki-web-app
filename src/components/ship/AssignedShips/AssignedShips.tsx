import { useHistory } from 'react-router-dom';
import { useCallback, useState } from 'react';
import { SHIP_LIST } from '@src/constants/constantString';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Ship, ShipFilter, ShipState } from '@src/types/ship.types';
import useCurrentUserShipsQuery from '@src/hooks/ship/useCurrentUserShipsQuery';
import PageTitle from '@src/components/base/PageTitle';
import TableBase from '@src/components/table/TableBase';
import SideLayout from '@src/components/base/SideLayout';
import InspectionRequestsBox from '../InspectionRequestsBox';
import AssignedShipsTable from './AssignedShipsTable';
import AssignedShipsTableAction from './AssignedShipsTableAction';

const AssignedShips = () => {
  const history = useHistory();

  const [shipFilter, setShipFilter] = useState<ShipFilter>();

  const { refetch: refetchShips } = useCurrentUserShipsQuery({ shipFilter });

  const handleRefetchShips = useCallback(() => {
    history.replace(ROUTE_PATHS.shipAssigned);
    refetchShips();
  }, [history, refetchShips]);

  const handleShipClick = useCallback(
    (ship: Ship) => {
      history.push(ROUTE_PATHS.inspectionRequest, { ship });
    },
    [history],
  );

  const handleSearchEnter = useCallback(
    (value: string) => {
      setShipFilter((prevFilter) => ({ ...prevFilter, shipName: value }));
    },
    [setShipFilter],
  );

  const handleFilterChange = useCallback(
    (state?: ShipState) => {
      setShipFilter((prevFilter) => ({ ...prevFilter, state }));
    },
    [setShipFilter],
  );

  return (
    <>
      <PageTitle title={SHIP_LIST} />
      <SideLayout.Container>
        <SideLayout.Side>
          <InspectionRequestsBox />
        </SideLayout.Side>
        <SideLayout.Main>
          <TableBase.TableSection>
            <AssignedShipsTableAction
              onRefetchShips={handleRefetchShips}
              onSearchEnter={handleSearchEnter}
              onFilterChange={handleFilterChange}
            />
            <AssignedShipsTable
              shipFilter={shipFilter}
              onShipClick={handleShipClick}
            />
          </TableBase.TableSection>
        </SideLayout.Main>
      </SideLayout.Container>
    </>
  );
};

export default AssignedShips;
