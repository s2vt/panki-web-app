import { Meta, Story } from '@storybook/react';
import AssignedShips from './AssignedShips';

export default {
  title: 'Components/ship/AssignedShips',
  component: AssignedShips,
} as Meta;

const Template: Story = (args) => <AssignedShips {...args} />;

export const Basic = Template.bind({});
