import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import taskApi from '@src/api/taskApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useCoatingInspectionMutation from '../hooks/useCoatingInspectionMutation';

describe('useCoatingInspectionMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useCoatingInspectionMutation(), { wrapper });
  };

  it('should isSuccess is true when succeed coating inspection mutate', async () => {
    // given
    const blockIds = ['1', '2', '3'];
    const scheduledAt = '2020-01-01';
    const place = 'place';

    taskApi.requestCoatingInspection = jest.fn().mockResolvedValue('success');

    // when
    const { result, waitFor } = setup();

    act(() =>
      result.current.mutate({
        blockIds,
        scheduledAt,
        place,
        reqTaskStep: '1st_stripe_coat',
      }),
    );

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.isSuccess).toEqual(true);
  });

  it('should set error when failed create coating inspection mutate', async () => {
    // given
    const blockIds = ['1', '2', '3'];
    const scheduledAt = '2020-01-01';
    const place = 'place';

    taskApi.requestCoatingInspection = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() =>
      result.current.mutate({
        blockIds,
        scheduledAt,
        place,
        reqTaskStep: '1st_stripe_coat',
      }),
    );

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
