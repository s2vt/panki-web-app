import HttpError from '@src/api/model/HttpError';
import taskApi from '@src/api/taskApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import {
  Block,
  BlockDivision,
  BlockInOut,
  BlockState,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import {
  CoatingInspectionRequestPayload,
  CoatingInspectionType,
} from '@src/types/ship.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import useCoatingInspectionRequest from '../hooks/useCoatingInspectionRequest';

describe('useCoatingInspectionRequest hook', () => {
  const setup = (selectedBlocks: (SelectItem & Block)[] = []) => {
    const { wrapper, store } = prepareMockWrapper();

    const onCoatingInspectionRequestSuccess = jest.fn();
    const onCoatingInspectionRequestError = jest.fn();
    const requestCoatingInspection = jest.spyOn(
      taskApi,
      'requestCoatingInspection',
    );

    const { result, waitFor } = renderHook(
      () =>
        useCoatingInspectionRequest({
          onCoatingInspectionRequestSuccess,
          onCoatingInspectionRequestError,
          selectedBlocks,
        }),
      { wrapper },
    );

    return {
      result,
      waitFor,
      store,
      onCoatingInspectionRequestSuccess,
      onCoatingInspectionRequestError,
      requestCoatingInspection,
    };
  };

  it('should call open alert dialog action when has not block selected', () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [];
    const coatingInspectionRequestPayload: CoatingInspectionRequestPayload = {
      reqTaskStep: CoatingInspectionType.coatingInspection,
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const { result, store } = setup(selectedBlocks);

    // when
    act(() => {
      result.current.handleSubmit(coatingInspectionRequestPayload);
    });

    // then
    expect(store.getActions()[0].meta.arg.text).toBe('블록을 체크하세요');
  });

  it('should call onBlastingInspectionRequestError when request failed', async () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [
      {
        id: '1',
        blockName: 'block1',
        blockDivision: BlockDivision.CENTER,
        inOut: BlockInOut.IN,
        state: BlockState.PREPARATION,
      },
    ];

    const coatingInspectionRequestPayload: CoatingInspectionRequestPayload = {
      reqTaskStep: CoatingInspectionType.coatingInspection,
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const {
      result,
      waitFor,
      onCoatingInspectionRequestError,
      requestCoatingInspection,
    } = setup(selectedBlocks);

    requestCoatingInspection.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleSubmit(coatingInspectionRequestPayload);
    });

    // then
    await waitFor(() => expect(onCoatingInspectionRequestError).toBeCalled());
  });

  it('should call onBlastingInspectionRequestSuccess when request succeed', async () => {
    // given
    const selectedBlocks: (SelectItem & Block)[] = [
      {
        id: '1',
        blockName: 'block1',
        blockDivision: BlockDivision.CENTER,
        inOut: BlockInOut.IN,
        state: BlockState.PREPARATION,
      },
    ];

    const coatingInspectionRequestPayload: CoatingInspectionRequestPayload = {
      reqTaskStep: CoatingInspectionType.coatingInspection,
      place: 'place',
      scheduledAt: '2020-01-01 00:00',
    };

    const {
      result,
      waitFor,
      onCoatingInspectionRequestSuccess,
      requestCoatingInspection,
    } = setup(selectedBlocks);

    requestCoatingInspection.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleSubmit(coatingInspectionRequestPayload);
    });

    // then
    await waitFor(() => expect(onCoatingInspectionRequestSuccess).toBeCalled());
    expect(requestCoatingInspection).toBeCalledWith({
      ...coatingInspectionRequestPayload,
      blockIds: ['1'],
    });
  });
});
