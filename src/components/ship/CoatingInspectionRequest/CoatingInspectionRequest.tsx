import { useEffect, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router';
import HttpError from '@src/api/model/HttpError';
import { COATING_INSPECTION_REQUEST } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useSelectItems from '@src/hooks/common/useSelectItems';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Block, BlockInOut } from '@src/types/block.types';
import {
  CoatingInspectionType,
  convertCoatingInspectionTypeText,
  Ship,
} from '@src/types/ship.types';
import PageTitle from '@src/components/base/PageTitle';
import { Box } from '@mui/material';
import { DropdownItem, FilterSelectItem } from '@src/types/common.types';
import useSelectItem from '@src/hooks/common/useSelectItem';
import CoatingRequestForm from './CoatingRequestForm';
import CoatingInspectionBlockTable from './CoatingInspectionBlockTable';
import useCoatingInspectionRequest from './hooks/useCoatingInspectionRequest';

const CoatingInspectionRequest = () => {
  const location = useLocation<{ ship?: Ship }>();
  const history = useHistory();

  const { openAlertDialog } = useAlertDialogAction();
  const { user } = useUserSelector();

  const typeDropdownItems: DropdownItem[] = useMemo(
    () =>
      Object.values(CoatingInspectionType).map((taskStep, index) => ({
        id: index.toString(),
        label: convertCoatingInspectionTypeText(taskStep),
        value: taskStep,
      })),
    [],
  );

  const {
    selectedItems: selectedBlocks,
    setSelectedItems: setSelectedBlocks,
    isSelected,
    toggleItem: handleCheckboxChange,
  } = useSelectItems<Block>();

  const { selectedItem: selectedCoatingType, handleChange } = useSelectItem<
    FilterSelectItem<CoatingInspectionType>
  >(typeDropdownItems[0]);

  const handleCoatingTypeClick = (
    selectItem: FilterSelectItem<CoatingInspectionType>,
  ) => {
    if (selectItem.value === CoatingInspectionType.dftInspection) {
      setSelectedBlocks((prevBlocks) =>
        prevBlocks.filter((block) => block.inOut === BlockInOut.IN),
      );
    }

    handleChange(selectItem);
  };

  const selectablePosition = useMemo(
    () =>
      selectedCoatingType.value === CoatingInspectionType.dftInspection
        ? [BlockInOut.IN]
        : [BlockInOut.IN, BlockInOut.OUT],
    [selectedCoatingType],
  );

  useEffect(() => {
    if (location.state?.ship === undefined) {
      history.push(ROUTE_PATHS.ship);
    }
  }, [history]);

  const ship = useMemo(() => location.state?.ship, [location.state?.ship]);

  const handleCoatingInspectionRequestSuccess = () => {
    history.push(ROUTE_PATHS.inspectionRequest, { ship: location.state?.ship });
  };

  const handleCoatingInspectionRequestError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { handleSubmit, isCoatingInspectionRequestLoading } =
    useCoatingInspectionRequest({
      selectedBlocks,
      onCoatingInspectionRequestSuccess: handleCoatingInspectionRequestSuccess,
      onCoatingInspectionRequestError: handleCoatingInspectionRequestError,
    });

  return (
    <Box>
      <PageTitle title={COATING_INSPECTION_REQUEST} />
      {user !== undefined && ship !== undefined && (
        <>
          <CoatingRequestForm
            currentUser={user}
            onSubmit={handleSubmit}
            disabledSubmit={isCoatingInspectionRequestLoading}
            shipName={ship.shipName}
            selectedCoatingType={selectedCoatingType}
            typeDropdownItems={typeDropdownItems}
            onCoatingTypeClick={handleCoatingTypeClick}
          />
          <CoatingInspectionBlockTable
            isSelected={isSelected}
            onCheckboxChange={handleCheckboxChange}
            shipId={ship.id}
            selectablePosition={selectablePosition}
          />
        </>
      )}
    </Box>
  );
};

export default CoatingInspectionRequest;
