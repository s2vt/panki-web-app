import { useMutation, UseMutationOptions } from 'react-query';
import taskApi from '@src/api/taskApi';
import HttpError from '@src/api/model/HttpError';
import { CoatingInspectionRequestPayload } from '@src/types/ship.types';

const useCoatingInspectionMutation = (
  options?: UseMutationOptions<
    unknown,
    HttpError,
    CoatingInspectionRequestPayload
  >,
) =>
  useMutation(
    (coatingInspectionRequestPayload: CoatingInspectionRequestPayload) =>
      taskApi.requestCoatingInspection(coatingInspectionRequestPayload),
    options,
  );

export default useCoatingInspectionMutation;
