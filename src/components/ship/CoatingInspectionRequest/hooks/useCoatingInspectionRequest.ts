import HttpError from '@src/api/model/HttpError';
import { SELECT_BLOCKS_ERROR } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { Block } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { CoatingInspectionRequestPayload } from '@src/types/ship.types';
import { useCallback } from 'react';
import useCoatingInspectionMutation from './useCoatingInspectionMutation';

type UseCoatingInspectionRequestProps = {
  selectedBlocks: (SelectItem & Block)[];
  onCoatingInspectionRequestSuccess: () => void;
  onCoatingInspectionRequestError: (error: HttpError) => void;
};

const useCoatingInspectionRequest = ({
  selectedBlocks,
  onCoatingInspectionRequestSuccess,
  onCoatingInspectionRequestError,
}: UseCoatingInspectionRequestProps) => {
  const { openAlertDialog } = useAlertDialogAction();

  const { mutate, isLoading: isCoatingInspectionRequestLoading } =
    useCoatingInspectionMutation({
      onSuccess: onCoatingInspectionRequestSuccess,
      onError: onCoatingInspectionRequestError,
    });

  const handleSubmit = useCallback(
    (payload: CoatingInspectionRequestPayload) => {
      if (selectedBlocks.length === 0) {
        openAlertDialog({ text: SELECT_BLOCKS_ERROR, position: 'rightTop' });
        return;
      }

      const blockIds = selectedBlocks.map((block) => block.id);

      mutate({
        ...payload,
        blockIds,
      });
    },
    [selectedBlocks, openAlertDialog, mutate],
  );

  return { handleSubmit, isCoatingInspectionRequestLoading };
};

export default useCoatingInspectionRequest;
