import { Meta, Story } from '@storybook/react';
import CoatingInspectionRequest from '../CoatingInspectionRequest';

export default {
  title: 'Components/ship/CoatingInspectionRequest',
  component: CoatingInspectionRequest,
} as Meta;

const Template: Story = (args) => <CoatingInspectionRequest {...args} />;

export const Basic = Template.bind({});
