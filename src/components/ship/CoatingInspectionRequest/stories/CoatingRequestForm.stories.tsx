import { Meta, Story } from '@storybook/react';
import CoatingRequestForm, {
  CoatingRequestFormProps,
} from '../CoatingRequestForm';

export default {
  title: 'Components/Ship/CoatingInpsectionRequest/CoatingRequestForm',
  component: CoatingRequestForm,
} as Meta;

const Template: Story<CoatingRequestFormProps> = (args) => (
  <CoatingRequestForm {...args} />
);

export const Basic = Template.bind({});
