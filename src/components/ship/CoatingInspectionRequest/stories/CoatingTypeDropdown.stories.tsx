import { Meta, Story } from '@storybook/react';
import CoatingTypeDropdown, {
  CoatingTypeDropdownProps,
} from '../CoatingTypeDropdown';

export default {
  title: 'Components/Ship/CoatingInspectionRequest/CoatingTypeDropdown',
  component: CoatingTypeDropdown,
} as Meta;

const Template: Story<CoatingTypeDropdownProps> = (args) => (
  <CoatingTypeDropdown {...args} />
);

export const Basic = Template.bind({});
