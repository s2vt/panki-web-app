import { Meta, Story } from '@storybook/react';
import CoatingInspectionBlockTable, {
  CoatingInspectionBlockTableProps,
} from '../CoatingInspectionBlockTable';

export default {
  title: 'Components/Ship/CoatingInspectionRequest/CoatingInspectionBlockTable',
  component: CoatingInspectionBlockTable,
} as Meta;

const Template: Story<CoatingInspectionBlockTableProps> = (args) => (
  <CoatingInspectionBlockTable {...args} />
);

export const Basic = Template.bind({});
