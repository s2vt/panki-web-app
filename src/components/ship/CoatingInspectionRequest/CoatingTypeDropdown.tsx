import { useCallback } from 'react';
import styled, { css } from 'styled-components';
import { DropdownItem, FilterSelectItem } from '@src/types/common.types';
import { CoatingInspectionType } from '@src/types/ship.types';
import DropdownSelectWrapper from '@src/components/common/DropdownSelectWrapper';
import Icon from '@src/components/common/Icon';

export type CoatingTypeDropdownProps = {
  dropdownItems: DropdownItem[];
  onClick: (selectItem: FilterSelectItem<CoatingInspectionType>) => void;
  selectedItem: FilterSelectItem<CoatingInspectionType>;
};

const CoatingTypeDropdown = ({
  dropdownItems,
  onClick,
  selectedItem,
}: CoatingTypeDropdownProps) => {
  const isSelected = useCallback(
    (item: FilterSelectItem<CoatingInspectionType>) =>
      item.id === selectedItem?.id,
    [selectedItem],
  );

  return (
    <StyleDropdownSelectWrapper
      dropdownItems={dropdownItems}
      isSelected={isSelected}
      onClick={onClick}
      boxWidth="32rem"
    >
      <InspectionTaskDropdownBox label={selectedItem?.label} />
    </StyleDropdownSelectWrapper>
  );
};

const InspectionTaskDropdownBox = ({
  label,
  isActive,
}: {
  label: string;
  isActive?: boolean;
}) => (
  <Box>
    <p>{label}</p>
    <ArrowIcon icon="arrowIcon" $isActive={isActive} />
  </Box>
);

const StyleDropdownSelectWrapper = styled(DropdownSelectWrapper)`
  flex: 1;
  max-width: 52.3rem;
`;

const Box = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 1.8rem;
  font-size: 1.6rem;
  font-weight: 500;
  height: 4.6rem;
  color: ${({ theme }) => theme.colors.primary};
  box-shadow: 0px 2px 20px 0px rgba(0, 0, 0, 0.05);
  background: ${({ theme }) => theme.colors.white};
`;

const ArrowIcon = styled(Icon)<{ $isActive?: boolean }>`
  ${({ $isActive }) =>
    $isActive &&
    css`
      transform: rotate(180deg);
    `}
`;

export default CoatingTypeDropdown;
