import ErrorBox from '@src/components/common/ErrorBox';
import { FAIL_DATA_FETCH, REQUEST_BLOCK } from '@src/constants/constantString';
import useEnableBlocksQuery from '@src/hooks/ship/useEnableBlocksQuery';
import { Block, BlockInOut, BlockState } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import BlockTable from '../BlockTable';

const SELECTABLE_BLOCK_STATES: BlockState[] = [
  BlockState.BLASTING_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_FINISHED,
  BlockState.CTG_1ST_INSPECTION_REJECTED,
  BlockState.CTG_2ND_INSPECTION_FINISHED,
  BlockState.CTG_2ND_INSPECTION_REJECTED,
  BlockState.CTG_3RD_INSPECTION_FINISHED,
  BlockState.CTG_3RD_INSPECTION_REJECTED,
  BlockState.CTG_4TH_INSPECTION_FINISHED,
  BlockState.CTG_4TH_INSPECTION_REJECTED,
  BlockState.CTG_5TH_INSPECTION_REJECTED,
];

export type CoatingInspectionBlockTableProps = {
  shipId: string;
  isSelected: (selectItem: SelectItem & Block) => boolean;
  onCheckboxChange: (selectItem: SelectItem & Block) => void;
  selectablePosition: BlockInOut[];
};

const CoatingInspectionBlockTable = ({
  shipId,
  isSelected,
  onCheckboxChange,
  selectablePosition,
}: CoatingInspectionBlockTableProps) => {
  const {
    data: blocks,
    isFetching,
    isError,
    refetch,
  } = useEnableBlocksQuery(shipId);

  if (isFetching) {
    <BlockTable.Loader />;
  }

  if (isError) {
    <BlockTable.LoadingWrapper>
      <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
    </BlockTable.LoadingWrapper>;
  }

  return (
    <>
      <BlockTable.TitleSection>{REQUEST_BLOCK}</BlockTable.TitleSection>
      {blocks !== undefined && (
        <BlockTable
          blocks={blocks}
          mode="selection"
          isSelected={isSelected}
          onCheckboxChange={onCheckboxChange}
          selectionType="select"
          selectableStates={SELECTABLE_BLOCK_STATES}
          selectablePosition={selectablePosition}
        />
      )}
    </>
  );
};
export default CoatingInspectionBlockTable;
