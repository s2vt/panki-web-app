/* eslint-disable no-underscore-dangle */
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import {
  APPLICANT,
  CALENDAR,
  INSPECTION_DUE_DATE,
  INSPECTION_PLACE,
  INSPECTION_TYPE,
  REQUEST,
  SHIP_NUMBER,
} from '@src/constants/constantString';
import { REQUIRED_VALIDATOR } from '@src/constants/validators';
import { DropdownItem, FilterSelectItem } from '@src/types/common.types';
import {
  CoatingInspectionRequestPayload,
  CoatingInspectionType,
} from '@src/types/ship.types';
import { User } from '@src/types/user.types';
import {
  filterWeekend,
  formatToDateTime,
  getNextWeekday,
} from '@src/utils/dateUtil';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import DatePickerWrapper from '@src/components/common/DatePickerWrapper';
import MainButton from '@src/components/common/MainButton';
import FormLabel from '@src/components/form/FormLabel';
import RowFormInput from '@src/components/form/RowFormInput';
import ShipFormBase from '../ShipFormBase';
import CoatingTypeDropdown from './CoatingTypeDropdown';

export type CoatingRequestFormProps = {
  currentUser: User;
  onSubmit: (payload: CoatingInspectionRequestPayload) => void;
  disabledSubmit: boolean;
  shipName: string;
  selectedCoatingType: FilterSelectItem<CoatingInspectionType>;
  typeDropdownItems: DropdownItem[];
  onCoatingTypeClick: (
    selectItem: FilterSelectItem<CoatingInspectionType>,
  ) => void;
};

const CoatingRequestForm = ({
  currentUser,
  onSubmit,
  disabledSubmit,
  shipName,
  selectedCoatingType,
  typeDropdownItems,
  onCoatingTypeClick,
}: CoatingRequestFormProps) => {
  const { handleSubmit, register, setValue, unregister } =
    useForm<CoatingInspectionRequestPayload>({
      defaultValues: {
        scheduledAt: '',
        reqTaskStep: CoatingInspectionType.coatingInspection,
        place: '',
      },
    });

  const currentDate = useMemo(() => {
    const date = new Date();
    date.setHours(9);
    date.setMinutes(0);
    date.setSeconds(0);

    return date;
  }, []);

  const nextWeekday = useMemo(() => getNextWeekday(currentDate), []);

  const handleDateChange = (date: Date) => {
    setValue('scheduledAt', formatToDateTime(date));
  };

  useEffect(() => {
    if (selectedCoatingType?.value) {
      setValue('reqTaskStep', selectedCoatingType.value);
    }

    if (selectedCoatingType.value === CoatingInspectionType.coatingInspection) {
      register('place', REQUIRED_VALIDATOR);
    } else {
      setValue('place', '');
      unregister('place');
    }
  }, [selectedCoatingType]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <ShipFormBase.InputSection height="19rem">
        <ShipFormBase.Row>
          <RowFormInput
            label={APPLICANT}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            defaultValue={currentUser.userName}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={SHIP_NUMBER}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            defaultValue={shipName}
            labelPadding={0}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <DropdownSection>
            <StyledFormLabel label={INSPECTION_TYPE} labelFontSize="1.6rem" />
            <CoatingTypeDropdown
              onClick={onCoatingTypeClick}
              selectedItem={selectedCoatingType}
              dropdownItems={typeDropdownItems}
            />
          </DropdownSection>
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={INSPECTION_PLACE}
            enableShadow
            labelFontSize="1.6rem"
            disabled={
              selectedCoatingType.value !==
              CoatingInspectionType.coatingInspection
            }
            {...register('place')}
          />
        </ShipFormBase.Row>
        <ShipFormBase.Row>
          <RowFormInput
            label={INSPECTION_DUE_DATE}
            enableShadow
            readOnly
            labelFontSize="1.6rem"
            {...register('scheduledAt')}
          />
          <DatePickerWrapper
            onChange={handleDateChange}
            startDate={nextWeekday}
            showTimeSelect
            timeIntervals={30}
            popperPlacement="bottom-start"
            filterDate={filterWeekend}
            timeCaption="시간"
          >
            <CalendarButton label={addWhiteSpacesBetweenCharacters(CALENDAR)} />
          </DatePickerWrapper>
        </ShipFormBase.Row>
      </ShipFormBase.InputSection>
      <ShipFormBase.ButtonSection>
        <MainButton label={REQUEST} type="submit" disabled={disabledSubmit} />
      </ShipFormBase.ButtonSection>
    </form>
  );
};

const CalendarButton = styled(MainButton)`
  width: 20.7rem;
  height: 4.6rem;
  font-size: 1.6rem;
  font-weight: 400;
`;

const DropdownSection = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

const StyledFormLabel = styled(FormLabel)`
  min-width: 16.1rem;
`;

export default CoatingRequestForm;
