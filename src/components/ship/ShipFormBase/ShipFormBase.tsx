import React, { memo } from 'react';
import styled from 'styled-components';
import Divider from '@src/components/common/Divider';

const InputSection = memo(styled.div<{ height?: string }>`
  padding: 2.2rem 3.5rem 2.9rem;
  min-height: ${({ height }) => height ?? '25rem'};
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  gap: 1.9rem;
  border: 1px solid ${({ theme }) => theme.colors.primary};
`);

const ButtonWrapper = styled.div`
  margin: 2.5rem 0;
  padding: 0 3.5rem;
  display: flex;
  justify-content: flex-end;
  gap: 2.7rem;

  button {
    font-size: 1.6rem;
    width: 21rem;
    height: 4.6rem;
  }
`;

export type ButtonSectionProps = { children: React.ReactNode };

const ButtonSection = ({ children }: ButtonSectionProps) => (
  <div>
    <ButtonWrapper>{children}</ButtonWrapper>
    <Divider orientation="horizontal" />
  </div>
);

const Row = memo(styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`);

const BlankRow = memo(styled.div`
  padding-left: 16.1rem;
  align-items: center;
  display: flex;
`);

export default { InputSection, ButtonSection, Row, BlankRow };
