import { Meta, Story } from '@storybook/react';
import ShipFormBase from './ShipFormBase';

export default {
  title: 'Components/ship/ShipFormBase/InputSection',
  component: ShipFormBase.InputSection,
} as Meta;

const Template: Story = (args) => <ShipFormBase.InputSection {...args} />;

export const Basic = Template.bind({});
