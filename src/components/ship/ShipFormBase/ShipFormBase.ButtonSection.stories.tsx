import { Meta, Story } from '@storybook/react';
import ShipFormBase, { ButtonSectionProps } from './ShipFormBase';

export default {
  title: 'Components/ship/ShipFormBase/ButtonSection',
  component: ShipFormBase.ButtonSection,
} as Meta;

const Template: Story<ButtonSectionProps> = (args) => (
  <ShipFormBase.ButtonSection {...args}>
    <></>
  </ShipFormBase.ButtonSection>
);

export const Basic = Template.bind({});
