import { useEffect, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { INSPECTION_REQUEST } from '@src/constants/constantString';
import useEnableBlocksQuery from '@src/hooks/ship/useEnableBlocksQuery';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { BlockState } from '@src/types/block.types';
import { Ship } from '@src/types/ship.types';
import PageTitle from '@src/components/base/PageTitle';
import { Box } from '@mui/material';
import InspectionRequestForm from './InspectionRequestForm';
import BlockStateTable from '../BlockStateTable';

export type InspectionRequestProps = {};

const InspectionRequest = () => {
  const location = useLocation<{ ship?: Ship }>();
  const history = useHistory<{ ship?: Ship }>();

  useEffect(() => {
    if (location.state?.ship === undefined) {
      history.push(ROUTE_PATHS.ship);
    }
  }, [history]);

  const ship = useMemo(() => location.state?.ship, [location.state?.ship]);

  const { data: blocks } = useEnableBlocksQuery(ship?.id ?? '', {
    enabled: false,
  });

  const handleBlastingInspectionClick = () => {
    history.push(ROUTE_PATHS.blastingInspectionRequest, {
      ship,
    });
  };

  const handleCoatingInspectionClick = () => {
    history.push(ROUTE_PATHS.coatingInspectionRequest, {
      ship,
    });
  };

  const isPreparationStateExists = useMemo(
    () =>
      blocks?.some((block) => block.state === BlockState.PREPARATION) ?? true,
    [blocks],
  );

  return (
    <Box>
      <PageTitle title={INSPECTION_REQUEST} />
      {ship !== undefined && (
        <>
          <InspectionRequestForm
            ship={ship}
            onBlastingInspectionClick={handleBlastingInspectionClick}
            onCoatingInspectionClick={handleCoatingInspectionClick}
            isPreparationStateExists={isPreparationStateExists}
          />
          <BlockStateTable shipId={ship.id} />
        </>
      )}
    </Box>
  );
};

export default InspectionRequest;
