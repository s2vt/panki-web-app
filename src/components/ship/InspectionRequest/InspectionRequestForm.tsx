/* eslint-disable react/no-array-index-key */
import {
  REQUEST_BLASTING_BLASTING,
  REQUEST_COATING_INSPECTION,
} from '@src/constants/constantString';
import { Ship } from '@src/types/ship.types';
import MainButton from '@src/components/common/MainButton';
import ShipFormBase from '../ShipFormBase';
import ShipDetailBox from '../ShipDetailBox';

export type InspectionRequestFormProps = {
  ship: Ship;
  onBlastingInspectionClick: () => void;
  onCoatingInspectionClick: () => void;
  isPreparationStateExists: boolean;
};

const InspectionRequestForm = ({
  ship,
  onBlastingInspectionClick,
  onCoatingInspectionClick,
  isPreparationStateExists,
}: InspectionRequestFormProps) => (
  <>
    <ShipDetailBox ship={ship} />
    <ShipFormBase.ButtonSection>
      <MainButton
        backgroundColor="lightBlack"
        labelColor="white"
        label={REQUEST_BLASTING_BLASTING}
        onClick={onBlastingInspectionClick}
        disabled={!isPreparationStateExists}
      />
      <MainButton
        label={REQUEST_COATING_INSPECTION}
        onClick={onCoatingInspectionClick}
      />
    </ShipFormBase.ButtonSection>
  </>
);

export default InspectionRequestForm;
