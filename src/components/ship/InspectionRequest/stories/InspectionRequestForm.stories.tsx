import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import InspectionRequestForm, {
  InspectionRequestFormProps,
} from '../InspectionRequestForm';

export default {
  title: 'Components/ship/InspectionRequest/InspectionRequestForm',
  component: InspectionRequestForm,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<InspectionRequestFormProps> = (args) => (
  <InspectionRequestForm {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  ship: {
    id: '2',
    ownerCompany: '선주사',
    shipName: '1234',
    qmManager: [
      { id: '1', userName: 'test1' },
      { id: '2', userName: 'test2' },
    ],
    state: 'FINISHED',
  },
};
