import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import InspectionRequest, {
  InspectionRequestProps,
} from '../InspectionRequest';

export default {
  title: 'Components/ship/InspectionRequest',
  component: InspectionRequest,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<InspectionRequestProps> = (args) => (
  <InspectionRequest {...args} />
);

export const Basic = Template.bind({});
