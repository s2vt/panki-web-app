import { useCallback, useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import {
  DUPLICATE_SHIP_NAME_ERROR,
  SHIP_AND_BLOCK,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { Block } from '@src/types/block.types';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import HttpError from '@src/api/model/HttpError';
import { ResultCode } from '@src/types/http.types';
import PageTitle from '@src/components/base/PageTitle';
import RequiredIcon from '@src/components/common/RequiredIcon';
import AlertModal from '@src/components/common/AlertModal';
import ModalWrapper from '@src/components/base/ModalWrapper';
import { Box } from '@mui/material';
import SaveShipForm from '../SaveShipForm';
import useTemplateBlocksQuery from './hooks/useTemplateBlocksQuery';
import TemplateBlocksTable from './TemplateBlocksTable';
import useCreateShip from './hooks/useCreateShip';

export type CreateShipProps = {};

const CreateShip = () => {
  const history = useHistory();
  const { openAlertDialog } = useAlertDialogAction();
  const [isDuplicateShipAlertOpen, setIsDuplicateShipAlertOpen] =
    useState(false);

  const { data: templateBlocks } = useTemplateBlocksQuery();
  const {
    selectedItems: disabledBlocks,
    setSelectedItems: setDisabledBlocks,
    isSelected: isBlockSelected,
    toggleItem: handleCheckboxChange,
  } = useSelectItems<Block>();

  const handleCreateShipSuccess = useCallback(() => {
    history.push(ROUTE_PATHS.shipManagement);
  }, [history]);

  const handleCreateShipError = (error: HttpError) => {
    if (error.resultCode === ResultCode.duplicateId) {
      setIsDuplicateShipAlertOpen(true);
      return;
    }

    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const hasBlockSelected = useMemo(
    () => disabledBlocks.length !== templateBlocks?.length,
    [disabledBlocks, templateBlocks],
  );

  const hasAllBlockSelected = useMemo(
    () => disabledBlocks.length === 0,
    [disabledBlocks],
  );

  const { handleSubmit, isCreateShipMutationLoading } = useCreateShip({
    disabledBlocks,
    hasBlockSelected,
    onCreateShipSuccess: handleCreateShipSuccess,
    onCreateShipError: handleCreateShipError,
  });

  const handleAllCheckClick = useCallback(() => {
    if (hasAllBlockSelected && templateBlocks !== undefined) {
      setDisabledBlocks(templateBlocks);
    } else {
      setDisabledBlocks([]);
    }
  }, [hasAllBlockSelected, templateBlocks, setDisabledBlocks]);

  return (
    <>
      <PageTitle title={SHIP_AND_BLOCK}>
        <RequiredSection>
          (<RequiredIcon />
          )필수 입력 항목
        </RequiredSection>
      </PageTitle>
      <Box>
        <SaveShipForm
          onClickClose={handleCreateShipSuccess}
          onSubmit={handleSubmit}
          disabledSubmit={isCreateShipMutationLoading}
        />
        <TemplateBlocksTable
          isAllCheckboxChecked={disabledBlocks.length === 0}
          isBlockSelected={isBlockSelected}
          onCheckboxChange={handleCheckboxChange}
          onAllCheckClick={handleAllCheckClick}
        />
      </Box>
      <ModalWrapper isOpen={isDuplicateShipAlertOpen}>
        <AlertModal
          text={DUPLICATE_SHIP_NAME_ERROR}
          onClose={() => setIsDuplicateShipAlertOpen(false)}
        />
      </ModalWrapper>
    </>
  );
};

const RequiredSection = styled.p`
  font-size: 1.6rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
`;

export default CreateShip;
