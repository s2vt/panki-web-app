import { useQuery, UseQueryOptions } from 'react-query';
import blockApi from '@src/api/blockApi';
import HttpError from '@src/api/model/HttpError';
import { Block } from '@src/types/block.types';

const useTemplateBlocksQuery = (
  options?: UseQueryOptions<Block[], HttpError>,
) => useQuery(createKey(), blockApi.getTemplateBlocks, options);

const baseKey = ['templateBlocks'];
const createKey = () => [...baseKey];

useTemplateBlocksQuery.baseKey = baseKey;
useTemplateBlocksQuery.createKey = createKey;

export default useTemplateBlocksQuery;
