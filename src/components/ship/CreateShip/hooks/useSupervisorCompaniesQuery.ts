import { useQuery, UseQueryOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';
import { SupervisorCompany } from '@src/types/company.types';

const useSupervisorCompaniesQuery = (
  ownerCompanyName: string,
  options?: UseQueryOptions<SupervisorCompany[], HttpError>,
) =>
  useQuery(
    createKey(ownerCompanyName),
    () => companyApi.getSupervisorCompanies(ownerCompanyName),
    options,
  );

const baseKey = ['superVisorCompanies'];
const createKey = (ownerCompanyName: string) => [...baseKey, ownerCompanyName];

useSupervisorCompaniesQuery.baseKey = baseKey;
useSupervisorCompaniesQuery.createKey = createKey;

export default useSupervisorCompaniesQuery;
