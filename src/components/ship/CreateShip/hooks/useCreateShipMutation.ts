import { useMutation, UseMutationOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import { CreateShipPayload } from '@src/types/ship.types';

const useCreateShipMutation = (
  options?: UseMutationOptions<unknown, HttpError, CreateShipPayload>,
) =>
  useMutation(
    (createShipPayload: CreateShipPayload) =>
      shipApi.createShip(createShipPayload),
    options,
  );

export default useCreateShipMutation;
