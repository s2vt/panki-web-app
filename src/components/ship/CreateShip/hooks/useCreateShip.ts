import { useCallback } from 'react';
import { SaveShipPayload } from '@src/types/ship.types';
import { Block } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import HttpError from '@src/api/model/HttpError';
import { SELECT_BLOCKS_ERROR } from '@src/constants/constantString';
import useCreateShipMutation from './useCreateShipMutation';

export type UseCreateShipProps = {
  disabledBlocks: (SelectItem & Block)[];
  onCreateShipSuccess: () => void;
  onCreateShipError: (error: HttpError) => void;
  hasBlockSelected: boolean;
};

const useCreateShip = ({
  disabledBlocks,
  onCreateShipSuccess,
  onCreateShipError,
  hasBlockSelected,
}: UseCreateShipProps) => {
  const { openAlertDialog } = useAlertDialogAction();

  const { mutate, isLoading: isCreateShipMutationLoading } =
    useCreateShipMutation({
      onSuccess: onCreateShipSuccess,
      onError: onCreateShipError,
    });

  const handleSubmit = useCallback(
    (saveShipPayload: SaveShipPayload) => {
      if (!hasBlockSelected) {
        openAlertDialog({ text: SELECT_BLOCKS_ERROR, position: 'rightTop' });
        return;
      }

      const disabledNames = Array.from(
        new Set(disabledBlocks.map((block) => block.blockName)),
      );

      mutate({ ...saveShipPayload, disabledNames });
    },
    [disabledBlocks, mutate],
  );

  return { handleSubmit, isCreateShipMutationLoading };
};

export default useCreateShip;
