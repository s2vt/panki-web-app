import { renderHook } from '@testing-library/react-hooks/dom';
import companyApi from '@src/api/companyApi';
import { SupervisorCompany } from '@src/types/company.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useSupervisorCompaniesQuery from '../hooks/useSupervisorCompaniesQuery';

describe('useSupervisorCompaniesQuery hook', () => {
  const setup = (ownerCompanyName: string) => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useSupervisorCompaniesQuery(ownerCompanyName), {
      wrapper,
    });
  };

  it('should cache data when succeed fetch supervisor companies', async () => {
    // given
    const ownerCompanyName = '선';

    const sampleSuperVisorCompanies: SupervisorCompany[] = [
      { companyName: '선주사1' },
      { companyName: '선주사2' },
      { companyName: '선주사3' },
    ];

    companyApi.getSupervisorCompanies = jest
      .fn()
      .mockResolvedValue(sampleSuperVisorCompanies);

    // when
    const { result, waitFor } = setup(ownerCompanyName);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleSuperVisorCompanies);
  });

  it('should set error when failed fetch supervisor companies', async () => {
    // given
    const ownerCompanyName = '선';

    companyApi.getSupervisorCompanies = jest
      .fn()
      .mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(ownerCompanyName);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
