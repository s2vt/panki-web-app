import { act, renderHook } from '@testing-library/react-hooks/dom';
import shipApi from '@src/api/shipApi';
import { CreateShipPayload } from '@src/types/ship.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useCreateShipMutation from '../hooks/useCreateShipMutation';

describe('useCreateShipMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useCreateShipMutation(), { wrapper });
  };

  it('should isSuccess is true when succeed create ship mutate', async () => {
    // given
    const createShipPayload: CreateShipPayload = {
      shipName: '0001',
      disabledNames: [],
      managerIds: ['1'],
      ownerCompanyName: 'ownerCompany',
    };

    shipApi.createShip = jest.fn().mockResolvedValue(true);

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(createShipPayload);
    });

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.isSuccess).toEqual(true);
  });

  it('should set error when failed create ship mutate', async () => {
    // given
    const createShipPayload: CreateShipPayload = {
      shipName: '0001',
      disabledNames: [],
      managerIds: ['1'],
      ownerCompanyName: 'ownerCompany',
    };

    shipApi.createShip = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(createShipPayload);
    });

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
