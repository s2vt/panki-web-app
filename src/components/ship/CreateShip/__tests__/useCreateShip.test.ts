import HttpError from '@src/api/model/HttpError';
import shipApi from '@src/api/shipApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import {
  Block,
  BlockDivision,
  BlockInOut,
  BlockState,
} from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { SaveShipPayload } from '@src/types/ship.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import useCreateShip, { UseCreateShipProps } from '../hooks/useCreateShip';

describe('useCreateShip hook', () => {
  const setup = ({
    disabledBlocks = [],
    hasBlockSelected = true,
  }: {
    disabledBlocks?: (SelectItem & Block)[];
    hasBlockSelected?: boolean;
  }) => {
    const { wrapper, store } = prepareMockWrapper();

    const onCreateShipSuccess = jest.fn();
    const onCreateShipError = jest.fn();
    const createShip = jest.spyOn(shipApi, 'createShip');

    const useCreateShipProps: UseCreateShipProps = {
      hasBlockSelected,
      disabledBlocks,
      onCreateShipSuccess,
      onCreateShipError,
    };

    const { result, waitFor } = renderHook(
      () => useCreateShip(useCreateShipProps),
      { wrapper },
    );

    return {
      result,
      waitFor,
      store,
      onCreateShipSuccess,
      onCreateShipError,
      createShip,
    };
  };

  it('should call open alert dialog action when has not block selected', () => {
    // given
    const hasBlockSelected = false;
    const saveShipPayload: SaveShipPayload = {
      managerIds: [],
      ownerCompanyName: 'company',
      shipName: '1234',
    };

    const { result, store } = setup({ hasBlockSelected });

    // when
    act(() => {
      result.current.handleSubmit(saveShipPayload);
    });

    // then
    expect(store.getActions()[0].meta.arg.text).toBe('블록을 체크하세요');
  });

  it('should call onCreateShipError when api throw error', async () => {
    // given
    const saveShipPayload: SaveShipPayload = {
      managerIds: [],
      ownerCompanyName: 'company',
      shipName: '1234',
    };

    const { result, waitFor, onCreateShipError, createShip } = setup({});

    createShip.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleSubmit(saveShipPayload);
    });

    // then
    await waitFor(() => expect(onCreateShipError).toBeCalled());
  });

  it('should call onCreateShipSuccess when success api request', async () => {
    // given
    const disabledBlocks: (SelectItem & Block)[] = [
      {
        id: '1',
        blockDivision: BlockDivision.CENTER,
        blockName: '1111',
        inOut: BlockInOut.IN,
        state: BlockState.CREATE,
      },
      {
        id: '2',
        blockDivision: BlockDivision.CENTER,
        blockName: '2222',
        inOut: BlockInOut.OUT,
        state: BlockState.CREATE,
      },
    ];

    const saveShipPayload: SaveShipPayload = {
      managerIds: [],
      ownerCompanyName: 'company',
      shipName: '1234',
    };

    const { result, waitFor, onCreateShipSuccess, createShip } = setup({
      disabledBlocks,
    });

    createShip.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleSubmit(saveShipPayload);
    });

    // then
    await waitFor(() => expect(onCreateShipSuccess).toBeCalled());
    expect(createShip).toBeCalledWith({
      ...saveShipPayload,
      disabledNames: ['1111', '2222'],
    });
  });
});
