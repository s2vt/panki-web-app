import { renderHook } from '@testing-library/react-hooks/dom';
import blockApi from '@src/api/blockApi';
import { Block, BlockState } from '@src/types/block.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useTemplateBlocksQuery from '../hooks/useTemplateBlocksQuery';

describe('useTemplateBlocksQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useTemplateBlocksQuery(), { wrapper });
  };

  it('should cache data when succeed fetch template blocks', async () => {
    // given
    const sampleBlocks: Block[] = [
      {
        id: '1',
        blockName: '1B11',
        blockDivision: 'PORT',
        inOut: 'IN',
        state: BlockState.PREPARATION,
      },
      {
        id: '2',
        blockName: '5B11',
        blockDivision: 'PORT',
        inOut: 'OUT',
        state: BlockState.PREPARATION,
      },
    ];

    blockApi.getTemplateBlocks = jest.fn().mockResolvedValue(sampleBlocks);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleBlocks);
  });

  it('should set error when failed fetch template blocks', async () => {
    // given

    blockApi.getTemplateBlocks = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
