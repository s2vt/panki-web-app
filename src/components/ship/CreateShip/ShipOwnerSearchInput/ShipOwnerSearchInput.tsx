import { forwardRef, memo, Ref, useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import { SHIP_OWNER } from '@src/constants/constantString';
import FormDropdownSearchInput from '@src/components/form/FormDropdownSearchInput';
import { FormInputProps } from '@src/components/form/FormInput/FormInput';
import useSupervisorCompaniesQuery from '../hooks/useSupervisorCompaniesQuery';

export type ShipOwnerSearchInputProps = FormInputProps & {
  /** 검색이 완료된 후 실행할 이벤트 */
  onSearch: (searchValue: string) => void;
};

const ShipOwnerSearchInput = (
  { onSearch, ...rest }: ShipOwnerSearchInputProps,
  ref: Ref<HTMLInputElement>,
) => {
  const [searchOwnerCompanyName, setSearchOwnerCompanyName] = useState('');

  const { data: supervisorCompanies } = useSupervisorCompaniesQuery(
    searchOwnerCompanyName,
    {
      enabled: searchOwnerCompanyName !== '',
    },
  );

  const dropdownItems = useMemo(
    () =>
      supervisorCompanies?.map(
        (supervisorCompany) => supervisorCompany.companyName,
      ),
    [supervisorCompanies],
  );

  const handleOwnerCompanySearch = useCallback(
    (searchValue: string) => {
      setSearchOwnerCompanyName(searchValue);
      onSearch(searchValue);
    },
    [setSearchOwnerCompanyName, onSearch],
  );

  return (
    <RowSearchInput
      label={SHIP_OWNER}
      enableShadow
      onSearch={handleOwnerCompanySearch}
      searchDelayDuration={0}
      ref={ref}
      dropdownItems={dropdownItems ?? []}
      {...rest}
    />
  );
};

const RowSearchInput = styled(FormDropdownSearchInput)`
  flex: 1;
  flex-direction: row;
  align-items: center;
  max-width: 68.4rem;
  margin-right: auto;

  label {
    min-width: 16.1rem;
  }

  input {
    border: none;
    height: 4.6rem;
    font-weight: 400;
  }
`;

export default memo(forwardRef(ShipOwnerSearchInput));
