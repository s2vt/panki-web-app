import { Meta, Story } from '@storybook/react';
import ShipOwnerSearchInput, {
  ShipOwnerSearchInputProps,
} from './ShipOwnerSearchInput';

export default {
  title: 'Components/ship/CreateShip/ShipOwnerSearchInput',
  component: ShipOwnerSearchInput,
} as Meta;

const Template: Story<ShipOwnerSearchInputProps> = (args) => (
  <div style={{ border: '1px solid black' }}>
    <ShipOwnerSearchInput {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  onSearch: (searchValue: string) => console.log(searchValue),
};
