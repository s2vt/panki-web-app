import { Meta, Story } from '@storybook/react';
import CreateShip, { CreateShipProps } from './CreateShip';

export default {
  title: 'Components/ship/CreateShip',
  component: CreateShip,
} as Meta;

const Template: Story<CreateShipProps> = (args) => <CreateShip {...args} />;

export const Basic = Template.bind({});
