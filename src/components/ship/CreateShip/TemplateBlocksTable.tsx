import CheckboxButton from '@src/components/common/CheckboxButton';
import ErrorBox from '@src/components/common/ErrorBox';
import RequiredIcon from '@src/components/common/RequiredIcon';
import {
  ALL_SELECT,
  CREATE_BLOCK,
  FAIL_DATA_FETCH,
} from '@src/constants/constantString';
import { Block, BlockState } from '@src/types/block.types';
import { SelectItem } from '@src/types/common.types';
import styled from 'styled-components';
import BlockTable from '../BlockTable';
import useTemplateBlocksQuery from './hooks/useTemplateBlocksQuery';

export type TemplateBlocksTableProps = {
  isBlockSelected: (block: SelectItem & Block) => boolean;
  onCheckboxChange: (block: SelectItem & Block) => void;
  onAllCheckClick: () => void;
  isAllCheckboxChecked: boolean;
};

const TemplateBlocksTable = ({
  isBlockSelected,
  onCheckboxChange,
  onAllCheckClick,
  isAllCheckboxChecked,
}: TemplateBlocksTableProps) => {
  const {
    data: blocks,
    isFetching,
    isError,
    refetch,
  } = useTemplateBlocksQuery({ enabled: false });

  if (isFetching) {
    return <BlockTable.Loader />;
  }

  if (isError) {
    return (
      <BlockTable.LoadingWrapper>
        <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
      </BlockTable.LoadingWrapper>
    );
  }

  return (
    <>
      <BlockTableTitleSection>
        <CheckboxSection>
          <p>
            {CREATE_BLOCK}
            <RequiredIcon />
          </p>
          <CheckboxButton
            label={ALL_SELECT}
            onClick={onAllCheckClick}
            checkboxIcon="disableCheckbox"
            checked={isAllCheckboxChecked}
          />
        </CheckboxSection>
        <RequiredSection>
          (<RequiredIcon />
          )필수 입력 항목
        </RequiredSection>
      </BlockTableTitleSection>
      {blocks && (
        <BlockTable
          blocks={blocks}
          mode="selection"
          isSelected={isBlockSelected}
          onCheckboxChange={onCheckboxChange}
          selectionType="disable"
          selectableStates={[BlockState.CREATE]}
          enableMultipleSelection
        />
      )}
    </>
  );
};

const BlockTableTitleSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1.7rem 0 1.4rem 3.5rem;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 2.2rem;
  font-weight: 700;
`;

const CheckboxSection = styled.div`
  display: flex;
  gap: 3rem;
`;

const RequiredSection = styled.p`
  font-size: 1.6rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
`;

export default TemplateBlocksTable;
