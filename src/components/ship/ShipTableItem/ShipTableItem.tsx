import {
  FINISHED,
  NEED_REQUEST_INSPECTION,
  PROGRESS,
} from '@src/constants/constantString';
import { Ship, ShipState } from '@src/types/ship.types';
import { debugAssert } from '@src/utils/commonUtil';
import TableItem from '@src/components/table/TableItem';

export type ShipTableItemProps = {
  ship: Ship;
  onClick: (ship: Ship) => void;
  onCheckboxChange?: (ship: Ship) => void;
  checked?: boolean;
  isOddItem: boolean;
};

const ShipTableItem = ({
  ship,
  onClick,
  onCheckboxChange,
  checked,
  isOddItem,
}: ShipTableItemProps) => {
  const { id, shipName, ownerCompany, qmManager, state } = ship;

  const convertStateText = (state: ShipState) => {
    switch (state) {
      case ShipState.NEED_REQUEST_INSPECTION:
        return NEED_REQUEST_INSPECTION;
      case ShipState.WORKING:
        return PROGRESS;
      case ShipState.FINISHED:
        return FINISHED;
      default:
        debugAssert(false, `Ship state is not includes ${state}`);
        return '';
    }
  };

  const qmManagerText =
    qmManager.length > 1
      ? `${qmManager[0].userName} 외 ${qmManager.length - 1}명`
      : qmManager[0].userName;

  const userTableRows = [
    shipName,
    ownerCompany,
    qmManagerText,
    convertStateText(state),
  ];

  return (
    <TableItem
      id={id}
      rows={userTableRows}
      isOddItem={isOddItem}
      onClick={() => onClick(ship)}
      onCheckboxChange={() =>
        onCheckboxChange !== undefined && onCheckboxChange(ship)
      }
      checked={checked}
    />
  );
};

export default ShipTableItem;
