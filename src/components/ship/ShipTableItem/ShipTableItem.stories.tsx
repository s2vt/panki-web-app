import { Meta, Story } from '@storybook/react';
import ShipTableItem, { ShipTableItemProps } from './ShipTableItem';

export default {
  title: 'Components/Ship/ShipTableItem',
  component: ShipTableItem,
} as Meta;

const Template: Story<ShipTableItemProps> = (args) => (
  <ShipTableItem {...args} />
);

export const Basic = Template.bind({});
