/* eslint-disable react/no-array-index-key */
import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { v4 as uuidV4 } from 'uuid';
import {
  CLOSE,
  SAVE,
  ASSIGNED_QUALITY_MANAGER,
  SHIP_NUMBER,
  SHIP_OWNER,
} from '@src/constants/constantString';
import {
  REQUIRED_VALIDATOR,
  SHIP_NAME_VALIDATOR,
} from '@src/constants/validators';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useMask from '@src/hooks/common/useMask';
import { SaveShipPayload, Ship } from '@src/types/ship.types';
import { CompanyUser } from '@src/types/user.types';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import Icon from '@src/components/common/Icon';
import MainButton from '@src/components/common/MainButton';
import Input from '@src/components/form/Input';
import RowFormInput from '@src/components/form/RowFormInput';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import ModalWrapper from '@src/components/base/ModalWrapper';
import SearchUserModal from '../SearchUserModal';
import ShipFormBase from '../ShipFormBase';
import ShipOwnerSearchInput from '../CreateShip/ShipOwnerSearchInput/ShipOwnerSearchInput';

export type SaveShipFormProps = {
  onClickClose: () => void;
  onSubmit: (saveShipPayload: SaveShipPayload) => void;
  disabledSubmit: boolean;
  ship?: Ship;
};

const SaveShipForm = ({
  onClickClose,
  onSubmit,
  disabledSubmit,
  ship,
}: SaveShipFormProps) => {
  const [isLoading, setIsLoading] = useState(true);
  const [managerInputItems, setManagerInputItems] = useState(
    new Map<string, CompanyUser | null>(),
  );
  const [selectedInputId, setSelectedInputId] = useState<string>();

  const managerIds = useMemo(
    () =>
      Array.from(managerInputItems).reduce((total, [, user]) => {
        if (user !== null) {
          total.push(user.id);
        }

        return total;
      }, [] as string[]),
    [managerInputItems],
  );

  const { handleSubmit, register, setValue, setFocus } =
    useForm<SaveShipPayload>({
      defaultValues: {
        shipName: ship?.shipName,
        ownerCompanyName: ship?.ownerCompany,
      },
    });

  const [isSearchUserModalOpen, setIsSearchUserModalOpen] = useState(false);

  const { maskEnglishNumber } = useMask();
  const { openAlertDialog } = useAlertDialogAction();

  useEffect(() => {
    if (!isLoading) {
      setFocus('shipName');
    }
  }, [isLoading]);

  useEffect(() => {
    if (ship !== undefined) {
      const managers = ship.qmManager.map((manager) => manager);

      setManagerInputItems(
        new Map(managers.map((manager) => [uuidV4(), manager])),
      );
    } else {
      setManagerInputItems(
        new Map([
          [uuidV4(), null],
          [uuidV4(), null],
        ]),
      );
    }

    setIsLoading(false);
  }, [ship, setManagerInputItems, setIsLoading]);

  const handleShipNameChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const maskedValue = maskEnglishNumber(e.target.value);
      e.target.value = maskedValue;
      setValue('shipName', maskedValue);
    },
    [setValue, maskEnglishNumber],
  );

  const handleSearchEmployeeClick = useCallback(
    (id: string) => {
      setSelectedInputId(id);
      setIsSearchUserModalOpen(true);
    },
    [setSelectedInputId, setIsSearchUserModalOpen],
  );

  const handleSearchUserModalClose = useCallback(() => {
    setIsSearchUserModalOpen(false);
  }, [setIsSearchUserModalOpen]);

  const handleSearchInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      e.target.value = e.target.value.toUpperCase();
    },
    [],
  );

  const handleSelectUserSubmit = useCallback(
    (companyUser: CompanyUser) => {
      const { id } = companyUser;

      if (managerIds.includes(id)) {
        openAlertDialog({
          text: '이미 선택한 QM입니다.',
          position: 'rightTop',
        });
        return;
      }

      if (selectedInputId !== undefined) {
        setManagerInputItems(
          (prevManagerIds) =>
            new Map(prevManagerIds.set(selectedInputId, companyUser)),
        );
      }
    },
    [managerIds, selectedInputId, setManagerInputItems],
  );

  const handleOwnerCompanySearch = useCallback(
    (searchValue: string) => {
      setValue('ownerCompanyName', searchValue);
    },
    [setValue],
  );

  const handleRemoveInputClick = useCallback(
    (id: string) => {
      setManagerInputItems((prevInputItem) => {
        const copiedItem = new Map(prevInputItem);
        copiedItem.delete(id);
        return copiedItem;
      });
    },
    [setManagerInputItems],
  );

  const handleMangeInputClearClick = (id: string) => {
    setManagerInputItems((prevInputItem) => {
      const copiedItem = new Map(prevInputItem);
      copiedItem.set(id, null);
      return copiedItem;
    });
  };

  const renderManagerInput = ({
    id,
    renderRemoveIcon = false,
  }: {
    id: string;
    renderRemoveIcon?: boolean;
  }) => (
    <>
      <StyledInput
        inputWidth="52.3rem"
        value={managerInputItems.get(id)?.userName || ''}
        enableShadow
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          e.preventDefault()
        }
        onClick={() => handleSearchEmployeeClick(id)}
        isShowClearIcon={managerInputItems.get(id)?.userName !== undefined}
        onClearClick={() => handleMangeInputClearClick(id)}
      />
      {renderRemoveIcon && (
        <TrashIcon
          icon="trashIcon"
          onClick={() => handleRemoveInputClick(id)}
        />
      )}
    </>
  );

  const handleAddManagerClick = useCallback(() => {
    setManagerInputItems(
      (prevInputItems) => new Map(prevInputItems.set(uuidV4(), null)),
    );
  }, [setManagerInputItems]);

  const handleFormSubmit = useCallback(
    (saveShipPayload: SaveShipPayload) => {
      if (managerIds.length === 0) {
        openAlertDialog({ text: '담당 QM을 지정하세요', position: 'rightTop' });
        return;
      }

      onSubmit({ ...saveShipPayload, managerIds });
    },
    [managerIds, onSubmit],
  );

  return (
    <>
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <ShipFormBase.InputSection>
          {isLoading ? (
            <LoadingSpinner />
          ) : (
            <>
              <Row>
                <RowFormInput
                  label={SHIP_NUMBER}
                  isRequired
                  labelFontSize="1.6rem"
                  enableShadow
                  {...register('shipName', SHIP_NAME_VALIDATOR)}
                  onChange={handleShipNameChange}
                  labelPadding={0}
                />
              </Row>
              <Row>
                <ShipOwnerSearchInput
                  label={SHIP_OWNER}
                  labelFontSize="1.6rem"
                  {...register('ownerCompanyName', REQUIRED_VALIDATOR)}
                  onSearch={handleOwnerCompanySearch}
                  onChange={handleSearchInputChange}
                  enableShadow
                  labelPadding={0}
                />
              </Row>
              {Array.from(managerInputItems).map(([id], index) =>
                index === 0 ? (
                  <Row key={index}>
                    <Label>{ASSIGNED_QUALITY_MANAGER}</Label>
                    {renderManagerInput({ id })}
                  </Row>
                ) : (
                  <ShipFormBase.BlankRow key={index}>
                    {renderManagerInput({ id, renderRemoveIcon: true })}
                  </ShipFormBase.BlankRow>
                ),
              )}
              <ShipFormBase.BlankRow>
                <MainButton
                  width="52.3rem"
                  height="4.6rem"
                  label="담당 QM 추가하기"
                  fontSize="1.6rem"
                  onClick={handleAddManagerClick}
                />
              </ShipFormBase.BlankRow>
            </>
          )}
        </ShipFormBase.InputSection>
        <ShipFormBase.ButtonSection>
          <MainButton
            type="submit"
            label={addWhiteSpacesBetweenCharacters(SAVE)}
            disabled={disabledSubmit}
          />
          <MainButton
            label={addWhiteSpacesBetweenCharacters(CLOSE)}
            onClick={onClickClose}
            backgroundColor="white"
            labelColor="primary"
          />
        </ShipFormBase.ButtonSection>
      </form>
      <ModalWrapper isOpen={isSearchUserModalOpen}>
        <SearchUserModal
          onClose={handleSearchUserModalClose}
          onSubmit={handleSelectUserSubmit}
        />
      </ModalWrapper>
    </>
  );
};

const Row = memo(styled.div`
  align-items: center;
  display: flex;
`);

const Label = styled.p`
  min-width: 16.1rem;
  font-size: 1.6rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const StyledInput = styled(Input)`
  height: 4.6rem;
  border: none;
  cursor: pointer;
  font-weight: 400;
`;

const TrashIcon = styled(Icon)`
  margin-left: 4rem;
  cursor: pointer;
`;

export default memo(SaveShipForm);
