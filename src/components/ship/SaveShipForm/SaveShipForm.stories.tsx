import { Meta, Story } from '@storybook/react';
import SaveShipForm, { SaveShipFormProps } from './SaveShipForm';

export default {
  title: 'Components/Ship/SaveShipForm',
  component: SaveShipForm,
} as Meta;

const Template: Story<SaveShipFormProps> = (args) => <SaveShipForm {...args} />;

export const Basic = Template.bind({});
