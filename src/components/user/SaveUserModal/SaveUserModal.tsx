import { useEffect, useMemo } from 'react';
import { SaveUserPayload } from '@src/types/user.types';
import {
  ASSIGN_ROLE,
  REVIEW_AND_CONFIRM,
  USER_INFORMATION,
} from '@src/constants/constantString';
import ModalTitle from '@src/components/base/ModalTitle';
import { debugAssert } from '@src/utils/commonUtil';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import useSaveUserModalAction from '@src/hooks/user/useSaveUserModalAction';
import AssignRolesForm from './AssignRolesForm';
import SaveUserReview from './SaveUserReview';
import SaveUserForm from './SaveUserForm';

export type SaveUserModalProps = {
  /** 직원 추가 시 실행할 이벤트 */
  onSubmit: (saveUserPayload: SaveUserPayload) => void;
  /** 닫기 버튼 혹은 백그라운드 클릭 시 실행할 이벤트 */
  onClose: () => void;
  /** 유저 저장 진행 중 여부  */
  isSubmitting: boolean;
};

/** 관리자가 새로운 직원을 추가하거나 수정 시 사용할 모달 컴포넌트입니다. */
const SaveUserModal = ({
  onSubmit,
  onClose,
  isSubmitting,
}: SaveUserModalProps) => {
  const { page, saveUserPayload } = useSaveUserModalSelector();
  const { reset } = useSaveUserModalAction();

  useEffect(() => reset, []);

  const modalTitle = useMemo(() => {
    switch (page) {
      case 'userInfo':
        return USER_INFORMATION;
      case 'assignRoles':
        return ASSIGN_ROLE;
      case 'review':
        return REVIEW_AND_CONFIRM;
      default:
        debugAssert(false, 'this value is not includes save user modal pages');
        return '';
    }
  }, [page]);

  const handleSaveUserReviewSubmit = ({
    checkSendPassword,
  }: {
    checkSendPassword: boolean;
  }) => {
    onSubmit({ ...saveUserPayload, checkSendPassword });
  };

  return (
    <>
      <ModalTitle title={modalTitle} onClose={onClose} />
      {(() => {
        switch (page) {
          case 'userInfo':
            return <SaveUserForm />;
          case 'assignRoles':
            return <AssignRolesForm />;
          case 'review':
            return (
              <SaveUserReview
                onSubmit={handleSaveUserReviewSubmit}
                isSubmitting={isSubmitting}
              />
            );
          default:
            debugAssert(
              false,
              `${page} is not included into save user modal pages`,
            );
            return null;
        }
      })()}
    </>
  );
};

export default SaveUserModal;
