import React, { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import styled, { css } from 'styled-components';
import {
  ASSIGN_ROLE,
  AUTO_GENERATE,
  CATEGORY,
  MODIFY,
  PASSWORD,
  PREVIOUS,
  SAVE,
  SEND_SMS,
  USER_ACCOUNT,
} from '@src/constants/constantString';
import { AuthInputName } from '@src/types/auth.types';
import { convertUserClassText } from '@src/types/user.types';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import MainButton from '@src/components/common/MainButton';
import ModifyButton from '@src/components/common/ModifyButton';
import FormCheckbox from '@src/components/form/FormCheckbox';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import useSaveUserModalAction from '../../../hooks/user/useSaveUserModalAction';

export type SaveUserReviewProps = {
  /** 최종 제출시 실행할 이벤트 */
  onSubmit: ({ checkSendPassword }: { checkSendPassword: boolean }) => void;
  /** 유저 저장 진행 중 여부  */
  isSubmitting: boolean;
};

const SaveUserReview = ({ onSubmit, isSubmitting }: SaveUserReviewProps) => {
  const { saveUserPayload } = useSaveUserModalSelector();
  const {
    setIsModifying,
    setHasCompleted,
    setPayloadCheckSendPassword,
    setPage,
  } = useSaveUserModalAction();

  useEffect(() => {
    setHasCompleted(true);
  }, []);

  const handleModifyUserInfoClick = useCallback(() => {
    setIsModifying(true);
    setPage('userInfo');
  }, [setIsModifying, setPage]);

  const handleModifyRolesClick = useCallback(() => {
    setIsModifying(true);
    setPage('assignRoles');
  }, [setIsModifying, setPage]);

  const handlePreviousClick = useCallback(() => {
    setIsModifying(false);
    setPage('assignRoles');
  }, [setIsModifying, setPage]);

  const { register, handleSubmit } = useForm<{
    checkSendPassword: boolean;
  }>({
    defaultValues: {
      checkSendPassword: saveUserPayload?.checkSendPassword ?? false,
    },
  });

  const handleSendSmsChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setPayloadCheckSendPassword(e.target.checked);
    },
    [setPayloadCheckSendPassword],
  );

  if (saveUserPayload === undefined) {
    return null;
  }

  const {
    userName,
    userId,
    email,
    phone,
    className,
    checkAutoGeneratePassword,
  } = saveUserPayload;

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <FlexibleSection>
        <InfoTitle>
          <b>{USER_ACCOUNT}</b>
          <ModifyButton
            type="button"
            label={MODIFY}
            onClick={handleModifyUserInfoClick}
          />
        </InfoTitle>
        <DottedLine />
        <DescriptionSection>
          <p>{userName}</p>
          <p>{userId}</p>
          <p>{email}</p>
          <p>{phone}</p>
          <p>{convertUserClassText(className)}</p>
        </DescriptionSection>
      </FlexibleSection>
      {checkAutoGeneratePassword && (
        <FlexibleSection>
          <InfoTitle>
            <b>{PASSWORD}</b>
          </InfoTitle>
          <DottedLine />
          <DescriptionSection>
            {`${CATEGORY} : ${AUTO_GENERATE}`}
          </DescriptionSection>
          <StyledFormCheckbox
            label={SEND_SMS}
            checkboxIcon="blankCheckbox"
            checkboxSize="sm"
            {...register(AuthInputName.checkSendPassword)}
            onChange={handleSendSmsChange}
          />
          <DescriptionSection>
            <p>{`${phone} 으로 자동 생성된\n 비밀번호를 전송합니다.`}</p>
          </DescriptionSection>
        </FlexibleSection>
      )}
      <FlexibleSection>
        <InfoTitle>
          <b>{ASSIGN_ROLE}</b>
          <ModifyButton
            type="button"
            label={MODIFY}
            onClick={handleModifyRolesClick}
          />
        </InfoTitle>
        <DottedLine />
        <DescriptionSection>
          {saveUserPayload.roles?.map((role, index, roles) => (
            <span key={role.id}>
              {index < roles.length - 1 ? `${role.roleKR},` : role.roleKR}
            </span>
          ))}
        </DescriptionSection>
      </FlexibleSection>
      <ButtonSection checkAutoGeneratePassword={checkAutoGeneratePassword}>
        <MainButton
          onClick={handlePreviousClick}
          label={addWhiteSpacesBetweenCharacters(PREVIOUS)}
          backgroundColor="white"
          labelColor="primary"
        />
        <MainButton
          type="submit"
          label={addWhiteSpacesBetweenCharacters(SAVE)}
          disabled={isSubmitting}
        />
      </ButtonSection>
    </Form>
  );
};

const Form = styled.form`
  width: 46.875vw;
  min-height: 27vh;
  max-height: 80vh;
  padding: 4.5rem 6.1rem 5.3rem 7.3rem;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  overflow-y: auto;
  gap: 2rem;
`;

const FlexibleSection = styled.div`
  flex: 1 1 33.8rem;
`;

const DottedLine = styled.div`
  width: 100%;
  margin: 2rem 0;
  background-image: linear-gradient(
    to right,
    ${({ theme }) => theme.colors.primary} 33%,
    ${({ theme }) => theme.colors.white} 0%
  );
  background-position: bottom;
  background-size: 2.93px 1.62px;
  background-repeat: repeat-x;
  padding-bottom: 10px;
`;

const InfoTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1.8rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const DescriptionSection = styled.div`
  font-size: 1.6rem;
  font-weight: 400;
  line-height: 2.664rem;
  color: ${({ theme }) => theme.colors.primary};
  overflow-wrap: break-word;
`;

const StyledFormCheckbox = styled(FormCheckbox)`
  margin-top: 3.7rem;
  padding: 0;

  p {
    margin-left: 1.3rem;
    font-size: 1.6rem;
    font-weight: 700;
  }
`;

const ButtonSection = styled(FlexibleSection)<{
  checkAutoGeneratePassword?: boolean;
}>`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  gap: 0 3.1rem;

  ${({ checkAutoGeneratePassword }) =>
    !checkAutoGeneratePassword &&
    css`
      max-width: 100%;
      flex-basis: 100%;
    `}

  button {
    width: 14.6rem;
    height: 5rem;
    font-size: 1.6rem;
    font-weight: 400;
  }
`;

export default SaveUserReview;
