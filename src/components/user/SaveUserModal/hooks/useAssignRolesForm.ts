import { SELECT_ROLES_ERROR } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useSaveUserModalAction from '@src/hooks/user/useSaveUserModalAction';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import { Role } from '@src/types/user.types';
import { useCallback, useEffect } from 'react';

type UseAssignRolesFormProps = {
  selectedRoles: Role[];
};

const useAssignRolesForm = ({ selectedRoles }: UseAssignRolesFormProps) => {
  const { saveUserPayload } = useSaveUserModalSelector();
  const { setPage, setPayloadRoles } = useSaveUserModalAction();
  const { openAlertDialog } = useAlertDialogAction();

  useEffect(() => {
    setPayloadRoles(selectedRoles);
  }, [selectedRoles]);

  const handlePreviousClick = useCallback(() => setPage('userInfo'), [setPage]);

  const handleNextClick = useCallback(() => {
    if (
      saveUserPayload?.roles === undefined ||
      saveUserPayload.roles.length === 0
    ) {
      openAlertDialog({ text: SELECT_ROLES_ERROR, position: 'rightTop' });
      return;
    }

    setPage('review');
  }, [saveUserPayload?.roles, openAlertDialog]);

  return { handlePreviousClick, handleNextClick };
};

export default useAssignRolesForm;
