import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import { SelectItem } from '@src/types/common.types';
import { Role, UserClass } from '@src/types/user.types';
import React, { useCallback, useEffect, useMemo } from 'react';
import useAllRolesQuery from './useAllRolesQuery';
import useClassRolesQuery from './useClassRolesQuery';

type UseRolesQueriesProps = {
  className: UserClass | undefined;
  classRolesQueryEnabled: boolean;
  setSelectedRoles: React.Dispatch<React.SetStateAction<(SelectItem & Role)[]>>;
};

const useRolesQueries = ({
  className,
  classRolesQueryEnabled,
  setSelectedRoles,
}: UseRolesQueriesProps) => {
  const { hasCompleted } = useSaveUserModalSelector();

  const sortRolesByClassRoles = (roles: Role[]) =>
    [...roles]?.sort((a, b) => {
      if (classRoles?.some((classRole) => classRole.id === a.id)) {
        return -1;
      }

      if (classRoles?.some((classRole) => classRole.id === b.id)) {
        return 1;
      }

      return 0;
    });

  const {
    data: classRoles,
    isFetching: isClassRolesFetching,
    isError: isFetchClassRolesError,
    refetch: refetchClassRoles,
  } = useClassRolesQuery(className, {
    enabled: classRolesQueryEnabled,
  });

  const {
    data: roles,
    isFetching: isRolesFetching,
    isError: isFetchRolesError,
    refetch: refetchRoles,
  } = useAllRolesQuery({
    select: sortRolesByClassRoles,
  });

  const isFetching = useMemo(
    () => isRolesFetching || isClassRolesFetching,
    [isRolesFetching, isClassRolesFetching],
  );

  const isError = useMemo(
    () => isFetchRolesError || isFetchClassRolesError,
    [isFetchRolesError, isFetchClassRolesError],
  );

  const refetch = useCallback(() => {
    refetchRoles();
    refetchClassRoles();
  }, [refetchRoles, refetchClassRoles]);

  useEffect(() => {
    if (!hasCompleted && classRoles !== undefined) {
      setSelectedRoles(classRoles);
    }
  }, [classRoles]);

  return {
    roles,
    isFetching,
    isError,
    refetch,
  };
};

export default useRolesQueries;
