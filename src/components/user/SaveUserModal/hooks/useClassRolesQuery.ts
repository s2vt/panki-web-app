import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import rolesApi from '@src/api/rolesApi';
import { Role, UserClass } from '@src/types/user.types';

const useClassRolesQuery = (
  className: UserClass | undefined,
  options?: UseQueryOptions<Role[], HttpError>,
) =>
  useQuery(
    createKey(className),
    () => rolesApi.getClassRoles(className),
    options,
  );

const createKey = (className: UserClass | undefined) => [
  'classRoles',
  className,
];

useClassRolesQuery.createKey = createKey;

export default useClassRolesQuery;
