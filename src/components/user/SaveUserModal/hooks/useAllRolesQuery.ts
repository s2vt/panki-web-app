import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import rolesApi from '@src/api/rolesApi';
import { Role } from '@src/types/user.types';

const useAllRolesQuery = (options?: UseQueryOptions<Role[], HttpError>) =>
  useQuery(createKey(), rolesApi.getAllRoles, options);

const baseKey = ['allRoles'];
const createKey = () => [...baseKey];

useAllRolesQuery.baseKey = baseKey;
useAllRolesQuery.createKey = createKey;

export default useAllRolesQuery;
