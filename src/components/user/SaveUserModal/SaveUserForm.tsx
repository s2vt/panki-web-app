import { memo, useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { useQueryClient } from 'react-query';
import {
  EMAIL,
  SHORT_EMPLOYEE_ID,
  ENG_NAME,
  KOR_NAME,
  ORGANIZATION,
  USER_CLASS,
  PHONE_NUMBER,
  AUTO_GENERATE_PASSWORD,
  NEXT,
  REQUIRED_INPUT_ERROR,
  KOREAN_NAME_PLACEHOLDER,
  ENGLISH_NAME_PLACEHOLDER,
  SAVE,
} from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useMask from '@src/hooks/common/useMask';
import {
  Company,
  convertUserClassText,
  getUserClassLowerThanTargetClass,
  SaveUserPayload,
  UserClass,
} from '@src/types/user.types';
import { AuthInputName } from '@src/types/auth.types';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import {
  EMAIL_VALIDATOR,
  ID_VALIDATOR,
  PHONE_VALIDATOR,
  USER_NAME_VALIDATOR,
} from '@src/constants/validators';
import useOrganizationChartQuery from '@src/hooks/company/useOrganizationChartQuery';
import useUserSelector from '@src/hooks/user/useUserSelector';
import FormInput from '@src/components/form/FormInput';
import MainButton from '@src/components/common/MainButton';
import FormCheckbox from '@src/components/form/FormCheckbox';
import FormDropdown from '@src/components/form/FormDropdown';
import RequiredIcon from '@src/components/common/RequiredIcon';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import useSaveUserModalAction from '@src/hooks/user/useSaveUserModalAction';

const SaveUserForm = () => {
  const queryClient = useQueryClient();
  const { maskPhoneNumber, maskKorean, maskEnglish } = useMask();

  const { user } = useUserSelector();
  const { openAlertDialog } = useAlertDialogAction();
  const { isModifying, mode, saveUserPayload } = useSaveUserModalSelector();
  const { handleSaveUserFormSubmit } = useSaveUserModalAction();

  const {
    handleSubmit,
    register,
    setValue,
    setFocus,
    formState: { errors, submitCount },
  } = useForm<SaveUserPayload>({
    defaultValues: saveUserPayload,
  });

  const company = queryClient.getQueryData<Company>(
    useOrganizationChartQuery.createKey(),
  );

  const companyNames = useMemo(
    () =>
      company?.children
        ?.map((child) => child.companyName)
        .concat(company.companyName),
    [company],
  );

  const companySelectItems = useMemo(
    () =>
      companyNames?.map((companyName) => ({
        id: companyName,
        label: companyName,
      })) ?? [],
    [companyNames],
  );

  const userClassSelectItems = useMemo(
    () =>
      getUserClassLowerThanTargetClass(
        user?.className ?? UserClass.employee,
      ).map((userClass) => ({
        id: userClass,
        label: convertUserClassText(userClass),
      })),
    [user, getUserClassLowerThanTargetClass, convertUserClassText],
  );

  useEffect(() => {
    if (errors && submitCount > 0) {
      openAlertDialog({
        text: REQUIRED_INPUT_ERROR,
        position: 'rightTop',
      });
    }
  }, [submitCount]);

  useEffect(() => {
    setFocus(AuthInputName.userName);
  }, []);

  useEffect(() => {
    if (saveUserPayload?.className !== undefined) {
      const { className } = saveUserPayload;

      setValue(AuthInputName.className, className);
    }
  }, [saveUserPayload]);

  const handleKoreanNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(AuthInputName.userName, maskKorean(e.target.value));
  };

  const handleEnglishNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(AuthInputName.engName, maskEnglish(e.target.value.toUpperCase()));
  };

  const handlePhoneChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(AuthInputName.phone, maskPhoneNumber(e.target.value));
  };

  return (
    <Box>
      <RequiredSection>
        필수 입력 항목(
        <RequiredIcon />
        )을 확인하세요.
      </RequiredSection>
      <Form onSubmit={handleSubmit(handleSaveUserFormSubmit)}>
        <InputSection>
          <FormInput
            borderColor="border"
            isRequired
            label={KOR_NAME}
            labelFontSize="1.8rem"
            placeholder={KOREAN_NAME_PLACEHOLDER}
            {...register(AuthInputName.userName, USER_NAME_VALIDATOR)}
            onChange={handleKoreanNameChange}
          />
          <FormInput
            borderColor="border"
            isRequired
            label={ENG_NAME}
            labelFontSize="1.8rem"
            placeholder={ENGLISH_NAME_PLACEHOLDER}
            {...register(AuthInputName.engName, USER_NAME_VALIDATOR)}
            onChange={handleEnglishNameChange}
          />
          <FormInput
            borderColor="border"
            isRequired
            label={SHORT_EMPLOYEE_ID}
            labelFontSize="1.8rem"
            {...register(AuthInputName.userId, ID_VALIDATOR)}
          />
          <FormInput
            borderColor="border"
            isRequired
            labelFontSize="1.8rem"
            label={EMAIL}
            {...register(AuthInputName.email, EMAIL_VALIDATOR)}
          />
          <FormDropdown
            label={ORGANIZATION}
            labelFontSize="1.8rem"
            isRequired
            dropdownItems={companySelectItems}
            {...register(AuthInputName.company)}
          />
          <FormInput
            borderColor="border"
            isRequired
            label={PHONE_NUMBER}
            labelFontSize="1.8rem"
            maxLength={13}
            {...register(AuthInputName.phone, PHONE_VALIDATOR)}
            onChange={handlePhoneChange}
          />
          <FormDropdown
            label={USER_CLASS}
            labelFontSize="1.8rem"
            isRequired
            dropdownItems={userClassSelectItems}
            {...register(AuthInputName.className)}
          />
        </InputSection>
        <ButtonSection>
          <StyledFormCheckbox
            label={AUTO_GENERATE_PASSWORD}
            checkboxIcon="blankCheckbox"
            inactive={mode === 'add'}
            {...(mode === 'add' && { checked: true, readOnly: true })}
            {...register('checkAutoGeneratePassword')}
          />
          <NextButton
            type="submit"
            label={
              isModifying
                ? addWhiteSpacesBetweenCharacters(SAVE)
                : addWhiteSpacesBetweenCharacters(NEXT)
            }
          />
        </ButtonSection>
      </Form>
    </Box>
  );
};

const Box = styled.div`
  width: 46.875vw;
  max-height: 80vh;
  padding: 4.1rem 5.1rem 4.7rem 9rem;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
`;

const RequiredSection = memo(styled.div`
  display: flex;
  justify-content: flex-end;
  font-size: 1.6rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
`);

const Form = styled.form`
  flex: 1;
  padding: 2.1rem 0 0;
  display: flex;
  flex-direction: column;
`;

const InputSection = styled.div`
  display: grid;
  align-items: stretch;
  grid-template-columns: repeat(auto-fill, minmax(22rem, 1fr));
  gap: 2rem 8.1rem;
`;

const ButtonSection = styled.div`
  padding-top: 2.6rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  white-space: nowrap;
  gap: 2rem 8.1rem;
`;

const StyledFormCheckbox = styled(FormCheckbox)`
  flex: 1 1 22rem;
  padding: 0;
`;

const NextButton = styled(MainButton)`
  flex: 1 1 22rem;
  height: 5.6rem;
  font-size: 2rem;
`;

export default SaveUserForm;
