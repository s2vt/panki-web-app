import HttpError from '@src/api/model/HttpError';
import rolesApi from '@src/api/rolesApi';
import { RootState } from '@src/reducers/rootReducer';
import rootState from '@src/test/fixtures/reducers/rootState';
import saveUserModalState from '@src/test/fixtures/reducers/saveUserModalState';
import roles from '@src/test/fixtures/user/roles';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { Role, UserClass } from '@src/types/user.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import useAllRolesQuery from '../hooks/useAllRolesQuery';
import useClassRolesQuery from '../hooks/useClassRolesQuery';
import useRolesQueries from '../hooks/useRolesQueries';

describe('useRolesQueries hook', () => {
  const setup = ({
    className = 'Admin',
    classRolesQueryEnabled = true,
    hasCompleted = false,
  }: {
    className?: UserClass;
    classRolesQueryEnabled?: boolean;
    hasCompleted?: boolean;
  }) => {
    const initialState: RootState = {
      ...rootState,
      saveUserModal: {
        ...saveUserModalState,
        hasCompleted,
      },
    };

    const { wrapper, queryClient, store } = prepareWrapper(initialState);

    const setSelectedRoles = jest.fn();

    const { result, waitFor, waitForValueToChange } = renderHook(
      () =>
        useRolesQueries({
          className,
          classRolesQueryEnabled,
          setSelectedRoles,
        }),
      {
        wrapper,
      },
    );

    return {
      result,
      queryClient,
      store,
      waitFor,
      waitForValueToChange,
      setSelectedRoles,
    };
  };

  describe('query state', () => {
    it('isFetching return true when all roles fetching', async () => {
      // given
      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, queryClient } = setup({});

      const allRolesQueryState = queryClient.getQueryState(
        useAllRolesQuery.createKey(),
      );

      // when

      // then
      expect(allRolesQueryState?.isFetching).toEqual(true);
      expect(result.current.isFetching).toEqual(true);
    });

    it('isFetching return true when class roles fetching', async () => {
      // given
      const className: UserClass = UserClass.admin;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, queryClient } = setup({ className });

      const classRolesQueryState = queryClient.getQueryState(
        useClassRolesQuery.createKey(className),
      );

      // when

      // then
      expect(classRolesQueryState?.isFetching).toEqual(true);
      expect(result.current.isFetching).toEqual(true);
    });

    it('isFetching return false when roles not fetching', async () => {
      // given
      const className: UserClass = UserClass.admin;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, queryClient, waitForValueToChange } = setup({
        className,
      });

      // when
      await waitForValueToChange(() => result.current.isFetching);

      const allRolesQueryState = queryClient.getQueryState(
        useAllRolesQuery.createKey(),
      );

      const classRolesQueryState = queryClient.getQueryState(
        useClassRolesQuery.createKey(className),
      );

      // then
      expect(allRolesQueryState?.isFetching).toEqual(false);
      expect(classRolesQueryState?.isFetching).toEqual(false);
      expect(result.current.isFetching).toEqual(false);
    });

    it('isError return true when failed all roles fetching', async () => {
      // given
      const error: HttpError = new HttpError(
        HttpErrorStatus.InternalServerError,
        ResultCode.error,
      );

      rolesApi.getAllRoles = jest.fn().mockRejectedValue(error);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, queryClient, waitForValueToChange } = setup({});

      // when
      await waitForValueToChange(() => result.current.isError);

      const allRolesQueryState = queryClient.getQueryState(
        useAllRolesQuery.createKey(),
      );

      // then
      expect(allRolesQueryState?.error).toEqual(error);
      expect(result.current.isError).toEqual(true);
    });

    it('isError return true when failed class roles fetching', async () => {
      // given
      const className: UserClass = UserClass.admin;

      const error: HttpError = new HttpError(
        HttpErrorStatus.InternalServerError,
        ResultCode.error,
      );

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockRejectedValue(error);

      const { result, queryClient, waitForValueToChange } = setup({});

      // when
      await waitForValueToChange(() => result.current.isError);

      const classRolesQueryState = queryClient.getQueryState(
        useClassRolesQuery.createKey(className),
      );

      // then
      expect(classRolesQueryState?.error).toEqual(error);
      expect(result.current.isError).toEqual(true);
    });

    it('isError return false when succeed roles fetching', async () => {
      // given
      const className: UserClass = UserClass.admin;

      const error: HttpError = new HttpError(
        HttpErrorStatus.InternalServerError,
        ResultCode.error,
      );

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockRejectedValue(error);

      const { result, queryClient } = setup({ className });

      // when
      const allRolesQueryState = queryClient.getQueryState(
        useAllRolesQuery.createKey(),
      );

      const classRolesQueryState = queryClient.getQueryState(
        useClassRolesQuery.createKey(className),
      );

      // then
      expect(allRolesQueryState?.error).toBeNull();
      expect(classRolesQueryState?.error).toBeNull();
      expect(result.current.isError).toEqual(false);
    });

    it('refetch allRolesQuery and classRolesQuery when using refetch', async () => {
      // given
      const className: UserClass = UserClass.admin;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, waitFor, waitForValueToChange, queryClient } = setup({
        className,
      });

      // when
      await waitFor(() => !result.current.isFetching);

      act(() => result.current.refetch());

      await waitForValueToChange(() => result.current.isFetching);

      const allRolesQueryState = queryClient.getQueryState(
        useAllRolesQuery.createKey(),
      );

      const classRolesQueryState = queryClient.getQueryState(
        useClassRolesQuery.createKey(className),
      );

      // then
      expect(allRolesQueryState?.dataUpdateCount).toEqual(2);
      expect(classRolesQueryState?.dataUpdateCount).toEqual(2);
    });
  });

  describe('getClassRoles', () => {
    it('should call getClassRoles when classRolesQueryEnabled is true', () => {
      // given
      const className: UserClass = UserClass.admin;
      const classRolesQueryEnabled = true;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);

      const getClassRoles = jest.spyOn(rolesApi, 'getClassRoles');

      setup({ className, classRolesQueryEnabled });

      // when

      // then
      expect(getClassRoles).toBeCalledWith(className);
    });

    it("don't call getClassRoles when classRolesQueryEnabled is false", () => {
      // given
      const classRolesQueryEnabled = false;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);

      const getClassRoles = jest.spyOn(rolesApi, 'getClassRoles');

      setup({ classRolesQueryEnabled });

      // when

      // then
      expect(getClassRoles).not.toBeCalled();
    });

    it('should call setSelectedRoles when hasCompleted is false and changed class roles', async () => {
      // given
      const className: UserClass = UserClass.admin;
      const classRolesQueryEnabled = true;
      const hasCompleted = false;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, store, waitForValueToChange, setSelectedRoles } = setup({
        className,
        classRolesQueryEnabled,
        hasCompleted,
      });

      // when
      await waitForValueToChange(() => result.current.roles);

      // then
      expect(store.getState().saveUserModal.hasCompleted).toEqual(hasCompleted);
      expect(setSelectedRoles).toBeCalled();
    });

    it('not call setSelectedRoles when hasCompleted is true and changed class roles', async () => {
      // given
      const className: UserClass = UserClass.admin;
      const classRolesQueryEnabled = true;
      const hasCompleted = true;

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

      const { result, store, waitForValueToChange, setSelectedRoles } = setup({
        className,
        classRolesQueryEnabled,
        hasCompleted,
      });

      // when
      await waitForValueToChange(() => result.current.roles);

      // then
      expect(store.getState().saveUserModal.hasCompleted).toEqual(hasCompleted);
      expect(setSelectedRoles).not.toBeCalled();
    });
  });

  describe('getAllRoles', () => {
    it('sort by roles included in the class roles', async () => {
      // given
      const allRoles: Role[] = [
        {
          id: '3',
          description: 'role3',
          role: 'role3',
          roleKR: '역할3',
        },
        {
          id: '2',
          description: 'role2',
          role: 'role2',
          roleKR: '역할2',
        },
        {
          id: '1',
          description: 'role1',
          role: 'role1',
          roleKR: '역할1',
        },
      ];

      const classRoles: Role[] = [
        {
          id: '1',
          description: 'role1',
          role: 'role1',
          roleKR: '역할1',
        },
      ];

      rolesApi.getAllRoles = jest.fn().mockResolvedValue(allRoles);
      rolesApi.getClassRoles = jest.fn().mockResolvedValue(classRoles);

      const { result, waitFor } = setup({});

      // when
      await waitFor(() => result.current.roles !== undefined);

      const expectedRoles = [
        {
          id: '1',
          description: 'role1',
          role: 'role1',
          roleKR: '역할1',
        },
        {
          id: '3',
          description: 'role3',
          role: 'role3',
          roleKR: '역할3',
        },
        {
          id: '2',
          description: 'role2',
          role: 'role2',
          roleKR: '역할2',
        },
      ];

      // then
      expect(result.current.roles).toEqual(expectedRoles);
    });
  });
});
