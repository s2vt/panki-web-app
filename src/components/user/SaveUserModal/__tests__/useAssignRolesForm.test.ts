import { RootState } from '@src/reducers/rootReducer';
import {
  SaveUserModalState,
  actions as saveUserModalActions,
} from '@src/reducers/saveUserModal';
import { actions as alertDialogActions } from '@src/reducers/alertDialog';
import rootState from '@src/test/fixtures/reducers/rootState';
import saveUserModalState from '@src/test/fixtures/reducers/saveUserModalState';
import roles from '@src/test/fixtures/user/roles';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { Role } from '@src/types/user.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import useAssignRolesForm from '../hooks/useAssignRolesForm';

describe('useAssignRolesForm hook', () => {
  const setup = ({
    saveUserModal = saveUserModalState,
    selectedRoles = [],
  }: {
    saveUserModal?: SaveUserModalState;
    selectedRoles?: Role[];
  }) => {
    const initialState: RootState = {
      ...rootState,
      saveUserModal,
    };

    const { wrapper, store } = prepareMockWrapper(initialState);

    const { result, waitFor } = renderHook(
      () => useAssignRolesForm({ selectedRoles }),
      { wrapper },
    );

    return { result, waitFor, store };
  };

  describe('setPayloadRoles', () => {
    it('should call setPayloadRoles action when changed selectedRoles', () => {
      // given
      const selectedRoles: Role[] = roles;

      const { store } = setup({ selectedRoles });

      // when

      // then
      expect(store.getActions()).toContainEqual(
        saveUserModalActions.setPayloadRoles(selectedRoles),
      );
    });
  });

  describe('handlePreviousClick', () => {
    it('should call set page to userInfo action when using handlePreviousClick', () => {
      // given
      const { result, store } = setup({});

      // when
      act(() => {
        result.current.handlePreviousClick();
      });

      // then
      expect(store.getActions()).toContainEqual(
        saveUserModalActions.setPage('userInfo'),
      );
    });
  });

  describe('handleNextClick', () => {
    it("call openAlertDialog action when store's saveUserPayload roles is empty", () => {
      // given
      const saveUserModal: SaveUserModalState = {
        ...saveUserModalState,
        saveUserPayload: {
          ...saveUserPayload,
          roles: [],
        },
      };

      const { result, store } = setup({ saveUserModal });

      // when
      act(() => {
        result.current.handleNextClick();
      });

      // then
      expect(store.getActions()).toContainEqual(
        alertDialogActions.openAlertDialog({
          text: '역할을 체크하세요',
          position: 'rightTop',
        }),
      );
    });

    it("call set page to review when store's saveUserPayload roles is not empty", () => {
      // given
      const saveUserModal: SaveUserModalState = {
        ...saveUserModalState,
        saveUserPayload: {
          ...saveUserPayload,
          roles,
        },
      };

      const { result, store } = setup({ saveUserModal });

      // when
      act(() => {
        result.current.handleNextClick();
      });

      // then
      expect(store.getActions()).toContainEqual(
        saveUserModalActions.setPage('review'),
      );
    });
  });
});
