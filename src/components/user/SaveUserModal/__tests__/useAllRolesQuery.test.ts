import { renderHook } from '@testing-library/react-hooks/dom';
import rolesApi from '@src/api/rolesApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import roles from '@src/test/fixtures/user/roles';
import useAllRolesQuery from '../hooks/useAllRolesQuery';

describe('useAllRolesQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();
    const { result, waitFor } = renderHook(() => useAllRolesQuery(), {
      wrapper,
    });

    return { result, waitFor };
  };

  it('should cache data when succeed fetch roles', async () => {
    // given
    rolesApi.getAllRoles = jest.fn().mockResolvedValue(roles);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(roles);
  });

  it('should set error when failed fetch roles', async () => {
    // given
    rolesApi.getAllRoles = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
