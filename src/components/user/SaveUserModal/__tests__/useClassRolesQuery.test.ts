import { renderHook } from '@testing-library/react-hooks/dom';
import rolesApi from '@src/api/rolesApi';
import { UserClass } from '@src/types/user.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import roles from '@src/test/fixtures/user/roles';
import useClassRolesQuery from '../hooks/useClassRolesQuery';

describe('useClassRolesQuery hook', () => {
  const setup = (className: UserClass) => {
    const { wrapper } = prepareWrapper();
    const { result, waitFor } = renderHook(
      () => useClassRolesQuery(className),
      { wrapper },
    );

    return { result, waitFor };
  };

  it('should cache data when succeed fetch class roles', async () => {
    // given
    const sampleClassName = UserClass.admin;

    rolesApi.getClassRoles = jest.fn().mockResolvedValue(roles);

    // when
    const { result, waitFor } = setup(sampleClassName);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(roles);
  });

  it('should set error when failed fetch class roles', async () => {
    // given
    const sampleClassName = UserClass.admin;

    rolesApi.getClassRoles = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(sampleClassName);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
