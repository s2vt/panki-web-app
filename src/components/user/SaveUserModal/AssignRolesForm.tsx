import { memo, useCallback, useEffect } from 'react';
import styled from 'styled-components';
import {
  FAIL_DATA_FETCH,
  NEXT,
  PREVIOUS,
  SAVE,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { Role } from '@src/types/user.types';
import CheckboxSelectItem from '@src/components/common/CheckboxSelectItem';
import MainButton from '@src/components/common/MainButton';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import ErrorBox from '@src/components/common/ErrorBox';
import useAssignRolesForm from './hooks/useAssignRolesForm';
import useRolesQueries from './hooks/useRolesQueries';

const AssignRolesForm = () => {
  const { isModifying, hasCompleted, mode, saveUserPayload } =
    useSaveUserModalSelector();

  const {
    toggleItem,
    isSelected,
    selectedItems: selectedRoles,
    setSelectedItems: setSelectedRoles,
  } = useSelectItems<Role>(hasCompleted ? saveUserPayload?.roles : []);

  const { handleNextClick, handlePreviousClick } = useAssignRolesForm({
    selectedRoles,
  });

  const { roles, isError, isFetching, refetch } = useRolesQueries({
    className: saveUserPayload?.className,
    classRolesQueryEnabled: mode === 'add' && !hasCompleted,
    setSelectedRoles,
  });

  useEffect(() => {
    if (!hasCompleted && mode === 'add') {
      return;
    }

    if (saveUserPayload?.roles !== undefined) {
      setSelectedRoles(saveUserPayload.roles);
    }
  }, [setSelectedRoles]);

  const handleSelectItemChange = useCallback(
    (role: Role) => toggleItem(role),
    [toggleItem],
  );

  if (isFetching) {
    return (
      <Box>
        <LoadingSpinner />
      </Box>
    );
  }

  if (isError) {
    return (
      <Box>
        <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
      </Box>
    );
  }

  return (
    <Box>
      <DescriptionSection>역할을 선택해주세요.</DescriptionSection>
      <CheckboxSection>
        {roles?.map((role) => (
          <div key={role.id}>
            <RoleCheckbox
              key={role.id}
              isSelected={isSelected(role)}
              onChange={() => handleSelectItemChange(role)}
              selectItem={role}
              label={role.roleKR}
              checkboxSize="sm"
            />
            <RoleDescription>{role.description}</RoleDescription>
          </div>
        ))}
      </CheckboxSection>
      <ButtonWrapper>
        {!isModifying && (
          <MainButton
            label={addWhiteSpacesBetweenCharacters(PREVIOUS)}
            onClick={handlePreviousClick}
            backgroundColor="white"
            labelColor="primary"
          />
        )}
        <MainButton
          label={
            isModifying
              ? addWhiteSpacesBetweenCharacters(SAVE)
              : addWhiteSpacesBetweenCharacters(NEXT)
          }
          onClick={handleNextClick}
        />
      </ButtonWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 63vw;
  height: 60vh;
  display: flex;
  flex-direction: column;
  padding: 2.4rem 3.6rem 4.5rem 7.9rem;
`;

const DescriptionSection = memo(styled.div`
  display: flex;
  justify-content: flex-end;
  font-size: 1.6rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
`);

const CheckboxSection = styled.div`
  flex: 1;
  margin-top: 1.7rem;
  display: grid;
  overflow-y: auto;
  grid-template-columns: repeat(auto-fill, minmax(27.9rem, 1fr));
  gap: 2rem 4.6rem;
`;

const RoleDescription = styled.p`
  padding-left: 3.3rem;
  font-size: 1.6rem;
  font-weight: 400;
  line-height: 2.664rem;
  color: ${({ theme }) => theme.colors.primary};
`;

const RoleCheckbox = styled(CheckboxSelectItem)`
  label {
    font-size: 1.8rem;
    font-weight: 700;
    line-height: 2.96rem;
  }
`;

const ButtonWrapper = styled.div`
  min-height: 5rem;
  margin-top: 6rem;
  display: flex;
  justify-content: flex-end;
  gap: 3rem;

  button {
    flex-basis: 14.6rem;
    height: 100%;
    font-size: 1.6rem;
  }
`;

export default AssignRolesForm;
