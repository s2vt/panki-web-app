import { Meta, Story } from '@storybook/react';
import AssignRolesForm from '../AssignRolesForm';

export default {
  title: 'Components/user/SaveUserModal/AssignRolesForm',
  component: AssignRolesForm,
} as Meta;

const Template: Story = (args) => <AssignRolesForm {...args} />;

export const Basic = Template.bind({});
Basic.args = {};
