import { Meta, Story } from '@storybook/react';
import SaveUserForm from '../SaveUserForm';

export default {
  title: 'Components/user/SaveUserModal/SaveUserForm',
  component: SaveUserForm,
} as Meta;

const Template: Story = (args) => <SaveUserForm {...args} />;

export const Basic = Template.bind({});
