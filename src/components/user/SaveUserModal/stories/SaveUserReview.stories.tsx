import { Meta, Story } from '@storybook/react';
import SaveUserReview, { SaveUserReviewProps } from '../SaveUserReview';

export default {
  title: 'Components/user/SaveUserModal/SaveUserReview',
  component: SaveUserReview,
} as Meta;

const Template: Story<SaveUserReviewProps> = (args) => (
  <SaveUserReview {...args} />
);

export const Basic = Template.bind({});
Basic.args = {};
