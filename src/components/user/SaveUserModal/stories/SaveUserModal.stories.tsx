import { Meta, Story } from '@storybook/react';
import SaveUserModal, { SaveUserModalProps } from '../SaveUserModal';

export default {
  title: 'Components/user/SaveUserModal',
  component: SaveUserModal,
} as Meta;

const Template: Story<SaveUserModalProps> = (args) => (
  <SaveUserModal {...args} />
);

export const Basic = Template.bind({});
