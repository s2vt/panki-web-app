import { memo, useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import { CompanyUser, CompanyWithUsers } from '@src/types/user.types';
import Icon from '@src/components/common/Icon';

export type OrganizationUserChartProps = {
  company: CompanyWithUsers;
  level: number;
  selectedUser: CompanyUser | undefined;
  onUserClick: (user: CompanyUser) => void;
  onUserDoubleClick: () => void;
};

const OrganizationUserChart = ({
  company,
  level,
  selectedUser,
  onUserClick,
  onUserDoubleClick,
}: OrganizationUserChartProps) => {
  const { companyName, users } = company;
  const [selected, setSelected] = useState(level === 0);

  const hasChildren = useMemo(
    () => company.children !== undefined && company.children.length > 0,
    [company],
  );

  const toggleSelected = useCallback(
    () => setSelected((prevSelected) => !prevSelected),
    [setSelected],
  );

  const handleClickCompanyUser = useCallback(
    (user: CompanyUser) => {
      onUserClick(user);
    },
    [onUserClick],
  );

  return (
    <>
      <Box level={level} onClick={toggleSelected}>
        {(hasChildren || company.users?.length !== 0) && (
          <ArrowIconWrapper level={level}>
            {selected ? (
              <DownArrow icon="arrowBoxIcon" />
            ) : (
              <Icon icon="arrowBoxIcon" />
            )}
          </ArrowIconWrapper>
        )}
        {level === 0 && <OrganizationIcon icon="organizationIcon" />}
        <TextSection>
          <p>{companyName}</p>
          <p>{users?.length}</p>
        </TextSection>
      </Box>
      {selected &&
        users?.map((user) => (
          <UserText
            key={user.id}
            level={level}
            onClick={() => handleClickCompanyUser(user)}
            onDoubleClick={onUserDoubleClick}
            isSelected={user.id === selectedUser?.id}
          >
            {user.userName}
          </UserText>
        ))}
      {selected &&
        hasChildren &&
        company.children?.map((child) => (
          <OrganizationUserChart
            key={child.id}
            company={child}
            level={level + 1}
            selectedUser={selectedUser}
            onUserClick={onUserClick}
            onUserDoubleClick={onUserDoubleClick}
          />
        ))}
    </>
  );
};

const Box = styled.div<{ level: number }>`
  display: flex;
  align-items: center;
  height: 2.4rem;
  margin: 0.85rem 6.6rem 0.85rem ${({ level }) => `${level * 3}rem`};
  font-size: 2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
`;

const ArrowIconWrapper = styled.div<{ level: number }>`
  margin-right: ${({ level }) => (level === 0 ? '1rem' : '0.8rem')};
`;

const TextSection = styled.div`
  flex: 1;
  display: flex;
  justify-content: space-between;
`;

const UserText = styled.p<{ level: number; isSelected: boolean }>`
  margin-left: ${({ level }) => ` ${level === 0 ? 6.5 : level * 6.5}rem`};
  font-size: 2rem;
  font-weight: ${({ isSelected }) => (isSelected ? '700' : '400')};
  line-height: 2.96rem;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
`;

const OrganizationIcon = styled(Icon)`
  margin-right: 1.4rem;
`;

const DownArrow = styled(Icon)`
  transform: rotate(90deg);
`;
export default memo(OrganizationUserChart);
