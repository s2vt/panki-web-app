import { Meta, Story } from '@storybook/react';
import { CompanyWithUsers } from '@src/types/user.types';
import OrganizationUserChart, {
  OrganizationUserChartProps,
} from './OrganizationUserChart';

export default {
  title: 'Components/user/OrganizationUserChart',
  component: OrganizationUserChart,
} as Meta;

const Template: Story<OrganizationUserChartProps> = (args) => (
  <OrganizationUserChart {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  level: 0,
  company: {
    id: '1',
    adminCount: 0,
    companyName: 'A 회사',
    users: [{ id: '1', userName: '김유저' }],
    children: [
      {
        id: '2',
        adminCount: 0,
        companyName: 'A 하청',
        users: [{ id: '2', userName: '김유저' }],
        children: [
          {
            id: '5',
            adminCount: 0,
            companyName: 'A 하청의 하청',
          } as CompanyWithUsers,
        ],
      } as CompanyWithUsers,
      {
        id: '3',
        adminCount: 0,
        companyName: 'B 하청',
      } as CompanyWithUsers,
      {
        id: '4',
        adminCount: 0,
        companyName: 'C 하청',
      } as CompanyWithUsers,
    ],
  } as CompanyWithUsers,
};
