/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import { convertUserClassText, User } from '@src/types/user.types';
import TableItem from '@src/components/table/TableItem';

export type UserTableItemProps = {
  user: User;
  onClick: (user: User) => void;
  onCheckboxChange: (user: User) => void;
  checked: boolean;
  isOddItem: boolean;
};

const UserTableItem = ({
  user,
  onClick,
  onCheckboxChange,
  checked,
  isOddItem,
}: UserTableItemProps) => {
  const {
    id,
    userId,
    userName,
    company: { companyName },
    className,
    email,
  } = user;

  const userTableRows = [
    userId,
    userName,
    companyName,
    convertUserClassText(className),
    email,
  ];

  return (
    <TableItem
      key={id}
      id={id}
      onClick={() => onClick(user)}
      onCheckboxChange={() => onCheckboxChange(user)}
      checked={checked}
      isOddItem={isOddItem}
      rows={userTableRows}
    />
  );
};

export default memo(UserTableItem);
