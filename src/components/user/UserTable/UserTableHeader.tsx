import { memo } from 'react';
import {
  ID,
  NAME,
  ORGANIZATION,
  USER_CLASS,
  EMAIL,
} from '@src/constants/constantString';
import TableHeader from '@src/components/table/TableHeader';

export type UserTableHeaderProps = {
  onCheckboxClick?: () => void;
  checked?: boolean;
};

const USER_TABLE_COLUMNS = [ID, NAME, ORGANIZATION, USER_CLASS, EMAIL];

const UserTableHeader = ({
  onCheckboxClick,
  checked,
}: UserTableHeaderProps) => (
  <TableHeader
    columns={USER_TABLE_COLUMNS}
    checked={checked}
    onCheckboxClick={onCheckboxClick}
  />
);

export default memo(UserTableHeader);
