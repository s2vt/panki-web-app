import ModalWrapper from '@src/components/base/ModalWrapper';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import Pagination from '@src/components/common/Pagination';
import TableBase from '@src/components/table/TableBase';
import { FAIL_DATA_FETCH } from '@src/constants/constantString';
import usePagination from '@src/hooks/common/usePagination';
import useRoutePagination from '@src/hooks/common/useRoutePagination';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { SelectItem } from '@src/types/common.types';
import { SavedUser, User, UserFilter } from '@src/types/user.types';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import SaveUserResultModal from '../SaveUserResultModal';
import UserTableBody from './UserTableBody';
import UserTableHeader from './UserTableHeader';
import useCompanyUsersQuery from '../UserManagement/hooks/useCompanyUsersQuery';

export type UserTableProps = {
  companyName: string;
  userFilter: UserFilter | undefined;
  selectedUsers: (SelectItem & User)[];
  onHeaderCheckboxClick: () => void;
  onCheckboxChange: (selectItem: SelectItem & User) => void;
  isUserSelected: (selectItem: SelectItem & User) => boolean;
};

const UserTable = ({
  companyName,
  userFilter,
  selectedUsers,
  onHeaderCheckboxClick,
  onCheckboxChange,
  isUserSelected,
}: UserTableProps) => {
  const { page } = useParams<{ page?: string }>();
  const [isSaveUserResultModalOpen, setIsSaveUserResultModalOpen] =
    useState(false);
  const [savedUser, setSavedUser] = useState<SavedUser>();

  const {
    data: users,
    isFetching,
    refetch,
    isError,
  } = useCompanyUsersQuery(companyName, {
    enabled: false,
    userFilter,
  });

  const {
    setCurrentPage,
    lastPage,
    currentPageItems: currentPageUsers,
  } = usePagination<User>(users ?? [], 10);

  const { currentPage, handlePageClick } = useRoutePagination({
    pageParam: page,
    basePath: ROUTE_PATHS.userManagement,
    lastPage,
    setCurrentPage,
  });

  const handleUpdateUserSuccess = (savedUser: SavedUser) => {
    setSavedUser(savedUser);
    setIsSaveUserResultModalOpen(true);
    refetch();
  };

  if (isFetching) {
    return (
      <TableBase.LoadingWrapper>
        <LoadingSpinner />
      </TableBase.LoadingWrapper>
    );
  }

  if (isError) {
    return (
      <TableBase.LoadingWrapper>
        <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetch} />
      </TableBase.LoadingWrapper>
    );
  }

  return (
    <>
      <TableBase.TableWrapper>
        <TableBase.Table>
          <UserTableHeader
            onCheckboxClick={onHeaderCheckboxClick}
            checked={selectedUsers.length === users?.length}
          />
          <UserTableBody
            users={currentPageUsers}
            onCheckboxChange={onCheckboxChange}
            isSelected={isUserSelected}
            onUserUpdateSuccess={handleUpdateUserSuccess}
          />
        </TableBase.Table>
      </TableBase.TableWrapper>
      <TableBase.PaginationSection>
        <Pagination
          currentPage={currentPage}
          lastPage={lastPage}
          onPageClick={handlePageClick}
        />
      </TableBase.PaginationSection>
      {savedUser && (
        <ModalWrapper isOpen={isSaveUserResultModalOpen}>
          <SaveUserResultModal
            savedUser={savedUser}
            onClose={() => setIsSaveUserResultModalOpen(false)}
          />
        </ModalWrapper>
      )}
    </>
  );
};

export default UserTable;
