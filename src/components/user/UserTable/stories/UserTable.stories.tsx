import { Meta, Story } from '@storybook/react';
import UserTable, { UserTableProps } from '../UserTable';

export default {
  title: 'Components/User/UserTable',
  component: UserTable,
} as Meta;

const Template: Story<UserTableProps> = (args) => <UserTable {...args} />;

export const Basic = Template.bind({});
