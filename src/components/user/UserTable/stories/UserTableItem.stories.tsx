import { Meta, Story } from '@storybook/react';
import UserTableItem, { UserTableItemProps } from '../UserTableItem';

export default {
  title: 'Components/User/UserTable/UserTableItem',
  component: UserTableItem,
} as Meta;

const Template: Story<UserTableItemProps> = (args) => (
  <table style={{ width: '100%' }}>
    <tbody>
      <UserTableItem {...args} />
    </tbody>
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  user: {
    id: '1',
    className: 'Admin',
    email: 'test1@ssvt.co.kr',
    engName: 'test',
    orgName: 'ShipYardOrg',
    roles: [],
    userId: 'test1',
    userName: '김유저',
    phone: '010-1234-5678',
    company: {
      id: '1',
      adminCount: 0,
      companyName: 'ssvt',
      companyEmail: 'ssvt.co.kr',
      orgName: 'ShipYardOrg',
    },
  },
};
