import { Meta, Story } from '@storybook/react';
import UserInfoModal, { UserInfoModalProps } from '../UserInfoModal';

export default {
  title: 'Components/User/UserTable/UserInfoModal',
  component: UserInfoModal,
} as Meta;

const Template: Story<UserInfoModalProps> = (args) => (
  <UserInfoModal {...args} />
);

export const Basic = Template.bind({});
