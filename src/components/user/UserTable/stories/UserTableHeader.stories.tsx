import { Meta, Story } from '@storybook/react';
import UserTableHeader, { UserTableHeaderProps } from '../UserTableHeader';

export default {
  title: 'Components/User/UserTable/UserTableHeader',
  component: UserTableHeader,
} as Meta;

const Template: Story<UserTableHeaderProps> = (args) => (
  <UserTableHeader {...args} />
);

export const Basic = Template.bind({});
