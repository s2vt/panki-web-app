import { Meta, Story } from '@storybook/react';
import UserTableBody, { UserTableBodyProps } from '../UserTableBody';

export default {
  title: 'Components/User/UserTable/UserTableBody',
  component: UserTableBody,
} as Meta;

const Template: Story<UserTableBodyProps> = (args) => (
  <table style={{ width: '100%' }}>
    <UserTableBody {...args} />
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  isSelected: () => true,
  users: [
    {
      id: '1',
      className: 'Admin',
      email: 'test1@ssvt.co.kr',
      engName: 'test',
      orgName: 'ShipYardOrg',
      roles: [],
      userId: 'test1',
      userName: '김유저',
      phone: '010-1234-5678',
      company: {
        id: '1',
        adminCount: 0,
        companyName: 'ssvt',
        companyEmail: 'ssvt.co.kr',
        orgName: 'ShipYardOrg',
      },
    },
  ],
};
