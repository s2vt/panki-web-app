import { useState, memo, useCallback } from 'react';
import HttpError from '@src/api/model/HttpError';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { SavedUser, User } from '@src/types/user.types';
import ModalWrapper from '@src/components/base/ModalWrapper';
import useUpdateUserMutation from './hooks/useUpdateUserMutation';
import UserTableItem from './UserTableItem';
import UserInfoModal from './UserInfoModal';
import useSaveUserModalAction from '../../../hooks/user/useSaveUserModalAction';

export type UserTableBodyProps = {
  /** 유저 리스트 데이터 */
  users: User[];
  /** 체크박스 클릭 시 실행할 이벤트 */
  onCheckboxChange: (user: User) => void;
  /** 해당 유저가 체크되어 있는지 확인하기 위해 사용하는 함수  */
  isSelected: (user: User) => boolean;
  onUserUpdateSuccess: (savedUser: SavedUser) => void;
};

/** 직원 관리 페이지의 테이블 리스트 컴포넌트입니다. */
const UserTableBody = ({
  users,
  onUserUpdateSuccess,
  onCheckboxChange,
  isSelected,
}: UserTableBodyProps) => {
  const [isUserInfoModalOpen, setIsUserInfoModalOpen] = useState(false);

  const { setSelectedUser } = useSaveUserModalAction();
  const { openAlertDialog } = useAlertDialogAction();

  const handleTableItemClick = useCallback(
    (user: User) => {
      setSelectedUser(user);
      setIsUserInfoModalOpen(true);
    },
    [setSelectedUser, setIsUserInfoModalOpen],
  );

  const closeUserInfoModal = useCallback(
    () => setIsUserInfoModalOpen(false),
    [setIsUserInfoModalOpen],
  );

  const handleUpdateUserError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { mutate: updateUserMutate, isLoading: isUpdateUserMutateLoading } =
    useUpdateUserMutation({
      onSuccess: onUserUpdateSuccess,
      onError: handleUpdateUserError,
    });

  return (
    <>
      <tbody>
        {users?.map((user, index) => (
          <UserTableItem
            key={user.userId}
            user={user}
            onClick={handleTableItemClick}
            onCheckboxChange={onCheckboxChange}
            isOddItem={index % 2 === 0}
            checked={isSelected(user)}
          />
        ))}
      </tbody>
      <ModalWrapper isOpen={isUserInfoModalOpen}>
        <UserInfoModal
          onSuccess={updateUserMutate}
          onClose={closeUserInfoModal}
          isSubmitting={isUpdateUserMutateLoading}
        />
      </ModalWrapper>
    </>
  );
};

export default memo(UserTableBody);
