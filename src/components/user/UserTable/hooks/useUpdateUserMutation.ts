import { useMutation, UseMutationOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import { SavedUser, SaveUserPayload } from '@src/types/user.types';

const useUpdateUserMutation = (
  options?: UseMutationOptions<SavedUser, HttpError, SaveUserPayload>,
) =>
  useMutation(
    (saveUserPayload: SaveUserPayload) => userApi.updateUser(saveUserPayload),
    options,
  );

export default useUpdateUserMutation;
