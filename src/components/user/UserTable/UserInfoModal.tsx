import { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import {
  CLASS,
  COMPANY_NAME,
  EMAIL,
  SHORT_EMPLOYEE_ID,
  MODIFY,
  USER_BASIC_INFORMATION,
  NAME,
  PHONE_NUMBER,
} from '@src/constants/constantString';
import useUserSelector from '@src/hooks/user/useUserSelector';
import {
  convertUserClassText,
  isLowerThenInputUserClass,
  SaveUserPayload,
} from '@src/types/user.types';
import ModalTitle from '@src/components/base/ModalTitle';
import ModifyButton from '@src/components/common/ModifyButton';
import useSaveUserModalSelector from '@src/hooks/user/useSaveUserModalSelector';
import SaveUserModal from '../SaveUserModal';
import useSaveUserModalAction from '../../../hooks/user/useSaveUserModalAction';

export type UserInfoModalProps = {
  /** 정보 수정 성공 시 실행할 이벤트  */
  onSuccess: (saveUserPayload: SaveUserPayload) => void;
  /** 모달 닫기 버튼 클릭 시 실행할 이벤트 */
  onClose: () => void;
  /** 유저 저장 진행 중 여부  */
  isSubmitting: boolean;
};

/** 직원 정보 보기 및 수정이 가능한 모달을 보여주는 컴포넌트입니다. */
const UserInfoModal = ({
  onSuccess,
  onClose,
  isSubmitting,
}: UserInfoModalProps) => {
  const [isSaveUserModalOpen, setIsSaveUserModalOpen] = useState(false);

  const { user: currentUser } = useUserSelector();
  const { selectedUser } = useSaveUserModalSelector();
  const { setMode, setPayloadFromSelectedUser } = useSaveUserModalAction();

  const openSaveUserModal = useCallback(() => {
    setMode('modify');
    setPayloadFromSelectedUser();
    setIsSaveUserModalOpen(true);
  }, [setMode, setIsSaveUserModalOpen, setPayloadFromSelectedUser]);

  const isLowerThenCurrentUserClass = useMemo(
    () =>
      currentUser !== undefined &&
      selectedUser !== undefined &&
      isLowerThenInputUserClass(selectedUser.className, currentUser.className),
    [selectedUser, currentUser],
  );

  if (selectedUser === undefined) {
    return null;
  }

  const {
    userName,
    userId,
    email,
    company: { companyName },
    phone,
    className,
  } = selectedUser;

  return isSaveUserModalOpen ? (
    <SaveUserModal
      onSubmit={onSuccess}
      onClose={onClose}
      isSubmitting={isSubmitting}
    />
  ) : (
    <>
      <ModalTitle title={USER_BASIC_INFORMATION} onClose={onClose} />
      <ModalSection>
        {isLowerThenCurrentUserClass && (
          <ButtonSection>
            <ModifyButton label={MODIFY} onClick={openSaveUserModal} />
          </ButtonSection>
        )}
        <Box>
          <Column>
            <b>{NAME}</b>
            <b>{SHORT_EMPLOYEE_ID}</b>
            <b>{EMAIL}</b>
            <b>{COMPANY_NAME}</b>
            <b>{PHONE_NUMBER}</b>
            <b>{CLASS}</b>
          </Column>
          <Column>
            <p>{userName}</p>
            <p>{userId}</p>
            <p>{email}</p>
            <p>{companyName}</p>
            <p>{phone}</p>
            <p>{convertUserClassText(className)}</p>
          </Column>
        </Box>
      </ModalSection>
    </>
  );
};

const ModalSection = styled.div`
  min-width: 40.6rem;
  height: 48rem;
  display: flex;
  flex-direction: column;
  padding: 2.2rem 4.8rem 4.8rem;
`;

const ButtonSection = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const Box = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 1.6rem;

  b {
    min-width: 14.3rem;
    font-weight: 700;
  }

  p {
    font-weight: 400;
  }
`;

const Column = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export default UserInfoModal;
