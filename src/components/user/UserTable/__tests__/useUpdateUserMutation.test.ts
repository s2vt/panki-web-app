import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import userApi from '@src/api/userApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import useUpdateUserMutation from '../hooks/useUpdateUserMutation';

describe('useUpdateUserMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    const { result, waitFor } = renderHook(() => useUpdateUserMutation(), {
      wrapper,
    });

    return { result, waitFor };
  };

  it('should get updated user when succeed update user mutate', async () => {
    // given
    const sampleResponse = {
      ...saveUserPayload,
      company: {
        id: faker.datatype.number().toString(),
        adminCount: faker.datatype.number(),
        companyEmail: faker.internet.email(),
        companyName: faker.company.companyName(),
        orgName: faker.company.companyName(),
      },
      temporaryPassword: faker.internet.password(),
    };

    userApi.updateUser = jest.fn().mockResolvedValue(sampleResponse);

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(saveUserPayload);
    });

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleResponse);
  });

  it('should set error when failed update user mutate', async () => {
    // given
    userApi.updateUser = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(saveUserPayload);
    });

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
