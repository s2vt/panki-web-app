import { Meta, Story } from '@storybook/react';
import { Company } from '@src/types/user.types';
import OrganizationChart, { OrganizationChartProps } from './OrganizationChart';

export default {
  title: 'Components/user/OrganizationChart',
  component: OrganizationChart,
} as Meta;

const Template: Story<OrganizationChartProps> = (args) => (
  <OrganizationChart {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  level: 0,
  company: {
    id: '1',
    adminCount: 0,
    companyName: 'A 회사',
    children: [
      {
        id: '2',
        adminCount: 0,
        companyName: 'A 하청',
        children: [
          {
            id: '5',
            adminCount: 0,
            companyName: 'A 하청의 하청',
          } as Company,
        ],
      } as Company,
      {
        id: '3',
        adminCount: 0,
        companyName: 'B 하청',
      } as Company,
      {
        id: '4',
        adminCount: 0,
        companyName: 'C 하청',
      } as Company,
    ],
  } as Company,
};
