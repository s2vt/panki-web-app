import { memo, useMemo, useState } from 'react';
import styled from 'styled-components';
import { Company } from '@src/types/user.types';
import Icon from '@src/components/common/Icon';

export type OrganizationChartProps = {
  company: Company;
  level: number;
  selectedCompany: Company | undefined;
  onClick: (company: Company | undefined) => void;
  isModifyMode?: boolean;
  onAddOrganizationInputChange?: (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => void;
};

const OrganizationChart = ({
  company,
  level,
  selectedCompany,
  onClick,
  isModifyMode,
  onAddOrganizationInputChange,
}: OrganizationChartProps) => {
  const { companyName, adminCount } = company;
  const [isOpen, setIsOpen] = useState(false);

  const hasChildren = useMemo(
    () => company.children !== undefined && company.children.length > 0,
    [company],
  );

  const toggleIsOpen = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };

  const toggleSelected = () => {
    if (selectedCompany?.id === company.id) {
      onClick(undefined);
      return;
    }

    onClick(company);
  };

  return (
    <>
      <Box level={level}>
        {hasChildren && (
          <ArrowIconWrapper level={level} onClick={toggleIsOpen}>
            {isOpen ? (
              <DownArrow icon="arrowBoxIcon" />
            ) : (
              <Icon icon="arrowBoxIcon" />
            )}
          </ArrowIconWrapper>
        )}
        <TextWrapper onClick={toggleSelected}>
          {level === 0 && <OrganizationIcon icon="organizationIcon" />}
          <TextSection isSelected={selectedCompany === company}>
            <p>{companyName}</p>
            <p>{adminCount}</p>
          </TextSection>
        </TextWrapper>
      </Box>
      {isOpen &&
        hasChildren &&
        company.children?.map((child) => (
          <OrganizationChart
            key={child.id}
            company={child}
            level={level + 1}
            onClick={onClick}
            selectedCompany={selectedCompany}
          />
        ))}
      {isModifyMode && level === 0 && (
        <Box level={1}>
          <OrganizationIcon icon="organizationIcon" />
          <AddOrganizationInput
            type="text"
            placeholder="조직명"
            onChange={onAddOrganizationInputChange}
          />
        </Box>
      )}
    </>
  );
};

const Box = styled.div<{ level: number }>`
  display: flex;
  align-items: center;
  height: 2.4rem;
  margin-left: ${({ level }) => `${level * 2.8}rem`};
  margin-bottom: 1.7rem;
  font-size: 1.4rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
`;

const AddOrganizationInput = styled.input`
  height: 100%;
  border: none;
  background: none;
  font-size: inherit;
  color: inherit;
  padding: 0;
`;

const ArrowIconWrapper = styled.div<{ level: number }>`
  margin-right: ${({ level }) => (level === 0 ? '1rem' : '0.8rem')};
`;

const TextWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

const TextSection = styled.div<{ isSelected: boolean }>`
  flex: 1;
  display: flex;
  justify-content: space-between;
  font-weight: ${({ isSelected }) => (isSelected ? '700' : '400')};
`;

const OrganizationIcon = styled(Icon)`
  margin-right: 1.2rem;
`;

const DownArrow = styled(Icon)`
  transform: rotate(90deg);
`;

export default memo(OrganizationChart);
