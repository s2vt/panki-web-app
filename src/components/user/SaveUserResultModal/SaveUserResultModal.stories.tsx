import { Meta, Story } from '@storybook/react';
import faker from 'faker';
import { Company, UserClass } from '@src/types/user.types';
import SaveUserResultModal, {
  SaveUserResultModalProps,
} from './SaveUserResultModal';

export default {
  title: 'Components/user/SaveUserResultModal',
  component: SaveUserResultModal,
} as Meta;

const Template: Story<SaveUserResultModalProps> = (args) => (
  <SaveUserResultModal {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  savedUser: {
    id: faker.datatype.number().toString(),
    userId: faker.internet.userName(),
    userName: faker.internet.userName(),
    engName: faker.internet.userName(),
    email: faker.internet.email(),
    company: { companyName: faker.company.companyName() } as Company,
    orgName: faker.company.companyName(),
    roles: [],
    className: UserClass.employee,
    temporaryPassword: faker.internet.password(),
  },
};
