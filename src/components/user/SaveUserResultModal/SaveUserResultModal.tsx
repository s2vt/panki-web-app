import styled from 'styled-components';
import {
  CLASS,
  EMAIL,
  NAME,
  PHONE_NUMBER,
  RESULT_SAVE_USER,
  SHORT_EMPLOYEE_ID,
  TEMPORARY_PASSWORD,
} from '@src/constants/constantString';
import { SavedUser } from '@src/types/user.types';
import ModalTitle from '@src/components/base/ModalTitle';

export type SaveUserResultModalProps = {
  /** 유저 생성 성공 후 받아오는 유저 */
  savedUser: SavedUser;
  /** 닫기 버튼 클릭 시 실행할 이벤트 */
  onClose: () => void;
};

/** 유저 생성 후 결과를 보여주는 모달 컴포넌트입니다. */
const SaveUserResultModal = ({
  savedUser,
  onClose,
}: SaveUserResultModalProps) => {
  const { className, userId, email, userName, phone, temporaryPassword } =
    savedUser;

  return (
    <>
      <ModalTitle title={RESULT_SAVE_USER} onClose={onClose} />
      <Box>
        <UserInfoSection>
          <Row>
            <b>{NAME}</b>
            <p>{userName}</p>
          </Row>
          <Row>
            <b>{SHORT_EMPLOYEE_ID}</b>
            <p>{userId}</p>
          </Row>
          <Row>
            <b>{EMAIL}</b>
            <p>{email}</p>
          </Row>
          <Row>
            <b>{PHONE_NUMBER}</b>
            <p>{phone}</p>
          </Row>
          <Row>
            <b>{CLASS}</b>
            <p>{className}</p>
          </Row>
          {temporaryPassword && (
            <Row>
              <b>{TEMPORARY_PASSWORD}</b>
              <p>{temporaryPassword}</p>
            </Row>
          )}
        </UserInfoSection>
      </Box>
    </>
  );
};

const Box = styled.div`
  width: 26vw;
  min-width: 40.6rem;
  height: 32rem;
  padding: 2rem;
  display: flex;
  flex-direction: column;
`;

const UserInfoSection = styled.div`
  flex: 1;
  padding: 1.8rem 2rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 1.6rem;

  b {
    min-width: 14.3rem;
    flex: 1;
    font-weight: 700;
  }

  p {
    flex: 1;
    font-weight: 400;
  }
`;

export default SaveUserResultModal;
