import { useCallback, useState } from 'react';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { MANAGE_ORGANIZATION_AND_ROLE } from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { Company, User, UserClass, UserFilter } from '@src/types/user.types';
import PageTitle from '@src/components/base/PageTitle';
import TableBase from '@src/components/table/TableBase';
import SideLayout from '@src/components/base/SideLayout';
import SideOrganizationChart from '../SideOrganizationChart/SideOrganizationChart';
import useCompanyUsersQuery from './hooks/useCompanyUsersQuery';
import UserTable from '../UserTable';
import UserTableAction from '../UserTableAction';

const UserManagement = () => {
  const { companyName, isAdminOrManagerUser } = useUserSelector();
  const [userFilter, setUserFilter] = useState<UserFilter>();

  const {
    selectedItems: selectedUsers,
    setSelectedItems: setSelectedUsers,
    toggleItem: handleCheckboxChange,
    isSelected: isUserSelected,
  } = useSelectItems<User>();

  const { data: users, refetch } = useCompanyUsersQuery(companyName, {
    enabled: companyName !== '',
    userFilter,
  });

  const handleFilterChange = useCallback(
    (userClass?: UserClass) => {
      setUserFilter((prevFilter) => ({
        ...prevFilter,
        className: userClass,
      }));
    },
    [setUserFilter],
  );

  const handleCompanySelect = (selectedCompany: Company | undefined) => {
    setUserFilter((prevFilter) => ({
      ...prevFilter,
      company: selectedCompany,
    }));
  };

  const handleSearchEnter = useCallback(
    (value: string) => {
      setUserFilter((prevFilter) => ({ ...prevFilter, userName: value }));
    },
    [setUserFilter],
  );

  const handleHeaderCheckboxClick = useCallback(() => {
    if (selectedUsers.length === users?.length) {
      setSelectedUsers([]);
      return;
    }

    if (users !== undefined) {
      setSelectedUsers(users);
    }
  }, [users, selectedUsers, setSelectedUsers]);

  const handleRefetchUsers = useCallback(() => {
    setSelectedUsers([]);
    refetch();
  }, [setSelectedUsers, refetch]);

  return (
    <>
      <PageTitle title={MANAGE_ORGANIZATION_AND_ROLE} />
      <SideLayout.Container>
        <SideLayout.Side>
          <SideOrganizationChart onSelect={handleCompanySelect} />
        </SideLayout.Side>
        <SideLayout.Main>
          <TableBase.TableSection>
            <UserTableAction
              isAdminOrManagerUser={isAdminOrManagerUser}
              onRefetchUsers={handleRefetchUsers}
              selectedUsers={selectedUsers}
              onSearchEnter={handleSearchEnter}
              onFilterChange={handleFilterChange}
            />
            <UserTable
              companyName={companyName}
              userFilter={userFilter}
              isUserSelected={isUserSelected}
              onHeaderCheckboxClick={handleHeaderCheckboxClick}
              onCheckboxChange={handleCheckboxChange}
              selectedUsers={selectedUsers}
            />
          </TableBase.TableSection>
        </SideLayout.Main>
      </SideLayout.Container>
    </>
  );
};

export default UserManagement;
