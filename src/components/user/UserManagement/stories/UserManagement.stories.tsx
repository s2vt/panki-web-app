import { Meta, Story } from '@storybook/react';
import UserManagement from '../UserManagement';

export default {
  title: 'Components/User/UserManagement',
  component: UserManagement,
} as Meta;

const Template: Story = (args) => <UserManagement {...args} />;

export const Basic = Template.bind({});
