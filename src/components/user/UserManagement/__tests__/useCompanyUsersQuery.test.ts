import { renderHook } from '@testing-library/react-hooks/dom';
import faker from 'faker';
import userApi from '@src/api/userApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';

import users from '@src/test/fixtures/user/users';
import company from '@src/test/fixtures/user/company';
import { User, UserClass, UserFilter } from '@src/types/user.types';
import useCompanyUsersQuery from '../hooks/useCompanyUsersQuery';

describe('useCompanyUsersQuery hook', () => {
  const setup = (companyName: string, userFilter?: UserFilter) => {
    const { wrapper } = prepareWrapper();
    const { result, waitFor } = renderHook(
      () => useCompanyUsersQuery(companyName, { userFilter }),
      { wrapper },
    );

    return { result, waitFor };
  };

  it('should cache data when succeed fetch company users', async () => {
    // given
    userApi.getCompanyUsers = jest.fn().mockResolvedValue(users);

    // when
    const { result, waitFor } = setup(company.companyName);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(userApi.getCompanyUsers).toBeCalledWith(company.companyName);
    expect(result.current.data).toEqual(users);
  });

  it('should set error when failed fetch company users', async () => {
    // given
    const sampleCompanyName = faker.company.companyName();

    userApi.getCompanyUsers = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(sampleCompanyName);

    await waitFor(() => result.current.isError);

    // then
    expect(userApi.getCompanyUsers).toBeCalledWith(sampleCompanyName);
    expect(result.current.error).not.toBeNull();
  });

  it('should filter user with user name', async () => {
    // given
    const users: User[] = [
      {
        id: '1',
        userId: 'userId1',
        userName: '유저명1',
        engName: 'userName1',
        email: 'test1@test.co.kr',
        company: {
          id: '1',
          adminCount: 1,
          companyEmail: 'test.co.kr',
          companyName: 'company',
          orgName: 'ShipYardOrg',
        },
        orgName: 'ShipYardOrg',
        roles: [],
        className: UserClass.admin,
        phone: '010-1234-5678',
      },
      {
        id: '2',
        userId: 'userId2',
        userName: '유저명2',
        engName: 'userName2',
        email: 'test2@test.co.kr',
        company: {
          id: '1',
          adminCount: 1,
          companyEmail: 'test.co.kr',
          companyName: 'company',
          orgName: 'ShipYardOrg',
        },
        orgName: 'ShipYardOrg',
        roles: [],
        className: UserClass.admin,
        phone: '010-1234-5678',
      },
    ];

    userApi.getCompanyUsers = jest.fn().mockResolvedValue(users);

    const userFilter: UserFilter = {
      userName: '유저명1',
    };

    // when
    const { result, waitFor } = setup(company.companyName, userFilter);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data?.length).toEqual(1);
  });

  it('should filter user with user class', async () => {
    // given
    const users: User[] = [
      {
        id: '1',
        userId: 'userId1',
        userName: '유저명1',
        engName: 'userName1',
        email: 'test1@test.co.kr',
        company: {
          id: '1',
          adminCount: 1,
          companyEmail: 'test.co.kr',
          companyName: 'company',
          orgName: 'ShipYardOrg',
        },
        orgName: 'ShipYardOrg',
        roles: [],
        className: UserClass.admin,
        phone: '010-1234-5678',
      },
      {
        id: '2',
        userId: 'userId2',
        userName: '유저명2',
        engName: 'userName2',
        email: 'test2@test.co.kr',
        company: {
          id: '1',
          adminCount: 1,
          companyEmail: 'test.co.kr',
          companyName: 'company',
          orgName: 'ShipYardOrg',
        },
        orgName: 'ShipYardOrg',
        roles: [],
        className: UserClass.employee,
        phone: '010-1234-5678',
      },
    ];

    userApi.getCompanyUsers = jest.fn().mockResolvedValue(users);

    const userFilter: UserFilter = {
      className: UserClass.admin,
    };

    // when
    const { result, waitFor } = setup(company.companyName, userFilter);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data?.length).toEqual(1);
  });
});
