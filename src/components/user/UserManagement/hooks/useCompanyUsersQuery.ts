import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import { User, UserFilter } from '@src/types/user.types';

const useCompanyUsersQuery = (
  companyName: string,
  options?: UseQueryOptions<User[], HttpError> & {
    userFilter?: UserFilter;
  },
) =>
  useQuery(createKey(companyName), () => userApi.getCompanyUsers(companyName), {
    ...options,
    select: (users: User[]) => {
      const filter = options?.userFilter;

      const filteredUsers = users.filter((user) => {
        if (
          filter?.userName !== undefined &&
          !user.userName.includes(filter.userName)
        ) {
          return false;
        }

        if (
          filter?.className !== undefined &&
          filter.className !== user.className
        ) {
          return false;
        }

        if (
          filter?.company !== undefined &&
          filter.company.id !== user.company.id
        ) {
          return false;
        }

        return true;
      });

      return filteredUsers;
    },
  });

const baseKey = ['companyUsers'];
const createKey = (companyName: string) => [...baseKey, companyName];

useCompanyUsersQuery.baseKey = baseKey;
useCompanyUsersQuery.createKey = createKey;

export default useCompanyUsersQuery;
