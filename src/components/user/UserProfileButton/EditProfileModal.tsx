import { Box } from '@mui/material';
import { styled as muiStyled } from '@mui/material/styles';
import styled from 'styled-components';
import {
  CANCEL,
  EDIT_PROFILE,
  NAME,
  PHONE_NUMBER,
  PROFILE_IMAGE,
  SAVE,
} from '@src/constants/constantString';
import { User } from '@src/types/user.types';
import Icon from '@src/components/common/Icon';
import MainButton from '@src/components/common/MainButton';

const StyledBox = muiStyled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '51.4rem',
  height: '55.1rem',
  padding: '4.4rem 4.4rem 5rem',
  background: theme.palette.background.default,
  color: theme.palette.primary.main,
  fontSize: '1.4rem',
}));

export type EditProfileModalProps = {
  user: User;
  onClose: () => void;
};

const EditProfileModal = ({ user, onClose }: EditProfileModalProps) => {
  const { userName, phone } = user;

  return (
    <StyledBox>
      <Title>{EDIT_PROFILE}</Title>
      <ContentSection>
        <Row>
          <Label>{PROFILE_IMAGE}</Label>
          <Icon icon="userAvatarIcon" width="13.1rem" height="13.1rem" />
        </Row>
        <Row>
          <Label>{NAME}</Label>
          <p>{userName}</p>
        </Row>
        <Row>
          <Label>{PHONE_NUMBER}</Label>
          <p>{phone}</p>
        </Row>
      </ContentSection>
      <ButtonSection>
        <MainButton
          label={CANCEL}
          labelColor="primary"
          width="9.5rem"
          backgroundColor="white"
          fontSize="1.6rem"
          onClick={onClose}
        />
        <MainButton label={SAVE} width="9.5rem" fontSize="1.6rem" />
      </ButtonSection>
    </StyledBox>
  );
};

const Title = styled.h1`
  margin-bottom: 6rem;
  font-size: 2.2rem;
  font-weight: 700;
  text-align: center;
`;

const Row = styled.div`
  display: flex;
`;

const Label = styled.p`
  width: 15rem;
  font-weight: 700;
`;

const ContentSection = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const ButtonSection = styled.section`
  margin-top: 6rem;
  height: 5rem;
  display: flex;
  justify-content: flex-end;
  gap: 2.7rem;
`;

export default EditProfileModal;
