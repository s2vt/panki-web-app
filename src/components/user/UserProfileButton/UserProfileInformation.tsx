import { memo } from 'react';
import styled from 'styled-components';
import { User } from '@src/types/user.types';
import Icon from '@src/components/common/Icon';

export type UserProfileInformationProps = {
  user: User;
};

const UserProfileInformation = ({ user }: UserProfileInformationProps) => {
  const { userName, userId, email } = user;

  return (
    <Box>
      <IconWrapper>
        <UserAvatarIcon icon="userAvatarIcon" />
      </IconWrapper>
      <UserNameText>
        {userName}
        <SirText> 님</SirText>
      </UserNameText>
      <IdEmailSection>
        <p>{userId}</p>
        <p>{email}</p>
      </IdEmailSection>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const IconWrapper = styled.div`
  width: 7.9rem;
  height: 7.9rem;
`;

const UserAvatarIcon = styled(Icon)`
  width: 100%;
  height: 100%;
`;

const UserNameText = styled.p`
  margin-top: 2.5rem;
  font-size: 1.6rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const SirText = styled.span`
  font-weight: 400;
`;

const IdEmailSection = styled.div`
  margin-top: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 0.4rem;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.gray};
`;

export default memo(UserProfileInformation);
