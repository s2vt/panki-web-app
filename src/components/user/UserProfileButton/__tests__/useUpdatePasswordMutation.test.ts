import { act, renderHook } from '@testing-library/react-hooks/dom';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import authApi from '@src/api/authApi';
import { UpdatePasswordPayload } from '@src/types/auth.types';
import useUpdatePasswordMutation from '../hooks/useUpdatePasswordMutation';

describe('useUpdatePasswordMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    const { result, waitFor } = renderHook(() => useUpdatePasswordMutation(), {
      wrapper,
    });

    return { result, waitFor };
  };

  it('should isSuccess is true when succeed delete user mutate', async () => {
    // given
    const updatePasswordPayload: UpdatePasswordPayload = {
      password: '1234qwer!',
      newPassword: '1234qwer@',
    };

    authApi.updatePassword = jest.fn().mockResolvedValue(true);

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(updatePasswordPayload);
    });

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.isSuccess).toEqual(true);
  });

  it('should set error when failed delete user mutate', async () => {
    // given
    const updatePasswordPayload: UpdatePasswordPayload = {
      password: '1234qwer!',
      newPassword: '1234qwer@',
    };

    authApi.updatePassword = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() => {
      result.current.mutate(updatePasswordPayload);
    });

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
