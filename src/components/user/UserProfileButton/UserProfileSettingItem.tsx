import { memo } from 'react';
import styled from 'styled-components';

export type UserProfileSettingItemProps = {
  label: string;
  onClick: () => void;
};

const UserProfileSettingItem = ({
  label,
  onClick,
}: UserProfileSettingItemProps) => <Box onClick={onClick}>{label}</Box>;

const Box = styled.li`
  width: 100%;
  height: 3.8rem;
  padding: 0 1.1rem;
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};
  cursor: pointer;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.primary};

  &:hover {
    background: ${({ theme }) => theme.colors.inactive};
  }
`;

export default memo(UserProfileSettingItem);
