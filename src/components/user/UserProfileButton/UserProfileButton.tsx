import { ClickAwayListener, IconButton, Popover } from '@mui/material';
import { useCallback, useState } from 'react';
import styled from 'styled-components';
import useUserAction from '@src/hooks/user/useUserAction';
import tokenStorage from '@src/storage/tokenStorage';
import { User } from '@src/types/user.types';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ModalWrapper from '@src/components/base/ModalWrapper';
import AccountSettingModal from './AccountSettingModal';
import UserProfileInformation from './UserProfileInformation';
import UserSettingButton from './UserSettingButton';

export type UserProfileButtonProps = {
  user: User;
};

const UserProfileButton = ({ user }: UserProfileButtonProps) => {
  const { clearUser } = useUserAction();

  const [dialogElement, setDialogElement] = useState<HTMLElement | null>(null);
  const isDialogOpen = Boolean(dialogElement);

  const [isAccountSettingModalOpen, setIsAccountSettingModalOpen] =
    useState(false);

  const toggleDialog = (e: React.MouseEvent<HTMLElement>) => {
    if (dialogElement === null) {
      setDialogElement(e.currentTarget);
    } else {
      setDialogElement(null);
    }
  };

  const handleAccountSettingClick = useCallback(() => {
    setDialogElement(null);
    setIsAccountSettingModalOpen(true);
  }, [setIsAccountSettingModalOpen]);

  const handleLogoutClick = useCallback(() => {
    tokenStorage.clearToken();
    clearUser();
  }, [clearUser, tokenStorage]);

  return (
    <>
      <Box>
        <UserAvatarWrapper onClick={toggleDialog}>
          <IconButton>
            <AccountCircleIcon fontSize="large" />
          </IconButton>
        </UserAvatarWrapper>
        <Popover
          open={isDialogOpen}
          anchorEl={dialogElement}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
        >
          <ClickAwayListener onClickAway={() => setDialogElement(null)}>
            <DialogSection>
              <UserProfileInformation user={user} />
              <UserSettingButton
                onAccountSettingClick={handleAccountSettingClick}
                onLogoutClick={handleLogoutClick}
              />
            </DialogSection>
          </ClickAwayListener>
        </Popover>
      </Box>
      <ModalWrapper isOpen={isAccountSettingModalOpen}>
        <AccountSettingModal
          onClose={() => setIsAccountSettingModalOpen(false)}
        />
      </ModalWrapper>
    </>
  );
};

const Box = styled.div`
  width: 100%;
  height: 100%;
  padding: 0;
  border: none;
  background: none;
  cursor: pointer;
  display: flex;
  position: relative;
`;

const UserAvatarWrapper = styled.div`
  border: none;
  background: none;
  cursor: pointer;
  position: relative;
`;

const DialogSection = styled.div`
  gap: 1.7rem;
  width: 28rem;
  height: 22.3rem;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 2.3rem 0 3.8rem;
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.15);
  cursor: default;
`;

export default UserProfileButton;
