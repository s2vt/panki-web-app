import { useMutation, UseMutationOptions } from 'react-query';
import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';
import { UpdatePasswordPayload } from '@src/types/auth.types';

const useUpdatePasswordMutation = (
  options?: UseMutationOptions<unknown, HttpError, UpdatePasswordPayload>,
) =>
  useMutation(
    (updatePasswordPayload: UpdatePasswordPayload) =>
      authApi.updatePassword(updatePasswordPayload),
    options,
  );

export default useUpdatePasswordMutation;
