import { ClickAwayListener, Popover } from '@mui/material';
import { memo, useState } from 'react';
import styled from 'styled-components';
import { ACCOUNT_SETTING, LOGOUT } from '../../../constants/constantString';
import Icon from '../../common/Icon';
import UserProfileSettingItem from './UserProfileSettingItem';

export type UserSettingButtonProps = {
  onAccountSettingClick: () => void;
  onLogoutClick: () => void;
};

const UserSettingButton = ({
  onAccountSettingClick,
  onLogoutClick,
}: UserSettingButtonProps) => {
  const [settingElement, setSettingElement] = useState<HTMLElement | null>(
    null,
  );
  const isSettingsOpen = Boolean(settingElement);

  const toggleSetting = (e: React.MouseEvent<HTMLElement>) => {
    if (settingElement === null) {
      setSettingElement(e.currentTarget);
    } else {
      setSettingElement(null);
    }
  };

  const handleSettingClick = (onClick: () => void) => {
    setSettingElement(null);
    onClick();
  };

  return (
    <>
      <SettingButtonWrapper onClick={toggleSetting}>
        <Icon icon="settingsIcon" />
      </SettingButtonWrapper>
      <Popover
        open={isSettingsOpen}
        anchorEl={settingElement}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <ClickAwayListener onClickAway={() => setSettingElement(null)}>
          <SettingSection>
            <UserProfileSettingItem
              label={ACCOUNT_SETTING}
              onClick={() => handleSettingClick(onAccountSettingClick)}
            />
            <UserProfileSettingItem label={LOGOUT} onClick={onLogoutClick} />
          </SettingSection>
        </ClickAwayListener>
      </Popover>
    </>
  );
};

const SettingButtonWrapper = styled.div`
  width: 2.8rem;
  height: 2.8rem;
  position: absolute;
  top: 0.9rem;
  right: 1.2rem;
  cursor: pointer;

  svg {
    width: 2.8rem;
    height: 2.8rem;
  }
`;

const SettingSection = styled.ul`
  top: 3rem;
  width: 11rem;
  background-color: ${({ theme }) => theme.colors.white};
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.15);
`;

export default memo(UserSettingButton);
