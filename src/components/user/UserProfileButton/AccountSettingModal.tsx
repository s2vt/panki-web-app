import { styled as muiStyled } from '@mui/material/styles';
import { Box } from '@mui/system';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import HttpError from '@src/api/model/HttpError';
import {
  ACCOUNT_SETTING,
  CANCEL,
  EDIT,
  PASSWORD,
  SAVE,
} from '@src/constants/constantString';
import { PASSWORD_VALIDATOR } from '@src/constants/validators';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { UpdatePasswordPayload } from '@src/types/auth.types';
import Icon from '@src/components/common/Icon';
import MainButton from '@src/components/common/MainButton';
import Input from '@src/components/form/Input';
import useUpdatePasswordMutation from './hooks/useUpdatePasswordMutation';

const StyledBox = muiStyled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: '51.4rem',
  minHeight: '55.1rem',
  padding: '4.4rem 4.4rem 5rem',
  background: theme.palette.background.default,
  color: theme.palette.primary.main,
  fontSize: '1.4rem',
}));

export type AccountSettingModalProps = {
  onClose: () => void;
};

const AccountSettingModal = ({ onClose }: AccountSettingModalProps) => {
  const { openAlertDialog } = useAlertDialogAction();
  const [isPasswordModify, setIsPasswordModify] = useState(false);

  const {
    register,
    handleSubmit,
    setFocus,
    formState: { isSubmitting },
  } = useForm<UpdatePasswordPayload & { newPasswordCheck: string }>();

  useEffect(() => {
    if (isPasswordModify) {
      setFocus('password');
    }
  }, [isPasswordModify, setFocus]);

  const handleUpdatePasswordError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { mutate: updatePasswordMutate } = useUpdatePasswordMutation({
    onSuccess: onClose,
    onError: handleUpdatePasswordError,
  });

  const handleUpdatePasswordSubmit = (
    passwordResetPayload: UpdatePasswordPayload & { newPasswordCheck: string },
  ) => {
    const { password, newPassword, newPasswordCheck } = passwordResetPayload;

    if (newPassword !== newPasswordCheck) {
      openAlertDialog({ text: '비밀번호를 확인하세요', position: 'rightTop' });
      return;
    }

    updatePasswordMutate({
      password,
      newPassword,
    });
  };

  return (
    <StyledBox>
      <form onSubmit={handleSubmit(handleUpdatePasswordSubmit)}>
        <Title>{ACCOUNT_SETTING}</Title>
        <ContentSection>
          <PasswordIconWrapper>
            <Icon icon="passwordIcon" />
          </PasswordIconWrapper>
          <Row>
            <Label>{PASSWORD}</Label>
            {isPasswordModify ? (
              <PasswordInputSection>
                <StyledInput
                  placeholder="현재 비밀번호 입력"
                  type="password"
                  {...register('password', PASSWORD_VALIDATOR)}
                />
                <StyledInput
                  placeholder="새 비밀번호 (8~32자의 특수기호, 숫자 포함)"
                  type="password"
                  {...register('newPassword', PASSWORD_VALIDATOR)}
                />
                <StyledInput
                  placeholder="새 비밀번호 확인"
                  type="password"
                  {...register('newPasswordCheck', PASSWORD_VALIDATOR)}
                />
              </PasswordInputSection>
            ) : (
              <PasswordSection>
                <p>*******</p>
                <EditButton onClick={() => setIsPasswordModify(true)}>
                  {EDIT}
                </EditButton>
              </PasswordSection>
            )}
          </Row>
        </ContentSection>
        <ButtonSection>
          <MainButton
            label={CANCEL}
            labelColor="primary"
            width="9.5rem"
            backgroundColor="white"
            fontSize="1.6rem"
            onClick={onClose}
          />
          <MainButton
            type={isPasswordModify ? 'submit' : 'button'}
            label={SAVE}
            width="9.5rem"
            fontSize="1.6rem"
            disabled={isSubmitting}
          />
        </ButtonSection>
      </form>
    </StyledBox>
  );
};

const Title = styled.h1`
  margin-bottom: 6rem;
  font-size: 2.2rem;
  font-weight: 700;
  text-align: center;
`;

const PasswordIconWrapper = styled.div`
  margin-bottom: 6.5rem;
  text-align: center;
`;

const ContentSection = styled.section`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const PasswordSection = styled.section`
  flex: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const EditButton = styled.button`
  color: ${({ theme }) => theme.colors.gray};
  border: none;
  background: none;
  cursor: pointer;
`;

const Row = styled.div`
  display: flex;
`;

const Label = styled.p`
  width: 19.8rem;
  font-weight: 700;
`;

const PasswordInputSection = styled.section`
  display: flex;
  flex-direction: column;
  gap: 0.8rem;
`;

const StyledInput = styled(Input)`
  width: 21.7rem;
  height: 3.7rem;
  border: none;
  background: ${({ theme }) => theme.colors.lightGray};
  font-size: 1rem;
`;

const ButtonSection = styled.section`
  margin-top: 6rem;
  height: 5rem;
  display: flex;
  justify-content: flex-end;
  gap: 2.7rem;
`;

export default AccountSettingModal;
