import { Meta, Story } from '@storybook/react';
import UserProfileButton, {
  UserProfileButtonProps,
} from '../UserProfileButton';

export default {
  title: 'Components/User/UserProfileButton',
  component: UserProfileButton,
} as Meta;

const Template: Story<UserProfileButtonProps> = (args) => (
  <UserProfileButton {...args} />
);

export const Basic = Template.bind({});
