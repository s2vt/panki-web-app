import { Meta, Story } from '@storybook/react';
import EditProfileModal, { EditProfileModalProps } from '../EditProfileModal';

export default {
  title: 'Components/user/UserProfileButton/EditProfileModal',
  component: EditProfileModal,
} as Meta;

const Template: Story<EditProfileModalProps> = (args) => (
  <EditProfileModal {...args} />
);

export const Basic = Template.bind({});
