import { Meta, Story } from '@storybook/react';
import UserSettingButton, {
  UserSettingButtonProps,
} from '../UserSettingButton';

export default {
  title: 'Components/User/UserProfileButton/UserSettingButton',
  component: UserSettingButton,
} as Meta;

const Template: Story<UserSettingButtonProps> = (args) => (
  <UserSettingButton {...args} />
);

export const Basic = Template.bind({});
