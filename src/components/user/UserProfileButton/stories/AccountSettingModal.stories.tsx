import { Meta, Story } from '@storybook/react';
import AccountSettingModal, {
  AccountSettingModalProps,
} from '../AccountSettingModal';

export default {
  title: 'Components/user/UserProfileButton/AccountSettingModal',
  component: AccountSettingModal,
} as Meta;

const Template: Story<AccountSettingModalProps> = (args) => (
  <AccountSettingModal {...args} />
);

export const Basic = Template.bind({});
