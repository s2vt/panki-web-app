import { Meta, Story } from '@storybook/react';
import UserProfileSettingItem, {
  UserProfileSettingItemProps,
} from '../UserProfileSettingItem';

export default {
  title: 'Components/User/UserProfileButton/UserProfileSettingItem',
  component: UserProfileSettingItem,
} as Meta;

const Template: Story<UserProfileSettingItemProps> = (args) => (
  <UserProfileSettingItem {...args} />
);

export const Basic = Template.bind({});
