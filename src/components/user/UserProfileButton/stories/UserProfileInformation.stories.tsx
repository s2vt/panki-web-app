import { Meta, Story } from '@storybook/react';
import UserProfileInformation, {
  UserProfileInformationProps,
} from '../UserProfileInformation';

export default {
  title: 'Components/User/MyPageDialog/UserProfileInformation',
  component: UserProfileInformation,
} as Meta;

const Template: Story<UserProfileInformationProps> = (args) => (
  <UserProfileInformation {...args} />
);

export const Basic = Template.bind({});
