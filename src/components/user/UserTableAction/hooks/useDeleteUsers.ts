import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { User } from '@src/types/user.types';
import { useCallback, useState } from 'react';
import { useMutation } from 'react-query';

type UseDeleteUsersProps = {
  selectedUsers: User[];
  onDeleteUsersSuccess: () => void;
  onDeleteUsersError: (error: HttpError) => void;
};

const useDeleteUsers = ({
  selectedUsers,
  onDeleteUsersSuccess,
  onDeleteUsersError,
}: UseDeleteUsersProps) => {
  const [isDeleteUserModalOpen, setIsDeleteUserModalOpen] = useState(false);
  const { openAlertDialog } = useAlertDialogAction();

  const { mutate } = useMutation(
    (userIds: string[]) => userApi.deleteUsers(userIds),
    {
      onSuccess: onDeleteUsersSuccess,
      onError: onDeleteUsersError,
    },
  );

  const closeDeleteUserModal = useCallback(() => {
    setIsDeleteUserModalOpen(false);
  }, [setIsDeleteUserModalOpen]);

  const handleDeleteClick = useCallback(() => {
    if (selectedUsers.length <= 0) {
      openAlertDialog({
        text: '삭제할 유저를 선택하세요',
        position: 'rightTop',
      });
      return;
    }

    setIsDeleteUserModalOpen(true);
  }, [selectedUsers, openAlertDialog, setIsDeleteUserModalOpen]);

  const handleDeleteUserSubmit = useCallback(() => {
    mutate(selectedUsers.map((selectedUser) => selectedUser.id));
  }, [selectedUsers, mutate]);

  return {
    isDeleteUserModalOpen,
    closeDeleteUserModal,
    handleDeleteClick,
    handleDeleteUserSubmit,
  };
};

export default useDeleteUsers;
