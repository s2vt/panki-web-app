import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import { SavedUser, SaveUserPayload } from '@src/types/user.types';
import { useCallback, useState } from 'react';
import { useMutation } from 'react-query';

type UseAddUserProps = {
  onAddUserSuccess: (savedUser: SavedUser) => void;
  onAddUserError: (error: HttpError) => void;
};

const useAddUser = ({ onAddUserSuccess, onAddUserError }: UseAddUserProps) => {
  const [isAddUserModalOpen, setIsAddUserModalOpen] = useState(false);

  const { mutate, isLoading: isAddUserLoading } = useMutation(
    (saveUserPayload: SaveUserPayload) => userApi.createUser(saveUserPayload),
    {
      onSuccess: onAddUserSuccess,
      onError: onAddUserError,
    },
  );

  const closeAddUserModal = useCallback(
    () => setIsAddUserModalOpen(false),
    [setIsAddUserModalOpen],
  );

  const handleAddClick = useCallback(() => {
    setIsAddUserModalOpen(true);
  }, [setIsAddUserModalOpen]);

  const handleAddUserSubmit = useCallback(
    (saveUserPayload: SaveUserPayload) => {
      if (!isAddUserLoading) {
        mutate(saveUserPayload);
      }
    },
    [isAddUserLoading, mutate],
  );

  return {
    isAddUserModalOpen,
    isAddUserLoading,
    closeAddUserModal,
    handleAddClick,
    handleAddUserSubmit,
  };
};

export default useAddUser;
