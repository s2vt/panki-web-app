import { memo, useCallback, useRef, useState } from 'react';
import { useQueryClient } from 'react-query';
import {
  ADD_USER,
  DELETE_USER,
  FILTERING,
  REFRESH,
  USER_SEARCH_PLACEHOLDER,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import DropdownMultiSelectWrapper from '@src/components/common/DropdownMultiSelectWrapper';
import SearchInput from '@src/components/common/SearchInput';
import RoundButton from '@src/components/common/RoundButton';
import TableActionWrapper from '@src/components/table/TableActionWrapper';
import { SavedUser, User, UserClass } from '@src/types/user.types';
import HttpError from '@src/api/model/HttpError';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import ConfirmModal from '@src/components/common/ConfirmModal';
import { FilterDropdownItem } from '@src/types/common.types';
import ModalWrapper from '@src/components/base/ModalWrapper';
import SaveUserResultModal from '../SaveUserResultModal';
import SaveUserModal from '../SaveUserModal';
import useDeleteUsers from './hooks/useDeleteUsers';
import useAddUser from './hooks/useAddUser';

export type UserTableActionProps = {
  isAdminOrManagerUser: boolean;
  selectedUsers: User[];
  onRefetchUsers: () => void;
  onSearchEnter: (value: string) => void;
  onFilterChange: (userClass?: UserClass) => void;
};

const FILTER_DROPDOWN_ITEMS: FilterDropdownItem<User>[] = [
  { id: '1', label: '모든 사용자', value: undefined },
  { id: '2', label: '관리자', value: UserClass.admin },
  { id: '3', label: '매니저', value: UserClass.manager },
  { id: '4', label: '직원', value: UserClass.employee },
];

const UserTableAction = ({
  isAdminOrManagerUser,
  selectedUsers,
  onRefetchUsers,
  onSearchEnter,
  onFilterChange,
}: UserTableActionProps) => {
  const [isSaveUserResultModalOpen, setIsSaveUserResultModalOpen] =
    useState(false);

  const { selectedItems, toggleItem, setSelectedItems, isSelected } =
    useSelectItems<FilterDropdownItem<User>>([FILTER_DROPDOWN_ITEMS[0]]);
  const [savedUser, setSavedUser] = useState<SavedUser>();
  const searchInputRef = useRef<HTMLInputElement>(null);

  const queryClient = useQueryClient();
  const { openAlertDialog } = useAlertDialogAction();

  const closeSaveUserResultModal = useCallback(() => {
    setIsSaveUserResultModalOpen(false);
  }, [setIsSaveUserResultModalOpen]);

  const handleFilterChange = useCallback(
    (selectItem: FilterDropdownItem<User>) => {
      if (selectedItems.some((item) => item.id === selectItem.id)) {
        return;
      }

      setSelectedItems([]);
      toggleItem(selectItem);
      onFilterChange(selectItem.value as UserClass);
    },
    [selectedItems, toggleItem, onFilterChange],
  );

  const handleRefreshClick = useCallback(() => {
    if (searchInputRef.current !== null) {
      searchInputRef.current.value = '';
      onSearchEnter('');
    }

    onRefetchUsers();
  }, [queryClient, searchInputRef]);

  const handleAddUserSuccess = useCallback(
    (savedUser: SavedUser) => {
      setSavedUser(savedUser);
      closeAddUserModal();
      setIsSaveUserResultModalOpen(true);
      onRefetchUsers();
    },
    [setSavedUser, setIsSaveUserResultModalOpen, onRefetchUsers],
  );

  const handleDeleteUsersSuccess = () => {
    closeDeleteUserModal();
    onRefetchUsers();
  };

  const handleMutateError = (error: HttpError) =>
    openAlertDialog({ text: error.message, position: 'rightTop' });

  const {
    handleAddClick,
    handleAddUserSubmit,
    isAddUserLoading,
    isAddUserModalOpen,
    closeAddUserModal,
  } = useAddUser({
    onAddUserSuccess: handleAddUserSuccess,
    onAddUserError: handleMutateError,
  });

  const {
    closeDeleteUserModal,
    handleDeleteClick,
    handleDeleteUserSubmit,
    isDeleteUserModalOpen,
  } = useDeleteUsers({
    onDeleteUsersSuccess: handleDeleteUsersSuccess,
    onDeleteUsersError: handleMutateError,
    selectedUsers,
  });

  return (
    <>
      <TableActionWrapper>
        {isAdminOrManagerUser && (
          <>
            <RoundButton label={ADD_USER} onClick={handleAddClick} />
            <RoundButton label={DELETE_USER} onClick={handleDeleteClick} />
          </>
        )}
        <RoundButton label={REFRESH} onClick={handleRefreshClick} />
        <DropdownMultiSelectWrapper
          dropdownItems={FILTER_DROPDOWN_ITEMS}
          isSelected={isSelected}
          onChange={handleFilterChange}
          checkboxSize="xs"
        >
          <RoundButton label={FILTERING} />
        </DropdownMultiSelectWrapper>
        <SearchInput
          placeholder={USER_SEARCH_PLACEHOLDER}
          onSearchEnter={onSearchEnter}
          ref={searchInputRef}
        />
      </TableActionWrapper>
      <ModalWrapper isOpen={isAddUserModalOpen}>
        <SaveUserModal
          onClose={closeAddUserModal}
          onSubmit={handleAddUserSubmit}
          isSubmitting={isAddUserLoading}
        />
      </ModalWrapper>
      <ModalWrapper isOpen={isDeleteUserModalOpen}>
        <ConfirmModal
          text="정말 해당 사용자를 삭제하시겠습니까?"
          onConfirm={handleDeleteUserSubmit}
          onClose={closeDeleteUserModal}
        />
      </ModalWrapper>
      {savedUser && (
        <ModalWrapper isOpen={isSaveUserResultModalOpen}>
          <SaveUserResultModal
            savedUser={savedUser}
            onClose={closeSaveUserResultModal}
          />
        </ModalWrapper>
      )}
    </>
  );
};

export default memo(UserTableAction);
