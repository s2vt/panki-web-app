import userApi from '@src/api/userApi';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { User } from '@src/types/user.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import user from '@src/test/fixtures/user/user';
import HttpError from '@src/api/model/HttpError';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import useDeleteUsers from '../hooks/useDeleteUsers';

describe('useDeleteUsers hook', () => {
  const setup = (selectedUsers: User[] = []) => {
    const { wrapper, store } = prepareMockWrapper();

    const onDeleteUsersError = jest.fn();
    const onDeleteUsersSuccess = jest.fn();
    const deleteUsers = jest.spyOn(userApi, 'deleteUsers');

    const { result, waitFor } = renderHook(
      () =>
        useDeleteUsers({
          selectedUsers,
          onDeleteUsersError,
          onDeleteUsersSuccess,
        }),
      { wrapper },
    );

    return {
      result,
      waitFor,
      store,
      onDeleteUsersError,
      onDeleteUsersSuccess,
      deleteUsers,
    };
  };

  it('should call open alert dialog action when has not user selected', () => {
    // given
    const selectedUsers: User[] = [];

    const { result, store } = setup(selectedUsers);

    // when
    act(() => result.current.handleDeleteClick());

    // then
    expect(store.getActions()[0].meta.arg.text).toEqual(
      '삭제할 유저를 선택하세요',
    );
    expect(result.current.isDeleteUserModalOpen).toEqual(false);
  });

  it('should set isDeleteUserModalOpen to true when has user selected', () => {
    // given
    const selectedUsers: User[] = [user];

    const { result } = setup(selectedUsers);

    // when
    act(() => result.current.handleDeleteClick());

    // then
    expect(result.current.isDeleteUserModalOpen).toEqual(true);
  });

  it('should call onDeleteUsersError when api request failed', async () => {
    // given
    const selectedUsers: User[] = [user];

    const { result, waitFor, onDeleteUsersError, deleteUsers } =
      setup(selectedUsers);

    deleteUsers.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleDeleteUserSubmit();
    });

    // then
    await waitFor(() => expect(onDeleteUsersError).toBeCalled());
  });

  it('should call onDeleteUsersSuccess when api request succeed', async () => {
    // given
    const selectedUsers: User[] = [user];

    const { result, waitFor, onDeleteUsersSuccess, deleteUsers } =
      setup(selectedUsers);

    deleteUsers.mockResolvedValue({
      msg: 'success',
      resultCode: ResultCode.success,
    });

    // when
    act(() => {
      result.current.handleDeleteUserSubmit();
    });

    // then
    await waitFor(() => expect(onDeleteUsersSuccess).toBeCalled());
    expect(deleteUsers).toBeCalledWith([user.id]);
  });
});
