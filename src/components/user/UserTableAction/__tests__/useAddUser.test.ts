import HttpError from '@src/api/model/HttpError';
import userApi from '@src/api/userApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import { act, renderHook } from '@testing-library/react-hooks/dom';
import saveUserPayload from '@src/test/fixtures/user/saveUserPayload';
import user from '@src/test/fixtures/user/user';
import useAddUser from '../hooks/useAddUser';

describe('useAddUser hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    const onAddUserSuccess = jest.fn();
    const onAddUserError = jest.fn();
    const createUser = jest.spyOn(userApi, 'createUser');

    const { result, waitFor } = renderHook(
      () =>
        useAddUser({
          onAddUserSuccess,
          onAddUserError,
        }),
      { wrapper },
    );

    return { result, waitFor, onAddUserSuccess, onAddUserError, createUser };
  };

  it('should set isAddUserModalOpen state to true when call handleAddClick', () => {
    // given
    const { result } = setup();

    // when
    act(() => {
      result.current.handleAddClick();
    });

    // then
    expect(result.current.isAddUserModalOpen).toEqual(true);
  });

  it('should set isAddUserModalOpen state to false when call closeAddUserModal', () => {
    // given
    const { result } = setup();

    // when
    act(() => {
      result.current.closeAddUserModal();
    });

    // then
    expect(result.current.isAddUserModalOpen).toEqual(false);
  });

  it('should call onAddUserError when api request failed', async () => {
    // given
    const { result, waitFor, onAddUserError, createUser } = setup();

    createUser.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    // when
    act(() => {
      result.current.handleAddUserSubmit(saveUserPayload);
    });

    // then
    await waitFor(() => expect(onAddUserError).toBeCalled());
  });

  it('should call onAddUserSuccess when api request succeed', async () => {
    // given
    const { result, waitFor, onAddUserSuccess, createUser } = setup();

    createUser.mockResolvedValue({ ...user, temporaryPassword: '1234' });

    // when
    act(() => {
      result.current.handleAddUserSubmit(saveUserPayload);
    });

    // then
    await waitFor(() => expect(onAddUserSuccess).toBeCalled());
    expect(createUser).toBeCalledWith(saveUserPayload);
  });
});
