import { Meta, Story } from '@storybook/react';
import UserTableAction, { UserTableActionProps } from './UserTableAction';

export default {
  title: 'Components/user/UserTableAction',
  component: UserTableAction,
} as Meta;

const Template: Story<UserTableActionProps> = (args) => (
  <UserTableAction {...args} />
);

export const Basic = Template.bind({});
