import { Meta, Story } from '@storybook/react';
import SideOrganizationChart, {
  SideOrganizationChartProps,
} from './SideOrganizationChart';

export default {
  title: 'Components/user/SideOrganizationChart',
  component: SideOrganizationChart,
} as Meta;

const Template: Story<SideOrganizationChartProps> = (args) => (
  <SideOrganizationChart {...args} />
);

export const Basic = Template.bind({});
