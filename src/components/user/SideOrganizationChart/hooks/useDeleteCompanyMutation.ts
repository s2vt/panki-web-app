import { useMutation, UseMutationOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';

const useDeleteCompanyMutation = (
  options?: UseMutationOptions<unknown, HttpError, string>,
) =>
  useMutation(
    (companyId: string) => companyApi.deleteCompany(companyId),
    options,
  );

export default useDeleteCompanyMutation;
