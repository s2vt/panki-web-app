import { useMutation, UseMutationOptions } from 'react-query';
import companyApi from '@src/api/companyApi';
import HttpError from '@src/api/model/HttpError';
import { CreateCompanyPayload } from '@src/types/company.types';

const useCreateCompanyMutation = (
  options?: UseMutationOptions<unknown, HttpError, CreateCompanyPayload>,
) =>
  useMutation(
    (createCompanyPayload: CreateCompanyPayload) =>
      companyApi.createCompany(createCompanyPayload),
    options,
  );

export default useCreateCompanyMutation;
