import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import companyApi from '@src/api/companyApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useDeleteCompanyMutation from '../hooks/useDeleteCompanyMutation';

describe('useDeleteCompanyMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useDeleteCompanyMutation(), {
      wrapper,
    });
  };

  it('should isSuccess is true when succeed delete company mutate', async () => {
    // given
    const companyId = '1';

    companyApi.deleteCompany = jest.fn().mockResolvedValue('success');

    // when
    const { result, waitFor } = setup();

    act(() => result.current.mutate(companyId));

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.isSuccess).toEqual(true);
  });

  it('should set error when failed delete company mutate', async () => {
    // given
    const companyId = '1';

    companyApi.deleteCompany = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() => result.current.mutate(companyId));

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
