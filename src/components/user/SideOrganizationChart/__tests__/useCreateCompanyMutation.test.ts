import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks/dom';
import companyApi from '@src/api/companyApi';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useCreateCompanyMutation from '../hooks/useCreateCompanyMutation';

describe('useCreateCompanyMutation hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();
    return renderHook(() => useCreateCompanyMutation(), {
      wrapper,
    });
  };

  it('should isSuccess is true when succeed create company mutate', async () => {
    // given
    const companyName = 'company';
    const parentId = '1';

    companyApi.createCompany = jest.fn().mockResolvedValue('success');

    // when
    const { result, waitFor } = setup();

    act(() => result.current.mutate({ companyName, parentId }));

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.isSuccess).toEqual(true);
  });

  it('should set error when failed create company mutate', async () => {
    // given
    const companyName = 'company';
    const parentId = '1';

    companyApi.createCompany = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    act(() => result.current.mutate({ companyName, parentId }));

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
