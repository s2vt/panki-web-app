import React, { memo, useCallback, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import HttpError from '@src/api/model/HttpError';
import {
  ADD_ORGANIZATION,
  ADD_ORGANIZATION_ERROR,
  CANCEL,
  DELETE,
  DELETE_COMPANY_CONFIRM_MESSAGE,
  DELETE_ORGANIZATION_ERROR,
  FAIL_DATA_FETCH,
  MODIFY,
  ORGANIZATION_CHART,
} from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useOrganizationChartQuery from '@src/hooks/company/useOrganizationChartQuery';
import { CreateCompanyPayload } from '@src/types/company.types';
import { Company } from '@src/types/user.types';
import ConfirmModal from '@src/components/common/ConfirmModal';
import ModifyButton from '@src/components/common/ModifyButton';
import RoundButton from '@src/components/common/RoundButton';
import SideBox from '@src/components/common/SideBox';
import Divider from '@src/components/common/Divider';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import ErrorBox from '@src/components/common/ErrorBox';
import useUserSelector from '@src/hooks/user/useUserSelector';
import ModalWrapper from '@src/components/base/ModalWrapper';
import OrganizationChart from '../OrganizationChart';
import useCreateCompanyMutation from './hooks/useCreateCompanyMutation';
import useDeleteCompanyMutation from './hooks/useDeleteCompanyMutation';

export type SideOrganizationChartProps = {
  onSelect?: (selectedCompany: Company | undefined) => void;
};

const SideOrganizationChart = ({ onSelect }: SideOrganizationChartProps) => {
  const { isAdminUser } = useUserSelector();
  const { openAlertDialog } = useAlertDialogAction();

  const [isModifyMode, setIsModifyMode] = useState(false);
  const [isDeleteConfirmModalOpen, setIsDeleteConfirmModalOpen] =
    useState(false);
  const [selectedCompany, setSelectedCompany] = useState<Company | undefined>();

  const {
    data: company,
    isFetching: isOrganizationChartFetching,
    isSuccess: isOrganizationChartQuerySuccess,
    isError: isOrganizationChartQueryError,
    refetch: refetchOrganizationChart,
  } = useOrganizationChartQuery();

  useEffect(() => {
    if (company) {
      setValue('parentId', company?.id);
    }
  }, [company]);

  const { setValue, getValues } = useForm<CreateCompanyPayload>({
    defaultValues: { parentId: company?.id ?? '' },
  });

  const renderModifyButton = useCallback(() => {
    if (!isAdminUser || isModifyMode) {
      return null;
    }

    return <ModifyButton label={MODIFY} onClick={handleModifyClick} />;
  }, [isModifyMode, isAdminUser]);

  const handleCompanyClick = useCallback(
    (company: Company | undefined) => {
      setSelectedCompany(company);
      if (onSelect !== undefined) {
        onSelect(company);
      }
    },
    [setSelectedCompany, onSelect],
  );

  const handleModifyClick = useCallback(
    () => setIsModifyMode(true),
    [setIsModifyMode],
  );

  const handleCancelClick = useCallback(
    () => setIsModifyMode(false),
    [setIsModifyMode],
  );

  const handleConfirmModalClose = useCallback(
    () => setIsDeleteConfirmModalOpen(false),
    [setIsDeleteConfirmModalOpen],
  );

  const handleAddOrganizationInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setValue('companyName', e.target.value);
    },
    [setValue],
  );

  const handleMutateError = (e: HttpError) => {
    openAlertDialog({ text: e.message, position: 'rightTop' });
  };

  const handleDeleteCompanyMutateSuccess = useCallback(() => {
    setIsDeleteConfirmModalOpen(false);
    setSelectedCompany(undefined);
    refetchOrganizationChart();
  }, [setIsDeleteConfirmModalOpen, refetchOrganizationChart]);

  const { mutate: createCompanyMutate } = useCreateCompanyMutation({
    onSuccess: () => refetchOrganizationChart(),
    onError: handleMutateError,
  });

  const { mutate: deleteCompanyMutate } = useDeleteCompanyMutation({
    onSuccess: handleDeleteCompanyMutateSuccess,
    onError: handleMutateError,
  });

  const handleAddCompanyClick = useCallback(() => {
    const { companyName, parentId } = getValues();
    if (!companyName) {
      openAlertDialog({ text: ADD_ORGANIZATION_ERROR, position: 'rightTop' });
      return;
    }

    createCompanyMutate({ companyName, parentId });
  }, [openAlertDialog, getValues, createCompanyMutate]);

  const handleDeleteCompanyClick = useCallback(() => {
    if (!selectedCompany) {
      openAlertDialog({
        text: DELETE_ORGANIZATION_ERROR,
        position: 'rightTop',
      });
      return;
    }

    setIsDeleteConfirmModalOpen(true);
  }, [openAlertDialog, setIsDeleteConfirmModalOpen, selectedCompany]);

  const handleDeleteCompanyConfirm = useCallback(() => {
    if (selectedCompany) {
      deleteCompanyMutate(selectedCompany.id);
    }
  }, [selectedCompany, deleteCompanyMutate]);

  if (isOrganizationChartFetching) {
    return (
      <SideBox>
        <LoadingSpinner />
      </SideBox>
    );
  }

  if (isOrganizationChartQueryError) {
    return (
      <SideBox>
        <ErrorBox
          error={FAIL_DATA_FETCH}
          onRefetchClick={refetchOrganizationChart}
        />
      </SideBox>
    );
  }

  return (
    <>
      <SideBox title={ORGANIZATION_CHART} renderButton={renderModifyButton}>
        {company !== undefined && isOrganizationChartQuerySuccess && (
          <>
            {isModifyMode ? (
              <ButtonSection>
                <RoundButton
                  label={ADD_ORGANIZATION}
                  size="xs"
                  onClick={handleAddCompanyClick}
                />
                <RoundButton
                  label={DELETE}
                  size="xs"
                  onClick={handleDeleteCompanyClick}
                />
                <RoundButton
                  label={CANCEL}
                  size="xs"
                  onClick={handleCancelClick}
                />
              </ButtonSection>
            ) : (
              <Divider orientation="horizontal" />
            )}
            <ChartSection>
              <OrganizationChart
                company={company}
                level={0}
                selectedCompany={selectedCompany}
                isModifyMode={isModifyMode}
                onClick={handleCompanyClick}
                onAddOrganizationInputChange={handleAddOrganizationInputChange}
              />
            </ChartSection>
          </>
        )}
      </SideBox>
      <ModalWrapper isOpen={isDeleteConfirmModalOpen}>
        <ConfirmModal
          onClose={handleConfirmModalClose}
          onConfirm={handleDeleteCompanyConfirm}
          text={DELETE_COMPANY_CONFIRM_MESSAGE}
        />
      </ModalWrapper>
    </>
  );
};

const ButtonSection = styled.div`
  display: flex;
  align-items: center;
  gap: 0.8rem;
`;

const ChartSection = styled.div`
  margin-top: 2.8rem;
`;

export default memo(SideOrganizationChart);
