import RequiredIcon from '@src/components/common/RequiredIcon';
import { memo } from 'react';
import styled from 'styled-components';

export type FormLabelProps = React.LabelHTMLAttributes<HTMLLabelElement> & {
  id?: string | undefined;
  label: string;
  /** 라벨과 인풋 사이의 간격 */
  labelPadding?: string | number;
  /** 라벨 텍스트 사이즈 */
  labelFontSize?: string | number;
  isRequired?: boolean;
};

const FormLabel = ({
  id,
  label,
  labelPadding,
  labelFontSize,
  isRequired,
  ...rest
}: FormLabelProps) => (
  <Label
    htmlFor={id}
    labelPadding={labelPadding}
    labelFontSize={labelFontSize}
    {...rest}
  >
    {label}
    {isRequired && <RequiredIcon />}
  </Label>
);

const Label = styled.label<{
  labelPadding?: string | number;
  labelFontSize?: string | number;
}>`
  margin-bottom: ${({ labelPadding }) => labelPadding ?? '0.7rem'};
  font-size: ${({ labelFontSize }) => labelFontSize ?? '1.8rem'};
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

export default memo(FormLabel);
