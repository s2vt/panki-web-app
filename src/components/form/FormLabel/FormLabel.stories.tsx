import { Meta, Story } from '@storybook/react';
import FormLabel, { FormLabelProps } from './FormLabel';

export default {
  title: 'Components/form/FormLabel',
  component: FormLabel,
} as Meta;

const Template: Story<FormLabelProps> = (args) => <FormLabel {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: '라벨',
};
