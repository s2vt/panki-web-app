import { forwardRef, InputHTMLAttributes, memo } from 'react';
import styled from 'styled-components';
import FormInput, { FormInputProps } from '../FormInput';

export type RowFormInputProps = InputHTMLAttributes<HTMLInputElement> &
  FormInputProps & {
    className?: string;
  };

const RowFormInput = (
  { className, ...rest }: RowFormInputProps,
  ref: React.Ref<HTMLInputElement>,
) => <StyledFormInput className={className} ref={ref} {...rest} />;

const StyledFormInput = memo(styled(FormInput)`
  flex: 1;
  flex-direction: row;
  align-items: center;
  max-width: 68.4rem;

  label {
    min-width: 16.1rem;
  }

  input {
    border: none;
    height: 4.6rem;
    font-weight: 400;

    ::placeholder {
      font-size: 1.6rem;
      font-weight: 400;
      color: ${({ theme }) => theme.colors.primary};
    }

    :read-only {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`);

export default memo(forwardRef(RowFormInput));
