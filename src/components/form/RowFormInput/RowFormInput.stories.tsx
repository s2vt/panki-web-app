import { Meta, Story } from '@storybook/react';
import RowFormInput, { RowFormInputProps } from './RowFormInput';

export default {
  title: 'Components/form/RowFormInput',
  component: RowFormInput,
} as Meta;

const Template: Story<RowFormInputProps> = (args) => <RowFormInput {...args} />;

export const Basic = Template.bind({});
