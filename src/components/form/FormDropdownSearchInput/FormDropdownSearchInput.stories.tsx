import { Meta, Story } from '@storybook/react';
import FormDropdownSearchInput, {
  FormDropdownSearchInputProps,
} from './FormDropdownSearchInput';

export default {
  title: 'Components/form/FormDropdownSearchInput',
  component: FormDropdownSearchInput,
} as Meta;

const Template: Story<FormDropdownSearchInputProps> = (args) => (
  <FormDropdownSearchInput {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  dropdownItems: ['아이템1', '아이템2', '아이템3', '아이템4'],
  onSearch: () => {
    console.log();
  },
};
