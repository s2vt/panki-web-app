import React, { forwardRef, memo, useRef } from 'react';
import styled from 'styled-components';
import useDetectOutsideClick from '@src/hooks/common/useDetectOutsideClick';
import { FormInputProps } from '../FormInput/FormInput';
import FormLabel from '../FormLabel';
import Input from '../Input';

export type FormDropdownSearchInputProps = FormInputProps & {
  /** 검색이 완료된 후 실행할 이벤트 */
  onSearch: (searchValue: string) => void;
  /** 검색어 입력 후 얼마나 시간이 지난 후 onSearch 이벤트를 실행할지 지정 */
  searchDelayDuration: number;
  dropdownItems: string[];
};

const ARROW_DOWN_KEY = 'ArrowDown';
const ARROW_UP_KEY = 'ArrowUp';

const FormDropdownSearchInput = (
  {
    onSearch,
    searchDelayDuration,
    dropdownItems,
    label,
    name,
    errorMessage,
    labelPadding,
    labelFontSize,
    isRequired,
    className,
    onChange,
    ...rest
  }: FormDropdownSearchInputProps,
  ref: React.Ref<HTMLInputElement>,
) => {
  const dropdownInputRef = useRef<HTMLDivElement>(null);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const searchTimeoutRef = useRef<NodeJS.Timeout | null>(null);
  const dropdownItemRefs = useRef<HTMLButtonElement[]>([]);

  const [isDropdownOpen, setIsDropdownOpen] =
    useDetectOutsideClick(dropdownInputRef);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (searchTimeoutRef.current !== null) {
      clearTimeout(searchTimeoutRef.current);
    }

    searchTimeoutRef.current = setTimeout(
      () => onSearch(e.target.value),
      searchDelayDuration,
    );

    if (onChange !== undefined) {
      onChange(e);
    }
  };

  const handleDropdownItemSelect = (dropdownItem: string) => {
    onSearch(dropdownItem);
    setIsDropdownOpen(false);
  };

  const handleFocus = () => {
    setIsDropdownOpen(true);
  };

  const focusToDropdownItem = (index: number) => {
    dropdownItemRefs.current[index]?.focus();
  };

  const handleInputKeydown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (!isDropdownOpen || dropdownItems.length === 0) {
      return;
    }

    if (e.key === ARROW_DOWN_KEY || e.key === ARROW_UP_KEY) {
      e.preventDefault();
    }

    if (e.key === ARROW_DOWN_KEY) {
      focusToDropdownItem(0);
    }

    if (e.key === ARROW_UP_KEY) {
      const lastDropdownItemIndex = dropdownItems.length - 1;
      focusToDropdownItem(lastDropdownItemIndex);
    }
  };

  const handleDropdownItemKeydown = (
    e: React.KeyboardEvent<HTMLButtonElement>,
    index: number,
  ) => {
    if (e.key === ARROW_DOWN_KEY || e.key === ARROW_UP_KEY) {
      e.preventDefault();
    }

    if (
      (e.key === ARROW_DOWN_KEY || e.key === ARROW_UP_KEY) &&
      dropdownItems.length <= 1
    ) {
      return;
    }

    if (e.key === ARROW_DOWN_KEY) {
      const nextItemIndex = (index + 1) % dropdownItems.length;
      focusToDropdownItem(nextItemIndex);
      return;
    }

    if (e.key === ARROW_UP_KEY) {
      if (index === 0) {
        const lastDropdownItemIndex = dropdownItems.length - 1;
        focusToDropdownItem(lastDropdownItemIndex);
        return;
      }

      const previousItemIndex = (index - 1) % dropdownItems.length;
      focusToDropdownItem(previousItemIndex);
    }
  };

  return (
    <Box className={className}>
      <FormInputWrapper>
        {label && (
          <FormLabel
            id={name}
            label={label}
            isRequired={isRequired}
            labelFontSize={labelFontSize}
            labelPadding={labelPadding}
            htmlFor={name}
          />
        )}
        <DropdownInputWrapper ref={dropdownInputRef}>
          <Input
            {...rest}
            ref={ref}
            onChange={handleInputChange}
            onFocus={handleFocus}
            onKeyDown={handleInputKeydown}
          />
          {isDropdownOpen && dropdownItems.length !== 0 && (
            <DropdownSection ref={dropdownRef}>
              {dropdownItems.map((dropdownItem, index) => (
                <DropdownItem
                  key={dropdownItem}
                  onClick={() => handleDropdownItemSelect(dropdownItem)}
                  onKeyDown={(e: React.KeyboardEvent<HTMLButtonElement>) =>
                    handleDropdownItemKeydown(e, index)
                  }
                  ref={(el: HTMLButtonElement) => {
                    dropdownItemRefs.current[index] = el;
                  }}
                >
                  {dropdownItem}
                </DropdownItem>
              ))}
            </DropdownSection>
          )}
        </DropdownInputWrapper>
      </FormInputWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`;

const FormInputWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
`;

const DropdownInputWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`;

const DropdownSection = styled.div`
  width: 100%;
  padding: 2.1rem;
  display: flex;
  flex-direction: column;
  gap: 1.7rem;
  position: absolute;
  background-color: ${({ theme }) => theme.colors.white};
  z-index: ${({ theme }) => theme.zIndexes.dropdown};
  left: 0;
`;

const DropdownItem = styled.button`
  border: none;
  background: none;
  font-size: 1.6rem;
  text-align: left;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;

  &:focus {
    background: ${({ theme }) => theme.colors.pageBackground};
  }
`;

export default memo(forwardRef(FormDropdownSearchInput));
