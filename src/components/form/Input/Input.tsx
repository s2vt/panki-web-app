import React, { forwardRef, memo } from 'react';
import styled, { css } from 'styled-components';
import { colors } from '@src/styles/theme';
import Icon from '@src/components/common/Icon';

export type InputProps = React.InputHTMLAttributes<HTMLInputElement> & {
  /** 보더 컬러 */
  borderColor?: keyof typeof colors;
  enableShadow?: boolean;
  className?: string;
  children?: React.ReactNode;
  useClearIcon?: boolean;
  isShowClearIcon?: boolean;
  inputWidth?: string | number;
  onClearClick?: () => void;
};

const Input = (
  {
    type = 'text',
    name,
    borderColor = 'primary',
    enableShadow,
    autoComplete = 'off',
    className,
    useClearIcon = false,
    isShowClearIcon = false,
    inputWidth = '100%',
    onClearClick,
    ...rest
  }: InputProps,
  ref: React.Ref<HTMLInputElement>,
) => (
  <InputWrapper inputWidth={inputWidth}>
    <Box
      id={name}
      type={type}
      ref={ref}
      name={name}
      borderColor={borderColor}
      enableShadow={enableShadow}
      autoComplete={autoComplete}
      className={className}
      {...rest}
    />
    {isShowClearIcon && <ClearIcon icon="closeButton" onClick={onClearClick} />}
  </InputWrapper>
);

const InputWrapper = styled.div<{
  inputWidth: string | number;
}>`
  width: ${({ inputWidth }) => inputWidth};
  position: relative;
`;

const Box = styled.input<{
  borderColor: keyof typeof colors;
  enableShadow?: boolean;
}>`
  width: 100%;
  height: 5.6rem;
  padding: 0 1.8rem;
  border: 1px solid ${({ borderColor, theme }) => theme.colors[borderColor]};
  font-size: 1.6rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.primary};
  ${({ enableShadow }) =>
    enableShadow &&
    css`
      box-shadow: 0px 2px 20px 0px rgba(0, 0, 0, 0.05);
    `}

  :read-only {
    color: ${({ theme }) => theme.colors.border};
  }

  ::placeholder {
    color: ${({ theme }) => theme.colors.inactive};
  }
`;

const ClearIcon = styled(Icon)`
  position: absolute;
  padding: 0.541rem;
  width: 2.4rem;
  height: 2.4rem;
  right: 1.6rem;
  top: 50%;
  transform: translateY(-50%);
  cursor: pointer;

  line {
    stroke: rgba(0, 0, 0, 0.54);
  }
`;

export default memo(forwardRef(Input));
