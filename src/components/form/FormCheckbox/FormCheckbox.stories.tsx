import { Meta, Story } from '@storybook/react';
import FormCheckbox, { FormCheckboxProps } from './FormCheckbox';

export default {
  title: 'Components/form/FormCheckbox',
  component: FormCheckbox,
} as Meta;

const Template: Story<FormCheckboxProps> = (args) => <FormCheckbox {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: '개인정보 수집 및 이용에 동의',
  name: 'privacyPolicy',
};

export const IsError = Template.bind({});
IsError.args = {
  label: '이용 약관 동의',
  name: 'privacyPolicy',
  errorMessage: '필수항목을 체크하세요',
};
