import { forwardRef, InputHTMLAttributes, memo, Ref } from 'react';
import styled, { css } from 'styled-components';
import { Size } from '@src/types/style.types';
import Checkbox, { CheckboxIcon } from '@src/components/common/Checkbox';

export type FormCheckboxProps = InputHTMLAttributes<HTMLInputElement> & {
  /** 인풋 라벨 */
  label?: string;
  /** 에러메세지 */
  errorMessage?: string;
  className?: string;
  checkboxIcon?: CheckboxIcon;
  checkboxSize?: Size;
  inactive?: boolean;
};

const FormCheckbox = (
  {
    label,
    name,
    errorMessage,
    className,
    checkboxIcon = 'blankCheckbox',
    checkboxSize,
    readOnly,
    inactive,
    ...rest
  }: FormCheckboxProps,
  ref: Ref<HTMLInputElement>,
) => (
  <Block htmlFor={name} className={className}>
    <Checkbox
      checkboxIcon={checkboxIcon}
      id={name}
      name={name}
      checkboxSize={checkboxSize}
      readOnly={readOnly}
      inactive={inactive}
      {...rest}
      ref={ref}
    />
    <LabelText inactive={inactive}>{label}</LabelText>
    {errorMessage && <ErrorText>{errorMessage}</ErrorText>}
  </Block>
);

const Block = styled.label`
  padding: 0 2.1rem;
  display: flex;
  align-items: center;
  cursor: pointer;
  height: 5.4rem;
`;

const LabelText = styled.p<{ inactive?: boolean }>`
  margin-left: 2.7rem;
  font-size: 1.8rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
  ${({ inactive, theme }) =>
    inactive &&
    css`
      color: ${theme.colors.inactive};
    `}
`;

const ErrorText = styled.p`
  color: red;
`;

export default memo(forwardRef(FormCheckbox));
