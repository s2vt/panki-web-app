import { forwardRef, memo, Ref } from 'react';
import styled from 'styled-components';
import { colors } from '@src/styles/theme';
import FormLabel from '../FormLabel';

type DropdownItem = {
  id: string;
  label: string;
};

export type FormDropdownProps = React.InputHTMLAttributes<HTMLSelectElement> & {
  label?: string;
  dropdownItems: DropdownItem[];
  /** 라벨과 인풋 사이의 간격 */
  labelPadding?: string | number;
  /** 라벨 텍스트 사이즈 */
  labelFontSize?: string | number;
  /** 필수 항목 아이콘 표시여부 */
  isRequired?: boolean;
  borderColor?: keyof typeof colors;
  className?: string;
};

const FormDropdown = (
  {
    label,
    dropdownItems,
    labelPadding,
    labelFontSize,
    isRequired,
    borderColor = 'border',
    className,
    name,
    ...rest
  }: FormDropdownProps,
  ref: Ref<HTMLSelectElement>,
) => (
  <Box className={className} borderColor={borderColor}>
    {label !== undefined && (
      <FormLabel
        id={name}
        label={label}
        isRequired={isRequired}
        labelFontSize={labelFontSize}
        labelPadding={labelPadding}
      />
    )}
    <Select borderColor={borderColor} name={name} ref={ref} {...rest}>
      {dropdownItems.map((dropdownItem) => (
        <option key={dropdownItem.id} value={dropdownItem.id}>
          {dropdownItem.label}
        </option>
      ))}
    </Select>
  </Box>
);

const Box = styled.div<{ borderColor: keyof typeof colors }>`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const Select = styled.select<{ borderColor: keyof typeof colors }>`
  height: 5.6rem;
  padding: 0 1.8rem;
  border: 1px solid ${({ borderColor, theme }) => theme.colors[borderColor]};
  background: ${({ theme }) => theme.colors.white};
  font-size: 1.6rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.primary};

  ::placeholder {
    color: ${({ theme }) => theme.colors.inactive};
  }
`;

export default memo(forwardRef(FormDropdown));
