import { Meta, Story } from '@storybook/react';
import FormDropdown, { FormDropdownProps } from './FormDropdown';

export default {
  title: 'Components/form/FormDropdown',
  component: FormDropdown,
} as Meta;

const Template: Story<FormDropdownProps> = (args) => <FormDropdown {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  dropdownItems: [
    { id: '1', label: '아이템1' },
    { id: '2', label: '아이템2' },
  ],
};
