import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '@src/styles/theme';
import FormInput, { FormInputProps } from './FormInput';

describe('<FormInput />', () => {
  const setup = (
    props: FormInputProps = {
      errorMessage: false,
      label: 'label',
      name: 'name',
    },
  ) =>
    render(
      <ThemeProvider theme={theme}>
        <FormInput {...props} />
      </ThemeProvider>,
    );

  it('should render properly', () => {
    setup();
  });
});
