import { forwardRef, memo } from 'react';
import styled from 'styled-components';
import { colors } from '@src/styles/theme';
import FormLabel from '../FormLabel';
import Input from '../Input';

export type FormInputProps = React.InputHTMLAttributes<HTMLInputElement> & {
  /** 인풋 라벨 */
  label?: string;
  /** 에러메세지 */
  errorMessage?: string | boolean;
  /** 라벨과 인풋 사이의 간격 */
  labelPadding?: string | number;
  /** 라벨 텍스트 사이즈 */
  labelFontSize?: string | number;
  /** 필수 항목 아이콘 표시여부 */
  isRequired?: boolean;
  /** 보더 컬러 */
  borderColor?: keyof typeof colors;
  enableShadow?: boolean;
  className?: string;
};

const FormInput = (
  {
    label,
    type = 'text',
    name,
    errorMessage,
    labelPadding,
    labelFontSize,
    isRequired,
    borderColor = 'primary',
    enableShadow,
    className,
    ...rest
  }: FormInputProps,
  ref: React.Ref<HTMLInputElement>,
) => (
  <Box className={className}>
    {label !== undefined && (
      <FormLabel
        id={name}
        label={label}
        isRequired={isRequired}
        labelFontSize={labelFontSize}
        labelPadding={labelPadding}
        htmlFor={name}
      />
    )}
    <Input
      id={name}
      type={type}
      ref={ref}
      name={name}
      borderColor={borderColor}
      enableShadow={enableShadow}
      {...rest}
    />
    {errorMessage !== undefined && errorMessage !== false && (
      <ErrorText>{errorMessage}</ErrorText>
    )}
  </Box>
);

const Box = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const ErrorText = styled.p`
  margin-top: 0.8rem;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.red};
`;

export default memo(forwardRef(FormInput));
