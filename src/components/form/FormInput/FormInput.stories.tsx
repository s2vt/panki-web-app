import { Meta, Story } from '@storybook/react';
import FormInput, { FormInputProps } from './FormInput';

export default {
  title: 'Components/form/FormInput',
  component: FormInput,
} as Meta;

const Template: Story<FormInputProps> = (args) => <FormInput {...args} />;

export const Email = Template.bind({});
Email.args = {
  label: '이메일',
  name: 'email',
  type: 'email',
};

export const Password = Template.bind({});
Password.args = {
  label: '패스워드',
  name: 'password',
  type: 'password',
};

export const PasswordError = Template.bind({});
PasswordError.args = {
  label: '패스워드',
  name: 'password',
  errorMessage: '에러입니다',
  type: 'password',
};
