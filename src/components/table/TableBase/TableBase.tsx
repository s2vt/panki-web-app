import styled from 'styled-components';

const LoadingWrapper = styled.div`
  height: 56.16rem;
`;

const TableSection = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const TableWrapper = styled.div`
  width: 100%;
  height: 56.16rem;
`;

const Table = styled.table`
  width: 100%;
  table-layout: fixed;
`;

const PaginationSection = styled.div`
  margin-top: 4.8rem;
  display: flex;
  justify-content: center;
`;

export default {
  LoadingWrapper,
  TableSection,
  TableWrapper,
  Table,
  PaginationSection,
};
