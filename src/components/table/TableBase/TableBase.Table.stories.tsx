import { Meta, Story } from '@storybook/react';
import TableBase from './TableBase';

export default {
  title: 'Components/table/TableBase/Table',
  component: TableBase.Table,
} as Meta;

const Template: Story = (args) => <TableBase.Table {...args} />;

export const Basic = Template.bind({});
