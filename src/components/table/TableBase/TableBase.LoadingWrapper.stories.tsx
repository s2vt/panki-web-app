import { Meta, Story } from '@storybook/react';
import TableBase from './TableBase';

export default {
  title: 'Components/table/TableBase/LoadingWrapper',
  component: TableBase.LoadingWrapper,
} as Meta;

const Template: Story = (args) => <TableBase.LoadingWrapper {...args} />;

export const Basic = Template.bind({});
