import { Meta, Story } from '@storybook/react';
import TableBase from './TableBase';

export default {
  title: 'Components/table/TableBase/PaginationSection',
  component: TableBase.PaginationSection,
} as Meta;

const Template: Story = (args) => <TableBase.PaginationSection {...args} />;

export const Basic = Template.bind({});
