import { Meta, Story } from '@storybook/react';
import TableBase from './TableBase';

export default {
  title: 'Components/table/TableBase/TableSection',
  component: TableBase.TableSection,
} as Meta;

const Template: Story = (args) => <TableBase.TableSection {...args} />;

export const Basic = Template.bind({});
