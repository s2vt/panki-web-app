/* eslint-disable react/no-array-index-key */
import React, { memo } from 'react';
import styled from 'styled-components';
import Checkbox from '@src/components/common/Checkbox';

export type TableItemProps = React.InputHTMLAttributes<HTMLInputElement> & {
  /** 체크박스 input의 id값 */
  id: string;
  /** 아이템을 눌렸을 때 실행할 이벤트 */
  onClick?: () => void;
  /** 체크박스 클릭 시 실행할 이벤트 */
  onCheckboxChange?: () => void;
  /**
   * 해당 아이템이 홀수번째 아이템인지 확인하는 변수
   * 짝수, 홀수번째 아이템 별로 배경색이 달라집니다
   */
  isOddItem: boolean;
  rows: string[];
  className?: string;
};

/** 테이블 리스트 아이템 컴포넌트입니다. */
const TableItem = ({
  id,
  onClick,
  onCheckboxChange,
  isOddItem,
  rows,
  className,
  ...rest
}: TableItemProps) => {
  const handleCheckboxClick = (e: React.MouseEvent<HTMLInputElement>) => {
    e.stopPropagation();
  };

  return (
    <TableRow isOddItem={isOddItem} className={className} onClick={onClick}>
      <CheckboxWrapper>
        <Checkbox
          checkboxIcon="blankCheckbox"
          id={id}
          onChange={onCheckboxChange}
          enableShadow
          checkboxSize="sm"
          onClick={handleCheckboxClick}
          {...rest}
        />
      </CheckboxWrapper>
      {rows.map((row, index) => (
        <TableCell key={index}>{row}</TableCell>
      ))}
    </TableRow>
  );
};

const TableRow = styled.tr<{ isOddItem: boolean }>`
  width: 100%;
  height: 5rem;
  cursor: pointer;
  border-bottom: 0.5px solid ${({ theme }) => theme.colors.primary};
  transition: background 0.2s;

  &:hover {
    background: ${({ theme }) => theme.colors.tableHoverColor};
  }
`;

const CheckboxWrapper = styled.td`
  max-width: 10rem;
  height: 5rem;
  ${({ theme }) => theme.layout.flexCenterLayout};
`;

const TableCell = styled.td`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 1.6rem;
  font-weight: 400;
  text-align: start;
  vertical-align: middle;
  overflow-x: hidden;
  text-overflow: ellipsis;
`;

export default memo(TableItem);
