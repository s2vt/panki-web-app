import { Meta, Story } from '@storybook/react';
import TableItem, { TableItemProps } from './TableItem';

export default {
  title: 'Components/table/TableItem',
  component: TableItem,
} as Meta;

const Template: Story<TableItemProps> = (args) => (
  <table style={{ width: '100%' }}>
    <tbody>
      <TableItem {...args} />
    </tbody>
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  onClick: () => {
    console.log('onClick');
  },
  rows: ['아이템1', '아이템2', '아이템3', '아이템4', '아이템5'],
};
