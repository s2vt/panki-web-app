import { memo } from 'react';
import styled from 'styled-components';

export type TableActionWrapperProps = {
  children?: React.ReactNode;
};

const TableActionWrapper = ({ children }: TableActionWrapperProps) => (
  <Block>{children}</Block>
);

const Block = styled.div`
  padding-bottom: 5.8rem;
  display: flex;
  align-items: center;
  gap: 1.8rem;
`;

export default memo(TableActionWrapper);
