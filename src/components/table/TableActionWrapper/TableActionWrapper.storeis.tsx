import { Meta, Story } from '@storybook/react';
import TableActionWrapper, {
  TableActionWrapperProps,
} from './TableActionWrapper';

export default {
  title: 'Components/table/TableActionWrapper',
  component: TableActionWrapper,
} as Meta;

const Template: Story<TableActionWrapperProps> = (args) => (
  <TableActionWrapper {...args} />
);

export const Basic = Template.bind({});
