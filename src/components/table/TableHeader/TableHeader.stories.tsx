import { Meta, Story } from '@storybook/react';
import TableHeader, { TableHeaderProps } from './TableHeader';

export default {
  title: 'Components/table/TableHeader',
  component: TableHeader,
} as Meta;

const Template: Story<TableHeaderProps> = (args) => (
  <table style={{ width: '100%' }}>
    <TableHeader {...args} />
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  columns: ['컬럼1', '컬럼2', '컬럼3', '컬럼4', '컬럼5'],
};
