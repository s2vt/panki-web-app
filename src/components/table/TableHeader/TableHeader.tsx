/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import styled from 'styled-components';
import Checkbox from '@src/components/common/Checkbox';

export type TableHeaderProps = {
  columns: string[];
  onCheckboxClick?: () => void;
  checked?: boolean;
  className?: string;
};

const TableHeader = ({
  columns,
  onCheckboxClick,
  checked,
  className,
}: TableHeaderProps) => (
  <thead>
    <TableRow className={className}>
      <CheckboxWrapper>
        <Checkbox
          checkboxIcon="blankCheckbox"
          checkboxSize="sm"
          onClick={onCheckboxClick}
          checked={checked}
          enableShadow
          readOnly
        />
      </CheckboxWrapper>
      {columns.map((column, index) => (
        <TableHeaderCell key={index}>{column}</TableHeaderCell>
      ))}
    </TableRow>
  </thead>
);

const TableRow = styled.tr`
  height: 5rem;
  border-bottom: 0.5px solid ${({ theme }) => theme.colors.primary};
`;

const CheckboxWrapper = styled.th`
  max-width: 10rem;
  height: 5rem;
  ${({ theme }) => theme.layout.flexCenterLayout};
`;

const TableHeaderCell = styled.th`
  font-size: 1.6rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.primary};
  text-align: start;
  vertical-align: middle;
`;

export default memo(TableHeader);
