import React, { useCallback, useMemo } from 'react';
import styled, { css } from 'styled-components';
import { TaskStep } from '@src/types/task.types';
import { DropdownItem, FilterSelectItem } from '@src/types/common.types';
import { convertTaskStepText } from '@src/types/report.types';
import DropdownSelectWrapper from '@src/components/common/DropdownSelectWrapper';
import Icon from '@src/components/common/Icon';

export type TaskDropdownProps = {
  selectedTaskStep: DropdownItem | null;
  onClick: (selectItem: FilterSelectItem<TaskStep>) => void;
  enabled: boolean;
};

const TaskDropdown = ({
  selectedTaskStep,
  onClick,
  enabled,
}: TaskDropdownProps) => {
  const isSelected = useCallback(
    (item: DropdownItem) => selectedTaskStep?.id === item.id,
    [selectedTaskStep],
  );

  const filterDropdownItems: DropdownItem[] = useMemo(
    () =>
      Object.values(TaskStep).map((taskStep, index) => ({
        id: index.toString(),
        label: convertTaskStepText(taskStep),
        value: taskStep,
      })),
    [],
  );

  return (
    <StyleDropdownSelectWrapper
      dropdownItems={filterDropdownItems}
      isSelected={isSelected}
      onClick={onClick}
      checkboxSize="xs"
      boxWidth="100%"
    >
      <Dropdown enabled={enabled}>{selectedTaskStep?.label ?? '-'}</Dropdown>
    </StyleDropdownSelectWrapper>
  );
};

type DropdownProps = {
  isActive?: boolean;
  children: React.ReactNode;
  enabled: boolean;
};
const Dropdown = ({ isActive, children, enabled }: DropdownProps) => (
  <Box enabled={enabled}>
    {children}
    <DropdownIcon icon="arrowIcon" $isActive={isActive} />
  </Box>
);

const Box = styled.div<{ enabled: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 1.2rem;
  height: 3.6rem;
  min-width: 18.4rem;
  background: ${({ enabled, theme }) =>
    enabled ? theme.colors.white : theme.colors.disabled};
  border-width: 1px;
  border-style: solid;
  border-color: ${({ enabled, theme }) =>
    enabled ? theme.colors.black : theme.colors.disabled};
  color: ${({ theme }) => theme.colors.primary};
  font-size: 1.4rem;
  font-weight: 700;
  cursor: pointer;
`;

const StyleDropdownSelectWrapper = styled(DropdownSelectWrapper)`
  flex: 1;
`;

const DropdownIcon = styled(Icon)<{ $isActive?: boolean }>`
  width: 1.6rem;
  height: 1.6rem;

  ${({ $isActive }) =>
    $isActive &&
    css`
      transform: rotate(180deg);
    `}
`;

export default TaskDropdown;
