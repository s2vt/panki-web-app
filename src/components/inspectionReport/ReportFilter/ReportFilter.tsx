import { useCallback, useState } from 'react';
import styled, { css } from 'styled-components';
import {
  ALL_SHIPS,
  ASSIGNED_SHIPS,
  INQUIRY,
  RECENT,
  SHIP_NUMBER,
} from '@src/constants/constantString';
import useMask from '@src/hooks/common/useMask';
import { TaskStep } from '@src/types/task.types';
import { FilterSelectItem } from '@src/types/common.types';
import MainButton from '@src/components/common/MainButton';
import Input from '@src/components/form/Input';
import TaskDropdown from './TaskDropdown';

export type ReportFilterProps = {
  onRecentClick: () => void;
  onFilterClick: (item: FilterSelectItem<TaskStep>) => void;
  selectedTaskStep: FilterSelectItem<TaskStep> | null;
  onInquirySubmit: (shipName: string) => void;
};

const ReportFilter = ({
  onRecentClick,
  onFilterClick,
  selectedTaskStep,
  onInquirySubmit,
}: ReportFilterProps) => {
  const [shipName, setShipName] = useState<string>('');
  const { maskEnglishNumber } = useMask();

  const handleSubmit = useCallback(
    () => onInquirySubmit(shipName),
    [shipName, onInquirySubmit],
  );

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const maskedValue = maskEnglishNumber(e.target.value);
      e.target.value = maskedValue;
      setShipName(maskedValue);
    },
    [maskEnglishNumber, setShipName],
  );

  const handleKeyPress = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter' && !e.shiftKey) {
        e.preventDefault();
        handleSubmit();
      }
    },
    [handleSubmit, shipName],
  );

  return (
    <Box>
      <Row>
        <MainButton label={ASSIGNED_SHIPS} fontSize="1.4rem" fontWeight={500} />
        <MainButton
          label={ALL_SHIPS}
          fontSize="1.4rem"
          fontWeight={500}
          disabled
        />
      </Row>
      <Row>
        <RecentButton
          label={RECENT}
          onClick={onRecentClick}
          fontSize="1.4rem"
          fontWeight={500}
          width="7.2rem"
          enabled={selectedTaskStep === null}
        />
        <TaskDropdown
          onClick={onFilterClick}
          selectedTaskStep={selectedTaskStep}
          enabled={selectedTaskStep !== null}
        />
      </Row>
      <Row>
        <StyledInput
          borderColor="reportInspectionBadgeColor"
          placeholder={SHIP_NUMBER}
          onChange={handleInputChange}
          onKeyPress={handleKeyPress}
        />
        <MainButton
          label={INQUIRY}
          fontWeight={500}
          fontSize="1.4rem"
          backgroundColor="reportInspectionBadgeColor"
          onClick={handleSubmit}
        />
      </Row>
    </Box>
  );
};

const Box = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2.6rem;
`;

const StyledInput = styled(Input)`
  height: 100%;
  font-size: 1.4rem;
`;

const RecentButton = styled(MainButton)<{ enabled: boolean }>`
  ${({ enabled, theme }) =>
    !enabled &&
    css`
      background-color: ${theme.colors.white};
      border-color: ${theme.colors.reportInspectionDateColor};
      color: ${theme.colors.reportInspectionDateColor};
    `};
`;

const Row = styled.div`
  display: flex;
`;

export default ReportFilter;
