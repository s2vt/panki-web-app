import { Meta, Story } from '@storybook/react';
import ReportFilter, { ReportFilterProps } from '../ReportFilter';

export default {
  title: 'Components/InspectionReport/ReportFilter',
  component: ReportFilter,
} as Meta;

const Template: Story<ReportFilterProps> = (args) => <ReportFilter {...args} />;

export const Basic = Template.bind({});
