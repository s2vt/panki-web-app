import { Meta, Story } from '@storybook/react';
import TaskDropdown, { TaskDropdownProps } from '../TaskDropdown';

export default {
  title: 'Components/TaskDropdown',
  component: TaskDropdown,
} as Meta;

const Template: Story<TaskDropdownProps> = (args) => <TaskDropdown {...args} />;

export const Basic = Template.bind({});
