import { Meta, Story } from '@storybook/react';
import { TaskStep } from '../../../types/task.types';
import CoatingInspectionHistoryItem, {
  CoatingInspectionHistoryItemProps,
} from './CoatingInspectionHistoryItem';

export default {
  title: 'Components/InspectionReport/CoatingInspectionHistoryItem',
  component: CoatingInspectionHistoryItem,
} as Meta;

const Template: Story<CoatingInspectionHistoryItemProps> = (args) => (
  <CoatingInspectionHistoryItem {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  coatingInspectionHistory: {
    taskId: '1',
    taskStep: TaskStep.blastingTask,
    completionDate: new Date(),
    result: 'ACCEPTED',
  },
  onDownloadClick: console.log,
};
