import { memo } from 'react';
import styled from 'styled-components';
import { TaskStep } from '@src/types/task.types';
import {
  InspectionReportHistory,
  InspectionReportResult,
} from '@src/types/report.types';
import * as dateUtil from '@src/utils/dateUtil';
import Divider from '@src/components/common/Divider';
import Icon from '@src/components/common/Icon';
import CoatingInspectionHistoryCard from './CoatingInspectionHistoryCard';

const REPORT_DOWNLOADABLE_TASKS: TaskStep[] = [
  TaskStep.blastingTask,
  TaskStep.secondSprayTask,
];

export type CoatingInspectionHistoryItemProps = {
  coatingInspectionHistory: InspectionReportHistory;
  onDownloadClick: (coatingInspectionHistory: InspectionReportHistory) => void;
};

const CoatingInspectionHistoryItem = ({
  coatingInspectionHistory,
  onDownloadClick,
}: CoatingInspectionHistoryItemProps) => {
  const { completionDate, result, taskStep } = coatingInspectionHistory;

  const hasDownloadButton =
    result === InspectionReportResult.accepted &&
    REPORT_DOWNLOADABLE_TASKS.includes(taskStep);

  return (
    <Box>
      <StyledDivider orientation="vertical" color="reportLineColor" />
      <DateBadgeWrapper>
        <DateBadgeCircle />
        <DateBadge>{dateUtil.formatToCommon(completionDate, '.')}</DateBadge>
      </DateBadgeWrapper>
      <CardWrapper>
        <Icon icon="reportCircleIcon" />
        <CoatingInspectionHistoryCard
          coatingInspectionHistory={coatingInspectionHistory}
          hasDownloadButton={hasDownloadButton}
          onDownloadClick={() => onDownloadClick(coatingInspectionHistory)}
        />
      </CardWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  position: relative;
`;

const StyledDivider = styled(Divider)`
  position: absolute;
  left: 1rem;
  height: 100%;
`;

const DateBadgeWrapper = styled.div`
  display: flex;
  padding-top: 2rem;
  align-items: center;
`;

const DateBadgeCircle = styled.div`
  width: 2rem;
  height: 2rem;
  background: ${({ theme }) => theme.colors.primary};
  border-radius: 24px;
  z-index: 1;
`;

const DateBadge = styled.div`
  width: 10.7rem;
  height: 3.4rem;
  margin-left: 2.1rem;
  ${({ theme }) => theme.layout.flexCenterLayout};
  background: ${({ theme }) => theme.colors.primary};
  border-radius: 10px;
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.6rem;
  font-weight: 700;
  z-index: 1;
`;

const CardWrapper = styled.div`
  margin: 2.4rem 0;
  display: flex;
  align-items: center;
  z-index: 1;
`;

export default memo(CoatingInspectionHistoryItem);
