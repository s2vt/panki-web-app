import styled from 'styled-components';
import {
  convertTaskStepText,
  InspectionReportHistory,
} from '@src/types/report.types';
import * as dateUtil from '@src/utils/dateUtil';
import Icon from '@src/components/common/Icon';

export type CoatingInspectionHistoryCardProps = {
  coatingInspectionHistory: InspectionReportHistory;
  hasDownloadButton: boolean;
  onDownloadClick?: () => void;
};

const CoatingInspectionHistoryCard = ({
  coatingInspectionHistory,
  hasDownloadButton,
  onDownloadClick,
}: CoatingInspectionHistoryCardProps) => {
  const { completionDate, result, taskStep } = coatingInspectionHistory;

  return (
    <Card>
      <FlexBox>
        <p>{convertTaskStepText(taskStep)}</p>
      </FlexBox>
      <FlexBox>
        <DateText>
          {dateUtil
            .formatToDateTime(completionDate, '.')
            .split(' ')
            .map((date) => (
              <p key={date}>{date}</p>
            ))}
        </DateText>
      </FlexBox>
      <FlexBox>
        <p>{result}</p>
      </FlexBox>
      <FlexBox>
        {hasDownloadButton && (
          <StyledIcon icon="downloadIcon" onClick={onDownloadClick} />
        )}
      </FlexBox>
    </Card>
  );
};

const Card = styled.div`
  padding: 0 4.4rem 0 3.3rem;
  flex: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 6.4rem;
  margin-left: 2.1rem;
  background-color: ${({ theme }) => theme.colors.white};
  box-shadow: 0px 10px 15px -3px rgba(0, 0, 0, 0.1),
    0px 4px 6px -2px rgba(0, 0, 0, 0.05);
  font-size: 1.6rem;
  font-weight: 700;
`;

const DateText = styled.div`
  display: flex;
  gap: 2.4rem;
`;

const FlexBox = styled.div<{ flex?: number }>`
  flex: ${({ flex }) => flex ?? 1};
`;

const StyledIcon = styled(Icon)`
  margin-left: 2.6rem;
  width: 2.4rem;
  height: 2.4rem;
  cursor: pointer;
`;

export default CoatingInspectionHistoryCard;
