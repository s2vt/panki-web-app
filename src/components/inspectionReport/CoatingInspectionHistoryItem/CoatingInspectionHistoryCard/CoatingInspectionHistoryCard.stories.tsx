import { Meta, Story } from '@storybook/react';
import { TaskStep } from '@src/types/task.types';
import CoatingInspectionHistoryCard, {
  CoatingInspectionHistoryCardProps,
} from './CoatingInspectionHistoryCard';

export default {
  title:
    'Components/InspectionReport/CoatingInspectionHistoryItem/CoatingInspectionHistoryCard',
  component: CoatingInspectionHistoryCard,
} as Meta;

const Template: Story<CoatingInspectionHistoryCardProps> = (args) => (
  <CoatingInspectionHistoryCard {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  coatingInspectionHistory: {
    taskId: '1',
    taskStep: TaskStep.blastingTask,
    completionDate: new Date(),
    result: 'ACCEPTED',
  },
  onDownloadClick: console.log,
};
