import { memo } from 'react';
import styled from 'styled-components';
import { SERVER_ERROR } from '@src/constants/constantString';
import { TaskStep } from '@src/types/task.types';
import { InspectionReport } from '@src/types/report.types';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import CoatingInspectionHistoryItem from '../CoatingInspectionHistoryItem';
import CoatingInspectionReportHeader from './CoatingInspectionReportHeader';
import CoatingInspectionReportTitle from './CoatingInspectionReportTitle';
import useTaskReportDownload from './hooks/useTaskReport';
import useCoatingInspectionReport from './hooks/useCoatingInspectionReport';
import useInspectionReportHistoriesQuery from './hooks/useInspectionReportHistoriesQuery';

export type CoatingInspectionReportProps = {
  isReportsFetching: boolean;
  selectedReport: InspectionReport | undefined;
};

const CoatingInspectionReport = ({
  isReportsFetching,
  selectedReport,
}: CoatingInspectionReportProps) => {
  const {
    data: inspectionReportHistories,
    isLoading,
    isError,
    refetch,
  } = useInspectionReportHistoriesQuery(selectedReport?.blockId ?? '', {
    enabled: selectedReport !== undefined,
  });

  const { downloadCoatingInspectionReport } = useCoatingInspectionReport(
    selectedReport?.blockId,
  );
  const { downloadTaskReport } = useTaskReportDownload(selectedReport?.blockId);

  if (isLoading || isReportsFetching) {
    return (
      <CenterBox>
        <LoadingSpinner />
      </CenterBox>
    );
  }

  if (isError) {
    return (
      <CenterBox>
        <ErrorBox error={SERVER_ERROR} onRefetchClick={refetch} />
      </CenterBox>
    );
  }

  return (
    <Box>
      <CoatingInspectionReportTitle
        onDownloadClick={downloadCoatingInspectionReport}
        hasDownloadButton={
          selectedReport?.recentInspectionTask === TaskStep.secondSprayTask
        }
      />
      <CoatingInspectionReportHeader />
      {inspectionReportHistories ? (
        <>
          <HistoriesSection>
            {inspectionReportHistories.map((inspectionReportHistory) => {
              const { taskId, completionDate, result, taskStep } =
                inspectionReportHistory;

              return (
                <CoatingInspectionHistoryItem
                  key={taskId + completionDate + result + taskStep}
                  coatingInspectionHistory={inspectionReportHistory}
                  onDownloadClick={downloadTaskReport}
                />
              );
            })}
          </HistoriesSection>
        </>
      ) : (
        <HistoriesSection>
          <EmptyText>현재 완료된 검사가 없습니다.</EmptyText>
        </HistoriesSection>
      )}
    </Box>
  );
};

const CenterBox = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  flex: 1;
  margin-top: 4rem;
  display: flex;
  flex-direction: column;
  min-height: 0;
`;

const EmptyText = styled.div`
  ${({ theme }) => theme.layout.flexCenterLayout};
  padding: 2.4rem 0;
  font-size: 1.6rem;
  font-weight: 700;
`;

const HistoriesSection = styled.div`
  flex: 1;
  padding: 0 2.4rem;
  border: 1px solid ${({ theme }) => theme.colors.lightBlack};
  border-top: none;
  overflow-y: auto;
`;

export default memo(CoatingInspectionReport);
