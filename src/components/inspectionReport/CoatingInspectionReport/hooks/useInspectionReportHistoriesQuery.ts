import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import reportApi from '@src/api/reportApi';
import { InspectionReportHistory } from '@src/types/report.types';

const useInspectionReportHistoriesQuery = (
  blockId: string,
  options?: UseQueryOptions<InspectionReportHistory[], HttpError>,
) =>
  useQuery(
    createKey(blockId),
    () => reportApi.getInspectionReportHistories(blockId),
    options,
  );

const baseKey = ['inspectionReportHistories'];
const createKey = (blockId: string) => [...baseKey, blockId];

useInspectionReportHistoriesQuery.baseKey = baseKey;
useInspectionReportHistoriesQuery.createKey = createKey;

export default useInspectionReportHistoriesQuery;
