import { useCallback } from 'react';
import reportApi from '@src/api/reportApi';
import { SERVER_ERROR } from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useDocumentDownload from './useDocumentDownload';

const COATING_INSPECTION_REPORT_FILE_NAME = 'coating_inspection_report.docx';

const useCoatingInspectionReport = (blockId: string | undefined) => {
  const { openAlertDialog } = useAlertDialogAction();
  const { downloadDocument } = useDocumentDownload();

  const downloadCoatingInspectionReport = useCallback(async () => {
    try {
      if (!blockId) {
        return;
      }

      const documentBuffer = await reportApi.getCoatingInspectionReport(
        blockId,
      );

      downloadDocument(documentBuffer, COATING_INSPECTION_REPORT_FILE_NAME);
    } catch (e) {
      openAlertDialog({ text: SERVER_ERROR, position: 'rightTop' });
      throw e;
    }
  }, [blockId, openAlertDialog]);

  return { downloadCoatingInspectionReport };
};

export default useCoatingInspectionReport;
