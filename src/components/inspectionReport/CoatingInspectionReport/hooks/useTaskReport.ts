import { useCallback } from 'react';
import reportApi from '@src/api/reportApi';
import { TaskStep } from '@src/types/task.types';
import { InspectionReportHistory } from '@src/types/report.types';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { SERVER_ERROR } from '@src/constants/constantString';
import { debugAssert } from '@src/utils/commonUtil';
import useDocumentDownload from './useDocumentDownload';

const SECONDARY_SURFACE_PREPARATION_REPORT_FILE_NAME =
  'secondary_surface_preparation_report.docx';
const FINAL_COATING_INSPECTION_REPORT_FILE_NAME =
  'final_coating_inspection.docx';

const useTaskReport = (blockId: string | undefined) => {
  const { openAlertDialog } = useAlertDialogAction();
  const { downloadDocument } = useDocumentDownload();

  const downloadTaskReport = useCallback(
    async (inspectionReportHistory: InspectionReportHistory) => {
      try {
        if (!blockId) {
          return;
        }

        const { taskStep } = inspectionReportHistory;

        switch (taskStep) {
          case TaskStep.blastingTask:
            downloadSurfacePreparationReport(blockId);
            break;
          case TaskStep.secondSprayTask:
            downloadFinalCoatingReport(blockId);
            break;
          default:
            debugAssert(false, `Can't download ${taskStep} report`);
        }
      } catch (e) {
        openAlertDialog({ text: SERVER_ERROR, position: 'rightTop' });
        throw e;
      }
    },
    [blockId, openAlertDialog],
  );

  const downloadSurfacePreparationReport = async (blockId: string) => {
    const documentBuffer = await reportApi.getSurfacePreparationReport(blockId);
    const fileName = SECONDARY_SURFACE_PREPARATION_REPORT_FILE_NAME;

    downloadDocument(documentBuffer, fileName);
  };

  const downloadFinalCoatingReport = async (blockId: string) => {
    const documentBuffer = await reportApi.getFinalCoatingReport(blockId);
    const fileName = FINAL_COATING_INSPECTION_REPORT_FILE_NAME;

    downloadDocument(documentBuffer, fileName);
  };

  return { downloadTaskReport };
};

export default useTaskReport;
