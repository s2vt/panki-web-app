import { saveAs } from 'file-saver';
import { useCallback } from 'react';
import * as dateUtil from '@src/utils/dateUtil';

const useDocumentDownload = () => {
  const downloadDocument = useCallback(
    (documentBuffer: ArrayBuffer, fileName: string) => {
      const blob = new Blob([documentBuffer]);
      const currentDate = dateUtil.formatToDateTimeWithoutWhiteSpace(
        new Date(),
        '_',
      );

      saveAs(blob, `${currentDate}_${fileName}`);
    },
    [],
  );

  return { downloadDocument };
};

export default useDocumentDownload;
