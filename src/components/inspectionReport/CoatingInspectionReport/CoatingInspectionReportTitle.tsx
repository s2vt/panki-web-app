import { memo } from 'react';
import styled from 'styled-components';
import { COATING_INSPECTION_REPORT } from '@src/constants/constantString';
import Icon from '@src/components/common/Icon';

export type CoatingInspectionReportTitleProps = {
  onDownloadClick: () => void;
  hasDownloadButton: boolean;
};

const CoatingInspectionReportTitle = ({
  onDownloadClick,
  hasDownloadButton,
}: CoatingInspectionReportTitleProps) => (
  <Box>
    <p>{COATING_INSPECTION_REPORT}</p>
    {hasDownloadButton && (
      <DownloadIcon icon="downloadIcon" onClick={onDownloadClick} />
    )}
  </Box>
);

const Box = styled.div`
  display: flex;
  padding: 0 4.8rem;
  align-items: center;
  justify-content: space-between;
  min-height: 8.5rem;
  border: 1px solid ${({ theme }) => theme.colors.lightBlack};
  font-size: 1.8rem;
  font-weight: 700;
`;

const DownloadIcon = styled(Icon)`
  width: 2.4rem;
  height: 2.4rem;
  cursor: pointer;
`;

export default memo(CoatingInspectionReportTitle);
