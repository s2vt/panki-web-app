import { memo } from 'react';
import styled from 'styled-components';

const CoatingInspectionReportHeader = () => (
  <Box>
    <Inner>
      <FlexBox />
      <FlexBox>
        <p>COMPLETION DATE</p>
      </FlexBox>
      <FlexBox>
        <p>RESULT</p>
      </FlexBox>
      <FlexBox>
        <ReportText>REPORT</ReportText>
      </FlexBox>
    </Inner>
  </Box>
);

const Box = styled.div`
  height: 6.4rem;
  font-size: 1.6rem;
  font-weight: 700;
  border: 1px solid ${({ theme }) => theme.colors.lightBlack};
  border-top: none;
`;

const Inner = styled.div`
  display: flex;
  height: 100%;
  padding: 0 4.6rem 0 3.3rem;
  margin-left: 4rem;
  justify-content: space-between;
  align-items: center;
`;

const FlexBox = styled.div<{ flex?: number }>`
  flex: ${({ flex }) => flex ?? 1};
`;

const ReportText = styled.p`
  min-width: 10rem;
`;

export default memo(CoatingInspectionReportHeader);
