import { Meta, Story } from '@storybook/react';
import CoatingInspectionReport, {
  CoatingInspectionReportProps,
} from './CoatingInspectionReport';

export default {
  title: 'Components/InspectionReport/CoatingInspectionReport',
  component: CoatingInspectionReport,
} as Meta;

const Template: Story<CoatingInspectionReportProps> = (args) => (
  <CoatingInspectionReport {...args} />
);

export const Basic = Template.bind({});
