import { Meta, Story } from '@storybook/react';
import InspectionResultReport from './InspectionResultReport';

export default {
  title: 'Components/InspectionReport/InspectionResultReport',
  component: InspectionResultReport,
} as Meta;

const Template: Story = (args) => <InspectionResultReport {...args} />;

export const Basic = Template.bind({});
