import styled from 'styled-components';
import {
  INSPECTION_REPORT_CURRENT_STATUS,
  SERVER_ERROR,
} from '@src/constants/constantString';
import { InspectionReport, ReportFilterPayload } from '@src/types/report.types';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import useMyInspectionReportsQuery from '@src/hooks/report/useMyInspectionReportsQuery';
import InspectionReportItem from '../InspectionReportItem';

export type InspectionReportsProps = {
  reportFilterPayload: ReportFilterPayload;
  onClick: (inspectionReport: InspectionReport) => void;
  selectedBlockId: string | undefined;
};

const InspectionReports = ({
  reportFilterPayload,
  onClick,
  selectedBlockId,
}: InspectionReportsProps) => {
  const {
    data: inspectionReports,
    refetch,
    isLoading,
    isError,
  } = useMyInspectionReportsQuery(reportFilterPayload, { enabled: false });

  if (isLoading) {
    return (
      <Box>
        <LoadingSpinner />
      </Box>
    );
  }

  if (isError) {
    return (
      <Box>
        <ErrorBox error={SERVER_ERROR} onRefetchClick={refetch} />
      </Box>
    );
  }

  return (
    <Box>
      <CurrentStatusBadge>
        {INSPECTION_REPORT_CURRENT_STATUS}
      </CurrentStatusBadge>
      <CountTextWrapper>
        <CountText>{inspectionReports?.length ?? 0} 건</CountText>
        <span>의 업데이트 내용이 있습니다.</span>
      </CountTextWrapper>
      <ReportsSection>
        {inspectionReports?.map((inspectionReport) => (
          <InspectionReportItem
            key={inspectionReport.blockId}
            inspectionReport={inspectionReport}
            isSelected={selectedBlockId === inspectionReport.blockId}
            onClick={onClick}
          />
        ))}
      </ReportsSection>
    </Box>
  );
};

const Box = styled.div`
  flex: 1;
  margin-top: 4rem;
  display: flex;
  flex-direction: column;
  min-height: 0;
`;

const ReportsSection = styled.div`
  flex: 1;
  overflow-y: auto;
`;

const CountTextWrapper = styled.div`
  margin: 2.4rem 0;
  font-size: 1.6rem;
  font-weight: 500;
`;

const CountText = styled.span`
  color: ${({ theme }) => theme.colors.lightSky};
`;

const CurrentStatusBadge = styled.div`
  display: flex;
  align-items: center;
  padding-left: 1.8rem;
  min-height: 4.4rem;
  font-size: 1.6rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.black};
  background: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.black};
`;

export default InspectionReports;
