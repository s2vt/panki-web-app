import { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { subDays } from 'date-fns';
import { INSPECTION_REPORT } from '@src/constants/constantString';
import useSelectItem from '@src/hooks/common/useSelectItem';
import { BlockInOut } from '@src/types/block.types';
import { TaskStep } from '@src/types/task.types';
import { FilterSelectItem } from '@src/types/common.types';
import { InspectionReport, ReportFilterPayload } from '@src/types/report.types';
import * as dateUtil from '@src/utils/dateUtil';
import PageTitle from '@src/components/base/PageTitle';
import useMyInspectionReportsQuery from '@src/hooks/report/useMyInspectionReportsQuery';
import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import CoatingInspectionReport from '../CoatingInspectionReport/CoatingInspectionReport';
import ReportFilter from '../ReportFilter';
import InspectionReports from './InspectionReports';

const defaultReportFilterPayload: ReportFilterPayload = {
  inOut: BlockInOut.IN,
  startDate: dateUtil.formatToDateTimeWithSeconds(
    subDays(dateUtil.changeTimeToStartOfDay(new Date()), 7),
  ),
  endDate: dateUtil.formatToDateTimeWithSeconds(
    dateUtil.changeTimeToEndOfDay(new Date()),
  ),
};

const PAGE_MAX_HEIGHT = '855px';

const InspectionResultReport = () => {
  const {
    selectedItem: selectedTaskStep,
    setSelectedItem: setSelectedTaskStep,
    handleChange: handleFilterClick,
  } = useSelectItem<FilterSelectItem<TaskStep> | null>(null);

  const [selectedReport, setSelectedReport] = useState<InspectionReport>();
  const [reportFilterPayload, setReportFilterPayload] =
    useState<ReportFilterPayload>(defaultReportFilterPayload);

  const {
    data: inspectionReports,
    isFetching,
    refetch,
  } = useMyInspectionReportsQuery(reportFilterPayload);

  const handleRecentClick = useCallback(() => {
    setSelectedTaskStep(null);
    setReportFilterPayload((prevFilter) => ({
      ...prevFilter,
      ...defaultReportFilterPayload,
      recentTaskStep: undefined,
    }));
  }, [setSelectedTaskStep]);

  const handleInquirySubmit = useCallback(
    (shipName: string) => {
      setReportFilterPayload((prevFilter) => ({
        ...prevFilter,
        shipName: shipName !== '' ? shipName : undefined,
      }));
    },
    [setReportFilterPayload],
  );

  const handleReportClick = useCallback(
    (inspectionReport: InspectionReport) => setSelectedReport(inspectionReport),
    [setSelectedReport],
  );

  useEffect(() => {
    if (selectedTaskStep) {
      setReportFilterPayload((prevFilter) => ({
        ...prevFilter,
        recentTaskStep: selectedTaskStep.value,
        startDate: undefined,
        endDate: undefined,
      }));
    }
  }, [selectedTaskStep, setReportFilterPayload]);

  useEffect(() => {
    refetch();
  }, [reportFilterPayload]);

  useEffect(() => {
    if (isFetching) {
      setSelectedReport(undefined);
    }
  }, [isFetching]);

  useEffect(() => {
    if (inspectionReports) {
      setSelectedReport(inspectionReports[0]);
    }
  }, [inspectionReports, setSelectedReport]);

  return (
    <Grid container spacing={3}>
      <Grid item xs={3} sm={3} md={3} lg={3}>
        <Box
          sx={{
            height: PAGE_MAX_HEIGHT,
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <ReportFilter
            onRecentClick={handleRecentClick}
            onFilterClick={handleFilterClick}
            selectedTaskStep={selectedTaskStep}
            onInquirySubmit={handleInquirySubmit}
          />
          <InspectionReports
            onClick={handleReportClick}
            selectedBlockId={selectedReport?.blockId}
            reportFilterPayload={reportFilterPayload}
          />
        </Box>
      </Grid>
      <Grid item xs={9} sm={9} md={9} lg={9}>
        <Box
          sx={{
            maxHeight: PAGE_MAX_HEIGHT,
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <StyledPageTitle title={INSPECTION_REPORT} />
          <CoatingInspectionReport
            isReportsFetching={isFetching}
            selectedReport={selectedReport}
          />
        </Box>
      </Grid>
    </Grid>
  );
};

const StyledPageTitle = styled(PageTitle)`
  margin-top: 0;
`;

export default InspectionResultReport;
