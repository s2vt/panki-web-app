import styled, { css } from 'styled-components';
import { SHIP } from '@src/constants/constantString';
import { convertTaskStepText, InspectionReport } from '@src/types/report.types';
import * as dateUtil from '@src/utils/dateUtil';
import Divider from '../../common/Divider';

export type InspectionReportItemProps = {
  inspectionReport: InspectionReport;
  onClick: (inspectionReport: InspectionReport) => void;
  isSelected: boolean;
};

const InspectionReportItem = ({
  inspectionReport,
  onClick,
  isSelected,
}: InspectionReportItemProps) => {
  const {
    shipName,
    blockName,
    inOut,
    recentInspectionTask,
    recentInspectionDate,
  } = inspectionReport;

  return (
    <Box isSelected={isSelected} onClick={() => onClick(inspectionReport)}>
      <ShipInfoText>{`${shipName} ${SHIP} ${blockName} ${inOut}`}</ShipInfoText>
      <Divider orientation="horizontal" color="reportLineColor" />
      <RecentInspectionSection>
        <RecentInspectionTaskBadge>
          {convertTaskStepText(recentInspectionTask)}
        </RecentInspectionTaskBadge>
        <RecentInspectionTaskDateText>
          {dateUtil.formatToDateTimeDay(recentInspectionDate, '.')}
        </RecentInspectionTaskDateText>
      </RecentInspectionSection>
    </Box>
  );
};

const Box = styled.div<{ isSelected: boolean }>`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-bottom: 2.2rem;
  height: 10rem;
  padding: 1.8rem;
  border-radius: 10px;
  border: 1px solid ${({ theme }) => theme.colors.black};
  background: ${({ theme }) => theme.colors.white};
  font-weight: 700;
  cursor: pointer;
  ${({ isSelected, theme }) =>
    isSelected &&
    css`
      background: ${theme.colors.reportItemActiveColor};
    `};

  ::before {
    content: '';
    width: 8px;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background: ${({ theme }) => theme.colors.reportItemBadgeLineColor};
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }
`;

const ShipInfoText = styled.p`
  font-size: 1.8rem;
`;

const RecentInspectionSection = styled.div`
  display: flex;
  align-content: center;
`;

const RecentInspectionTaskBadge = styled.div`
  flex: 1;
  height: 2.1rem;
  ${({ theme }) => theme.layout.flexCenterLayout};
  background: ${({ theme }) => theme.colors.reportInspectionBadgeColor};
  color: ${({ theme }) => theme.colors.white};
`;

const RecentInspectionTaskDateText = styled.p`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 1.2rem;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.reportInspectionDateColor};
`;

export default InspectionReportItem;
