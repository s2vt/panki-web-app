import { Meta, Story } from '@storybook/react';
import { BlockInOut } from '@src/types/block.types';
import InspectionReportItem, {
  InspectionReportItemProps,
} from './InspectionReportItem';

export default {
  title: 'Components/InspectionReport/InspectionReportItem',
  component: InspectionReportItem,
} as Meta;

const Template: Story<InspectionReportItemProps> = (args) => (
  <InspectionReportItem {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  inspectionReport: {
    blockId: '1',
    blockName: '1B11',
    inOut: BlockInOut.IN,
    recentInspectionTask: '1st_stripe_coat',
    shipName: '2134',
    recentInspectionDate: new Date(),
  },
};
