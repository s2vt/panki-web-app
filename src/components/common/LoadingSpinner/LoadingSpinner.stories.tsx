import { Meta, Story } from '@storybook/react';
import LoadingSpinner, { LoadingSpinnerProps } from './LoadingSpinner';

export default {
  title: 'Components/common/LoadingSpinner',
  component: LoadingSpinner,
} as Meta;

const Template: Story<LoadingSpinnerProps> = (args) => (
  <LoadingSpinner {...args} />
);

export const Basic = Template.bind({});
