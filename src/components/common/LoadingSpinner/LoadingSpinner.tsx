import { Box, CircularProgress, styled } from '@mui/material';

const StyledBox = styled(Box)({
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export type LoadingSpinnerProps = {};

const LoadingSpinner = () => (
  <StyledBox>
    <CircularProgress />
  </StyledBox>
);

export default LoadingSpinner;
