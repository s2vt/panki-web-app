import { memo, useMemo } from 'react';
import styled from 'styled-components';
import { v4 as uuidV4 } from 'uuid';
import { SelectItem } from '@src/types/common.types';
import { Size } from '@src/types/style.types';
import Checkbox from '../Checkbox';

export type CheckboxSelectItemProps = {
  selectItem: SelectItem;
  /** 아이템 클릭 시 실행할 이벤트 */
  onChange: () => void;
  isSelected: boolean;
  /** 라벨 */
  label: string;
  checkboxSize?: Size;
  className?: string;
};

const CheckboxSelectItem = ({
  selectItem,
  onChange,
  isSelected,
  label,
  checkboxSize,
  className,
}: CheckboxSelectItemProps) => {
  const id = useMemo(() => uuidV4() + selectItem.id, [selectItem]);

  return (
    <Box className={className}>
      <Checkbox
        checkboxIcon="blankCheckbox"
        name={selectItem.id}
        id={id}
        onChange={onChange}
        checked={isSelected}
        checkboxSize={checkboxSize}
        enableShadow
        data-testid="checkbox"
      />
      <Label htmlFor={id}>{label}</Label>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

const Label = styled.label`
  padding-left: 1.1rem;
  font-size: 1.4rem;
  font-weight: 400;
  flex: 1;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
`;

export default memo(CheckboxSelectItem);
