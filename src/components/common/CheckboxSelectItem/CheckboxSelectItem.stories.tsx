import { Meta, Story } from '@storybook/react';
import CheckboxSelectItem, {
  CheckboxSelectItemProps,
} from './CheckboxSelectItem';

export default {
  title: 'Components/common/CheckboxSelectItem',
  component: CheckboxSelectItem,
} as Meta;

const Template: Story<CheckboxSelectItemProps> = (args) => (
  <CheckboxSelectItem {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  selectItem: {
    id: '1',
  },
  label: 'Item 1',
  onChange: () => {
    console.log('onChange');
  },
};
