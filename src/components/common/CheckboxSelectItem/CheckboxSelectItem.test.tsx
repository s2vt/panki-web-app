import { fireEvent, render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '@src/styles/theme';
import { SelectItem } from '@src/types/common.types';
import CheckboxSelectItem from '.';

describe('<CheckboxSelectItem />', () => {
  const setup = () => {
    const onChange = jest.fn();

    const selectItem: SelectItem = {
      id: '1',
    };

    const label = 'label';

    const result = render(
      <ThemeProvider theme={theme}>
        <CheckboxSelectItem
          selectItem={selectItem}
          onChange={onChange}
          isSelected={false}
          label={label}
          checkboxSize="md"
        />
      </ThemeProvider>,
    );

    const checkbox = result.getByTestId('checkbox');

    const checkboxLabel = result.getByText(label);

    return { onChange, result, checkbox, checkboxLabel };
  };

  it('should render properly', () => {
    const { checkbox, checkboxLabel } = setup();

    expect(checkbox).toBeInTheDocument();
    expect(checkboxLabel).toBeInTheDocument();
  });

  it('should call onChange when clicked checkbox', async () => {
    // given
    const { checkbox, onChange } = setup();

    // when
    fireEvent.click(checkbox);

    // then
    expect(onChange).toBeCalled();
  });
});
