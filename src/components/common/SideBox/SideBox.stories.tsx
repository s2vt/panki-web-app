import { Meta, Story } from '@storybook/react';
import SideBox, { SideBoxProps } from './SideBox';

export default {
  title: 'Components/common/SideBox',
  component: SideBox,
} as Meta;

const Template: Story<SideBoxProps> = (args) => <SideBox {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  title: '타이틀',
};
