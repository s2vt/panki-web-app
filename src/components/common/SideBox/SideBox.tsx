import styled from 'styled-components';

export type SideBoxProps = {
  title?: string;
  children?: React.ReactNode;
  renderButton?: () => React.ReactNode;
  className?: string;
};

const SideBox = ({
  title,
  children,
  renderButton,
  className,
}: SideBoxProps) => (
  <Box className={className}>
    <TitleSection>
      <Title>{title}</Title>
      {renderButton && renderButton()}
    </TitleSection>
    {children}
  </Box>
);

const Box = styled.div`
  height: 24rem;
  padding: 1.9rem 2.6rem;
  border: 1px solid;
  border-color: ${({ theme }) => theme.colors.primary};
  border-radius: 4px;
  overflow-y: auto;

  ${({ theme }) => theme.breakPoint.large} {
    height: ${({ theme }) => theme.sizes.centerMaxHeight};
  }
`;

const TitleSection = styled.div`
  margin-bottom: 2.7rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled.p`
  font-size: 2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

export default SideBox;
