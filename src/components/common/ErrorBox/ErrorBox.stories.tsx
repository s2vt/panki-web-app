import { Meta, Story } from '@storybook/react';
import ErrorBox, { ErrorBoxProps } from './ErrorBox';

export default {
  title: 'Components/Common/ErrorBox',
  component: ErrorBox,
} as Meta;

const Template: Story<ErrorBoxProps> = (args) => <ErrorBox {...args} />;

export const Basic = Template.bind({});
