import styled from 'styled-components';
import { REFETCH } from '@src/constants/constantString';
import MainButton from '../MainButton';

export type ErrorBoxProps = {
  error: string;
  onRefetchClick: () => void;
};

const ErrorBox = ({ error, onRefetchClick }: ErrorBoxProps) => (
  <Box>
    <ErrorText>{error}</ErrorText>
    <MainButton
      label={REFETCH}
      onClick={onRefetchClick}
      fontSize="1.4rem"
      width="10rem"
      height="3.4rem"
    />
  </Box>
);

const Box = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 1.6rem;
`;

const ErrorText = styled.p`
  font-size: 1.6rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default ErrorBox;
