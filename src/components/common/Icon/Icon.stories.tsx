import { Meta, Story } from '@storybook/react';
import Icon, { IconProps } from './Icon';

export default {
  title: 'Components/common/Icon',
  component: Icon,
} as Meta;

const Template: Story<IconProps> = (args) => <Icon {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  icon: 'pankiLogo',
};
