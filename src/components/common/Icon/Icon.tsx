import { createElement, CSSProperties, memo } from 'react';
import * as svg from '@src/assets/icons';

export type IconType = keyof typeof svg;

export type IconProps = React.SVGAttributes<SVGElement> & {
  /**
   * 사용할 아이콘 svg 파일명을 지정하면 됩니다
   * assets/icons/index.tsx 에 등록된 파일만 가져옵니다.
   */
  icon: IconType;
  className?: string;
  style?: CSSProperties;
};

/**
 * aseets에 등록된 svg 아이콘 파일들을 사용하기 위한 컴포넌트 입니다
 */
const Icon = ({ icon, className, style, ...rest }: IconProps) =>
  createElement(svg[icon], { className, style, ...rest });

export default memo(Icon);
