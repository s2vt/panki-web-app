import { Meta, Story } from '@storybook/react';
import CheckboxButton, { CheckboxButtonProps } from './CheckboxButton';

export default {
  title: 'Components/common/CheckboxButton',
  component: CheckboxButton,
} as Meta;

const Template: Story<CheckboxButtonProps> = (args) => (
  <CheckboxButton {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  label: '체크',
};
