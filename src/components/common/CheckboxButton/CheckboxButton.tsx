import { memo } from 'react';
import styled from 'styled-components';
import { Size } from '@src/types/style.types';
import Checkbox, { CheckboxIcon } from '../Checkbox';

export type CheckboxButtonProps =
  React.ButtonHTMLAttributes<HTMLButtonElement> & {
    label: string;
    checked: boolean;
    checkboxSize?: Size;
    checkboxIcon: CheckboxIcon;
    className?: string;
  };

const CheckboxButton = ({
  label,
  checked,
  checkboxSize,
  checkboxIcon,
  className,
  type = 'button',
  ...rest
}: CheckboxButtonProps) => (
  <Button type={type} className={className} {...rest}>
    <Label>{label}</Label>
    <Checkbox
      checkboxIcon={checkboxIcon}
      checkboxSize={checkboxSize}
      checked={checked}
      readOnly
    />
  </Button>
);

const Button = styled.button`
  display: flex;
  align-items: center;
  gap: 0.8rem;
  border: none;
  background: none;
  cursor: pointer;
`;

const Label = styled.p`
  font-size: 1.6rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

export default memo(CheckboxButton);
