import { ButtonHTMLAttributes, memo } from 'react';
import styled, { css } from 'styled-components';
import { Size } from '@src/types/style.types';

export type RoundButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  /** 버튼 라벨 */
  label: string;
  /** 버튼 누를 때 호출 할 함수 */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  isActive?: boolean;
  size?: Size;
  className?: string;
  fontSize?: string | number;
  fontWeight?: number | number;
  width?: string | number;
  height?: string | number;
};

const RoundButton = ({
  label,
  onClick,
  isActive,
  size = 'md',
  fontSize,
  fontWeight,
  width,
  height,
  ...rest
}: RoundButtonProps) => (
  <Button
    type="button"
    onClick={onClick}
    isActive={isActive}
    size={size}
    width={width}
    height={height}
    {...rest}
  >
    <Label isActive={isActive} size={size}>
      {label}
    </Label>
  </Button>
);

const getLabelSize = (size?: Size) => {
  switch (size) {
    case 'xs':
      return css`
        font-size: 1.2rem;
      `;
    default:
      return css`
        font-size: 1.6rem;
      `;
  }
};

const getButtonSize = (size?: Size) => {
  switch (size) {
    case 'xs':
      return css`
        min-width: 4.3rem;
        height: 2.6rem;
      `;
    default:
      return css`
        min-width: 12rem;
        height: 3.6rem;
      `;
  }
};

const Label = styled.p<{ isActive?: boolean; size?: Size }>`
  ${({ size }) => getLabelSize(size)}
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};

  ${({ isActive, theme }) =>
    isActive &&
    css`
      color: ${theme.colors.white};
    `}
`;

const Button = styled.button<{
  isActive?: boolean;
  size?: Size;
  width?: string | number;
  height?: string | number;
}>`
  ${({ size }) => getButtonSize(size)}
  ${({ theme }) => theme.layout.flexCenterLayout}
  min-width: ${({ width }) => width};
  height: ${({ height }) => height};
  background: ${({ theme }) => theme.colors.white};
  border-radius: 28px;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  cursor: pointer;
  transition: background 0.2s;

  ${({ isActive, theme }) =>
    isActive &&
    css`
      background: ${theme.colors.primary};
    `}

  &:hover {
    background: ${({ theme }) => theme.colors.primary};
  }

  &:hover ${Label} {
    color: ${({ theme }) => theme.colors.white};
  }
`;

export default memo(RoundButton);
