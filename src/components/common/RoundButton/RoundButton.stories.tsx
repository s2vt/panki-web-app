import { Meta, Story } from '@storybook/react';
import RoundButton, { RoundButtonProps } from './RoundButton';

export default {
  title: 'Components/common/RoundButton',
  component: RoundButton,
} as Meta;

const Template: Story<RoundButtonProps> = (args) => <RoundButton {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: '버튼',
};
