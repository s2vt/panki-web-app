import { Meta, Story } from '@storybook/react';
import DatePickerWrapper, { DatePickerWrapperProps } from './DatePickerWrapper';

export default {
  title: 'Components/common/DatePickerWrapper',
  component: DatePickerWrapper,
} as Meta;

const Template: Story<DatePickerWrapperProps> = (args) => (
  <div style={{ height: '350px' }}>
    <DatePickerWrapper {...args}>
      <button type="button">날짜 선택</button>
    </DatePickerWrapper>
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  minDate: new Date(),
  onChange: (date: Date) => console.log(date),
};
