import DatePicker, {
  registerLocale,
  ReactDatePickerProps,
  RenderCustomHeaderParams,
} from 'react-datepicker';
import ko from 'date-fns/locale/ko';
import { useCallback, useEffect, useState } from 'react';

import 'react-datepicker/dist/react-datepicker.css';
import './style.css';
import styled from 'styled-components';
import Icon from '../Icon';

registerLocale('ko', ko);

export type DatePickerWrapperProps = ReactDatePickerProps<string> & {
  children: React.ReactNode;
  onChange: (date: Date) => void;
  minDate?: Date | undefined | null;
  startDate?: Date | undefined | null;
};

const DatePickerWrapper = ({
  children,
  onChange,
  minDate,
  startDate,
  ...rest
}: DatePickerWrapperProps) => {
  const [selectedDate, setSelectedDate] = useState<Date | undefined | null>(
    startDate,
  );

  const formatToCommon = useCallback(
    (date: Date) => `${date.getFullYear()}년 ${date.getMonth() + 1}월 `,
    [],
  );

  useEffect(() => {
    if (selectedDate) {
      onChange(selectedDate);
    }
  }, [selectedDate]);

  const renderCustomHeader = ({
    date,
    decreaseMonth,
    increaseMonth,
    prevMonthButtonDisabled,
    nextMonthButtonDisabled,
  }: RenderCustomHeaderParams) => (
    <HeaderWrapper>
      <ArrowButton
        type="button"
        onClick={decreaseMonth}
        disabled={prevMonthButtonDisabled}
      >
        <PreviousIcon icon="arrowIcon" />
      </ArrowButton>
      <div>
        <p>{formatToCommon(date)}</p>
      </div>
      <ArrowButton
        type="button"
        onClick={increaseMonth}
        disabled={nextMonthButtonDisabled}
      >
        <NextIcon icon="arrowIcon" />
      </ArrowButton>
    </HeaderWrapper>
  );

  return (
    <DatePicker
      {...rest}
      minDate={minDate}
      showPopperArrow={false}
      selected={selectedDate}
      onChange={(date: Date) => setSelectedDate(date)}
      renderCustomHeader={renderCustomHeader}
      customInput={children}
      locale="ko"
      enableTabLoop={false}
    />
  );
};

const HeaderWrapper = styled.div`
  padding: 0 1.1rem;
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: center;
  font-size: 2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const ArrowButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
`;

const PreviousIcon = styled(Icon)`
  transform: rotate(90deg);
`;

const NextIcon = styled(Icon)`
  transform: rotate(270deg);
`;

export default DatePickerWrapper;
