import { Meta, Story } from '@storybook/react';
import ConfirmModal, { ConfirmModalProps } from './ConfirmModal';

export default {
  title: 'Components/management/ConfirmModal',
  component: ConfirmModal,
} as Meta;

const Template: Story<ConfirmModalProps> = (args) => <ConfirmModal {...args} />;

export const Basic = Template.bind({});
