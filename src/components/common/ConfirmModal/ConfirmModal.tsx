/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import styled from 'styled-components';
import { CANCEL, CONFIRM } from '@src/constants/constantString';
import MainButton from '../MainButton';

export type ConfirmModalProps = {
  /** 보여줄 텍스트 */
  text: string;
  /** 확인 버튼 클릭 시 실행할 이벤트 */
  onConfirm: () => void;
  /** 취소 버튼 클릭 시 실행할 이벤트 */
  onClose: () => void;
};

const ConfirmModal = ({ text, onConfirm, onClose }: ConfirmModalProps) => {
  const slicedTexts = text.split('\n');

  return (
    <Box>
      {slicedTexts.map((text, index) => (
        <Text key={index} id={text}>
          {text}
        </Text>
      ))}
      <ButtonSection>
        <MainButton label={CANCEL} onClick={onClose} />
        <MainButton label={CONFIRM} onClick={onConfirm} />
      </ButtonSection>
    </Box>
  );
};

const Box = styled.div`
  width: 36rem;
  height: 16rem;
  padding: 2rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  flex-direction: column;
`;

const Text = styled.div`
  padding: 0.4rem;
  display: flex;
  flex-direction: column;
  gap: 0.8rem;
  text-align: center;
  font-size: 1.6rem;
  color: ${({ theme }) => theme.colors.primary};
`;

const ButtonSection = styled.div`
  display: flex;
  gap: 1rem;
  margin-top: 2rem;
`;

export default memo(ConfirmModal);
