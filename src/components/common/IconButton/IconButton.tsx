import { memo, forwardRef } from 'react';
import styled, { Colors, css } from 'styled-components';
import { Size } from '@src/types/style.types';
import { debugAssert } from '@src/utils/commonUtil';
import Icon from '../Icon';
import { IconType } from '../Icon/Icon';

export type IconButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  icon: IconType;
  backgroundColor?: keyof Colors;
  labelColor?: keyof Colors;
  disabled?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  size?: Size;
  label?: string;
  className?: string;
  width?: string | number;
  height?: string | number;
  fontSize?: string | number;
  fontWeight?: number | number;
  iconStrokeColor?: keyof Colors;
  iconFillColor?: keyof Colors;
  iconWidth?: string | number;
  iconHeight?: string | number;
};

const IconButton = (
  {
    icon,
    type = 'button',
    backgroundColor = 'primary',
    labelColor = 'white',
    disabled,
    onClick,
    size = 'md',
    label,
    className,
    width,
    height,
    iconWidth,
    fontSize = '1.25rem',
    fontWeight,
    iconStrokeColor,
    iconFillColor,
    iconHeight,
    ...rest
  }: IconButtonProps,
  ref: React.Ref<HTMLButtonElement>,
) => (
  <Button
    type={type}
    onClick={onClick}
    backgroundColor={backgroundColor}
    size={size}
    disabled={disabled}
    className={className}
    width={width}
    height={height}
    ref={ref}
    {...rest}
  >
    <StyledIcon
      icon={icon}
      $iconStrokeColor={iconStrokeColor}
      $iconFillColor={iconFillColor}
      width={iconWidth}
      height={iconHeight}
    />
    {label && (
      <Label
        fontSize={fontSize}
        fontWeight={fontWeight}
        labelColor={labelColor}
      >
        {label}
      </Label>
    )}
  </Button>
);

const getDefaultButtonSize = (size: Size) => {
  switch (size) {
    case 'sm':
      return css`
        width: 4.8rem;
        height: 2.1rem;
      `;
    case 'md':
      return css`
        width: 9.125rem;
        height: 3.5rem;
      `;
    case 'lg':
      return css`
        width: 19.5rem;
        height: 3.5rem;
      `;
    default:
      debugAssert(false, `Size is not includes ${size}`);
      return 'md';
  }
};

const Button = styled.button<{
  backgroundColor: keyof Colors;
  size: Size;
  disabled?: boolean;
  width?: string | number;
  height?: string | number;
}>`
  ${({ theme }) => theme.layout.flexCenterLayout}
  cursor: pointer;
  border-style: solid;
  border-width: 1px;
  border-color: ${({ theme }) => theme.colors.primary};
  background-color: ${({ backgroundColor, theme }) =>
    theme.colors[backgroundColor]};
  ${({ size, width, height }) =>
    width !== undefined || height !== undefined
      ? css`
          width: ${width};
          height: ${height};
        `
      : getDefaultButtonSize(size)}

  // TODO : Change disabled color
  &:disabled {
    background-color: ${({ theme }) => theme.colors.modalBackground};
  }
`;

const Label = styled.p<{
  fontSize: string | number;
  fontWeight?: string | number;
  labelColor: keyof Colors;
}>`
  margin-left: 1rem;
  font-size: ${({ fontSize }) => fontSize};
  font-weight: ${({ fontWeight }) => fontWeight};
  color: ${({ labelColor, theme }) => theme.colors[labelColor]};
`;

const StyledIcon = styled(Icon)<{
  $iconStrokeColor?: keyof Colors;
  $iconFillColor?: keyof Colors;
  width?: string | number;
  height?: string | number;
}>`
  width: ${({ width }) => width};
  height: ${({ height }) => height};

  * {
    ${({ $iconStrokeColor, theme }) =>
      $iconStrokeColor !== undefined &&
      css`
        stroke: ${theme.colors[$iconStrokeColor]};
      `}

    ${({ $iconFillColor, theme }) =>
      $iconFillColor !== undefined &&
      css`
        fill: ${theme.colors[$iconFillColor]};
      `}
  }
`;

export default memo(forwardRef(IconButton));
