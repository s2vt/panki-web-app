import { Meta, Story } from '@storybook/react';
import IconButton, { IconButtonProps } from './IconButton';

export default {
  title: 'Components/Common/IconButton',
  component: IconButton,
} as Meta;

const Template: Story<IconButtonProps> = (args) => <IconButton {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  icon: 'arrowIcon',
  backgroundColor: 'white',
  iconWidth: '10px',
  iconHeight: '10px',
};

export const WithLabel = Template.bind({});
WithLabel.args = {
  icon: 'arrowIcon',
  iconStrokeColor: 'white',
  iconWidth: '10px',
  iconHeight: '10px',
  label: '라벨',
};
