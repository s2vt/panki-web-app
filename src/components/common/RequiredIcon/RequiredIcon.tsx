import { memo } from 'react';
import styled from 'styled-components';

export type RequiredIconProps = {};

const RequiredIcon = () => <Icon>*</Icon>;

const Icon = styled.span`
  color: ${({ theme }) => theme.colors.red};
`;

export default memo(RequiredIcon);
