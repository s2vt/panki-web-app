import { Meta, Story } from '@storybook/react';
import RequiredIcon, { RequiredIconProps } from './RequiredIcon';

export default {
  title: 'Components/common/RequiredIcon',
  component: RequiredIcon,
} as Meta;

const Template: Story<RequiredIconProps> = (args) => <RequiredIcon {...args} />;

export const Basic = Template.bind({});
