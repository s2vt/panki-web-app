import { Meta, Story } from '@storybook/react';
import TextButton, { TextButtonProps } from './TextButton';

export default {
  title: 'Components/common/TextButton',
  component: TextButton,
} as Meta;

const Template: Story<TextButtonProps> = (args) => <TextButton {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: 'Button',
};
