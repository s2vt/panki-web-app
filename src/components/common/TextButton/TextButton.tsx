import { memo } from 'react';
import styled, { Colors } from 'styled-components';

export type TextButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  /** 버튼 라벨 */
  label: string;
  /** 버튼 색상 */
  color?: keyof Colors;
  /** 버튼 누를 때 호출 할 이벤트 */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  className?: string;
};

const TextButton = ({ label, color, onClick, className }: TextButtonProps) => (
  <Button onClick={onClick} color={color} className={className}>
    {label}
  </Button>
);

const Button = styled.button<{ color?: string }>`
  ${({ theme }) => theme.layout.flexCenterLayout}
  border: none;
  background: none;
  color: ${({ color, theme }) => color ?? theme.colors.primary};
  font-size: 1.6rem;
  font-weight: 700;
  cursor: pointer;
`;

export default memo(TextButton);
