import { Meta, Story } from '@storybook/react';
import DropdownSelectWrapper, {
  DropdownSelectWrapperProps,
} from './DropdownSelectWrapper';

export default {
  title: 'Components/DropdownSelectWrapper',
  component: DropdownSelectWrapper,
} as Meta;

const Template: Story<DropdownSelectWrapperProps> = (args) => (
  <DropdownSelectWrapper {...args} />
);

export const Basic = Template.bind({});
