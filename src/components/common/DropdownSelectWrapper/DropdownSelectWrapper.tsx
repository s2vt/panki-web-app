import React, { memo, useCallback, useState } from 'react';
import styled from 'styled-components';
import { DropdownItem } from '@src/types/common.types';
import { Size } from '@src/types/style.types';
import { ClickAwayListener, Popover } from '@mui/material';
import DropdownSelectItem from '../DropdownSelectItem';

export type DropdownSelectWrapperProps = {
  /** 버튼에 들어갈 컴포넌트 */
  children: React.ReactElement<{ isActive: boolean }>;
  /** 드랍다운 아이템 */
  dropdownItems: DropdownItem[];
  onClick: (selectItem: DropdownItem) => void;
  isSelected: (selectItem: DropdownItem) => boolean;
  checkboxSize?: Size;
  boxWidth?: string | number;
  boxHeight?: string | number;
  className?: string;
};

const DropdownSelectWrapper = ({
  children,
  dropdownItems,
  onClick,
  isSelected,
  boxWidth,
  boxHeight,
  className,
}: DropdownSelectWrapperProps) => {
  const [dropdownElement, setDropdownElement] = useState<HTMLElement | null>(
    null,
  );
  const isDropdownOpen = Boolean(dropdownElement);

  const toggleDropdown = useCallback(
    (e: React.MouseEvent<HTMLElement>) => {
      if (dropdownElement === null) {
        setDropdownElement(e.currentTarget);
      } else {
        setDropdownElement(null);
      }
    },
    [setDropdownElement],
  );

  const handleClick = useCallback(
    (dropdownItem: DropdownItem) => {
      setDropdownElement(null);
      onClick(dropdownItem);
    },
    [setDropdownElement, onClick],
  );

  return (
    <Box className={className}>
      <Title onClick={toggleDropdown}>
        {React.cloneElement(children, { isActive: isDropdownOpen })}
      </Title>
      <Popover
        open={isDropdownOpen}
        anchorEl={dropdownElement}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <ClickAwayListener onClickAway={() => setDropdownElement(null)}>
          <DropdownSection boxWidth={boxWidth} boxHeight={boxHeight}>
            {dropdownItems.map((dropdownItem) => (
              <CheckboxItemWrapper key={dropdownItem.id}>
                <DropdownSelectItem
                  onClick={() => handleClick(dropdownItem)}
                  isSelected={isSelected(dropdownItem)}
                  label={dropdownItem.label}
                />
              </CheckboxItemWrapper>
            ))}
          </DropdownSection>
        </ClickAwayListener>
      </Popover>
    </Box>
  );
};

const Box = styled.div`
  height: 100%;
  display: flex;
  position: relative;
`;

const Title = styled.div`
  width: 100%;
  border: none;
  background: none;
  cursor: pointer;
  position: relative;
`;

const DropdownSection = styled.div<{
  boxWidth?: string | number;
  boxHeight?: string | number;
}>`
  display: flex;
  flex-direction: column;
  width: ${({ boxWidth }) => boxWidth ?? '14.6rem'};
  height: ${({ boxHeight }) => boxHeight};
  background-color: ${({ theme }) => theme.colors.white};
  padding: 1.6rem 0;
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.15);
  z-index: ${({ theme }) => theme.zIndexes.dropdown};
`;

const CheckboxItemWrapper = styled.div`
  display: flex;
`;

export default memo(DropdownSelectWrapper);
