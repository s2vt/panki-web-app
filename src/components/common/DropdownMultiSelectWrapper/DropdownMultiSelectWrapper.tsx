import { ClickAwayListener, Popover } from '@mui/material';
import { DropdownItem } from '@src/types/common.types';
import { Size } from '@src/types/style.types';
import React, { memo, useCallback, useState } from 'react';
import styled from 'styled-components';
import CheckboxSelectItem from '../CheckboxSelectItem';

export type DropdownMultiSelectWrapperProps = {
  /** 버튼에 들어갈 컴포넌트 */
  children: React.ReactElement<{ isActive: boolean }>;
  /** 드랍다운 아이템 */
  dropdownItems: DropdownItem[];
  onChange: (selectItem: DropdownItem) => void;
  isSelected: (selectItem: DropdownItem) => boolean;
  checkboxSize?: Size;
  boxWidth?: string | number;
  boxHeight?: string | number;
  className?: string;
};

const DropdownMultiSelectWrapper = ({
  children,
  dropdownItems,
  onChange,
  isSelected,
  checkboxSize,
}: DropdownMultiSelectWrapperProps) => {
  const [dropdownElement, setDropdownElement] = useState<HTMLElement | null>(
    null,
  );
  const isDropdownOpen = Boolean(dropdownElement);

  const toggleDropdown = useCallback(
    (e: React.MouseEvent<HTMLElement>) => {
      if (dropdownElement === null) {
        setDropdownElement(e.currentTarget);
      } else {
        setDropdownElement(null);
      }
    },
    [setDropdownElement],
  );

  const handleChange = useCallback(
    (dropdownItem: DropdownItem) => {
      onChange(dropdownItem);
    },
    [onChange],
  );

  return (
    <Box>
      <Title onClick={toggleDropdown}>
        {React.cloneElement(children, { isActive: isDropdownOpen })}
      </Title>
      <Popover
        open={isDropdownOpen}
        anchorEl={dropdownElement}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <ClickAwayListener onClickAway={() => setDropdownElement(null)}>
          <DropdownSection>
            {dropdownItems.map((dropdownItem) => (
              <CheckboxItemWrapper key={dropdownItem.id}>
                <CheckboxSelectItem
                  selectItem={dropdownItem}
                  onChange={() => handleChange(dropdownItem)}
                  isSelected={isSelected(dropdownItem)}
                  label={dropdownItem.label}
                  checkboxSize={checkboxSize}
                />
              </CheckboxItemWrapper>
            ))}
          </DropdownSection>
        </ClickAwayListener>
      </Popover>
    </Box>
  );
};

const Box = styled.div`
  height: 100%;
  display: flex;
  position: relative;
`;

const Title = styled.div`
  width: 100%;
  border: none;
  background: none;
  cursor: pointer;
  position: relative;
`;

const DropdownSection = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1.7rem;
  width: 14.6rem;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 1.3rem 0 1.1rem 1.9rem;
  box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.15);
  z-index: ${({ theme }) => theme.zIndexes.dropdown};
  left: 0;
`;

const CheckboxItemWrapper = styled.div`
  display: flex;
`;

export default memo(DropdownMultiSelectWrapper);
