import { Meta, Story } from '@storybook/react';
import MainButton from '../MainButton';
import DropdownMultiSelectWrapper, {
  DropdownMultiSelectWrapperProps,
} from './DropdownMultiSelectWrapper';

export default {
  title: 'Components/common/DropdownMultiSelectWrapper',
  component: DropdownMultiSelectWrapper,
} as Meta;

const Template: Story<DropdownMultiSelectWrapperProps> = (args) => (
  <div style={{ margin: '2rem' }}>
    <DropdownMultiSelectWrapper {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  children: <MainButton label="버튼" />,
  dropdownItems: [
    { id: '1', label: 'Item 1' },
    { id: '2', label: 'Item 2' },
    { id: '3', label: 'Item 3' },
  ],
  isSelected: (_) => true,
};
