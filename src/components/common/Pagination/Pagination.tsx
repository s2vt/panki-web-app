/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import styled, { css } from 'styled-components';
import { NEXT, PREVIOUS } from '@src/constants/constantString';
import usePagination from '@src/hooks/common/usePagination';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import Icon from '../Icon';

export type PaginationProps = {
  currentPage: number;
  lastPage: number;
  onPageClick: (page: number) => void;
  criteria?: number;
};

const Pagination = ({
  currentPage,
  lastPage,
  onPageClick,
  criteria = 10,
}: PaginationProps) => {
  const pages = Array.from({ length: lastPage }).map((_, index) => index + 1);

  const {
    currentPageItems,
    setCurrentPage,
    hasPreviousPage,
    hasNextPage,
    previousPage,
    nextPage,
    lastPage: paginationLastPage,
  } = usePagination(pages, criteria);

  const handlePageClick = (page: number) => {
    if (page === currentPage) {
      return;
    }

    onPageClick(page);
  };

  const handleFirstPageClick = () => {
    onPageClick(1);
    setCurrentPage(1);
  };

  const handleLastPageClick = () => {
    onPageClick(lastPage);
    setCurrentPage(paginationLastPage);
  };

  const handlePreviousClick = () => {
    onPageClick(currentPageItems[0] - criteria);
    previousPage();
  };

  const handleNextClick = () => {
    onPageClick(currentPageItems[0] + criteria);
    nextPage();
  };

  return (
    <PaginationWrapper>
      {hasPreviousPage && (
        <>
          <PreviousNextButton onClick={handlePreviousClick}>
            <Icon icon="pageArrowIcon" />
            <p>{PREVIOUS}</p>
          </PreviousNextButton>
          <PageButton
            isSelected={currentPage === 1}
            onClick={handleFirstPageClick}
          >
            1
          </PageButton>
          <EllipsisButton onClick={handlePreviousClick} />
        </>
      )}
      {currentPageItems.map((page) => (
        <PageButton
          key={page}
          isSelected={currentPage === page}
          onClick={() => handlePageClick(page)}
        >
          {page}
        </PageButton>
      ))}
      {hasNextPage && (
        <>
          <EllipsisButton onClick={handleNextClick} />
          <PageButton
            isSelected={currentPage === lastPage}
            onClick={handleLastPageClick}
          >
            {lastPage}
          </PageButton>
          <PreviousNextButton onClick={handleNextClick}>
            <p>{NEXT}</p>
            <NextIcon icon="pageArrowIcon" />
          </PreviousNextButton>
        </>
      )}
    </PaginationWrapper>
  );
};

const EllipsisButton = ({ onClick }: { onClick: () => void }) => (
  <Ellipsis onClick={onClick}>
    {addWhiteSpacesBetweenCharacters('...')}
  </Ellipsis>
);

const PaginationWrapper = styled.ul`
  height: 2.4rem;
  display: flex;
  align-items: center;
  gap: 1.6rem;
`;

const PreviousNextButton = styled.li`
  gap: 1.4rem;
  height: 2.4rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  border: none;
  background: none;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
`;

const NextIcon = styled(Icon)`
  transform: scaleX(-1);
`;

const Ellipsis = styled.li`
  height: 1.1rem;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
  border: none;
  background: none;
`;

const PageButton = styled.li<{ isSelected: boolean }>`
  min-width: 2.4rem;
  height: 2.4rem;
  font-size: 1.4rem;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.primary};
  border: none;
  background: none;

  ${({ theme }) => theme.layout.flexCenterLayout}

  ${({ isSelected, theme }) =>
    isSelected &&
    css`
      color: ${theme.colors.white};
      background: ${theme.colors.primary};
    `}
`;

export default memo(Pagination);
