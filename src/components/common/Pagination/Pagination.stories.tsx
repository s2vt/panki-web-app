import { Meta, Story } from '@storybook/react';
import Pagination, { PaginationProps } from './Pagination';

export default {
  title: 'Components/common/Pagination',
  component: Pagination,
} as Meta;

const Template: Story<PaginationProps> = (args) => <Pagination {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  lastPage: 35,
  currentPage: 1,
};
