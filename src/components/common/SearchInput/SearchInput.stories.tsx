import { Meta, Story } from '@storybook/react';
import SearchInput, { SearchInputProps } from './SearchInput';

export default {
  title: 'Components/common/SearchInput',
  component: SearchInput,
} as Meta;

const Template: Story<SearchInputProps> = (args) => <SearchInput {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  placeholder: '검색어를 입력하세요',
};
