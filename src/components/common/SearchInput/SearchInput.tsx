import React, {
  forwardRef,
  InputHTMLAttributes,
  memo,
  useCallback,
  useState,
} from 'react';
import styled, { css } from 'styled-components';
import Icon from '../Icon';

export type SearchInputProps = InputHTMLAttributes<HTMLInputElement> & {
  onSearchEnter: (value: string) => void;
  width?: string | number;
  height?: string | number;
  isEnableRadius?: boolean;
};

const ENTER_KEY = 'Enter';

const SearchInput = (
  {
    onSearchEnter,
    width = '32.8rem',
    height = '3.6rem',
    isEnableRadius = true,
    onChange,
    ...rest
  }: SearchInputProps,
  ref: React.Ref<HTMLInputElement>,
) => {
  const [value, setValue] = useState('');

  const handleKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === ENTER_KEY) {
        e.preventDefault();
        onSearchEnter(e.currentTarget.value);
      }
    },
    [onSearchEnter],
  );

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (onChange !== undefined) {
        onChange(e);
      }

      setValue(e.target.value);
    },
    [setValue, onChange],
  );

  const handleIconClick = useCallback(() => {
    onSearchEnter(value);
  }, [value, onSearchEnter]);

  return (
    <InputWrapper width={width} height={height} isEnableRadius={isEnableRadius}>
      <SearchIcon icon="searchIcon" onClick={handleIconClick} />
      <Input
        onKeyDown={handleKeyDown}
        onChange={handleChange}
        ref={ref}
        {...rest}
        isEnableRadius={isEnableRadius}
      />
    </InputWrapper>
  );
};

const InputWrapper = styled.div<{
  width: string | number;
  height: string | number;
  isEnableRadius: boolean;
}>`
  width: ${({ width }) => width ?? '32.8rem'};
  height: ${({ height }) => height ?? '3.6rem'};
  position: relative;
  display: flex;
  align-items: center;
  gap: 1.2rem;
  background: ${({ theme }) => theme.colors.white};
  box-shadow: 0px 4px 250px 0px rgba(0, 0, 0, 0.15);
  ${({ isEnableRadius }) =>
    isEnableRadius &&
    css`
      border-radius: 24px;
    `}
`;

const Input = styled.input<{
  isEnableRadius: boolean;
}>`
  width: 100%;
  height: 100%;
  padding-left: calc(1.9rem + 1.5rem + 1.2rem);
  border: none;
  font-size: 1.3rem;
  font-weight: 600;
  color: ${({ theme }) => theme.colors.primary};
  ${({ isEnableRadius }) =>
    isEnableRadius &&
    css`
      border-radius: 24px;
    `}

  ::placeholder {
    color: ${({ theme }) => theme.colors.primary};
  }
`;

const SearchIcon = styled(Icon)`
  cursor: pointer;
  position: absolute;
  left: 1.9rem;
  svg {
    width: 1.5rem;
    height: 1.5rem;
  }
`;

export default memo(forwardRef(SearchInput));
