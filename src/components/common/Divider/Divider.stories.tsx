import { Meta, Story } from '@storybook/react';
import Divider, { DividerProps } from './Divider';

export default {
  title: 'Components/common/Divider',
  component: Divider,
} as Meta;

const Template: Story<DividerProps> = (args) => <Divider {...args} />;

export const Basic = Template.bind({});
