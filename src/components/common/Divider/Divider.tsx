import { memo } from 'react';
import styled, { Colors, css } from 'styled-components';

export type DividerProps = {
  color?: keyof Colors;
  className?: string;
  length?: string;
  orientation: Orientation;
};

type Orientation = 'horizontal' | 'vertical';

const Divider = ({
  color = 'primary',
  className,
  length,
  orientation,
}: DividerProps) => (
  <Box
    lineColor={color}
    className={className}
    length={length}
    orientation={orientation}
  />
);

const Box = styled.div<{
  lineColor: keyof Colors;
  length?: string;
  orientation: Orientation;
}>`
  ${({ orientation, length }) =>
    orientation === 'horizontal'
      ? css`
          width: ${length};
          height: 1px;
        `
      : css`
          width: 1px;
          height: ${length};
        `};

  background-color: ${({ lineColor, theme }) => theme.colors[lineColor]};
`;

export default memo(Divider);
