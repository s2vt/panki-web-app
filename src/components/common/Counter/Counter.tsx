import {
  forwardRef,
  Ref,
  useEffect,
  useImperativeHandle,
  useMemo,
} from 'react';
import styled from 'styled-components';
import useCountdown from '@src/hooks/common/useCountdown';

export type CounterProps = {
  /** 카운트다운 시작 값 */
  initialCount: number;
  /** 카운트다운 초과 시 실행할 이벤트 */
  onCountOver: () => void;
};

export type CounterHandler = {
  reset: () => void;
};

/**
 * 카운트 다운 컴포넌트입니다
 *
 * CounterHandler 형태의 ref 오브젝트를 props로 넘겨서
 * 부모 컴포넌트에서 reset 함수를 사용할 수 있습니다
 *
 * */
const Counter = forwardRef(
  ({ initialCount, onCountOver }: CounterProps, ref: Ref<CounterHandler>) => {
    const { count, isCountOver, resetCount } = useCountdown(initialCount);

    const countTime = useMemo(() => {
      const countDate = new Date(count * 1000);

      const minutes = `0${countDate.getMinutes()}`.slice(-2);
      const seconds = `0${countDate.getSeconds()}`.slice(-2);

      return `${minutes} : ${seconds}`;
    }, [count]);

    useImperativeHandle(ref, () => ({
      reset() {
        resetCount();
      },
    }));

    useEffect(() => {
      if (isCountOver) {
        onCountOver();
      }
    }, [isCountOver]);

    return <CountText> {countTime}</CountText>;
  },
);

const CountText = styled.p`
  font-size: 2rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
  text-decoration: underline;
`;

export default Counter;
