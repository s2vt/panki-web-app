import { Meta, Story } from '@storybook/react';
import Counter, { CounterProps } from './Counter';

export default {
  title: 'Components/common/Counter',
  component: Counter,
} as Meta;

const Template: Story<CounterProps> = (args) => <Counter {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  initialCount: 100,
  onCountOver: () => {
    console.log('count over!');
  },
};
