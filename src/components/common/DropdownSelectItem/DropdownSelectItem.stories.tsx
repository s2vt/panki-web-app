import { Meta, Story } from '@storybook/react';
import DropdownSelectItem, {
  DropdownSelectItemProps,
} from './DropdownSelectItem';

export default {
  title: 'Components/Common/DropdownSelectItem',
  component: DropdownSelectItem,
} as Meta;

const Template: Story<DropdownSelectItemProps> = (args) => (
  <DropdownSelectItem {...args} />
);

export const Basic = Template.bind({});
