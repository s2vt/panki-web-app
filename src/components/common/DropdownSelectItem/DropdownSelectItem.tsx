import { memo } from 'react';
import styled from 'styled-components';

export type DropdownSelectItemProps = {
  onClick: () => void;
  isSelected: boolean;
  label: string;
  className?: string;
};

const DropdownSelectItem = ({
  onClick,
  label,
  className,
}: DropdownSelectItemProps) => (
  <Box className={className} onClick={onClick}>
    <Label>{label}</Label>
  </Box>
);

const Box = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

const Label = styled.p`
  padding: 0.8rem 1.6rem;
  font-size: 1.4rem;
  font-weight: 400;
  flex: 1;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.colors.disabled};
  }
`;

export default memo(DropdownSelectItem);
