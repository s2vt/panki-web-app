import { Meta, Story } from '@storybook/react';
import AlertModal, { AlertModalProps } from './AlertModal';

export default {
  title: 'Components/common/AlertModal',
  component: AlertModal,
} as Meta;

const Template: Story<AlertModalProps> = (args) => <AlertModal {...args} />;

export const Basic = Template.bind({});
