import { fireEvent, render } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import AlertModal from '.';

describe('<AlertModal />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareMockWrapper();

    const text = 'text';
    const onClose = jest.fn();

    const result = render(
      <Wrapper>
        <AlertModal text={text} onClose={onClose} />
      </Wrapper>,
    );

    const alertText = result.getByText(text);

    const confirmButton = result.getByText('확 인');

    return { text, onClose, result, alertText, confirmButton };
  };

  it('should render properly', () => {
    const { alertText, confirmButton } = setup();

    expect(alertText).toBeInTheDocument();
    expect(confirmButton).toBeInTheDocument();
  });

  it('should call onClose when clicked confirm button', () => {
    const { confirmButton, onClose } = setup();

    fireEvent.click(confirmButton);

    expect(onClose).toBeCalled();
  });
});
