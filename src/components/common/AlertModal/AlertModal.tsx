import { memo } from 'react';
import styled from 'styled-components';
import { CONFIRM } from '@src/constants/constantString';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import MainButton from '../MainButton';

export type AlertModalProps = {
  text: string;
  onClose: () => void;
};

const AlertModal = ({ text, onClose }: AlertModalProps) => (
  <Box>
    <TextSection>{text}</TextSection>
    <ConfirmButton
      label={addWhiteSpacesBetweenCharacters(CONFIRM)}
      onClick={onClose}
    />
  </Box>
);

const Box = styled.div`
  width: 65.5rem;
  height: 33.4rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 9.1rem 7rem 3.6rem;
`;

const TextSection = styled.div`
  display: flex;
  text-align: center;
  font-size: 2.2rem;
  font-weight: 400;
  line-height: 3.3rem;
  color: ${({ theme }) => theme.colors.primary};
  white-space: pre;
`;

const ConfirmButton = styled(MainButton)`
  width: 100%;
  height: 5.8rem;
  font-size: 2.4rem;
  font-weight: 700;
`;

export default memo(AlertModal);
