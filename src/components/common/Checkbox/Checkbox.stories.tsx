import { Meta, Story } from '@storybook/react';
import Checkbox, { CheckboxProps } from './Checkbox';

export default {
  title: 'Components/common/Checkbox',
  component: Checkbox,
  argTypes: {
    checkboxSize: {
      control: { type: 'radio', options: ['xs', 'sm', 'md', 'lg'] },
    },
    checkboxIcon: {
      control: {
        type: 'radio',
        options: ['fillCheckbox', 'disableCheckbox', 'blankCheckbox'],
      },
    },
  },
} as Meta;

const Template: Story<CheckboxProps> = (args) => <Checkbox {...args} />;

export const Basic = Template.bind({});
