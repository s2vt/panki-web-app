import { forwardRef, memo, Ref, useCallback } from 'react';
import styled, { css } from 'styled-components';
import { Size } from '@src/types/style.types';
import Icon from '../Icon';

export type CheckboxIcon =
  | 'fillCheckbox'
  | 'disableCheckbox'
  | 'blankCheckbox'
  | 'unselectedCheckbox';

export type CheckboxProps = React.InputHTMLAttributes<HTMLInputElement> & {
  checkboxIcon: CheckboxIcon;
  checkboxSize?: Size;
  enableShadow?: boolean;
  inactive?: boolean;
};

const Checkbox = (
  {
    name,
    id,
    checkboxIcon,
    enableShadow,
    checkboxSize = 'sm',
    inactive,
    ...rest
  }: CheckboxProps,
  ref: Ref<HTMLInputElement>,
) => {
  // TODO : fix forced reflow
  const renderCheckboxIcon = useCallback(() => {
    switch (checkboxIcon) {
      case 'fillCheckbox':
        return (
          <>
            <SelectedIcon icon="fillCheckboxSelectedIcon" />
            <UnselectedIcon icon="fillCheckboxUnselectedIcon" />
          </>
        );
      case 'disableCheckbox':
        return (
          <>
            <SelectedIcon icon="disableCheckboxSelectedIcon" />
            <UnselectedIcon icon="disableCheckboxUnselectedIcon" />
          </>
        );
      case 'blankCheckbox':
        return (
          <>
            <SelectedIcon icon="blankCheckboxSelectedIcon" />
            <UnselectedIcon icon="blankCheckboxUnselectedIcon" />
          </>
        );
      case 'unselectedCheckbox':
        return (
          <>
            <SelectedIcon icon="fillCheckboxSelectedIcon" />
            <UnselectedIcon icon="fillCheckboxSelectedIcon" />
          </>
        );
      default:
        return (
          <>
            <SelectedIcon icon="fillCheckboxSelectedIcon" />
            <UnselectedIcon icon="fillCheckboxUnselectedIcon" />
          </>
        );
    }
  }, [checkboxIcon]);

  return (
    <Box checkboxSize={checkboxSize} enableShadow={enableShadow}>
      <HiddenCheckbox type="checkbox" id={id} name={name} ref={ref} {...rest} />
      <CheckboxLabel htmlFor={id} inactive={inactive}>
        {renderCheckboxIcon()}
      </CheckboxLabel>
    </Box>
  );
};

const getCheckboxSize = (checkboxSize: Size) => {
  switch (checkboxSize) {
    case 'xs':
      return css`
        width: 1.5rem;
        height: 1.5rem;
      `;
    case 'sm':
      return css`
        width: 1.8rem;
        height: 1.8rem;
      `;
    case 'md':
      return css`
        width: 3.6rem;
        height: 3.6rem;
      `;
    case 'lg':
      return css`
        width: 4.8rem;
        height: 4.8rem;
      `;
    default:
      return css`
        width: 3.6rem;
        height: 3.6rem;
      `;
  }
};

const Box = styled.div<{
  checkboxSize: Size;
  enableShadow?: boolean;
}>`
  ${({ checkboxSize }) => getCheckboxSize(checkboxSize)}
  position: relative;
  ${({ theme }) => theme.layout.flexCenterLayout}
  ${({ enableShadow }) =>
    enableShadow &&
    css`
      box-shadow: 0px 2px 10px 0px rgba(0, 0, 0, 0.1);
    `}
`;

const CheckboxIcon = styled(Icon)`
  width: 100%;
  height: 100%;
  z-index: 1;
`;

const SelectedIcon = styled(CheckboxIcon)`
  display: none;
`;

const UnselectedIcon = styled(CheckboxIcon)`
  display: block;
`;

const CheckboxLabel = styled.label<{ inactive?: boolean }>`
  width: 100%;
  height: 100%;

  ${({ inactive }) =>
    inactive &&
    css`
      opacity: 0.5;
    `}
`;

const HiddenCheckbox = styled.input`
  width: 100%;
  height: 100%;
  margin: 0;
  opacity: 0;
  position: absolute;
  cursor: pointer;
  z-index: 2;

  &:checked + ${CheckboxLabel} > ${SelectedIcon} {
    display: block;
  }

  &:checked + ${CheckboxLabel} > ${UnselectedIcon} {
    display: none;
  }
`;

export default memo(forwardRef(Checkbox));
