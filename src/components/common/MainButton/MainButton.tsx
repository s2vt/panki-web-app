import { forwardRef, memo } from 'react';
import styled, { Colors, css } from 'styled-components';
import { Size } from '@src/types/style.types';
import { debugAssert } from '@src/utils/commonUtil';

export type MainButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  label: string;
  backgroundColor?: keyof Colors;
  labelColor?: keyof Colors;
  disabledBackgroundColor?: keyof Colors;
  disabledLabelColor?: keyof Colors;
  disabled?: boolean;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  size?: Size;
  className?: string;
  fontSize?: string | number;
  fontWeight?: number | number;
  width?: string | number;
  height?: string | number;
};

/**
 * 메인 버튼 컴포넌트입니다
 */
const MainButton = (
  {
    type = 'button',
    label,
    backgroundColor = 'primary',
    labelColor = 'white',
    disabledBackgroundColor = 'disabled',
    disabledLabelColor = 'disabledText',
    disabled,
    onClick,
    size = 'md',
    className,
    fontSize = '1.25rem',
    fontWeight,
    width,
    height,
    ...rest
  }: MainButtonProps,
  ref: React.Ref<HTMLButtonElement>,
) => (
  <Button
    type={type}
    onClick={onClick}
    backgroundColor={backgroundColor}
    labelColor={labelColor}
    disabledBackgroundColor={disabledBackgroundColor}
    disabledLabelColor={disabledLabelColor}
    size={size}
    disabled={disabled}
    ref={ref}
    className={className}
    fontSize={fontSize}
    fontWeight={fontWeight}
    width={width}
    height={height}
    {...rest}
  >
    {label}
  </Button>
);

const getDefaultButtonSize = (size: Size) => {
  switch (size) {
    case 'sm':
      return css`
        width: 4.8rem;
        height: 2.1rem;
      `;
    case 'md':
      return css`
        width: 9.125rem;
        height: 3.5rem;
      `;
    case 'lg':
      return css`
        width: 19.5rem;
        height: 3.5rem;
      `;
    default:
      debugAssert(false, `Size is not includes ${size}`);
      return '';
  }
};

const Button = styled.button<{
  backgroundColor: keyof Colors;
  disabledBackgroundColor: keyof Colors;
  labelColor: keyof Colors;
  disabledLabelColor: keyof Colors;
  size: Size;
  disabled?: boolean;
  fontSize?: string | number;
  fontWeight?: number | number;
  width?: string | number;
  height?: string | number;
}>`
  ${({ theme }) => theme.layout.flexCenterLayout}
  cursor: pointer;
  border-style: solid;
  border-width: 1px;
  border-color: ${({ theme }) => theme.colors.primary};
  background-color: ${({ backgroundColor, theme }) =>
    theme.colors[backgroundColor]};
  ${({ size, width, height }) =>
    width !== undefined || height !== undefined
      ? css`
          width: ${width};
          height: ${height};
        `
      : getDefaultButtonSize(size)}
  font-size: ${({ fontSize }) => fontSize};
  font-weight: ${({ fontWeight }) => fontWeight};
  color: ${({ labelColor, theme }) => theme.colors[labelColor]};

  &:disabled {
    ${({ disabledBackgroundColor, theme }) => css`
      background: ${theme.colors[disabledBackgroundColor]};
      border-color: ${theme.colors[disabledBackgroundColor]};
    `};
    color: ${({ disabledLabelColor, theme }) =>
      theme.colors[disabledLabelColor]};
  }
`;

export default memo(forwardRef(MainButton));
