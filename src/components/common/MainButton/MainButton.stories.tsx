import { Story, Meta } from '@storybook/react';
import MainButton, { MainButtonProps } from './MainButton';

export default {
  title: 'Components/common/MainButton',
  component: MainButton,
  argTypes: {
    size: {
      control: { type: 'radio', options: ['sm', 'md', 'lg'] },
    },
    backgroundColor: {
      control: { type: 'select', options: ['primary', 'white'] },
    },
  },
} as Meta;

const Template: Story<MainButtonProps> = (args) => <MainButton {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  label: 'Button',
  backgroundColor: 'primary',
};

export const White = Template.bind({});
White.args = {
  label: 'Button',
  backgroundColor: 'white',
  labelColor: 'primary',
};
