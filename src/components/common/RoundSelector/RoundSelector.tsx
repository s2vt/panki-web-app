import { memo } from 'react';
import styled from 'styled-components';
import { DropdownItem } from '@src/types/common.types';
import Icon from '../Icon';

export type RoundSelectorProps =
  React.SelectHTMLAttributes<HTMLSelectElement> & {
    items: DropdownItem[];
    width?: string | number;
    height?: string | number;
    className?: string;
  };

const RoundSelector = ({
  items,
  width,
  height,
  value,
  className,
  ...rest
}: RoundSelectorProps) => (
  <SelectWrapper width={width} height={height} className={className}>
    <Select value={value} {...rest}>
      {items.map((item) => (
        <option key={item.id} value={item.id}>
          {item.label}
        </option>
      ))}
    </Select>
    <SelectArrowIcon icon="arrowBoxIcon" />
  </SelectWrapper>
);

const SelectWrapper = styled.div<{
  width?: number | string;
  height?: number | string;
}>`
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  position: relative;
`;

const Select = styled.select`
  width: 100%;
  height: 100%;
  padding: 0 2.6rem;
  border-radius: 24px;
  font-size: 1.6rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
  appearance: none;
  cursor: pointer;
`;

const SelectArrowIcon = styled(Icon)`
  position: absolute;
  top: 0;
  right: 1.5rem;
  height: 100%;
  transform: rotate(90deg);
  cursor: pointer;
  pointer-events: none;
`;

export default memo(RoundSelector);
