import { Meta, Story } from '@storybook/react';
import RoundSelector, { RoundSelectorProps } from './RoundSelector';

export default {
  title: 'Components/Common/RoundSelector',
  component: RoundSelector,
} as Meta;

const Template: Story<RoundSelectorProps> = (args) => (
  <RoundSelector {...args} />
);

export const Basic = Template.bind({});
