import { memo } from 'react';
import styled, { css } from 'styled-components';
import { Size } from '@src/types/style.types';

export type ModifyButtonProps =
  React.ButtonHTMLAttributes<HTMLButtonElement> & {
    /** 버튼 라벨 */
    label: string;
    /** 버튼 사용 여부 */
    disabled?: boolean;
    /** 버튼 누를 때 호출 할 이벤트 */
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    /** 버튼 사이즈 */
    size?: Size;
  };

const ModifyButton = ({
  label,
  disabled,
  onClick,
  size = 'sm',
  ...rest
}: ModifyButtonProps) => (
  <Button onClick={onClick} size={size} disabled={disabled} {...rest}>
    <Label>{label}</Label>
  </Button>
);

const getButtonSize = (size: Size) => {
  switch (size) {
    case 'sm':
      return css`
        width: 4.8rem;
        height: 2rem;
      `;
    case 'md':
      return css`
        width: 9.125rem;
        height: 3.5rem;
      `;
    case 'lg':
      return css`
        width: 19.5rem;
        height: 3.5rem;
      `;
    default:
      return css`
        width: 9.125rem;
        height: 3.5rem;
      `;
  }
};

const Button = styled.button<{
  size: Size;
  disabled?: boolean;
}>`
  ${({ theme }) => theme.layout.flexCenterLayout}
  cursor: pointer;
  border: none;
  font-size: 1.4rem;
  font-weight: 700;
  background-color: ${({ theme }) => theme.colors.secondary};
  ${({ size }) => getButtonSize(size)}
`;

const Label = styled.p`
  color: ${({ theme }) => theme.colors.primary};
`;

export default memo(ModifyButton);
