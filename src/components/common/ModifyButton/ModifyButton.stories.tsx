import { Meta, Story } from '@storybook/react';
import ModifyButton, { ModifyButtonProps } from './ModifyButton';

export default {
  title: 'Components/common/ModifyButton',
  component: ModifyButton,
} as Meta;

const Template: Story<ModifyButtonProps> = (args) => <ModifyButton {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: '수정',
};
