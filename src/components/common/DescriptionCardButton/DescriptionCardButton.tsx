import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Icon, { IconType } from '../Icon/Icon';

export type DescriptionCardButtonProps = {
  icon: IconType;
  title: string;
  description: string;
  to: string;
};

const DescriptionCardButtonButton = ({
  icon,
  title,
  description,
  to,
}: DescriptionCardButtonProps) => (
  <Box to={to}>
    <TitleSection>
      <StyledIcon icon={icon} />
      {title}
    </TitleSection>
    <Description>{description}</Description>
  </Box>
);

const Box = styled(Link)`
  width: 33.3rem;
  padding: 1.8rem 2.6rem;
  display: flex;
  flex-direction: column;
  gap: 0.3rem;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  cursor: pointer;
  color: ${({ theme }) => theme.colors.primary};
  text-decoration: none;

  &:hover {
    background: ${({ theme }) => theme.colors.white};
  }
`;

const StyledIcon = styled(Icon)`
  width: 2.1rem;
  height: 2.1rem;
`;

const Description = styled.p`
  padding-left: 3.5rem;
  font-size: 1.4rem;
  text-align: left;
  line-height: 2.1rem;
`;

const TitleSection = styled.p`
  display: flex;
  align-items: center;
  font-size: 2rem;
  font-weight: 700;
  line-height: 3rem;
  gap: 1.3rem;
`;

export default DescriptionCardButtonButton;
