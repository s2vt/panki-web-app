import { Meta, Story } from '@storybook/react';
import DescriptionCardButtonButton, {
  DescriptionCardButtonProps,
} from './DescriptionCardButton';

export default {
  title: 'Components/common/DescriptionCardButton',
  component: DescriptionCardButtonButton,
} as Meta;

const Template: Story<DescriptionCardButtonProps> = (args) => (
  <DescriptionCardButtonButton {...args} />
);

export const Basic = Template.bind({});
