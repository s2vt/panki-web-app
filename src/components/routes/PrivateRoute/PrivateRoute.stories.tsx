import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import PrivateRoute, { PrivateRouteProps } from './PrivateRoute';

export default {
  title: 'Components/routes/PrivateRoute',
  component: PrivateRoute,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<PrivateRouteProps> = (args) => <PrivateRoute {...args} />;

export const Basic = Template.bind({});
