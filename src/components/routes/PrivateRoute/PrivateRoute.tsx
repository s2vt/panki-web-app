import { Redirect, Route, RouteProps } from 'react-router-dom';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { ROUTE_PATHS } from '@src/routes/routePaths';

export type PrivateRouteProps = RouteProps & {};

/**
 * 로그인 시에만 적용할 라우트에 사용하기 위한 라우트 컴포넌트입니다.
 *
 * 로그인이 되어있지 않은 경우 로그인 페이지로 리다이렉트 됩니다.
 */
const PrivateRoute = ({ ...rest }: PrivateRouteProps) => {
  const { isLoggedIn } = useUserSelector();

  return isLoggedIn ? (
    <Route {...rest} />
  ) : (
    <Redirect to={ROUTE_PATHS.signIn} />
  );
};

export default PrivateRoute;
