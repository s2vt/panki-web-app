import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import PublicRoute, { PublicRouteProps } from './PublicRoute';

export default {
  title: 'Components/routes/PublicRoute',
  component: PublicRoute,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<PublicRouteProps> = (args) => <PublicRoute {...args} />;

export const Basic = Template.bind({});
