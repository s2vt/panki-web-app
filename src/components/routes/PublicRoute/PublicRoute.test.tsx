import { render } from '@testing-library/react';
import user from '@src/test/fixtures/user/user';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { RootState } from '@src/reducers/rootReducer';
import { LoginState } from '@src/types/user.types';
import PublicRoute from '.';

describe('<PublicRoute />', () => {
  const setup = (initialState?: RootState) => {
    const {
      wrapper: Wrapper,
      store,
      history,
    } = prepareMockWrapper(initialState);

    const path = '/path';

    const component = () => <div>routeComponent</div>;

    const result = render(
      <Wrapper>
        <PublicRoute path={path} component={component} />
      </Wrapper>,
    );

    return { result, store, history, path };
  };

  it('should success redner route when logged out', () => {
    const initialState = {
      user: {
        loginState: LoginState.loggedOut,
        user: undefined,
      },
    } as RootState;

    const { path, history, result } = setup(initialState);

    history.push(path);

    const routeComponent = result.getByText('routeComponent');

    expect(routeComponent).toBeInTheDocument();
  });

  it('should redirect to sign in page when logged in', () => {
    const initialState = {
      user: {
        loginState: LoginState.loggedIn,
        user,
      },
    } as RootState;

    const { history } = setup(initialState);

    expect(history.location.pathname).toEqual('/');
  });
});
