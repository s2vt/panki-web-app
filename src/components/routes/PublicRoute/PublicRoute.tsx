import { Redirect, Route, RouteProps } from 'react-router-dom';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { ROUTE_PATHS } from '@src/routes/routePaths';

export type PublicRouteProps = RouteProps & {};

/**
 * 로그아웃 시에만 적용할 라우트에 사용하기 위한 라우트 컴포넌트입니다.
 *
 * 로그인이 되어있는 경우 홈 페이지로 리다이렉트 됩니다.
 */
const PublicRoute = ({ ...rest }: PublicRouteProps) => {
  const { isLoggedIn } = useUserSelector();

  return isLoggedIn ? <Redirect to={ROUTE_PATHS.root} /> : <Route {...rest} />;
};

export default PublicRoute;
