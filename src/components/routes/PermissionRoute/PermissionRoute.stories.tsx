import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import PermissionRoute, { PermissionRouteProps } from './PermissionRoute';

export default {
  title: 'Components/routes/PermissionRoute',
  component: PermissionRoute,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<PermissionRouteProps> = (args) => (
  <PermissionRoute {...args} />
);

export const Basic = Template.bind({});
