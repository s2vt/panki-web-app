import { render } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import { RootState } from '@src/reducers/rootReducer';
import { LoginState, Role, UserClass } from '@src/types/user.types';

import user from '@src/test/fixtures/user/user';
import PermissionRoute from '.';

describe('<PermissionRoute />', () => {
  const setup = (
    initialState: RootState,
    validationRoles: string[] = [],
    validationUserClasses: UserClass[] = [],
  ) => {
    const { wrapper: Wrapper, history } = prepareMockWrapper(initialState);

    const path = '/path';

    const component = () => <div>routeComponent</div>;

    const result = render(
      <Wrapper>
        <PermissionRoute
          path={path}
          component={component}
          validationRoles={validationRoles}
          validationUserClasses={validationUserClasses}
        />
      </Wrapper>,
    );

    return { history, path, result };
  };

  it('should success render route when roles valid', () => {
    const role: Role = {
      id: '1',
      description: 'role descrption',
      role: 'role1',
      roleKR: '역할1',
    };

    const initialState = {
      user: {
        user: {
          ...user,
          roles: [role],
        },
        loginState: LoginState.loggedIn,
      },
    } as RootState;

    const { history, path, result } = setup(initialState, [role.role]);

    history.push(path);

    const routeComponent = result.getByText('routeComponent');

    expect(routeComponent).toBeInTheDocument();
  });

  it('should redirect to forbidden page when roles invalid', () => {
    const role: Role = {
      id: '1',
      description: 'role descrption',
      role: 'role1',
      roleKR: '역할1',
    };

    const initialState = {
      user: {
        user: {
          ...user,
          roles: [
            {
              id: '2',
              description: 'role descrption',
              role: 'role2',
              roleKR: '역할2',
            },
          ],
        },
        loginState: LoginState.loggedIn,
      },
    } as RootState;

    const { history } = setup(initialState, [role.role]);

    expect(history.location.pathname).toEqual('/forbidden');
  });

  it('should success render route when user claases valid', () => {
    const initialState = {
      user: {
        user: {
          ...user,
          className: UserClass.admin,
        },
        loginState: LoginState.loggedIn,
      },
    } as RootState;

    const { history, path, result } = setup(
      initialState,
      [],
      [UserClass.admin],
    );

    history.push(path);

    const routeComponent = result.getByText('routeComponent');

    expect(routeComponent).toBeInTheDocument();
  });

  it('should redirect to forbidden page when user classes invalid', () => {
    const initialState = {
      user: {
        user: {
          ...user,
          className: UserClass.employee,
        },
        loginState: LoginState.loggedIn,
      },
    } as RootState;

    const { history } = setup(initialState, [], [UserClass.admin]);

    expect(history.location.pathname).toEqual('/forbidden');
  });
});
