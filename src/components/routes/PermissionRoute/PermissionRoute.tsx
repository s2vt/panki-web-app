import { Redirect, Route, RouteProps } from 'react-router-dom';
import useValidateRoles from '@src/hooks/user/useValidateRoles';
import useValidateUserClass from '@src/hooks/user/useValidateUserClass';
import { UserClass } from '@src/types/user.types';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import useUserSelector from '@src/hooks/user/useUserSelector';

export type PermissionRouteProps = RouteProps & {
  /** 검증할 역할 */
  validationRoles?: string[];
  /** 검증할 등급 */
  validationUserClasses?: UserClass[];
};

/**
 * 역할 및 등급 검증이 필요한 라우트를 위한 컴포넌트입니다.
 *
 * 유저가 가진 역할, 등급이 검증할 역할, 등급으로 검증되지 않으면
 * 권한 없음 페이지가 나타납니다.
 */
const PermissionRoute = ({
  validationUserClasses = [],
  validationRoles = [],
  ...rest
}: PermissionRouteProps) => {
  const { user } = useUserSelector();

  const isValidRoles = useValidateRoles(validationRoles, user);
  const isValidClass = useValidateUserClass(validationUserClasses, user);

  return isValidRoles && isValidClass ? (
    <Route {...rest} />
  ) : (
    <Redirect to={ROUTE_PATHS.forbidden} />
  );
};

export default PermissionRoute;
