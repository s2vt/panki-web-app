import { fireEvent, render, waitFor } from '@testing-library/react';
import faker from 'faker';
import authApi from '@src/api/authApi';
import { ResultCode } from '@src/types/http.types';
import user from '@src/test/fixtures/user/user';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import { LoginState } from '@src/types/user.types';
import SignInForm from '..';

describe('<SignInForm />', () => {
  const setup = () => {
    const { wrapper: Wrapper, store, history } = prepareWrapper();

    const signIn = jest.spyOn(authApi, 'signIn');

    const result = render(
      <Wrapper>
        <SignInForm />
      </Wrapper>,
    );

    const idInput = result.getByTestId('idInput') as HTMLInputElement;

    const passwordInput = result.getByTestId(
      'passwordInput',
    ) as HTMLInputElement;

    const signInButton = result.getByText('로그인') as HTMLButtonElement;

    const forgotPasswordAnchor = result.getByText(
      '비밀번호 찾기',
    ) as HTMLAnchorElement;

    return {
      result,
      store,
      history,
      idInput,
      passwordInput,
      signInButton,
      forgotPasswordAnchor,
      signIn,
    };
  };

  it('should render properly', () => {
    const { idInput, passwordInput, signInButton, forgotPasswordAnchor } =
      setup();

    expect(idInput).toBeInTheDocument();
    expect(passwordInput).toBeInTheDocument();
    expect(signInButton).toBeInTheDocument();
    expect(forgotPasswordAnchor).toBeInTheDocument();
  });

  it('should set user to store when success sign in', async () => {
    // given
    const { idInput, passwordInput, signInButton, signIn, store } = setup();

    signIn.mockResolvedValueOnce({
      msg: 'success',
      resultCode: ResultCode.success,
      token: faker.internet.password(),
      user,
    });

    const userId = 'test1';
    const password = '1234qwer!';

    // when
    fireEvent.change(idInput, { target: { value: userId } });
    fireEvent.change(passwordInput, { target: { value: password } });

    fireEvent.click(signInButton);

    // then
    await waitFor(() => {
      expect(store.getState().user.user).toEqual(user);
      expect(store.getState().user.loginState).toEqual(LoginState.loggedIn);
    });
  });

  it('should set error text when attempts sign in with not exists user', async () => {
    // given
    const { idInput, passwordInput, signInButton, signIn, result } = setup();

    signIn.mockRejectedValue({
      msg: 'mismatch',
      resultCode: ResultCode.notFound,
    });

    const userId = 'test1';
    const password = '1234qwer!';

    // when
    fireEvent.change(idInput, { target: { value: userId } });
    fireEvent.change(passwordInput, { target: { value: password } });

    fireEvent.click(signInButton);

    // then
    await waitFor(() => {
      const errorText = result.getByText('사용자가 존재하지 않습니다.');

      expect(errorText).toBeInTheDocument();
    });
  });

  it('should set error text when attempts sign in with mismatched password', async () => {
    // given
    const { idInput, passwordInput, signInButton, signIn, result } = setup();

    signIn.mockRejectedValue({
      msg: 'mismatch',
      resultCode: ResultCode.mismatch,
    });

    const userId = 'test1';
    const password = '1234qwer!';

    // when
    fireEvent.change(idInput, { target: { value: userId } });
    fireEvent.change(passwordInput, { target: { value: password } });

    fireEvent.click(signInButton);

    // then
    await waitFor(() => {
      const errorText = result.getByText('비밀번호가 일치하지 않습니다.');

      expect(errorText).toBeInTheDocument();
    });
  });

  it('should set error text when server error', async () => {
    // given
    const { idInput, passwordInput, signInButton, signIn, result } = setup();

    signIn.mockRejectedValue({
      msg: 'error',
      resultCode: ResultCode.error,
    });

    const userId = 'test1';
    const password = '1234qwer!';

    // when
    fireEvent.change(idInput, { target: { value: userId } });
    fireEvent.change(passwordInput, { target: { value: password } });

    fireEvent.click(signInButton);

    // then
    await waitFor(() => {
      const errorText = result.getByText('서버 에러');

      expect(errorText).toBeInTheDocument();
    });
  });

  it('should change page when clicked forgot password anchor', () => {
    // given
    const { forgotPasswordAnchor, history } = setup();

    // when
    fireEvent.click(forgotPasswordAnchor);

    // then
    expect(history.location.pathname).toEqual('/auth/forgot-password');
  });
});
