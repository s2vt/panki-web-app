import styled from 'styled-components';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import {
  LOGIN,
  PASSWORD,
  FULL_EMPLOYEE_ID,
} from '@src/constants/constantString';
import { AuthInputName, SignInPayload } from '@src/types/auth.types';
import {
  EMPLOYEE_ID_VALIDATOR,
  PASSWORD_VALIDATOR,
} from '@src/constants/validators';
import MainButton from '@src/components/common/MainButton';
import FormInput from '@src/components/form/FormInput';
import AuthFormWrapper from '../AuthFormWrapper';
import useSignIn from './hooks/useSignIn';

const SignInForm = () => {
  const {
    register,
    handleSubmit,
    setFocus,
    formState: { errors },
  } = useForm<SignInPayload>();

  const { errText, handleSignInSubmit, isLoading } = useSignIn();

  useEffect(() => {
    setFocus('userId');
  }, []);

  return (
    <AuthFormWrapper>
      <Form onSubmit={handleSubmit(handleSignInSubmit)}>
        <FormInput
          label={FULL_EMPLOYEE_ID}
          labelPadding="1.3rem"
          errorMessage={errors?.userId?.message}
          data-testid="idInput"
          {...register(AuthInputName.userId, EMPLOYEE_ID_VALIDATOR)}
        />
        <FormInput
          type="password"
          label={PASSWORD}
          labelPadding="1.3rem"
          data-testid="passwordInput"
          errorMessage={errors?.password?.message}
          {...register(AuthInputName.password, PASSWORD_VALIDATOR)}
        />
        <MainButton
          type="submit"
          label={LOGIN}
          disabled={isLoading}
          width="100%"
          height="5.2rem"
          fontSize="2rem"
          fontWeight={700}
        />
        {errText && <ErrorText>{errText}</ErrorText>}
        <LinkText to={ROUTE_PATHS.forgotPassword}>비밀번호 찾기</LinkText>
      </Form>
    </AuthFormWrapper>
  );
};

const Form = styled.form`
  width: 100%;
  height: 100%;
  padding-top: 4.275rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 3.7rem;
`;

const LinkText = styled(Link)`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 2rem;
  font-weight: 700;
  text-decoration: none;
`;

const ErrorText = styled.p`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.red};
`;

export default SignInForm;
