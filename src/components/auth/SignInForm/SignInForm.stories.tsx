import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import SignInForm from './SignInForm';

export default {
  title: 'Components/auth/SignInForm',
  component: SignInForm,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <SignInForm {...args} />;

export const Basic = Template.bind({});
