import { useState } from 'react';
import tokenStorage from '@src/storage/tokenStorage';
import {
  SERVER_ERROR,
  SIGN_IN_MISMATCH_ERROR,
  USER_NOT_FOUND_ERROR,
} from '@src/constants/constantString';
import useUserAction from '@src/hooks/user/useUserAction';
import { SignInPayload } from '@src/types/auth.types';
import authApi, { SignInResponse } from '@src/api/authApi';
import { ResultCode } from '@src/types/http.types';
import { useMutation } from 'react-query';

const useSignIn = () => {
  const [errText, setErrText] = useState<string>();
  const { setUser } = useUserAction();

  const handleSignInSubmit = (signInPayload: SignInPayload) => {
    const { userId, password } = signInPayload;

    mutate({ userId, password });
  };

  const handleSignInSuccess = (signInPayload: SignInResponse) => {
    const { token, user } = signInPayload;

    if (user !== undefined && token !== undefined) {
      const prefixRemovedToken = token.split(' ')[1];
      tokenStorage.setToken(prefixRemovedToken);
      setUser(user);
    }
  };

  const handleSignInError = (signInResponse: SignInResponse) => {
    const { resultCode } = signInResponse;

    switch (resultCode) {
      case ResultCode.mismatch:
        setErrText(SIGN_IN_MISMATCH_ERROR);
        break;
      case ResultCode.notFound:
        setErrText(USER_NOT_FOUND_ERROR);
        break;
      default:
        setErrText(SERVER_ERROR);
    }
  };

  const { mutate, isLoading } = useMutation(
    (signInPayload: SignInPayload) => authApi.signIn(signInPayload),
    {
      onSuccess: handleSignInSuccess,
      onError: handleSignInError,
    },
  );

  return { handleSignInSubmit, isLoading, errText };
};

export default useSignIn;
