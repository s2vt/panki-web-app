import { fireEvent, render, waitFor } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import authApi from '@src/api/authApi';
import SmsVerifyForm from '../SmsVerifyForm';

describe('<SmsVerifyForm />', () => {
  const setup = () => {
    const { wrapper: Wrapper, store } = prepareMockWrapper(rootState);

    const setForgotPasswordPageMode = jest.fn();
    const setToken = jest.fn();

    const verifySmsPasswordCode = jest.spyOn(authApi, 'verifySmsPasswordCode');
    const sendSmsPasswordCode = jest.spyOn(authApi, 'sendSmsPasswordCode');
    const refreshSmsPasswordCode = jest.spyOn(
      authApi,
      'refreshSmsPasswordCode',
    );

    const phone = '010-1234-5678';

    const result = render(
      <Wrapper>
        <SmsVerifyForm
          setForgotPasswordPageMode={setForgotPasswordPageMode}
          setToken={setToken}
          phone={phone}
        />
      </Wrapper>,
    );

    const phoneInput = result.getByDisplayValue(phone) as HTMLInputElement;

    const verifyKeyInput = result.getByPlaceholderText(
      '숫자 6자리 입력',
    ) as HTMLInputElement;

    const resendButton = result.getByText('재전송') as HTMLButtonElement;

    const countDown = result.getByText('03 : 00');

    const timeExtensionButton = result.getByText(
      '시간연장',
    ) as HTMLButtonElement;

    const timeExtensionDescription = result.getByText(
      '인증번호 입력시간을 연장하려면 시간연장 누르기',
    );

    const verifyKeyDescription = result.getByText(
      '3분 이내로 인증번호(6자리)를 입력해주세요.',
    );

    const submitButton = result.getByTestId(
      'submit-button',
    ) as HTMLButtonElement;

    return {
      result,
      store,
      setToken,
      setForgotPasswordPageMode,
      verifySmsPasswordCode,
      sendSmsPasswordCode,
      refreshSmsPasswordCode,
      phone,
      phoneInput,
      verifyKeyInput,
      resendButton,
      countDown,
      timeExtensionButton,
      timeExtensionDescription,
      verifyKeyDescription,
      submitButton,
    };
  };

  it('should render properly', () => {
    const {
      phoneInput,
      verifyKeyInput,
      resendButton,
      countDown,
      timeExtensionButton,
      timeExtensionDescription,
      verifyKeyDescription,
      submitButton,
    } = setup();

    expect(phoneInput).toBeInTheDocument();
    expect(verifyKeyInput).toBeInTheDocument();
    expect(resendButton).toBeInTheDocument();
    expect(countDown).toBeInTheDocument();
    expect(timeExtensionButton).toBeInTheDocument();
    expect(submitButton).toBeInTheDocument();
    expect(timeExtensionDescription).toBeInTheDocument();
    expect(verifyKeyDescription).toBeInTheDocument();
  });

  it('should change page when success verification', async () => {
    // given
    const { verifyKeyInput, submitButton, verifySmsPasswordCode, phone } =
      setup();

    const verifyKey = '123456';

    // when
    fireEvent.change(verifyKeyInput, { target: { value: verifyKey } });

    fireEvent.click(submitButton);

    // then
    await waitFor(() => {
      expect(verifySmsPasswordCode).toBeCalledWith({ phone, verifyKey });
    });
  });

  it('should show error when verify key is empty', async () => {
    // given
    const { submitButton, store } = setup();

    // when
    fireEvent.click(submitButton);

    await waitFor(async () => {
      await new Promise((resolve) => setTimeout(resolve, 1));

      // then
      expect(store.getActions()[0].meta.arg.text).toEqual(
        '인증번호를 입력해주세요',
      );
    });
  });

  it('should success resend password code', async () => {
    // given
    const { resendButton, sendSmsPasswordCode, phone, result } = setup();

    // when
    fireEvent.click(resendButton);

    // then
    await waitFor(() => {
      expect(sendSmsPasswordCode).toBeCalledWith(phone);
      expect(result.getByText('03 : 00')).toBeInTheDocument();
    });
  });

  it('should success refresh password code', async () => {
    // given
    const { timeExtensionButton, refreshSmsPasswordCode, phone, result } =
      setup();

    // when
    fireEvent.click(timeExtensionButton);

    // then
    await waitFor(() => {
      expect(refreshSmsPasswordCode).toBeCalledWith(phone);
      expect(result.getByText('03 : 00')).toBeInTheDocument();
    });
  });

  it('should change submit button to disable when count over', async () => {
    // given
    jest.useFakeTimers();

    const { submitButton, result } = setup();

    await waitFor(async () => {
      // when
      await new Promise((resolve) => setTimeout(resolve, 3000));
      jest.runAllTimers();

      // then
      expect(result.getByText('00 : 00')).toBeInTheDocument();
      expect(submitButton.disabled).toEqual(true);
    });
  });
});
