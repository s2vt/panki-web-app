import { render } from '@testing-library/react';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import ForgotPassword from '..';

describe('<ForgotPassword />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareWrapper();

    return render(
      <Wrapper>
        <ForgotPassword />
      </Wrapper>,
    );
  };

  it('should render properly', () => {
    setup();
  });

  it('should description text when forgotPasswordPageMode is not resetPassword', () => {
    const { getByText } = setup();

    const descriptionText1 = getByText('아래에 입력한 휴대폰 번호로');
    const descriptionText2 = getByText('비밀번호 인증코드가 전송됩니다.');

    expect(descriptionText1).toBeInTheDocument();
    expect(descriptionText2).toBeInTheDocument();
  });
});
