import { fireEvent, render, waitFor } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import SendSmsForm from '../SendSmsForm';

describe('<SendSmsForm />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareMockWrapper();

    const setForgotPasswordPageMode = jest.fn();
    const setPhone = jest.fn();
    const sendSmsPasswordCode = jest.spyOn(authApi, 'sendSmsPasswordCode');

    const result = render(
      <Wrapper>
        <SendSmsForm
          setForgotPasswordPageMode={setForgotPasswordPageMode}
          setPhone={setPhone}
        />
      </Wrapper>,
    );

    const phoneInput = result.getByPlaceholderText(
      '010-0000-0000',
    ) as HTMLInputElement;

    const submitButton = result.getByText('전 송') as HTMLButtonElement;

    return {
      result,
      phoneInput,
      submitButton,
      setForgotPasswordPageMode,
      setPhone,
      sendSmsPasswordCode,
    };
  };

  it('should render properly', () => {
    const { phoneInput, submitButton } = setup();

    expect(phoneInput).toBeInTheDocument();
    expect(submitButton).toBeInTheDocument();
  });

  it('should change page to verify page when success send sms', async () => {
    // given
    const {
      phoneInput,
      sendSmsPasswordCode,
      submitButton,
      setForgotPasswordPageMode,
    } = setup();

    sendSmsPasswordCode.mockResolvedValue({
      resultCode: ResultCode.success,
      msg: 'success',
    });

    const phone = '010-1234-5678';

    // when
    fireEvent.change(phoneInput, { target: { value: phone } });

    fireEvent.click(submitButton);

    // then
    await waitFor(() => {
      expect(sendSmsPasswordCode).toBeCalledWith(phone);
      expect(setForgotPasswordPageMode).toBeCalledWith('smsVerify');
    });
  });

  it('should show error when user not found', async () => {
    // given
    const { result, phoneInput, submitButton, sendSmsPasswordCode } = setup();

    sendSmsPasswordCode.mockRejectedValue(
      new HttpError(HttpErrorStatus.BadRequest, ResultCode.invalidRequest),
    );

    const phone = '010-1234-5678';

    // when
    fireEvent.change(phoneInput, { target: { value: phone } });

    fireEvent.click(submitButton);

    // then
    const errText = await result.findByText('사용자가 존재하지 않습니다.');

    expect(errText).toBeInTheDocument();
  });

  it('should show error when server error', async () => {
    // given
    const { result, phoneInput, submitButton, sendSmsPasswordCode } = setup();

    sendSmsPasswordCode.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    const phone = '010-1234-5678';

    // when
    fireEvent.change(phoneInput, { target: { value: phone } });

    fireEvent.click(submitButton);

    // then
    const errText = await result.findByText('서버 에러');

    expect(errText).toBeInTheDocument();
  });
});
