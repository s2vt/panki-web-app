import { fireEvent, render, waitFor } from '@testing-library/react';
import faker from 'faker';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';
import { HttpErrorStatus, ResultCode } from '@src/types/http.types';
import PasswordResetForm from '../PasswordResetForm';

describe('<PasswordResetForm />', () => {
  const setup = () => {
    const { wrapper: Wrapper, store, history } = prepareMockWrapper();

    const phone = '010-1234-5678';
    const token = faker.internet.password();

    const updatePasswordWithSms = jest.spyOn(authApi, 'updatePasswordWithSms');

    const result = render(
      <Wrapper>
        <PasswordResetForm phone={phone} token={token} />
      </Wrapper>,
    );

    const passwordInput = result.getByTestId(
      'password-input',
    ) as HTMLInputElement;

    const passwordCheckInput = result.getByTestId(
      'password-check-input',
    ) as HTMLInputElement;

    const submitButton = result.getByTestId(
      'submit-button',
    ) as HTMLButtonElement;

    return {
      store,
      history,
      result,
      phone,
      token,
      updatePasswordWithSms,
      passwordInput,
      passwordCheckInput,
      submitButton,
    };
  };

  it('should render properly', () => {
    const { passwordInput, passwordCheckInput, submitButton } = setup();

    expect(passwordInput).toBeInTheDocument();
    expect(passwordCheckInput).toBeInTheDocument();
    expect(submitButton).toBeInTheDocument();
  });

  it('should success call reset password api', async () => {
    // given
    const {
      passwordInput,
      passwordCheckInput,
      submitButton,
      phone,
      token,
      updatePasswordWithSms,
      history,
    } = setup();

    const password = '1234qwer!';

    // when
    fireEvent.change(passwordInput, { target: { value: password } });
    fireEvent.change(passwordCheckInput, { target: { value: password } });

    fireEvent.click(submitButton);

    // then
    await waitFor(() => {
      expect(updatePasswordWithSms).toBeCalledWith({
        phone,
        token,
        password,
      });
      expect(history.location.pathname).toEqual('/auth/sign-in');
    });
  });

  it('should show error when failed password check', async () => {
    // given
    const { passwordInput, passwordCheckInput, submitButton, store } = setup();

    const password = '1234qwer!';
    const passwordCheck = '1234qwer@';

    // when
    fireEvent.change(passwordInput, { target: { value: password } });
    fireEvent.change(passwordCheckInput, { target: { value: passwordCheck } });

    fireEvent.click(submitButton);

    // then
    await waitFor(async () => {
      await new Promise((resolve) => setTimeout(resolve, 1));

      expect(store.getActions()[0].meta.arg.text).toEqual(
        '비밀번호를 확인하세요',
      );
    });
  });

  it('should show error when failed password update', async () => {
    // given
    const {
      passwordInput,
      passwordCheckInput,
      submitButton,
      updatePasswordWithSms,
      store,
    } = setup();

    updatePasswordWithSms.mockRejectedValue(
      new HttpError(HttpErrorStatus.InternalServerError, ResultCode.error),
    );

    const password = '1234qwer!';

    // when
    fireEvent.change(passwordInput, { target: { value: password } });
    fireEvent.change(passwordCheckInput, { target: { value: password } });

    fireEvent.click(submitButton);

    // then
    await waitFor(() => {
      expect(store.getActions()[0].meta.arg.text).toEqual('서버 에러');
    });
  });
});
