import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { useEffect } from 'react';
import {
  NEW_PASSWORD,
  PASSWORD_CHECK,
  RESET_PASSWORD,
} from '@src/constants/constantString';
import { AuthInputName, PasswordResetPayload } from '@src/types/auth.types';
import { PASSWORD_VALIDATOR } from '@src/constants/validators';
import FormInput from '@src/components/form/FormInput';
import MainButton from '@src/components/common/MainButton';
import usePasswordReset from './hooks/usePasswordReset';

export type PasswordResetFormProps = {
  phone: string;
  token: string;
};

const PasswordResetForm = ({ phone, token }: PasswordResetFormProps) => {
  const {
    register,
    handleSubmit,
    setFocus,
    formState: { isSubmitting },
  } = useForm<PasswordResetPayload>({
    defaultValues: {
      phone,
      token,
      password: '',
      passwordCheck: '',
    },
  });

  const handleUpdatePasswordSubmit = usePasswordReset();

  useEffect(() => {
    setFocus('password');
  }, []);

  return (
    <Form onSubmit={handleSubmit(handleUpdatePasswordSubmit)}>
      <FormSection>
        <FormInput
          type="password"
          label={NEW_PASSWORD}
          labelPadding="1.3rem"
          data-testid="password-input"
          {...register(AuthInputName.password, PASSWORD_VALIDATOR)}
        />
        <FormInput
          type="password"
          label={PASSWORD_CHECK}
          labelPadding="1.3rem"
          data-testid="password-check-input"
          {...register(AuthInputName.passwordCheck, PASSWORD_VALIDATOR)}
        />
      </FormSection>
      <MainButton
        type="submit"
        label={RESET_PASSWORD}
        disabled={isSubmitting}
        width="100%"
        height="5.2rem"
        fontSize="2rem"
        fontWeight={700}
        data-testid="submit-button"
      />
    </Form>
  );
};

const Form = styled.form`
  width: 100%;
  height: 100%;
  margin-top: 6.1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const FormSection = styled.div`
  width: 100%;
  margin-bottom: 5.331rem;
  display: flex;
  flex-direction: column;
  gap: 3.8rem;
`;

export default PasswordResetForm;
