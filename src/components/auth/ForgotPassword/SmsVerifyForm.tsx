import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import {
  CELL_PHONE_NUMBER,
  RESEND,
  TIME_EXTENSION,
  SIX_DIGITS_NUMBER_PLACEHOLDER,
  CONFIRM,
} from '@src/constants/constantString';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import useMask from '@src/hooks/common/useMask';
import { AuthInputName, SmsVerifyPayload } from '@src/types/auth.types';
import {
  PHONE_VALIDATOR,
  VERIFY_KEY_VALIDATOR,
} from '@src/constants/validators';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import FormInput from '@src/components/form/FormInput';
import MainButton from '@src/components/common/MainButton';
import AlertModal from '@src/components/common/AlertModal';
import Counter, { CounterHandler } from '@src/components/common/Counter';
import ModalWrapper from '@src/components/base/ModalWrapper';
import { ForgotPasswordMode } from '.';
import useSmsVerify from './hooks/useSmsVerify';

export type SmsVerifyFormProps = {
  setForgotPasswordPageMode: React.Dispatch<
    React.SetStateAction<ForgotPasswordMode>
  >;
  setToken: React.Dispatch<React.SetStateAction<string>>;
  phone: string;
};

const VERIFICATION_COUNT = 180;

const SmsVerifyForm = ({
  setForgotPasswordPageMode,
  setToken,
  phone,
}: SmsVerifyFormProps) => {
  const [isCountOver, setIsCountOver] = useState(false);
  const countRef = useRef<CounterHandler>(null);

  const { maskNumber } = useMask();

  const {
    register,
    handleSubmit,
    setValue,
    setFocus,
    formState: { errors, isSubmitting, submitCount },
  } = useForm<SmsVerifyPayload>({ defaultValues: { phone, verifyKey: '' } });

  const { openAlertDialog } = useAlertDialogAction();

  const handleSmsVerifySuccess = (token: string) => {
    setToken(token);
    setForgotPasswordPageMode('resetPassword');
  };

  const handleRefreshSuccess = () => {
    countRef.current?.reset();
    setIsCountOver(false);
  };

  const {
    isAlertModalOpen,
    alertErrorText,
    closeAlertModal,
    handleRefreshSmsClick,
    handleResendSmsClick,
    handleVerifySmsSubmit,
  } = useSmsVerify({
    phone,
    onRefreshSuccess: handleRefreshSuccess,
    onSmsVerifySuccess: handleSmsVerifySuccess,
  });

  useEffect(() => {
    setFocus('verifyKey');
  }, []);

  useEffect(() => {
    if (submitCount > 0 && Object.keys(errors).length > 0) {
      openAlertDialog({
        text: '인증번호를 입력해주세요',
        position: 'rightTop',
      });
    }
  }, [submitCount]);

  const handleVerifyKeyChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setValue(AuthInputName.verifyKey, maskNumber(e.target.value));
    },
    [setValue, maskNumber],
  );

  const handleCountOver = () => {
    setIsCountOver(true);
  };

  return (
    <>
      <Form onSubmit={handleSubmit(handleVerifySmsSubmit)}>
        <PhoneInput
          maxLength={13}
          placeholder={CELL_PHONE_NUMBER}
          readOnly
          {...register(AuthInputName.phone, PHONE_VALIDATOR)}
        />
        <VerifySection>
          <VerifyInput
            maxLength={6}
            placeholder={SIX_DIGITS_NUMBER_PLACEHOLDER}
            {...register(AuthInputName.verifyKey, VERIFY_KEY_VALIDATOR)}
            onChange={handleVerifyKeyChange}
          />
          <ResendButton
            type="button"
            label={RESEND}
            onClick={handleResendSmsClick}
            backgroundColor="white"
            labelColor="primary"
          />
        </VerifySection>
        <CounterSection>
          <Counter
            ref={countRef}
            initialCount={VERIFICATION_COUNT}
            onCountOver={handleCountOver}
          />
          <CounterText>남음</CounterText>
          <MainButton
            type="button"
            label={TIME_EXTENSION}
            onClick={handleRefreshSmsClick}
            width="8.1rem"
            height="2.6rem"
            fontSize="1.6rem"
            fontWeight={600}
          />
        </CounterSection>
        <CounterDescription>
          <li>인증번호 입력시간을 연장하려면 시간연장 누르기</li>
          <li>3분 이내로 인증번호(6자리)를 입력해주세요.</li>
        </CounterDescription>
        <MainButton
          type="submit"
          label={addWhiteSpacesBetweenCharacters(CONFIRM)}
          disabled={isSubmitting || isCountOver}
          width="100%"
          height="5.2rem"
          fontSize="2rem"
          fontWeight={700}
          data-testid="submit-button"
        />
      </Form>
      <ModalWrapper isOpen={isAlertModalOpen}>
        <AlertModal text={alertErrorText} onClose={closeAlertModal} />
      </ModalWrapper>
    </>
  );
};

const Form = styled.form`
  width: 100%;
  height: 100%;
  margin-top: 1.5rem;
  display: flex;
  flex-direction: column;
`;

const PhoneInput = styled(FormInput)`
  height: 6.5rem;
  color: ${({ theme }) => theme.colors.inactive};

  input {
    height: 6.5rem;
    border-bottom: none;
  }
`;

const VerifySection = styled.div`
  width: 100%;
  height: 6.5rem;
  display: flex;
`;

const VerifyInput = styled(FormInput)`
  flex: 1;

  input {
    height: 6.5rem;
  }
`;

const ResendButton = styled(MainButton)`
  min-width: 14.8rem;
  height: 100%;
  font-size: 1.8rem;
  font-weight: 700;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-left: none;
`;

const CounterSection = styled.div`
  height: 3.1rem;
  margin: 3rem 0 1.3rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
`;

const CounterText = styled.p`
  margin: 0 1.5rem 0 0.7rem;
  font-size: 2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const CounterDescription = styled.ul`
  margin-bottom: 2.3rem;
  line-height: 3.1rem;
  font-size: 1.8rem;
  font-weight: 400;
  color: rgba(99, 99, 99, 1);

  li {
    list-style: disc;
  }
`;

export default SmsVerifyForm;
