import React, { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import {
  CELL_PHONE_NUMBER,
  PHONE_NUMBER_PLACEHOLDER,
  SEND,
} from '@src/constants/constantString';
import useMask from '@src/hooks/common/useMask';
import { AuthInputName, SendSmsPayload } from '@src/types/auth.types';
import { PHONE_VALIDATOR } from '@src/constants/validators';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import FormInput from '@src/components/form/FormInput';
import MainButton from '@src/components/common/MainButton';
import { Link } from 'react-router-dom';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { ForgotPasswordMode } from '.';
import useSendSms from './hooks/useSendSms';

export type SendSmsFormProps = {
  setForgotPasswordPageMode: React.Dispatch<
    React.SetStateAction<ForgotPasswordMode>
  >;
  setPhone: React.Dispatch<React.SetStateAction<string>>;
};

const SendSmsForm = ({
  setForgotPasswordPageMode,
  setPhone,
}: SendSmsFormProps) => {
  const { maskPhoneNumber } = useMask();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors, isSubmitting },
    setFocus,
    trigger,
  } = useForm<SendSmsPayload>({
    defaultValues: {
      phone: '',
    },
  });

  const handleSendSmsSuccess = () => {
    setForgotPasswordPageMode('smsVerify');
  };

  const { handleSendSmsSubmit, errText } = useSendSms({
    onSendSmsSuccess: handleSendSmsSuccess,
    setPhone,
  });

  useEffect(() => {
    setFocus('phone');
  }, []);

  const handlePhoneChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setValue(AuthInputName.phone, maskPhoneNumber(e.target.value));
      trigger();
    },
    [setValue, maskPhoneNumber, trigger],
  );

  return (
    <Form onSubmit={handleSubmit(handleSendSmsSubmit)}>
      <FormInput
        label={CELL_PHONE_NUMBER}
        labelPadding="1.3rem"
        maxLength={13}
        errorMessage={errors?.phone?.message}
        placeholder={PHONE_NUMBER_PLACEHOLDER}
        {...register(AuthInputName.phone, PHONE_VALIDATOR)}
        onChange={handlePhoneChange}
      />
      {errText !== '' && <ErrorText>{errText}</ErrorText>}
      <StyledMainButton
        type="submit"
        label={addWhiteSpacesBetweenCharacters(SEND)}
        disabled={isSubmitting}
        width="100%"
        height="5.2rem"
        fontSize="2rem"
        fontWeight={700}
      />
      <LinkText to={ROUTE_PATHS.signIn}>로그인으로 돌아가기</LinkText>
    </Form>
  );
};

const Form = styled.form`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ErrorText = styled.p`
  margin-top: 4rem;
  color: ${({ theme }) => theme.colors.red};
  font-size: 1.4rem;
`;

const StyledMainButton = styled(MainButton)`
  margin: 3rem 0 2.4rem;
`;

const LinkText = styled(Link)`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 2rem;
  font-weight: 700;
  text-decoration: none;
`;

export default SendSmsForm;
