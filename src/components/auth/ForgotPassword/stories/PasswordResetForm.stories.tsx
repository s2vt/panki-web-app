import { Meta, Story } from '@storybook/react';
import faker from 'faker';
import PasswordResetForm, {
  PasswordResetFormProps,
} from '../PasswordResetForm';

export default {
  title: 'Components/Auth/ForgotPassword/PasswordResetForm',
  component: PasswordResetForm,
} as Meta;

const Template: Story<PasswordResetFormProps> = (args) => (
  <PasswordResetForm {...args} />
);

export const Basic = Template.bind({});

Basic.args = {
  phone: faker.phone.phoneNumber(),
  token: faker.internet.password(),
};
