import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import ForgotPassword, { ForgotPasswordProps } from '../ForgotPassword';

export default {
  title: 'Components/Auth/ForgotPassword',
  component: ForgotPassword,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<ForgotPasswordProps> = (args) => (
  <ForgotPassword {...args} />
);

export const Basic = Template.bind({});
