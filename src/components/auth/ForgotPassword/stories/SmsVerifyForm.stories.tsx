import { Meta, Story } from '@storybook/react';
import SmsVerifyForm, { SmsVerifyFormProps } from '../SmsVerifyForm';

export default {
  title: 'Components/auth/ForgotPassword/SmsVerifyForm',
  component: SmsVerifyForm,
} as Meta;

const Template: Story<SmsVerifyFormProps> = (args) => (
  <SmsVerifyForm {...args} />
);

export const Basic = Template.bind({});
