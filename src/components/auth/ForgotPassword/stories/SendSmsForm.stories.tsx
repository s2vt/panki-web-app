import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import SendSmsForm, { SendSmsFormProps } from '../SendSmsForm';

export default {
  title: 'Components/Auth/ForgotPassword/SendSmsForm',
  component: SendSmsForm,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<SendSmsFormProps> = (args) => <SendSmsForm {...args} />;

export const Basic = Template.bind({});
