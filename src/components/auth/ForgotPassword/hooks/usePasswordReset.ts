import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';
import useAlertDialogAction from '@src/hooks/common/useAlertDialogAction';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { PasswordResetPayload } from '@src/types/auth.types';
import { useCallback } from 'react';
import { useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';

const usePasswordReset = () => {
  const history = useHistory();
  const { openAlertDialog } = useAlertDialogAction();

  const handleUpdatePasswordSuccess = () => {
    history.push(ROUTE_PATHS.signIn);
  };

  const handleUpdatePasswordError = (error: HttpError) => {
    openAlertDialog({ text: error.message, position: 'rightTop' });
  };

  const { mutate: updatePasswordMutate } = useMutation(
    (updatePasswordPayload: Omit<PasswordResetPayload, 'passwordCheck'>) =>
      authApi.updatePasswordWithSms(updatePasswordPayload),
    {
      onSuccess: handleUpdatePasswordSuccess,
      onError: handleUpdatePasswordError,
    },
  );

  const handleUpdatePasswordSubmit = useCallback(
    (passwordResetPayload: PasswordResetPayload) => {
      const { phone, token, password, passwordCheck } = passwordResetPayload;

      if (password !== passwordCheck) {
        openAlertDialog({
          text: '비밀번호를 확인하세요',
          position: 'rightTop',
        });
        return;
      }

      updatePasswordMutate({ phone, token, password });
    },
    [openAlertDialog, updatePasswordMutate],
  );

  return handleUpdatePasswordSubmit;
};

export default usePasswordReset;
