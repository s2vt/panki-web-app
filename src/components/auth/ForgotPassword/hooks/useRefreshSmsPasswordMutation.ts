import { useMutation, UseMutationOptions } from 'react-query';
import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';

const useRefreshSmsPasswordMutation = (
  options?: UseMutationOptions<unknown, HttpError, string>,
) =>
  useMutation(
    (phone: string) => authApi.refreshSmsPasswordCode(phone),
    options,
  );

export default useRefreshSmsPasswordMutation;
