import authApi from '@src/api/authApi';
import HttpError from '@src/api/model/HttpError';
import {
  SERVER_ERROR,
  USER_NOT_FOUND_ERROR,
} from '@src/constants/constantString';
import { SendSmsPayload } from '@src/types/auth.types';
import { HttpErrorStatus } from '@src/types/http.types';
import { useState } from 'react';
import { useMutation } from 'react-query';

type UseSendSmsProps = {
  onSendSmsSuccess: () => void;
  setPhone: React.Dispatch<React.SetStateAction<string>>;
};

const useSendSms = ({ onSendSmsSuccess, setPhone }: UseSendSmsProps) => {
  const [errText, setErrText] = useState<string>('');

  const handleSendSmsError = (error: HttpError) => {
    switch (error.status) {
      case HttpErrorStatus.BadRequest:
        setErrText(USER_NOT_FOUND_ERROR);
        break;
      default:
        setErrText(SERVER_ERROR);
    }
  };

  const { mutate } = useMutation(
    (phone: string) => authApi.sendSmsPasswordCode(phone),
    {
      onSuccess: onSendSmsSuccess,
      onError: handleSendSmsError,
    },
  );

  const handleSendSmsSubmit = (sendSmsPayload: SendSmsPayload) => {
    const { phone } = sendSmsPayload;
    setPhone(phone);

    mutate(phone);
  };

  return { handleSendSmsSubmit, errText };
};

export default useSendSms;
