import authApi from '@src/api/authApi';
import { SmsVerifyPayload } from '@src/types/auth.types';
import { useCallback, useState } from 'react';
import { useMutation } from 'react-query';

type UseSmsVerifyProps = {
  phone: string;
  onSmsVerifySuccess: (token: string) => void;
  onRefreshSuccess: () => void;
};

const useSmsVerify = ({
  phone,
  onSmsVerifySuccess,
  onRefreshSuccess,
}: UseSmsVerifyProps) => {
  const [isAlertModalOpen, setIsAlertModalOpen] = useState(false);
  const [alertErrorText, setAlertErrorText] = useState('');

  const openAlertModal = useCallback(
    () => setIsAlertModalOpen(true),
    [setIsAlertModalOpen],
  );

  const closeAlertModal = useCallback(
    () => setIsAlertModalOpen(false),
    [setIsAlertModalOpen],
  );

  const handleSendSmsError = () => {
    setAlertErrorText('전송이 실패하였습니다.');
    openAlertModal();
  };

  const handleVerifySmsError = () => {
    setAlertErrorText(`입력하신 정보가 일치하지 않습니다.\n다시 시도해주세요.`);
    openAlertModal();
  };

  const { mutate: sendSmsPasswordCodeMutate } = useMutation(
    (phone: string) => authApi.sendSmsPasswordCode(phone),
    {
      onSuccess: onRefreshSuccess,
      onError: handleSendSmsError,
    },
  );

  const { mutate: refreshSmsPasswordCodeMutate } = useMutation(
    (phone: string) => authApi.refreshSmsPasswordCode(phone),
    {
      onSuccess: onRefreshSuccess,
      onError: handleVerifySmsError,
    },
  );

  const { mutate: verifySmsMutate } = useMutation(
    (smsVerifyPayload: SmsVerifyPayload) =>
      authApi.verifySmsPasswordCode(smsVerifyPayload),
    {
      onSuccess: onSmsVerifySuccess,
      onError: handleVerifySmsError,
    },
  );

  const handleVerifySmsSubmit = useCallback(
    (smsVerifyPayload: SmsVerifyPayload) => {
      const { verifyKey } = smsVerifyPayload;

      verifySmsMutate({ phone, verifyKey });
    },
    [verifySmsMutate, phone],
  );

  const handleResendSmsClick = useCallback(() => {
    sendSmsPasswordCodeMutate(phone);
  }, [sendSmsPasswordCodeMutate, phone]);

  const handleRefreshSmsClick = useCallback(() => {
    refreshSmsPasswordCodeMutate(phone);
  }, [refreshSmsPasswordCodeMutate, phone]);

  return {
    isAlertModalOpen,
    alertErrorText,
    closeAlertModal,
    handleVerifySmsSubmit,
    handleResendSmsClick,
    handleRefreshSmsClick,
  };
};

export default useSmsVerify;
