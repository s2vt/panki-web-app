import { debugAssert } from '@src/utils/commonUtil';
import { useState } from 'react';
import styled from 'styled-components';
import AuthFormWrapper from '../AuthFormWrapper';
import PasswordResetForm from './PasswordResetForm';
import SendSmsForm from './SendSmsForm';
import SmsVerifyForm from './SmsVerifyForm';

export type ForgotPasswordProps = {};

export type ForgotPasswordMode = 'sendSms' | 'smsVerify' | 'resetPassword';

const ForgotPassword = () => {
  const [forgotPasswordPageMode, setForgotPasswordPageMode] =
    useState<ForgotPasswordMode>('sendSms');
  const [phone, setPhone] = useState<string>('');
  const [token, setToken] = useState<string>('');

  return (
    <AuthFormWrapper>
      {forgotPasswordPageMode !== 'resetPassword' && (
        <DescriptionSection>
          <DescriptionText>아래에 입력한 휴대폰 번호로</DescriptionText>
          <DescriptionText>비밀번호 인증코드가 전송됩니다.</DescriptionText>
        </DescriptionSection>
      )}
      {(() => {
        switch (forgotPasswordPageMode) {
          case 'sendSms':
            return (
              <SendSmsForm
                setForgotPasswordPageMode={setForgotPasswordPageMode}
                setPhone={setPhone}
              />
            );
          case 'smsVerify':
            return (
              <SmsVerifyForm
                setForgotPasswordPageMode={setForgotPasswordPageMode}
                setToken={setToken}
                phone={phone}
              />
            );
          case 'resetPassword':
            return <PasswordResetForm phone={phone} token={token} />;
          default:
            debugAssert(
              false,
              `${forgotPasswordPageMode} is not included in ForgotPasswordMode`,
            );
            return null;
        }
      })()}
    </AuthFormWrapper>
  );
};

const DescriptionSection = styled.div`
  margin: 2.1rem 0 1.1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const DescriptionText = styled.p`
  font-size: 2.2rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary};
  line-height: 3.74rem;
`;

export default ForgotPassword;
