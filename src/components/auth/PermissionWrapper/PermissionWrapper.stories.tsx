import { Meta, Story } from '@storybook/react';
import PermissionWrapper, { PermissionWrapperProps } from './PermissionWrapper';

export default {
  title: 'Components/auth/PermissionWrapper',
  component: PermissionWrapper,
} as Meta;

const Template: Story<PermissionWrapperProps> = (args) => (
  <PermissionWrapper {...args} />
);

export const Basic = Template.bind({});
