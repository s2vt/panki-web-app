import useUserSelector from '@src/hooks/user/useUserSelector';
import useValidateRoles from '@src/hooks/user/useValidateRoles';
import useValidateUserClass from '@src/hooks/user/useValidateUserClass';
import { UserClass } from '@src/types/user.types';

export type PermissionWrapperProps = {
  /** 검증할 역할 */
  validationRoles?: string[];
  /** 검증할 등급 */
  validationUserClasses?: UserClass[];
  children?: React.ReactNode;
};

/**
 * 역할 및 등급 검증이 필요한 컴포넌트 랜더링을 위한 컴포넌트입니다.
 *
 * 유저가 가진 역할, 등급이 검증할 역할, 등급으로 검증되지 않으면
 * 내부 컴포넌트가 랜더링 되지 않습니다
 */
const PermissionWrapper = ({
  validationRoles = [],
  validationUserClasses = [],
  children,
}: PermissionWrapperProps) => {
  const { user } = useUserSelector();

  const isValidRoles = useValidateRoles(validationRoles, user);
  const isValidClass = useValidateUserClass(validationUserClasses, user);

  if (!isValidRoles || !isValidClass) {
    return null;
  }

  return <>{children}</>;
};

export default PermissionWrapper;
