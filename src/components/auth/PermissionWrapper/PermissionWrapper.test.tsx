import { render } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import rootState from '@src/test/fixtures/reducers/rootState';
import PermissionWrapper from '.';
import { PermissionWrapperProps } from './PermissionWrapper';

describe('<PermissionWrapper />', () => {
  const setup = (props: PermissionWrapperProps) => {
    const { wrapper: Wrapper, store } = prepareMockWrapper(rootState);

    const result = render(
      <Wrapper>
        <PermissionWrapper {...props} />
      </Wrapper>,
    );

    return { result, store };
  };

  it('should render properly', () => {
    setup({});
  });
});
