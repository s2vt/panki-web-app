import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '@src/styles/theme';
import AuthFormWrapper from '.';

describe('<AuthFormWrapper />', () => {
  const setup = () =>
    render(
      <ThemeProvider theme={theme}>
        <AuthFormWrapper />
      </ThemeProvider>,
    );

  it('should render properly', () => {
    setup();
  });

  it('should render logo icon', () => {
    const { container } = setup();

    const svgIcon = container.querySelector('svg');

    expect(svgIcon?.textContent).toEqual('panki_small_logo.svg');
  });
});
