import { Meta, Story } from '@storybook/react';
import AuthFormWrapper, { AuthFormWrapperProps } from './AuthFormWrapper';

export default {
  title: 'Components/auth/AuthFormWrapper',
  component: AuthFormWrapper,
} as Meta;

const Template: Story<AuthFormWrapperProps> = (args) => (
  <AuthFormWrapper {...args} />
);

export const Basic = Template.bind({});
