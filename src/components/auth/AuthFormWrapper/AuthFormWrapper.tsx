import Icon from '@src/components/common/Icon';
import React, { memo } from 'react';
import styled from 'styled-components';

export type AuthFormWrapperProps = { children?: React.ReactNode };

const AuthFormWrapper = ({ children }: AuthFormWrapperProps) => (
  <Wrapper>
    <FormSection>
      <SmallLogo icon="pankiSmallLogo" />
      {children}
    </FormSection>
  </Wrapper>
);

const Wrapper = styled.div`
  width: 100%;
  align-items: center;
  min-height: 64rem;
  height: ${({ theme }) => `calc(100vh - ${theme.sizes.headerHeight})`};
  flex-direction: column;
  ${({ theme }) => theme.layout.flexCenterLayout}
`;

const SmallLogo = memo(styled(Icon)`
  min-width: 5.8rem;
  min-height: 7.7rem;
`);

const FormSection = styled.div`
  width: 81rem;
  padding: 0 14.7rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${({ theme }) => theme.colors.white};
`;

export default AuthFormWrapper;
