import { styled } from '@mui/material';
import Logo from '@src/components/base/Logo';
import { memo } from 'react';

const Header = styled('header')(({ theme }) => ({
  position: 'relative',
  width: '100%',
  height: theme.sizes.headerHeight,
  display: 'flex',
  background: 'white',
  zIndex: theme.zIndex.appBar,

  '::after': {
    content: '""',
    position: 'absolute',
    bottom: '0',
    left: '0',
    width: '100%',
    height: '1px',
    background: 'rgba(0, 0, 0, 0.265)',
  },
}));

const Flexbox = styled('div')({
  padding: '0 10.2rem',
  display: 'flex',
  alignItems: 'center',
});

const AuthPageHeader = () => (
  <Header>
    <Flexbox>
      <Logo />
    </Flexbox>
  </Header>
);

export default memo(AuthPageHeader);
