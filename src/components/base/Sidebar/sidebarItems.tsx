import DirectionsBoatIcon from '@mui/icons-material/DirectionsBoat';
import TimelineIcon from '@mui/icons-material/Timeline';
import GroupIcon from '@mui/icons-material/Group';
import {
  INSPECTION_REQUEST,
  INSPECTION_RESULT,
  ORGANIZATION_AND_ROLE,
  SHIP_AND_BLOCK,
} from '../../../constants/constantString';
import { ROUTE_PATHS } from '../../../routes/routePaths';
import { SidebarMenuItem } from './Sidebar';

export const INSPECTION_RESULT_ITEMS: SidebarMenuItem = {
  id: '검사 결과',
  type: 'group',
  children: [
    {
      id: INSPECTION_RESULT,
      title: INSPECTION_RESULT,
      type: 'item',
      icon: <TimelineIcon />,
      url: ROUTE_PATHS.inspectionResult,
    },
  ],
};

export const SHIP_MANAGEMENT_ITEMS: SidebarMenuItem = {
  id: '호선/블록',
  type: 'group',
  children: [
    {
      id: SHIP_AND_BLOCK,
      title: SHIP_AND_BLOCK,
      type: 'item',
      url: ROUTE_PATHS.shipManagement,
      icon: <DirectionsBoatIcon />,
    },
  ],
};

export const SHIP_INSPECTION_REQUEST_ITEMS: SidebarMenuItem = {
  id: '검사 신청',
  type: 'group',
  children: [
    {
      id: INSPECTION_REQUEST,
      title: INSPECTION_REQUEST,
      type: 'item',
      url: ROUTE_PATHS.shipAssigned,
      icon: <DirectionsBoatIcon />,
    },
  ],
};

export const USER_MANAGEMENT_ITEMS: SidebarMenuItem = {
  id: '유저',
  type: 'group',
  children: [
    {
      id: ORGANIZATION_AND_ROLE,
      title: ORGANIZATION_AND_ROLE,
      type: 'item',
      url: ROUTE_PATHS.userManagement,
      icon: <GroupIcon />,
    },
  ],
};
