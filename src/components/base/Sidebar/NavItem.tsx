import React, { useCallback, useMemo } from 'react';
import { useLocation, useRouteMatch } from 'react-router';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import {
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { Link } from 'react-router-dom';
import { styled } from '@mui/system';
import useSidebarAction from '../../../hooks/common/useSidebarAction';
import { SidebarMenuItem } from './Sidebar';

export type NavItemProps = {
  item: Omit<SidebarMenuItem, 'url'> & {
    url: string;
  };
  level: number;
};

const StyledListItemButton = styled(ListItemButton)<{
  level: number;
  component: React.ReactNode;
  to: string;
}>(({ level }) => ({
  paddingLeft: `${level * 2.4}rem`,
  marginBottom: '0.4rem',
  backgroundColor: level > 1 ? 'transparent !important' : 'inherit',
  borderRadius: '8px',
}));

const StyledListItemIcon = styled(ListItemIcon, {
  shouldForwardProp: (prop) => prop !== 'hasIcon',
})<{ hasIcon: boolean }>(({ hasIcon }) => ({
  minWidth: hasIcon ? '3.6rem' : '1.8rem',
}));

const NavItem = ({ item, level }: NavItemProps) => {
  const theme = useTheme();
  const isMatchDownLarge = useMediaQuery(theme.breakpoints.down('lg'));
  const { setIsOpen } = useSidebarAction();

  const { icon, url, title, caption } = item;

  const match = useRouteMatch(url);
  const location = useLocation();

  const isActive = useMemo(() => match !== null, [match, location]);

  const itemIcon =
    icon !== undefined ? (
      icon
    ) : (
      <FiberManualRecordIcon
        sx={{
          width: isActive ? 8 : 6,
          height: isActive ? 8 : 6,
        }}
        fontSize={level > 0 ? 'inherit' : 'medium'}
      />
    );

  const handleClickButton = useCallback(() => {
    if (isMatchDownLarge) {
      setIsOpen(false);
    }
  }, [isMatchDownLarge, setIsOpen]);

  return (
    <StyledListItemButton
      selected={isActive}
      level={level}
      onClick={handleClickButton}
      component={Link}
      to={url}
    >
      <StyledListItemIcon hasIcon={icon !== undefined}>
        {itemIcon}
      </StyledListItemIcon>
      <ListItemText
        primary={<Typography variant="body2">{title}</Typography>}
        secondary={
          caption && (
            <Typography variant="caption" gutterBottom>
              {caption}
            </Typography>
          )
        }
      />
    </StyledListItemButton>
  );
};

export default NavItem;
