import { useState } from 'react';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import {
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
  Typography,
} from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import { debugAssert } from '@src/utils/commonUtil';
import NavItem from './NavItem';
import { SidebarMenuItem } from './Sidebar';

export type NavCollapseProps = {
  item: SidebarMenuItem;
  level: number;
};

const StyledList = styled(List)({
  position: 'relative',
  '&:after': {
    content: "''",
    position: 'absolute',
    left: '32px',
    top: 0,
    height: '100%',
    width: '1px',
    opacity: 1,
    background: 'black',
  },
});

const IconWrapper = styled('div')({
  marginTop: 'auto',
  marginBottom: 'auto',
});

const NavCollapse = ({ item, level }: NavCollapseProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const { icon, title, caption } = item;

  const handleClick = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };

  const items = item.children?.map((childItem) => {
    const { id, type, url } = childItem;

    switch (type) {
      case 'collapse':
        return <NavCollapse key={id} item={childItem} level={level + 1} />;
      case 'item':
        if (url === undefined) {
          debugAssert(false, 'Nav item should be have url field');
        }

        const copied = { ...childItem, url: url as string };
        return <NavItem key={id} item={copied} level={level + 1} />;
      default:
        debugAssert(false, 'This type is not includes nav item types');
        return null;
    }
  });

  const itemIcon =
    icon !== undefined ? (
      icon
    ) : (
      <FiberManualRecordIcon
        sx={{
          width: isOpen ? 8 : 6,
          height: isOpen ? 8 : 6,
        }}
        fontSize={level > 0 ? 'inherit' : 'medium'}
      />
    );

  return (
    <>
      <ListItemButton
        sx={{
          mb: 0.5,
          alignItems: 'flex-start',
          backgroundColor: level > 1 ? 'transparent !important' : 'inherit',
          pl: `${level * 2.4}rem`,
          borderRadius: '8px',
        }}
        selected={isOpen}
        onClick={handleClick}
      >
        <ListItemIcon sx={{ my: 'auto', minWidth: 36 }}>
          {itemIcon}
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography color="inherit" variant="body2" sx={{ my: 'auto' }}>
              {title}
            </Typography>
          }
          secondary={
            caption && (
              <Typography variant="caption" display="block" gutterBottom>
                {caption}
              </Typography>
            )
          }
        />
        <IconWrapper>
          {isOpen ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
        </IconWrapper>
      </ListItemButton>
      <Collapse in={isOpen} timeout="auto" unmountOnExit>
        <StyledList disablePadding>{items}</StyledList>
      </Collapse>
    </>
  );
};

export default NavCollapse;
