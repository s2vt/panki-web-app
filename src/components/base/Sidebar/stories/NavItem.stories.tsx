import { Meta, Story } from '@storybook/react';
import NavItem, { NavItemProps } from '../NavItem';

export default {
  title: 'Components/Base/Sidebar/NavItem',
  component: NavItem,
} as Meta;

const Template: Story<NavItemProps> = (args) => <NavItem {...args} />;

export const Basic = Template.bind({});
