import { Meta, Story } from '@storybook/react';
import NavCollapse, { NavCollapseProps } from '../NavCollapse';

export default {
  title: 'Components/Base/Sidebar/NavCollapse',
  component: NavCollapse,
} as Meta;

const Template: Story<NavCollapseProps> = (args) => <NavCollapse {...args} />;

export const Basic = Template.bind({});
