import { Meta, Story } from '@storybook/react';
import NavGroup, { NavGroupProps } from '../NavGroup';

export default {
  title: 'Components/Base/Sidebar/NavGroup',
  component: NavGroup,
} as Meta;

const Template: Story<NavGroupProps> = (args) => <NavGroup {...args} />;

export const Basic = Template.bind({});
