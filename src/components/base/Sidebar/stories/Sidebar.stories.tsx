import { Meta, Story } from '@storybook/react';
import Sidebar, { SidebarProps } from '../Sidebar';

export default {
  title: 'Components/Base/Sidebar',
  component: Sidebar,
} as Meta;

const Template: Story<SidebarProps> = (args) => <Sidebar {...args} />;

export const Basic = Template.bind({});
