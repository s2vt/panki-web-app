import {
  Drawer,
  List,
  useTheme,
  useMediaQuery,
  ButtonBase,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { useCallback, useEffect } from 'react';
import useSidebarSelector from '../../../hooks/common/useSidebarSelector';
import useSidebarAction from '../../../hooks/common/useSidebarAction';
import NavGroup from './NavGroup';
import useUserSelector from '../../../hooks/user/useUserSelector';
import useCreateSidebarMenuItems from './hooks/useCreateSidebarMenuItems';
import Logo from '../Logo';

export type SidebarProps = {};

export type SidebarMenuItem = {
  id: string;
  title?: string;
  type: 'group' | 'item' | 'collapse';
  url?: string;
  icon?: JSX.Element;
  caption?: string;
  children?: SidebarMenuItem[];
};

const StyledDrawer = styled(Drawer)(({ theme }) => ({
  width: theme.sizes.sidebarWidth,
  '& .MuiDrawer-paper': {
    width: theme.sizes.sidebarWidth,
    background: theme.palette.background.default,
    borderRight: 'none',
  },
}));

const ScrollbarContainer = styled('div')({
  overflowY: 'auto',
});

const StyledButtonBase = styled(ButtonBase)({
  padding: '2.4rem 0 0 2.4rem',
});

const StyledList = styled(List)({
  marginTop: '6.4rem',
  padding: '0 0.8rem',
});

const Sidebar = () => {
  const theme = useTheme();
  const isMatchDownLarge = useMediaQuery(theme.breakpoints.down('lg'));

  const { user } = useUserSelector();
  const { isOpen } = useSidebarSelector();
  const { setIsOpen } = useSidebarAction();

  const sidebarMenuItems = useCreateSidebarMenuItems(user);

  useEffect(() => {
    setIsOpen(!isMatchDownLarge);
  }, [isMatchDownLarge, setIsOpen]);

  const closeDrawer = useCallback(() => {
    setIsOpen(false);
  }, [isOpen, setIsOpen]);

  const handleLogoClick = useCallback(() => {
    if (isMatchDownLarge) {
      closeDrawer();
    }
  }, [isMatchDownLarge, closeDrawer]);

  return (
    <StyledDrawer
      variant={isMatchDownLarge ? 'temporary' : 'persistent'}
      anchor="left"
      open={isOpen}
      onClose={closeDrawer}
      ModalProps={{ keepMounted: true }}
    >
      <ScrollbarContainer>
        <StyledButtonBase disableRipple onClick={handleLogoClick}>
          <Logo />
        </StyledButtonBase>
        <StyledList>
          {sidebarMenuItems.map((menuItem) => (
            <NavGroup key={menuItem.id} item={menuItem} />
          ))}
        </StyledList>
      </ScrollbarContainer>
    </StyledDrawer>
  );
};

export default Sidebar;
