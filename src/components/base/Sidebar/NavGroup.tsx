import { Divider, List, Typography } from '@mui/material';
import { debugAssert } from '@src/utils/commonUtil';
import { memo } from 'react';
import NavCollapse from './NavCollapse';
import NavItem from './NavItem';
import { SidebarMenuItem } from './Sidebar';

export type NavGroupProps = {
  item: SidebarMenuItem;
};

const NavGroup = ({ item }: NavGroupProps) => {
  const items = item?.children?.map((childItem) => {
    const { id, type, url } = childItem;

    switch (type) {
      case 'collapse':
        return <NavCollapse key={id} item={childItem} level={1} />;

      case 'item':
        if (url === undefined) {
          debugAssert(false, 'Nav item should be have url field');
        }

        const copied = { ...childItem, url: url as string };
        return <NavItem key={id} item={copied} level={1} />;
      default:
        debugAssert(false, 'This type is not includes nav item types');
        return null;
    }
  });

  const { title, caption } = item;

  return (
    <>
      <List
        subheader={
          title && (
            <Typography variant="caption" display="block" gutterBottom>
              {title}
              {caption && (
                <Typography variant="caption" display="block" gutterBottom>
                  {caption}
                </Typography>
              )}
            </Typography>
          )
        }
      >
        {items}
      </List>
      <Divider sx={{ mt: 0.25, mb: 1.25 }} />
    </>
  );
};

export default memo(NavGroup);
