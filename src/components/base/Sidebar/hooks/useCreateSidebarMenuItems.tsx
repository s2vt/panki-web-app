import { useMemo } from 'react';
import { User, UserClass, UserRole } from '@src/types/user.types';
import useValidateRoles from '@src/hooks/user/useValidateRoles';
import {
  INSPECTION_RESULT_ITEMS,
  SHIP_INSPECTION_REQUEST_ITEMS,
  SHIP_MANAGEMENT_ITEMS,
  USER_MANAGEMENT_ITEMS,
} from '../sidebarItems';
import { SidebarMenuItem } from '../Sidebar';

const useCreateSidebarMenuItems = (user: User | undefined) => {
  const hasRequestDataCheckRole = useValidateRoles(
    [UserRole.requestDataCheckRole],
    user,
  );

  const sidebarMenuItems: SidebarMenuItem[] = useMemo(() => {
    if (!user) {
      return [];
    }

    const result: SidebarMenuItem[] = [];

    const { className } = user;

    if (className === UserClass.admin || className === UserClass.manager) {
      result.push(SHIP_MANAGEMENT_ITEMS);
      result.push(INSPECTION_RESULT_ITEMS);
    }

    if (hasRequestDataCheckRole) {
      result.push(SHIP_INSPECTION_REQUEST_ITEMS);
    }

    result.push(USER_MANAGEMENT_ITEMS);

    return result;
  }, [user]);

  return sidebarMenuItems;
};

export default useCreateSidebarMenuItems;
