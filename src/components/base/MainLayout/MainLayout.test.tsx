import { render } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import rootState from '@src/test/fixtures/reducers/rootState';
import MainLayout from '.';

describe('<MainLayout />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareMockWrapper(rootState);

    const result = render(
      <Wrapper>
        <MainLayout />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup();

    expect(container).toBeInTheDocument();
  });
});
