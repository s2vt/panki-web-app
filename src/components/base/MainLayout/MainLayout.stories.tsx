import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import MainLayout, { MainLayoutProps } from './MainLayout';

export default {
  title: 'Components/base/MainLayout',
  component: MainLayout,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<MainLayoutProps> = (args) => <MainLayout {...args} />;

export const Basic = Template.bind({});
