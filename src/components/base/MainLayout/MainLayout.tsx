import { styled } from '@mui/material/styles';
import { Box } from '@mui/system';
import useSidebarSelector from '../../../hooks/common/useSidebarSelector';
import Header from '../Header';

export type MainLayoutProps = {
  /** 자식 컴포넌트들 */
  children?: React.ReactNode;
};

const Main = styled('main', {
  shouldForwardProp: (prop) => prop !== 'isOpen',
})<{
  isOpen?: boolean;
}>(({ theme, isOpen }) => ({
  display: 'flex',
  justifyContent: 'center',
  minWidth: theme.sizes.containerMinWidth,
  minHeight: '100%',
  paddingTop: theme.sizes.headerHeight,
  background: theme.palette.background.paper,
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),

  ...(!isOpen && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
  ...(isOpen && {
    [theme.breakpoints.up('lg')]: {
      marginLeft: theme.sizes.sidebarWidth,
    },
  }),
}));

const Container = styled(Box)(({ theme }) => ({
  flex: 1,
  minWidth: theme.sizes.containerMinWidth,
  maxWidth: theme.sizes.containerMaxWidth,
  padding: '3.2rem',
}));

function MainLayout({ children }: MainLayoutProps) {
  const { isOpen } = useSidebarSelector();

  return (
    <Main isOpen={isOpen}>
      <Header />
      <Container>{children}</Container>
    </Main>
  );
}

export default MainLayout;
