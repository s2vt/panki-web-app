import { Meta, Story } from '@storybook/react';
import ModalWrapper, { ModalWrapperProps } from './ModalWrapper';

export default {
  title: 'Components/Base/ModalWrapper',
  component: ModalWrapper,
} as Meta;

const Template: Story<ModalWrapperProps> = (args) => <ModalWrapper {...args} />;

export const Basic = Template.bind({});
