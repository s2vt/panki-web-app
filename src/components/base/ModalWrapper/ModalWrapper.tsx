import { Backdrop, Fade, Modal } from '@mui/material';
import { Box, styled } from '@mui/system';
import { forwardRef, useEffect, useState } from 'react';

const StyledBox = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  backgroundColor: theme.palette.background.default,
}));

export type ModalWrapperProps = {
  children: React.ReactElement;
  isOpen: boolean;
};

const TRANSITION_DURATION_MS = 150;

const ModalWrapper = (
  { children, isOpen }: ModalWrapperProps,
  ref: React.Ref<HTMLDivElement>,
) => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    let timeoutId: NodeJS.Timeout;

    if (isOpen) {
      setVisible(true);
    } else {
      timeoutId = setTimeout(() => setVisible(false), TRANSITION_DURATION_MS);
    }

    return () => {
      if (timeoutId !== undefined) {
        clearTimeout(timeoutId);
      }
    };
  }, [isOpen]);

  if (!visible) {
    return null;
  }

  return (
    <Modal
      open={visible}
      BackdropComponent={Backdrop}
      BackdropProps={{ transitionDuration: TRANSITION_DURATION_MS }}
      ref={ref}
    >
      <Fade in={isOpen}>
        <StyledBox ref={ref}>{children}</StyledBox>
      </Fade>
    </Modal>
  );
};

export default forwardRef(ModalWrapper);
