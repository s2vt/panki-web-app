import { Meta, Story } from '@storybook/react';
import ModalTitle, { ModalTitleProps } from './ModalTitle';

export default {
  title: 'Components/base/ModalTitle',
  component: ModalTitle,
} as Meta;

const Template: Story<ModalTitleProps> = (args) => <ModalTitle {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  title: '타이틀',
};
