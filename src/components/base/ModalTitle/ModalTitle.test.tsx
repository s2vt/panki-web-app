import { fireEvent, render } from '@testing-library/react';
import prepareMockWrapper from '@src/test/utils/prepareMockWrapper';
import ModalTitle from '.';

describe('<ModalTitle />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareMockWrapper();

    const handleClose = jest.fn();

    const result = render(
      <Wrapper>
        <ModalTitle title="title" onClose={handleClose} />
      </Wrapper>,
    );

    const closeButton = result.getByTestId('close-button');

    return { ...result, handleClose, closeButton };
  };

  it('should render properly', () => {
    const { container } = setup();

    expect(container).toBeInTheDocument();
  });

  it('should call handleClose when clicked close button', () => {
    // given
    const { handleClose, closeButton } = setup();

    // when
    fireEvent.click(closeButton);

    // then
    expect(handleClose).toBeCalled();
  });
});
