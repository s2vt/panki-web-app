import { memo } from 'react';
import styled from 'styled-components';
import Icon from '@src/components/common/Icon';

export type ModalTitleProps = {
  /** 모달 타이틀 */
  title: string;
  /** 닫기 버튼 클릭 시 실행할 이벤트 */
  onClose: () => void;
};

/** 모달 컴포넌트의 타이틀 부분에 사용될 컴포넌트입니다 */
const ModalTitle = ({ title, onClose }: ModalTitleProps) => (
  <TitleSection>
    <p>{title}</p>
    <CloseButton type="button" onClick={onClose} data-testid="close-button">
      <Icon icon="closeButton" />
    </CloseButton>
  </TitleSection>
);

const TitleSection = styled.div`
  height: 7.6rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 3.9rem;
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
`;

const CloseButton = styled.button`
  border: none;
  background: none;
  cursor: pointer;

  svg {
    width: 3rem;
    height: 2.8rem;
  }
`;

export default memo(ModalTitle);
