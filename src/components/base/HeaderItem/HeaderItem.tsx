import { memo } from 'react';
import { match, NavLink } from 'react-router-dom';
import styled from 'styled-components';
import History from 'history';

export type HeaderItemProps = {
  to: string;
  label: String;
  isActive?<Params extends { [K in keyof Params]?: string }>(
    match: match<Params>,
    location: History.Location,
  ): boolean;
};

const HeaderItem = ({ to, label, isActive }: HeaderItemProps) => (
  <LinkText to={to} isActive={isActive}>
    {label}
  </LinkText>
);

const LinkText = styled(NavLink)`
  font-size: 1.8rem;
  font-weight: 400;
  text-decoration: none;
  color: ${({ theme }) => theme.colors.primary};

  &.active {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;

export default memo(HeaderItem);
