import { fireEvent, render, screen } from '@testing-library/react';
import { Router } from 'react-router';
import { createMemoryHistory } from 'history';
import { ThemeProvider } from 'styled-components';
import theme from '@src/styles/theme';
import HeaderItem from './HeaderItem';

describe('<HeaderItem />', () => {
  const history = createMemoryHistory();
  const setup = (label = 'label', to = '/') => {
    const wrapper = render(
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <HeaderItem label={label} to={to} />
        </Router>
      </ThemeProvider>,
    );

    return { wrapper, history };
  };

  it('should render properly', () => {
    setup();
  });

  it('should change text with label props value', async () => {
    // given
    const label = 'testLabel';

    // when
    setup(label);
    const headerItem = await screen.findByText(label);

    // then
    expect(headerItem.textContent).toEqual(label);
  });

  it('should push route when clicked', async () => {
    // given
    const label = 'testLabel';
    const to = '/route';

    const { history } = setup(label, to);

    // when
    const headerItem = await screen.findByText(label);
    fireEvent.click(headerItem);

    // then
    expect(history.location.pathname).toEqual(to);
  });
});
