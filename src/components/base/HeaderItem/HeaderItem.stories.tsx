import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import HeaderItem, { HeaderItemProps } from './HeaderItem';

export default {
  title: 'Components/base/HeaderItem',
  component: HeaderItem,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<HeaderItemProps> = (args) => <HeaderItem {...args} />;

export const Active = Template.bind({});
Active.args = {
  label: 'Home',
  to: '/',
};

export const Inactive = Template.bind({});
Inactive.args = {
  label: 'Home',
  to: '/home',
};
