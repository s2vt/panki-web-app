import { Meta, Story } from '@storybook/react';
import AlertDialog, { AlertDialogProps } from './AlertDialog';

export default {
  title: 'Components/base/AlertDialog',
  component: AlertDialog,
} as Meta;

const Template: Story<AlertDialogProps> = (args) => <AlertDialog {...args} />;

export const Basic = Template.bind({});
