import { memo, useEffect, useState } from 'react';
import styled, { css } from 'styled-components';
import useAlertDialogSelector from '@src/hooks/common/useAlertDialogSelector';
import { AlertDialogPosition } from '@src/reducers/alertDialog';

export type AlertDialogProps = {};

const TRANSITION_TIME = 1 * 1000;

/** 메인 alert dialog 컴포넌트입니다. */
const AlertDialog = () => {
  const [visible, setVisible] = useState(false);
  const {
    alertDialog: { isOpen, text, color, position },
  } = useAlertDialogSelector();

  useEffect(() => {
    let timeoutId: NodeJS.Timeout;

    if (isOpen) {
      setVisible(true);
      timeoutId = setTimeout(() => setVisible(false), TRANSITION_TIME);
    }

    return () => {
      if (timeoutId !== undefined) {
        clearTimeout(timeoutId);
      }
    };
  }, [isOpen]);

  if (!isOpen) {
    return null;
  }

  return (
    <Block visible={visible} position={position}>
      <Box color={color}>{text}</Box>
    </Block>
  );
};

const Block = styled.div<{ visible: boolean; position: AlertDialogPosition }>`
  visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
  ${({ position }) => position === 'center' && centerPosition}
  ${({ position }) => position === 'rightTop' && rightTopPosition}
  position: fixed;
  ${({ theme }) => theme.layout.flexCenterLayout}
  z-index: ${({ theme }) => theme.zIndexes.dialog};
  transition: visibility 0.5s ease-in-out;
  animation: ${({ visible, theme }) =>
      visible ? theme.transition.fadeIn : theme.transition.fadeOut}
    0.5s ease-out;
`;

const centerPosition = css`
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const rightTopPosition = css`
  top: 5%;
  right: 5%;
`;

const Box = styled.div<{ color: string | undefined }>`
  padding: 2rem;
  font-size: 1.6rem;
  border-radius: 8px;
  background-color: ${({ color }) => color ?? 'rgba(255, 204, 204, 0.6)'};
  user-select: none;
`;

export default memo(AlertDialog);
