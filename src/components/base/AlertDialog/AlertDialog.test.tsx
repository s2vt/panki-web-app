import { render } from '@testing-library/react';
import { RootState } from '@src/reducers/rootReducer';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import AlertDialog from '.';

describe('<AlertDialog />', () => {
  const setup = (initialState?: RootState) => {
    const { wrapper: Wrapper, store } = prepareWrapper(initialState);

    const result = render(
      <Wrapper>
        <AlertDialog />
      </Wrapper>,
    );

    return { result, store };
  };

  it('should render properly when alert dialog state is open', () => {
    const initialState = {
      alertDialog: {
        isOpen: true,
        text: 'test',
        color: 'red',
        position: 'center',
      },
    } as RootState;

    const { result } = setup(initialState);

    expect(result.container.hasChildNodes()).toEqual(true);
  });

  it('should not render when alert dialog state is not open', () => {
    const initialState = {
      alertDialog: {
        isOpen: false,
        text: 'test',
        color: 'red',
        position: 'center',
      },
    } as RootState;

    const { result } = setup(initialState);

    expect(result.container.hasChildNodes()).toEqual(false);
  });
});
