import { Meta, Story } from '@storybook/react';
import PageTitle, { PageTitleProps } from './PageTitle';

export default {
  title: 'Components/base/PageTitle',
  component: PageTitle,
} as Meta;

const Template: Story<PageTitleProps> = (args) => <PageTitle {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  title: '타이틀',
};
