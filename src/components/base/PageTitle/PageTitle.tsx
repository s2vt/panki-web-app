import { memo } from 'react';
import styled from 'styled-components';

export type PageTitleProps = {
  title: string;
  children?: React.ReactNode;
  className?: string;
};

const PageTitle = ({ title, children, className }: PageTitleProps) => (
  <TitleWrapper className={className}>
    <h1>{title}</h1>
    {children}
  </TitleWrapper>
);

const TitleWrapper = styled.div`
  margin-top: 2.2rem;
  margin-bottom: 2.2rem;
  display: flex;
  justify-content: space-between;
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

export default memo(PageTitle);
