import { styled } from '@mui/material';
import pankiLogo from '@src/assets/icons/panki_logo.svg';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { memo } from 'react';
import { Link } from 'react-router-dom';

const Image = styled('img')({
  width: '13.8rem',
  height: '4.5rem',
});

const Logo = () => (
  <Link to={ROUTE_PATHS.root}>
    <Image src={pankiLogo} alt="logo" />
  </Link>
);

export default memo(Logo);
