import { Meta, Story } from '@storybook/react';
import Logo from './Logo';

export default {
  title: 'Components/Base/Logo',
  component: Logo,
} as Meta;

const Template: Story = (args) => <Logo {...args} />;

export const Basic = Template.bind({});
