import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import Header from './Header';

export default {
  title: 'Components/base/Header',
  component: Header,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <Header {...args} />;

export const Basic = Template.bind({});
