import { render } from '@testing-library/react';
import rootState from '@src/test/fixtures/reducers/rootState';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import Header from '.';

describe('<Header />', () => {
  const setup = () => {
    const { wrapper: Wrapper } = prepareWrapper(rootState);

    const result = render(
      <Wrapper>
        <Header />
      </Wrapper>,
    );

    return { ...result };
  };

  it('should render properly', () => {
    const { container } = setup();

    expect(container).toBeInTheDocument();
  });
});
