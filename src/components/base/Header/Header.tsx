import { memo, useCallback } from 'react';
import { AppBar, IconButton, Toolbar } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { Box } from '@mui/system';
import { styled } from '@mui/material/styles';
import useSidebarSelector from '@src/hooks/common/useSidebarSelector';
import useUserSelector from '@src/hooks/user/useUserSelector';
import useSidebarAction from '@src/hooks/common/useSidebarAction';
import UserProfileButton from '@src/components/user/UserProfileButton';

const StyledAppBar = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== 'isSidebarOpen',
})<{
  isSidebarOpen?: boolean;
}>(({ theme, isSidebarOpen }) => ({
  width: '100%',
  background: theme.palette.background.paper,
  backgroundAttachment: 'fixed',
  height: theme.sizes.headerHeight,
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(!isSidebarOpen && {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
  ...(isSidebarOpen && {
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${theme.sizes.sidebarWidth})`,
    },
  }),
}));

const StyledToolbar = styled(Toolbar)({
  height: '100%',
  padding: '0 2rem !important',
  justifyContent: 'space-between',
});

const ButtonSection = styled(Box)({
  display: 'flex',
});

const Header = () => {
  const { user, isLoggedIn } = useUserSelector();
  const { isOpen } = useSidebarSelector();
  const { setIsOpen } = useSidebarAction();

  const toggleSidebar = useCallback(() => {
    setIsOpen(!isOpen);
  }, [isOpen, setIsOpen]);

  return (
    <StyledAppBar color="inherit" elevation={0} isSidebarOpen={isOpen}>
      <StyledToolbar>
        <IconButton
          aria-label="sidebar toggle"
          onClick={toggleSidebar}
          size="medium"
        >
          <MenuIcon />
        </IconButton>
        {isLoggedIn && (
          <ButtonSection>
            {user !== undefined && <UserProfileButton user={user} />}
          </ButtonSection>
        )}
      </StyledToolbar>
    </StyledAppBar>
  );
};

export default memo(Header);
