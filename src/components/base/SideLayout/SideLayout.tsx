import { Grid, styled } from '@mui/material';

export type SideLayoutProps = {
  children?: React.ReactNode;
};

const SideBox = styled(Grid)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    minWidth: theme.sizes.sideBoxWidth,
  },
}));

const Container = ({ children }: SideLayoutProps) => (
  <Grid container spacing={3}>
    {children}
  </Grid>
);

const Side = ({ children }: SideLayoutProps) => (
  <SideBox item xs={3} sm={3} md={3} lg={3}>
    {children}
  </SideBox>
);

const Main = ({ children }: SideLayoutProps) => (
  <Grid item xs={12} sm={12} md={12} lg={9}>
    {children}
  </Grid>
);

export default { Container, Side, Main };
