import { Meta, Story } from '@storybook/react';
import { ChartData, ChartFilterDateType } from '@src/types/statistics.types';
import CompanyPassRateLineChart, {
  CompanyPassRateLineChartProps,
} from './CompanyPassRateLineChart';

export default {
  title: 'Components/Statistics/CompanyPassRateLineChart',
  component: CompanyPassRateLineChart,
} as Meta;

const Template: Story<CompanyPassRateLineChartProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <CompanyPassRateLineChart {...args} />
  </div>
);

const PERIOD_SAMPLE_DATA: ChartData[] = [
  {
    legend: '2021.09.01 ~ 2021.09.08',
    '블라스팅 SSP (IN)': 88,
    '도장 W.B. TK FINAL': 90,
  },
  {
    legend: '2021.09.09 ~ 2021.09.16',
    '블라스팅 SSP (IN)': 92,
    '도장 W.B. TK FINAL': 95,
  },
  {
    legend: '2021.09.17 ~ 2021.09.23',
    '블라스팅 SSP (IN)': 43,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021.09.24 ~ 2021.09.31',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
];

const YEAR_SAMPLE_DATA: ChartData[] = [
  {
    legend: '2021년 1월',
    '블라스팅 SSP (IN)': 88,
    '도장 W.B. TK FINAL': 90,
  },
  {
    legend: '2021년 2월',
    '블라스팅 SSP (IN)': 92,
    '도장 W.B. TK FINAL': 95,
  },
  {
    legend: '2021년 3월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 4월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 5월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 6월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 7월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 8월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 9월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 10월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 11월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
  {
    legend: '2021년 12월',
    '블라스팅 SSP (IN)': 93,
    '도장 W.B. TK FINAL': 87,
  },
];

export const PeriodMode = Template.bind({});
PeriodMode.args = {
  subject: '주제',
  companyName: '회사',
  data: PERIOD_SAMPLE_DATA,
  chartFilterDateType: ChartFilterDateType.period,
};

export const YearMode = Template.bind({});
YearMode.args = {
  subject: '주제',
  companyName: '회사',
  data: YEAR_SAMPLE_DATA,
  chartFilterDateType: ChartFilterDateType.year,
};
