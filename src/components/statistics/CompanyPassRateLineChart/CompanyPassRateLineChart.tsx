import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import {
  ChartMouseMoveParams,
  Label,
  LineChart,
  LineOnMouseOverParams,
  ReferenceLine,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import styled, { css } from 'styled-components';
import {
  ORIGIN_COATING_RATE,
  PERIOD,
  TARGET,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { colors } from '@src/styles/theme';
import {
  ChartData,
  ChartFilterDateType,
  getChartDataKeys,
  getChartDataMinimumValue,
} from '@src/types/statistics.types';
import ChartBase from '../ChartBase';
import ChartSubject from '../ChartSubject';
import CheckboxLegend, { CheckboxLegendItem } from '../CheckboxLegend';
import CustomLine from '../CustomLine';
import CustomReferenceLine from '../CustomReferenceLine';
import DateXAxisTick from '../DateXAxisTick';
import DotLabelList from '../DotLabelList';
import CompanyPassRateLineChartTooltip, {
  CompanyPassRateLineChartTooltipData,
} from './CompanyPassRateLineChartTooltip';

export type CompanyPassRateLineChartProps = {
  subject: string;
  companyName: string;
  data: ChartData[];
  chartFilterDateType: ChartFilterDateType;
};

const COMPANY_PASS_RATE_CHART_COLORS = [
  colors.brightBlue,
  colors.lightOrange,
  colors.lightGreen,
];

const ORIGIN_COATING_RATE_TARGET = 88;
const PASS_RATE_TARGET = 90;
const YEAR_X_AXIS_MAXIMUM_PADDING = 310;
const WEEK_X_AXIS_MAXIMUM_PADDING = 60;

const CompanyPassRateLineChart = ({
  subject,
  companyName,
  data,
  chartFilterDateType,
}: CompanyPassRateLineChartProps) => {
  const [activeLegend, setActiveLegend] = useState<string>();
  const [tooltipData, setTooltipData] =
    useState<CompanyPassRateLineChartTooltipData | null>(null);

  const dataKeys = useMemo(() => getChartDataKeys(data), [data]);

  const minimumValue = useMemo(() => getChartDataMinimumValue(data), [data]);

  const xAxisPadding = useMemo(() => {
    if (chartFilterDateType === ChartFilterDateType.year) {
      const legends = data.map((datum) => datum.legend);

      const paddingByTick = 20;

      return YEAR_X_AXIS_MAXIMUM_PADDING - legends.length * paddingByTick;
    }

    return WEEK_X_AXIS_MAXIMUM_PADDING;
  }, [data]);

  const getReferenceValue = (legend: string) => {
    if (legend.includes(ORIGIN_COATING_RATE)) {
      return ORIGIN_COATING_RATE_TARGET;
    }

    return PASS_RATE_TARGET;
  };

  const targetLegendItems: CheckboxLegendItem[] = useMemo(
    () =>
      dataKeys.map(
        (dataKey, index) =>
          ({
            id: `${dataKey} ${TARGET}`,
            color: COMPANY_PASS_RATE_CHART_COLORS[index],
            boxColor: dataKey.includes(ORIGIN_COATING_RATE)
              ? colors.primary
              : COMPANY_PASS_RATE_CHART_COLORS[index],
            icon: 'line',
            isSelectable: true,
            type: 'target',
            referenceValue: getReferenceValue(dataKey),
          } as CheckboxLegendItem),
      ),
    [dataKeys],
  );

  const legendItems: CheckboxLegendItem[] = useMemo(
    () =>
      dataKeys.map(
        (dataKey, index) =>
          ({
            id: dataKey,
            color: COMPANY_PASS_RATE_CHART_COLORS[index],
            icon: 'circle',
            isSelectable: true,
            type: 'value',
          } as CheckboxLegendItem),
      ),
    [dataKeys],
  );

  const yAxisTicks = useMemo(() => {
    const ticks = [50, 60, 70, 80, 90, 100];

    if (minimumValue < 50) {
      ticks.unshift(minimumValue - (minimumValue % 5));
    }

    return ticks;
  }, [minimumValue]);

  const { selectedItems, toggleItem, isSelected, setSelectedItems } =
    useSelectItems<CheckboxLegendItem>([...legendItems, ...targetLegendItems]);

  const formatPercentage = useCallback(
    (value: string | number) => `${value}%`,
    [],
  );

  const handleTurnOffAllLegendsClick = useCallback(() => {
    setSelectedItems([]);
  }, [setSelectedItems]);

  useEffect(() => {
    setSelectedItems([...legendItems, ...targetLegendItems]);
  }, [legendItems, targetLegendItems, setSelectedItems]);

  const handleDotMouseOver = ({
    dataKey,
    value,
    payload,
  }: LineOnMouseOverParams) => {
    const referenceContrastValue = dataKey.includes(ORIGIN_COATING_RATE)
      ? value - ORIGIN_COATING_RATE_TARGET
      : value - PASS_RATE_TARGET;

    setTooltipData({
      value,
      dataKey,
      legend: payload.legend,
      referenceContrastValue,
    });
  };

  const handleDotMouseLeave = () => setTooltipData(null);

  const handleLineChartMouseMove = ({
    isTooltipActive,
    activeLabel,
  }: ChartMouseMoveParams) => {
    if (!isTooltipActive) {
      setActiveLegend(undefined);
      return;
    }

    setActiveLegend(activeLabel);
  };

  return (
    <Box>
      <ChartSubject
        subject={subject}
        isLegendButtonEnable
        onTurnOffAllLegendsClick={handleTurnOffAllLegendsClick}
      />
      <ChartBase.ChartWrapper>
        <LineChart
          width={923}
          height={420}
          data={data}
          margin={{ top: 58, bottom: 68, left: 18, right: 58 }}
          onMouseMove={handleLineChartMouseMove}
        >
          <XAxis
            padding={{
              left: xAxisPadding,
              right: xAxisPadding,
            }}
            type="category"
            dataKey="legend"
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            interval={0}
            tick={
              dataKeys.length > 0 ? (
                <DateXAxisTick
                  chartFilterDateType={chartFilterDateType}
                  activeLegend={activeLegend}
                />
              ) : (
                false
              )
            }
            tickMargin={26}
          />
          <YAxis
            type="number"
            domain={[minimumValue, 100]}
            ticks={yAxisTicks}
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            tick={{ fill: colors.primary }}
            tickFormatter={formatPercentage}
          >
            <Label
              value={PERIOD}
              offset={-70}
              position="insideBottom"
              fontSize="2rem"
              fill={colors.primary}
            />
          </YAxis>
          <Tooltip
            cursor={{ stroke: colors.primary }}
            content={
              <CompanyPassRateLineChartTooltip
                data={tooltipData}
                companyName={companyName}
              />
            }
            isAnimationActive={false}
          />
          {yAxisTicks.map((tick, index) => (
            <ReferenceLine
              key={tick}
              y={tick}
              stroke={colors.primary}
              strokeDasharray={index !== 0 ? '5 3' : undefined}
            />
          ))}
          {selectedItems.map((selectedItem) => (
            <React.Fragment key={selectedItem.id}>
              {selectedItem.type === 'value' &&
                CustomLine({
                  key: selectedItem.id,
                  dataKey: selectedItem.id,
                  lineColor: selectedItem.color,
                  onDotMouseOver: handleDotMouseOver,
                  onDotMouseLeave: handleDotMouseLeave,
                  renderLabelList: () =>
                    DotLabelList({
                      formatter: formatPercentage,
                    }),
                })}
              {selectedItem.type === 'target' &&
                selectedItem.boxColor !== undefined &&
                CustomReferenceLine({
                  label: `${selectedItem.referenceValue}%`,
                  referenceValue: selectedItem.referenceValue,
                  lineColor: selectedItem.color,
                  labelBoxColor: selectedItem.boxColor,
                })}
            </React.Fragment>
          ))}
        </LineChart>
        <StyledCheckboxLegend
          isSelected={isSelected}
          onItemClick={toggleItem}
          items={[...legendItems, ...targetLegendItems]}
          iconWidth="1.9rem"
          $legendsAmount={legendItems.length + targetLegendItems.length}
        />
      </ChartBase.ChartWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
`;

const StyledCheckboxLegend = styled(CheckboxLegend)<{ $legendsAmount: number }>`
  ${({ $legendsAmount }) =>
    $legendsAmount > 2 &&
    css`
      padding: 2.2rem 8rem;
      display: grid;
      grid-template-columns: repeat(2, 1fr);
    `};
`;

export default memo(CompanyPassRateLineChart);
