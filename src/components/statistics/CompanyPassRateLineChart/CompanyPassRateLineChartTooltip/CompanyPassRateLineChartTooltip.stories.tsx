import { Meta, Story } from '@storybook/react';
import CompanyPassRateLineChartTooltip, {
  CompanyPassRateLineChartTooltipProps,
} from './CompanyPassRateLineChartTooltip';

export default {
  title:
    'Components/Statistics/CompanyPassRateLineChart/CompanyPassRateLineChartTooltip',
  component: CompanyPassRateLineChartTooltip,
} as Meta;

const Template: Story<CompanyPassRateLineChartTooltipProps> = (args) => (
  <CompanyPassRateLineChartTooltip {...args} />
);

export const Basic = Template.bind({});
