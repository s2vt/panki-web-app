import styled from 'styled-components';
import { REFERENCE_CONTRAST } from '@src/constants/constantString';

export type CompanyPassRateLineChartTooltipProps = {
  companyName: string;
  data: CompanyPassRateLineChartTooltipData | null;
};

export type CompanyPassRateLineChartTooltipData = {
  legend: string;
  dataKey: string;
  value: number;
  referenceContrastValue: number;
};

const CompanyPassRateLineChartTooltip = ({
  companyName,
  data,
}: CompanyPassRateLineChartTooltipProps) => {
  if (data === null) {
    return null;
  }

  const { legend, dataKey, value, referenceContrastValue } = data;

  return (
    <Box>
      <p>{companyName}</p>
      <p>{`${legend}`}</p>
      <p>{`${dataKey} ${value}%`}</p>
      <p>{`${REFERENCE_CONTRAST} ${referenceContrastValue.toLocaleString()}%`}</p>
    </Box>
  );
};

const Box = styled.div`
  min-width: 21.4rem;
  padding: 1.1rem 1rem;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.white};
  font-size: 1.4rem;
  line-height: 1.9rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default CompanyPassRateLineChartTooltip;
