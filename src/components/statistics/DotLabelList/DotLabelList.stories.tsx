import { Meta, Story } from '@storybook/react';
import DotLabelList, { DotLabelListProps } from './DotLabelList';

export default {
  title: 'Components/Statistics/DotLabelList',
  component: DotLabelList,
} as Meta;

const Template: Story<DotLabelListProps> = (args) => <DotLabelList {...args} />;

export const Basic = Template.bind({});
