import { LabelList, LabelListProps } from 'recharts';
import { colors } from '@src/styles/theme';

export type DotLabelListProps = {};

const DotLabelList = ({ formatter, dataKey }: LabelListProps<unknown>) => (
  <LabelList
    dataKey={dataKey}
    position="top"
    fontSize="1.4rem"
    fontWeight={700}
    formatter={formatter}
    fill={colors.primary}
    pointerEvents="none"
    style={{
      userSelect: 'none',
      transform: 'translateY(-0.2rem)',
    }}
  />
);

export default DotLabelList;
