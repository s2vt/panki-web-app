import styled from 'styled-components';
import {
  FAIL_DATA_FETCH,
  INSPECTION_PASS_RATE,
  INSPECTION_PASS_RATE_DESCRIPTION,
  SEMANTIC_ANALYSIS,
  SEMANTIC_ANALYSIS_DESCRIPTION,
  STATISTICS_BY_COMPANY,
  STATISTICS_BY_COMPANY_DESCRIPTION,
} from '@src/constants/constantString';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import DescriptionCardButtonButton from '@src/components/common/DescriptionCardButton';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import { createRecentStatisticsBarChartData } from '@src/types/statistics.types';
import ErrorBox from '@src/components/common/ErrorBox';
import { Box } from '@mui/material';
import RecentStatisticsCarousel from '../RecentStatisticsCarousel';
import RecentStatisticsBarChart from '../RecentStatisticsBarChart';
import useRecentStatisticsQuery from './hooks/useRecentStatisticsQuery';

const StatisticsOverView = () => {
  const {
    data: recentStatistics,
    isFetching: isStatisticsFetching,
    isError: isStatisticsQueryError,
    isSuccess: isStatisticsQuerySuccess,
    refetch: refetchStatistics,
  } = useRecentStatisticsQuery();

  const chartData =
    recentStatistics !== undefined
      ? createRecentStatisticsBarChartData(recentStatistics)
      : [];

  return (
    <Box>
      <CardSection>
        <DescriptionCardButtonButton
          icon="documentIcon"
          title={INSPECTION_PASS_RATE}
          description={INSPECTION_PASS_RATE_DESCRIPTION}
          to={ROUTE_PATHS.statisticsPassRate}
        />
        <DescriptionCardButtonButton
          icon="documentIcon"
          title={STATISTICS_BY_COMPANY}
          description={STATISTICS_BY_COMPANY_DESCRIPTION}
          to={ROUTE_PATHS.statisticsCompany}
        />
        <DescriptionCardButtonButton
          icon="documentIcon"
          title={SEMANTIC_ANALYSIS}
          description={SEMANTIC_ANALYSIS_DESCRIPTION}
          to={ROUTE_PATHS.statisticsSemanticAnalysis}
        />
      </CardSection>
      <ChartSection>
        {isStatisticsFetching && <LoadingSpinner />}
        {!isStatisticsFetching && isStatisticsQueryError && (
          <ErrorBox
            error={FAIL_DATA_FETCH}
            onRefetchClick={refetchStatistics}
          />
        )}
        {recentStatistics !== undefined &&
          !isStatisticsFetching &&
          isStatisticsQuerySuccess && (
            <>
              <RecentStatisticsCarousel recentStatistics={recentStatistics} />
              <RecentStatisticsBarChart data={chartData} />
            </>
          )}
      </ChartSection>
    </Box>
  );
};

const CardSection = styled.div`
  margin: 1.6rem 0;
  display: flex;
  gap: 2.4rem;
`;

const ChartSection = styled.div`
  height: 80.2rem;
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
`;

export default StatisticsOverView;
