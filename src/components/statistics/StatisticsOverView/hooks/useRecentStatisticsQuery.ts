import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import statisticsApi from '@src/api/statisticsApi';
import { RecentStatistic } from '@src/types/statistics.types';

const useRecentStatisticsQuery = (
  options?: UseQueryOptions<RecentStatistic[], HttpError>,
) => useQuery(createKey(), statisticsApi.getRecentStatistics, options);

const baseKey = ['recentStatistics'];
const createKey = () => [...baseKey];

useRecentStatisticsQuery.baseKey = baseKey;
useRecentStatisticsQuery.createKey = createKey;

export default useRecentStatisticsQuery;
