import { renderHook } from '@testing-library/react-hooks/dom';
import statisticsApi from '@src/api/statisticsApi';
import { RecentStatistic } from '@src/types/statistics.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useRecentStatisticsQuery from '../hooks/useRecentStatisticsQuery';

describe('useRecentStatisticsQuery hook', () => {
  const setup = () => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useRecentStatisticsQuery(), {
      wrapper,
    });
  };

  it('should cache data when succeed fetch recent three months statistics', async () => {
    // given
    const sampleRecentStatistics: RecentStatistic[] = [
      {
        year: 2021,
        month: 7,
        blasting: { passRate: 82, improvementRate: -2.5 },
        coating: { passRate: 85, improvementRate: -3.5 },
        originCoating: { passRate: 90, improvementRate: -5 },
      },
      {
        year: 2021,
        month: 8,
        blasting: { passRate: 78, improvementRate: 2 },
        coating: { passRate: 23, improvementRate: -3.5 },
        originCoating: { passRate: 74, improvementRate: 6 },
      },
      {
        year: 2021,
        month: 9,
        blasting: { passRate: 88, improvementRate: 3 },
        coating: { passRate: 77, improvementRate: -3.5 },
        originCoating: { passRate: 94, improvementRate: 11 },
      },
    ];

    statisticsApi.getRecentStatistics = jest
      .fn()
      .mockResolvedValue(sampleRecentStatistics);

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleRecentStatistics);
  });

  it('should set error when failed fetch recent three months statistics', async () => {
    // given
    statisticsApi.getRecentStatistics = jest
      .fn()
      .mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup();

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
