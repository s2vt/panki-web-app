import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import StatisticsOverView from './StatisticsOverView';

export default {
  title: 'Components/statistics/StatisticsOverView',
  component: StatisticsOverView,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story = (args) => <StatisticsOverView {...args} />;

export const Basic = Template.bind({});
