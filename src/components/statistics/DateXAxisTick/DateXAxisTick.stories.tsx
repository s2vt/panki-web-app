import { Meta, Story } from '@storybook/react';
import DateXAxisTick, { DateXAxisTickProps } from './DateXAxisTick';

export default {
  title: 'Components/statistics/DateXAxisTick',
  component: DateXAxisTick,
} as Meta;

const Template: Story<DateXAxisTickProps> = (args) => (
  <DateXAxisTick {...args} />
);

export const Basic = Template.bind({});
