import { useMemo } from 'react';
import { BarTooltipPayload } from 'recharts';
import styled, { css } from 'styled-components';
import { ChartFilterDateType } from '@src/types/statistics.types';

export type DateXAxisTickProps = React.SVGAttributes<SVGElement> & {
  payload?: BarTooltipPayload;
  chartFilterDateType: ChartFilterDateType;
  activeLegend?: string | undefined;
};

const DateXAxisTick = ({
  x,
  y,
  payload,
  chartFilterDateType,
  activeLegend,
}: DateXAxisTickProps) => {
  const legend = payload?.value;

  const isActive = useMemo(
    () => legend === activeLegend,
    [legend, activeLegend],
  );

  const slicedLegendText = useMemo(() => {
    if (chartFilterDateType === ChartFilterDateType.period) {
      const texts = legend?.split('~');
      let week = payload?.index;
      let firstText = '';
      let lastText = '';

      if (texts !== undefined && week !== undefined) {
        firstText = `(${texts[0]}~`;
        lastText = `${texts[1]})`;
        week += 1;
      }

      return [`${week}주차`, firstText, lastText];
    }

    if (chartFilterDateType === ChartFilterDateType.year) {
      return legend?.split(' ') as string[];
    }

    throw new Error(
      `This chartFilterDateType ${chartFilterDateType} is not includes chartFilterDateType`,
    );
  }, [legend]);

  return (
    <g>
      <foreignObject
        x={
          chartFilterDateType === ChartFilterDateType.period
            ? (x as number) - 60
            : (x as number) - 34
        }
        y={y}
        width={chartFilterDateType === ChartFilterDateType.period ? 118 : 66}
        height={66}
      >
        <Box isActive={isActive}>
          {slicedLegendText?.map((text) => (
            <Text key={text}>{text}</Text>
          ))}
        </Box>
      </foreignObject>
    </g>
  );
};

const Box = styled.div<{ isActive: boolean }>`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.colors.primary};

  ${({ isActive, theme }) =>
    isActive &&
    css`
      background: ${theme.colors.primary};
      color: ${theme.colors.white};
    `}
`;

const Text = styled.p`
  font-size: 1.4rem;
  line-height: 1.6rem;
  font-weight: 700;
`;

export default DateXAxisTick;
