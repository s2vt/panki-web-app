import { Meta, Story } from '@storybook/react';
import CoatingPatternBarChart, {
  CoatingPatternBarChartProps,
} from './CoatingPatternBarChart';

export default {
  title: 'Components/statistics/CoatingPatternBarChart',
  component: CoatingPatternBarChart,
} as Meta;

const Template: Story<CoatingPatternBarChartProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <CoatingPatternBarChart {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  subject: '주제',
  title: '타이틀',
  data: [
    {
      legend: '0~1/4 구간',
      '도장 (W.B.TK) 1ST': 130,
      '도장 (W.B.TK) FINAL': 310,
    },
    {
      legend: '1/4~2/4 구간',
      '도장 (W.B.TK) 1ST': 120,
      '도장 (W.B.TK) FINAL': 310,
    },
    {
      legend: '2/4~3/4 구간',
      '도장 (W.B.TK) 1ST': 110,
      '도장 (W.B.TK) FINAL': 310,
    },
    {
      legend: '3/4~4/4 구간',
      '도장 (W.B.TK) 1ST': 96,
      '도장 (W.B.TK) FINAL': 310,
    },
  ],
};
