import { Meta, Story } from '@storybook/react';
import CoatingPatternBarChartTooltip, {
  CoatingPatternBarChartTooltipProps,
} from './CoatingPatternBarChartTooltip';

export default {
  title:
    'Components/statistics/CoatingPatternBarChart/CoatingPatternBarChartTooltip',
  component: CoatingPatternBarChartTooltip,
} as Meta;

const Template: Story<CoatingPatternBarChartTooltipProps> = (args) => (
  <CoatingPatternBarChartTooltip {...args} />
);

export const Basic = Template.bind({});
