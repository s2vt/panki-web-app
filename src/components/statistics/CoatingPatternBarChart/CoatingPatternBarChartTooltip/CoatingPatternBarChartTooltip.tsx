import styled from 'styled-components';
import { TooltipProps } from 'recharts';
import {
  COATING_THICKNESS,
  COATING_THICKNESS_REFERENCE,
  MEASUREMENT_SECTION,
  MICROMETER,
  REFERENCE_CONTRAST,
} from '@src/constants/constantString';

export type CoatingPatternBarChartTooltipProps = TooltipProps<
  number,
  string
> & {
  referenceValue: number;
};

const CoatingPatternBarChartTooltip = (
  props: CoatingPatternBarChartTooltipProps,
) => {
  const { referenceValue, label, payload } = props;

  const combinedThickness =
    payload?.reduce((total, datum) => {
      if (datum.value !== undefined) {
        total += datum.value;
      }

      return total;
    }, 0) ?? 0;

  const referenceContrastValue = Math.round(
    ((combinedThickness - referenceValue) / referenceValue) * 100,
  );

  return (
    <Box>
      <p>{`${MEASUREMENT_SECTION} ${label}`}</p>
      <div>
        {payload?.map((datum) => {
          const { dataKey, value } = datum;

          return (
            <p key={dataKey}>
              {dataKey} {value} {MICROMETER}
            </p>
          );
        })}
      </div>
      <div>
        <p>
          {COATING_THICKNESS_REFERENCE} {referenceValue} {MICROMETER}
        </p>
        <p>
          {COATING_THICKNESS} {combinedThickness} {MICROMETER}
        </p>
        <p>
          {REFERENCE_CONTRAST} {referenceContrastValue > 0 && '+'}
          {referenceContrastValue}%
        </p>
      </div>
    </Box>
  );
};

const Box = styled.div`
  min-width: 11.4rem;
  padding: 1.1rem 1rem;
  display: flex;
  flex-direction: column;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.white};
  font-size: 1.4rem;
  color: ${({ theme }) => theme.colors.primary};
  gap: 2rem;
`;

export default CoatingPatternBarChartTooltip;
