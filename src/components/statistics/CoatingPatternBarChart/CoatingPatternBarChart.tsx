import { memo, useCallback, useMemo, useState } from 'react';
import {
  Bar,
  BarChart,
  ChartMouseMoveParams,
  Label,
  ReferenceLine,
  Tooltip,
  XAxis,
  YAxis,
  CartesianGrid,
  LabelList,
} from 'recharts';
import styled from 'styled-components';
import {
  MEASUREMENT_SECTION,
  TARGET,
  THICKNESS,
} from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { COATING_PATTERN_BAR_CHART_COLORS } from '@src/styles/chartColors';
import { colors } from '@src/styles/theme';
import {
  ChartData,
  CoatingFilterTask,
  convertCoatingFilterTaskText,
  getChartDataKeys,
} from '@src/types/statistics.types';
import ChartBase from '../ChartBase';
import ChartReferenceLabel from '../ChartReferenceLabel';
import ChartSubject from '../ChartSubject';
import CheckboxLegend, { CheckboxLegendItem } from '../CheckboxLegend';
import CoatingPatternBarChartTooltip from './CoatingPatternBarChartTooltip';
import CoatingPatternBarChartXAxisTick from './CoatingPatternBarChartXAxisTick';

export type CoatingPatternBarChartProps = {
  subject: string;
  title: string;
  data: ChartData[];
};

const REFERENCE_VALUE = 160;

const CoatingPatternBarChart = ({
  subject,
  title,
  data,
}: CoatingPatternBarChartProps) => {
  const [activeLegend, setActiveLegend] = useState<string>();

  const dataKeys = useMemo(() => getChartDataKeys(data), [data]);
  const maximumValue = useMemo(() => {
    const result = data.map((datum) => {
      const { legend, ...rest } = datum;

      const values = Object.values(rest) as number[];

      return values.reduce((total: number, value: number) => total + value, 0);
    });

    return Math.max(...(result.flat() as number[]));
  }, [data]);

  const targetLegendItem: CheckboxLegendItem = useMemo(
    () => ({
      id: TARGET,
      icon: 'line',
      isSelectable: false,
      color: colors.brightBlue,
      type: 'target',
    }),
    [],
  );

  const legendItems: CheckboxLegendItem[] = useMemo(
    () =>
      dataKeys
        .map(
          (dataKey, index) =>
            ({
              id: dataKey,
              color: COATING_PATTERN_BAR_CHART_COLORS[index],
              icon: 'circle',
              isSelectable: true,
              type: 'value',
            } as CheckboxLegendItem),
        )
        .concat(targetLegendItem),
    [dataKeys],
  );

  const { selectedItems, toggleItem, isSelected, setSelectedItems } =
    useSelectItems<CheckboxLegendItem>(legendItems);

  const referenceValue = useMemo(
    () =>
      selectedItems.filter((item) => item.type === 'value').length > 1
        ? REFERENCE_VALUE * 2
        : REFERENCE_VALUE,
    [selectedItems],
  );

  const yAxisTicks = useMemo(() => {
    const TICK_INTERVAL = 150;
    const tickCounts = Math.ceil(maximumValue / TICK_INTERVAL) + 1;

    return Array.from({ length: tickCounts }).map(
      (_, index) => index * TICK_INTERVAL,
    );
  }, [maximumValue]);

  const handleTurnOffAllLegendsClick = useCallback(() => {
    setSelectedItems([]);
  }, [setSelectedItems]);

  const handleBarChartMouseMove = ({
    isTooltipActive,
    activeLabel,
  }: ChartMouseMoveParams) => {
    if (!isTooltipActive) {
      setActiveLegend(undefined);
      return;
    }

    setActiveLegend(activeLabel);
  };

  const sortedSelectedItems = useMemo(
    () =>
      selectedItems.sort((a, b) => {
        if (a.id === convertCoatingFilterTaskText(CoatingFilterTask.first)) {
          return -1;
        }

        if (b.id === convertCoatingFilterTaskText(CoatingFilterTask.first)) {
          return 1;
        }

        return 0;
      }),
    [selectedItems],
  );

  return (
    <Box>
      <ChartSubject
        subject={subject}
        isLegendButtonEnable
        onTurnOffAllLegendsClick={handleTurnOffAllLegendsClick}
      />
      <ChartBase.ChartWrapper>
        <ChartBase.TitleSection>{title}</ChartBase.TitleSection>
        <BarChart
          width={929}
          height={441}
          data={data}
          margin={{ top: 63.5, left: 8, right: 8, bottom: 52 }}
          barSize={40}
          barGap={15}
          onMouseMove={handleBarChartMouseMove}
        >
          <CartesianGrid
            vertical={false}
            strokeDasharray="5 3"
            stroke={colors.primary}
          />
          <XAxis
            type="category"
            dataKey="legend"
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            tick={
              dataKeys.length > 0 ? (
                <CoatingPatternBarChartXAxisTick activeLegend={activeLegend} />
              ) : (
                false
              )
            }
            tickMargin={26}
          >
            <Label
              value={MEASUREMENT_SECTION}
              position={{ x: 30, y: 62 }}
              fontSize="2rem"
              fill={colors.primary}
            />
          </XAxis>
          <YAxis
            type="number"
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            tick={{ fill: colors.primary }}
            domain={[0, maximumValue]}
            ticks={yAxisTicks}
          >
            <Label
              value={THICKNESS}
              position={{ x: 32, y: 303 }}
              fontSize="2rem"
              fill={colors.primary}
            />
          </YAxis>
          <Tooltip
            cursor={false}
            isAnimationActive={false}
            content={
              <CoatingPatternBarChartTooltip referenceValue={referenceValue} />
            }
          />
          <ReferenceLine
            y={referenceValue}
            stroke={colors.brightBlue}
            strokeWidth={6}
            strokeDasharray="3 3"
          >
            <Label
              position="left"
              content={
                <ChartReferenceLabel
                  boxColor={colors.primary}
                  label={`${referenceValue}`}
                />
              }
            />
          </ReferenceLine>
          {sortedSelectedItems.map(
            (selectedItem) =>
              selectedItem.type === 'value' && (
                <Bar
                  key={selectedItem.id}
                  dataKey={selectedItem.id}
                  barSize={50}
                  fill={selectedItem.color}
                  isAnimationActive={false}
                  stackId="patternBar"
                >
                  <LabelList
                    dataKey={selectedItem.id}
                    position="insideTop"
                    fontSize="1.4rem"
                    fill={colors.white}
                  />
                  <LabelList
                    position="top"
                    fontSize="1.4rem"
                    fill={colors.primary}
                    fontWeight={700}
                  />
                </Bar>
              ),
          )}
        </BarChart>
        {legendItems.length > 0 && (
          <CheckboxLegend
            isSelected={isSelected}
            onItemClick={toggleItem}
            items={legendItems}
          />
        )}
      </ChartBase.ChartWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
`;

export default memo(CoatingPatternBarChart);
