import { BarTooltipPayload } from 'recharts';
import styled, { css } from 'styled-components';

export type CoatingPatternBarChartXAxisTickProps =
  React.SVGAttributes<SVGElement> & {
    payload?: BarTooltipPayload;
    activeLegend: string | undefined;
  };

const CoatingPatternBarChartXAxisTick = ({
  x,
  y,
  payload,
  activeLegend,
}: CoatingPatternBarChartXAxisTickProps) => {
  const legend = payload?.value;

  return (
    <g>
      <foreignObject x={(x as number) - 60} y={y} width={118} height={41}>
        <Box isActive={activeLegend === legend}>
          <Text>{legend}</Text>
        </Box>
      </foreignObject>
    </g>
  );
};

const Box = styled.div<{ isActive: boolean }>`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.colors.primary};

  ${({ isActive, theme }) =>
    isActive &&
    css`
      background: ${theme.colors.primary};
      color: ${theme.colors.white};
    `}
`;

const Text = styled.p`
  font-size: 1.6rem;
  line-height: 1.6rem;
  font-weight: 700;
`;

export default CoatingPatternBarChartXAxisTick;
