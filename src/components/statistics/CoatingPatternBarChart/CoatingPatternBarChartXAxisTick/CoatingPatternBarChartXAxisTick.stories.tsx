import { Meta, Story } from '@storybook/react';
import CoatingPatternBarChartXAxisTick, {
  CoatingPatternBarChartXAxisTickProps,
} from './CoatingPatternBarChartXAxisTick';

export default {
  title:
    'Components/statistics/CoatingPatternBarChart/CoatingPatternBarChartXAxisTick',
  component: CoatingPatternBarChartXAxisTick,
} as Meta;

const Template: Story<CoatingPatternBarChartXAxisTickProps> = (args) => (
  <CoatingPatternBarChartXAxisTick {...args} />
);

export const Basic = Template.bind({});
