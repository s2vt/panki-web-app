import { Meta, Story } from '@storybook/react';
import ChartBase, { ChartBaseProps } from './ChartBase';

export default {
  title: 'Components/Statistics/ChartBase/ChartBase',
  component: ChartBase.ChartWrapper,
} as Meta;

const Template: Story<ChartBaseProps> = (args) => (
  <ChartBase.ChartWrapper {...args} />
);

export const Basic = Template.bind({});
