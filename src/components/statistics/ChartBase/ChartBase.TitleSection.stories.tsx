import { Meta, Story } from '@storybook/react';
import ChartBase from './ChartBase';

export default {
  title: 'Components/Statistics/ChartBase/TitleSection',
  component: ChartBase.TitleSection,
} as Meta;

const Template: Story = (args) => <ChartBase.TitleSection {...args} />;

export const Basic = Template.bind({});
