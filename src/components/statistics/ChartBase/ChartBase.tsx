import { memo } from 'react';
import styled from 'styled-components';

export type ChartBaseProps = {};

const TitleSection = memo(styled.div`
  width: 100%;
  height: 6.7rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
  border-bottom: 1px solid ${({ theme }) => theme.colors.primary};
`);

const ChartWrapper = styled.div`
  width: 100%;
  padding-bottom: 2.9rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
`;

export default { TitleSection, ChartWrapper };
