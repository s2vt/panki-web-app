import { Meta, Story } from '@storybook/react';
import ChartReferenceLabel, {
  ChartReferenceLabelProps,
} from './ChartReferenceLabel';

export default {
  title: 'Components/statistics/ChartReferenceLabel',
  component: ChartReferenceLabel,
} as Meta;

const Template: Story<ChartReferenceLabelProps> = (args) => (
  <ChartReferenceLabel {...args} />
);

export const Basic = Template.bind({});
