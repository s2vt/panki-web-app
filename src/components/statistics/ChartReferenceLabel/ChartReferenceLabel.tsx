import { CartesianViewBox } from 'recharts/types/util/types';
import styled from 'styled-components';
import Icon from '@src/components/common/Icon';

export type ChartReferenceLabelProps = {
  viewBox?: CartesianViewBox;
  boxColor: string;
  label: string;
};

const ChartReferenceLabel = ({
  label,
  boxColor,
  viewBox,
}: ChartReferenceLabelProps) => {
  if (
    viewBox === undefined ||
    viewBox.x === undefined ||
    viewBox.y === undefined
  ) {
    return null;
  }

  const { x, y } = viewBox;

  return (
    <g>
      <foreignObject x={x - 60} y={y - 13} width={100} height={100}>
        <Box>
          <StyledIcon icon="chartTargetLabelIcon" $boxColor={boxColor} />
          <Label>{label}</Label>
        </Box>
      </foreignObject>
    </g>
  );
};

const Box = styled.div`
  width: 5.65rem;
  height: 2.6rem;
  position: relative;
`;

const StyledIcon = styled(Icon)<{ $boxColor: string }>`
  position: absolute;
  z-index: 1;

  path {
    fill: ${({ $boxColor }) => $boxColor};
  }
`;

const Label = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  ${({ theme }) => theme.layout.flexCenterLayout};
  z-index: 2;
  font-size: 1.6rem;
  color: ${({ theme }) => theme.colors.white};
`;

export default ChartReferenceLabel;
