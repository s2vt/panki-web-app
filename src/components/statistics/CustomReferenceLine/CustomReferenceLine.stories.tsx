import { Meta, Story } from '@storybook/react';
import CustomReferenceLine, {
  CustomReferenceLineProps,
} from './CustomReferenceLine';

export default {
  title: 'Components/Statistics/CustomReferenceLine',
  component: CustomReferenceLine,
} as Meta;

const Template: Story<CustomReferenceLineProps> = (args) => (
  <CustomReferenceLine {...args} />
);

export const Basic = Template.bind({});
