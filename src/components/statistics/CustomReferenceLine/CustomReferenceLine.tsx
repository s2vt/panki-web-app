import { Label, ReferenceLine } from 'recharts';
import ChartReferenceLabel from '../ChartReferenceLabel';

export type CustomReferenceLineProps = {
  referenceValue: number | undefined;
  label: string;
  lineColor: string;
  labelBoxColor: string;
};

const CustomReferenceLine = ({
  referenceValue,
  label,
  lineColor,
  labelBoxColor,
}: CustomReferenceLineProps) => (
  <ReferenceLine
    y={referenceValue}
    stroke={lineColor}
    strokeWidth={6}
    strokeDasharray="3 3"
  >
    <Label
      position="left"
      content={<ChartReferenceLabel boxColor={labelBoxColor} label={label} />}
    />
  </ReferenceLine>
);

export default CustomReferenceLine;
