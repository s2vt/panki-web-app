import { Meta, Story } from '@storybook/react';
import BlockStatisticsTableRow, {
  BlockStatisticsTableRowProps,
} from './BlockStatisticsTableRow';

export default {
  title: 'Components/Statistics/BlockStatisticsTable/BlockStatisticsTableRow',
  component: BlockStatisticsTableRow,
} as Meta;

const Template: Story<BlockStatisticsTableRowProps> = (args) => (
  <BlockStatisticsTableRow {...args} />
);

export const Basic = Template.bind({});
