/* eslint-disable react/no-array-index-key */
import styled from 'styled-components';
import {
  BlockStatistic,
  createInspectionStatisticTableData,
} from '@src/types/statistics.types';

export type BlockStatisticsTableRowProps = {
  blockStatistic: BlockStatistic;
};

const BlockStatisticsTableRow = ({
  blockStatistic,
}: BlockStatisticsTableRowProps) => {
  const { shipName, blockName } = blockStatistic;

  const { blastingStatistics, coatingStatistics } =
    createInspectionStatisticTableData(blockStatistic);

  return (
    <TableRow>
      <TableNameData>{shipName}</TableNameData>
      <TableNameData>{blockName}</TableNameData>
      {blastingStatistics.concat(coatingStatistics).map((value, index) => (
        <TableValueData key={index}>{value ?? '-'}</TableValueData>
      ))}
    </TableRow>
  );
};

const TableRow = styled.tr`
  font-size: 1.6rem;
`;

const TableData = styled.td`
  border: 1px solid ${({ theme }) => theme.colors.primary};
  padding: 1.2rem 0;
`;

const TableNameData = styled(TableData)`
  background: ${({ theme }) => theme.colors.tableBackground};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  padding: 1.2rem 0;
`;

const TableValueData = styled(TableData)`
  background: ${({ theme }) => theme.colors.white};
`;

export default BlockStatisticsTableRow;
