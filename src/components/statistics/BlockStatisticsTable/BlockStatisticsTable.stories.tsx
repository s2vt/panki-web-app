import { Meta, Story } from '@storybook/react';
import BlockStatisticsTable, {
  BlockStatisticsTableProps,
} from './BlockStatisticsTable';

export default {
  title: 'Components/Statistics/BlockStatisticsTable',
  component: BlockStatisticsTable,
} as Meta;

const Template: Story<BlockStatisticsTableProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <BlockStatisticsTable {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  blastingTitle: '블라스팅',
  coatingTitle: '도장',
  blockStatistics: [
    {
      blockName: '1B11',
      shipName: '1234',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: {
        inspectionCount: 10,
        measureCount: 23.1,
        passRate: 23.1,
        average: 23.1,
        standardDeviation: 500,
        lowerRate: 5.5,
        upperRate: 4.5,
        originRate: 90,
      },
    },
    {
      blockName: '1B12',
      shipName: '1234',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: {
        inspectionCount: 10,
        measureCount: 23.1,
        passRate: 23.1,
        average: 23.1,
        standardDeviation: 500,
        lowerRate: 5.5,
        upperRate: 4.5,
        originRate: 90,
      },
    },
    {
      blockName: '1S11',
      shipName: '1111',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: {
        inspectionCount: 10,
        measureCount: 23.1,
        passRate: 23.1,
        average: 23.1,
        standardDeviation: 500,
        lowerRate: 5.5,
        upperRate: 4.5,
        originRate: 90,
      },
    },
  ],
};
