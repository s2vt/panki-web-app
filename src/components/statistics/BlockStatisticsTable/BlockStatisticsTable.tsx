/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import styled from 'styled-components';
import { BLOCK_NAME, SHIP } from '@src/constants/constantString';
import { BlockStatistic } from '@src/types/statistics.types';
import BlockStatisticsTableRow from './BlockStatisticsTableRow';

export type BlockStatisticsTableProps = {
  blastingTitle: string;
  coatingTitle: string;
  blockStatistics: BlockStatistic[];
};

const COLUMNS = [
  '총 검사수',
  '합격률(%)',
  '총 검사수',
  '합격률(%)',
  '총 측정수',
  '평균(μm)',
  '표준편차(μm)',
  '저도막(%)',
  '과도막(%)',
  '정도막(%)',
];

const BlockStatisticsTable = ({
  blastingTitle,
  coatingTitle,
  blockStatistics,
}: BlockStatisticsTableProps) => (
  <Table>
    <thead>
      <TableRow>
        <NameTableHeaderCell rowSpan={2}>{SHIP}</NameTableHeaderCell>
        <NameTableHeaderCell rowSpan={2}>{BLOCK_NAME}</NameTableHeaderCell>
        <TableHeaderCell colSpan={2}>{blastingTitle}</TableHeaderCell>
        <TableHeaderCell colSpan={8}>{coatingTitle}</TableHeaderCell>
      </TableRow>
      <TableRow>
        {COLUMNS.map((column, index) => (
          <SmallTableHeaderCell key={index}>{column}</SmallTableHeaderCell>
        ))}
      </TableRow>
    </thead>
    <tbody>
      {blockStatistics.map((blockStatistic) => (
        <BlockStatisticsTableRow
          key={blockStatistic.shipName + blockStatistic.blockName}
          blockStatistic={blockStatistic}
        />
      ))}
    </tbody>
  </Table>
);

const Table = styled.table`
  width: 100%;
  background: 1px solid ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

const TableRow = styled.tr`
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.tableBackground};
  font-size: 1.8rem;
  font-weight: 700;
`;

const TableHeaderCell = styled.th`
  padding: 1rem 0;
  vertical-align: middle;
  border: 1px solid ${({ theme }) => theme.colors.primary};
`;

const NameTableHeaderCell = styled(TableHeaderCell)`
  width: 10.8rem;
`;

const SmallTableHeaderCell = styled(TableHeaderCell)`
  font-size: 1.4rem;
`;

export default memo(BlockStatisticsTable);
