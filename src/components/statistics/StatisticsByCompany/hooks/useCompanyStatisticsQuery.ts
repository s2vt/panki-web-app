import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import statisticsApi from '@src/api/statisticsApi';
import {
  ChartFilterParams,
  CompanyStatistic,
  ShipChartFilterParams,
} from '@src/types/statistics.types';

const useCompanyStatisticsQuery = (
  {
    companyId,
    ...chartFilterParams
  }: { companyId: string } & ShipChartFilterParams,
  options?: UseQueryOptions<CompanyStatistic, HttpError>,
) =>
  useQuery(
    createKey(companyId, chartFilterParams),
    () => statisticsApi.getCompanyStatistics(companyId, chartFilterParams),
    options,
  );

const baseKey = ['companyStatistics'];
const createKey = (companyId: string, chartFilterParams: ChartFilterParams) => [
  ...baseKey,
  companyId,
  chartFilterParams,
];

useCompanyStatisticsQuery.baseKey = baseKey;
useCompanyStatisticsQuery.createKey = createKey;

export default useCompanyStatisticsQuery;
