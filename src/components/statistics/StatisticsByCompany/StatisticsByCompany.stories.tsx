import { Meta, Story } from '@storybook/react';
import StatisticsByCompany, {
  StatisticsByCompanyProps,
} from './StatisticsByCompany';

export default {
  title: 'Components/Statistics/StatisticsByCompany',
  component: StatisticsByCompany,
} as Meta;

const Template: Story<StatisticsByCompanyProps> = (args) => (
  <StatisticsByCompany {...args} />
);

export const Basic = Template.bind({});
