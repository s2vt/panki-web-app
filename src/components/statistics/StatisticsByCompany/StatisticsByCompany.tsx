import { useCallback, useState, useMemo } from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import {
  BLASTING_AND_COATING_PASS_RATE,
  COATING_DISTRIBUTION_IN_PERIOD,
  COATING_PATTERN_BY_SECTION,
  FAIL_DATA_FETCH,
  GO_TO_SEMANTIC_ANALYSIS,
  ORIGIN_COATING_RATE,
  SHIP_NUMBER,
  STATISTICS_BY_SHOP_AND_BLOCK,
} from '@src/constants/constantString';
import useSubcontractorCompanies from '@src/hooks/company/useSubcontractorCompanies';
import useUserSelector from '@src/hooks/user/useUserSelector';
import {
  convertBlastingFilterTaskText,
  convertCoatingFilterTaskText,
  createCoatingPatternChartData,
  createCompanyOriginCoatingRateChartData,
  createCompanyPassRateChartData,
} from '@src/types/statistics.types';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import useMask from '@src/hooks/common/useMask';
import useChartFilterSelector from '@src/hooks/statistics/useChartFilterSelector';
import useChartFilterAction from '@src/hooks/statistics/useChartFilterAction';
import Divider from '@src/components/common/Divider';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import ErrorBox from '@src/components/common/ErrorBox';
import SearchInput from '@src/components/common/SearchInput';
import { Box } from '@mui/material';
import BlockStatisticsTable from '../BlockStatisticsTable';
import ChartFilter from '../ChartFilter';
import CoatingDistributionChart from '../CoatingDistributionChart';
import CoatingPatternBarChart from '../CoatingPatternBarChart';
import CompanyPassRateLineChart from '../CompanyPassRateLineChart';
import StatisticsPagesBreadCrumbs from '../StatisticsPagesBreadCrumbs';
import TableSubject from '../TableSubject';
import InquiryTargetBox from '../InquiryTargetBox';
import useCompanyStatisticsQuery from './hooks/useCompanyStatisticsQuery';

export type StatisticsByCompanyProps = {};

const StatisticsByCompany = () => {
  const history = useHistory();

  const [searchShipName, setSearchShipName] = useState('');
  const { chartFilterParams, searchQuery, subcontractorCompany } =
    useChartFilterSelector();
  const { setSearchQuery } = useChartFilterAction();
  const { companyId } = useUserSelector();

  const { maskEnglishNumber } = useMask();

  const { ctgType, blastingType } = chartFilterParams;

  const {
    data: subcontractorCompanies,
    isFetching: isCompaniesFetching,
    isError: isCompaniesQueryError,
    isSuccess: isCompaniesQuerySuccess,
    refetch: refetchCompanies,
  } = useSubcontractorCompanies(companyId ?? '', {
    enabled: companyId !== '',
  });

  const handleInquiryTargetButtonClick = useCallback(() => {
    history.push(ROUTE_PATHS.statisticsSemanticAnalysis);
  }, [history]);

  const handleSearchInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const maskedValue = maskEnglishNumber(e.target.value);
      e.target.value = maskedValue;
      setSearchShipName(maskedValue);
    },
    [setSearchShipName, maskEnglishNumber],
  );

  const handleSearchEnter = useCallback(() => {
    setSearchQuery(searchShipName);
    refetchStatistics();
  }, [setSearchQuery, searchShipName]);

  const renderSearchInput = useCallback(
    () => (
      <SearchInput
        width="12.6rem"
        isEnableRadius={false}
        onChange={handleSearchInputChange}
        onSearchEnter={handleSearchEnter}
        placeholder={SHIP_NUMBER}
      />
    ),
    [handleSearchInputChange, handleSearchEnter],
  );

  const {
    data: companyStatistics,
    isFetching: isStatisticsFetching,
    isError: isStatisticsQueryError,
    isSuccess: isStatisticsQuerySuccess,
    refetch: refetchStatistics,
  } = useCompanyStatisticsQuery(
    {
      companyId: subcontractorCompany?.id ?? '',
      ...chartFilterParams,
      searchShipName: searchQuery,
    },
    {
      enabled:
        subcontractorCompany?.id !== undefined &&
        subcontractorCompanies !== undefined,
    },
  );

  const passRateChartData = useMemo(
    () =>
      createCompanyPassRateChartData({
        rateCharts: companyStatistics?.rateCharts ?? [],
        blastingTask: chartFilterParams.blastingType,
        coatingTask: chartFilterParams.ctgType,
      }),
    [createCompanyPassRateChartData, companyStatistics],
  );

  const originCoatingRateChartData = useMemo(
    () =>
      createCompanyOriginCoatingRateChartData(
        companyStatistics?.rateCharts ?? [],
      ),
    [createCompanyOriginCoatingRateChartData, companyStatistics],
  );

  const coatingPatternChartData = useMemo(
    () => createCoatingPatternChartData(companyStatistics?.pattern ?? []),
    [createCoatingPatternChartData, companyStatistics],
  );

  return (
    <Box>
      <StatisticsPagesBreadCrumbs />
      <ChartFilter
        renderSearchInput={renderSearchInput}
        searchQuery={searchShipName}
        onSubmit={refetchStatistics}
      />
      <StyledDivider orientation="horizontal" />
      <ChartSection>
        {isCompaniesFetching && <LoadingSpinner />}
        {!isCompaniesFetching && isCompaniesQueryError && (
          <ErrorBox error={FAIL_DATA_FETCH} onRefetchClick={refetchCompanies} />
        )}
        {subcontractorCompanies !== undefined &&
          !isCompaniesFetching &&
          isCompaniesQuerySuccess && (
            <>
              <InquiryTargetBox
                subcontractorCompanies={subcontractorCompanies}
                buttonLabel={GO_TO_SEMANTIC_ANALYSIS}
                onButtonClick={handleInquiryTargetButtonClick}
              />
              {isStatisticsFetching && <LoadingSpinner />}
              {!isStatisticsFetching && isStatisticsQueryError && (
                <ErrorBox
                  error={FAIL_DATA_FETCH}
                  onRefetchClick={refetchStatistics}
                />
              )}
              {companyStatistics !== undefined &&
                subcontractorCompany !== undefined &&
                !isStatisticsFetching &&
                isStatisticsQuerySuccess && (
                  <>
                    <div>
                      <TableSubject subject={STATISTICS_BY_SHOP_AND_BLOCK} />
                      <BlockStatisticsTable
                        blastingTitle={convertBlastingFilterTaskText(
                          blastingType,
                        )}
                        coatingTitle={convertCoatingFilterTaskText(ctgType)}
                        blockStatistics={companyStatistics.statisticInfos}
                      />
                    </div>
                    <CompanyPassRateLineChart
                      data={passRateChartData}
                      subject={BLASTING_AND_COATING_PASS_RATE}
                      companyName={subcontractorCompany.companyName}
                      chartFilterDateType={chartFilterParams.searchDateType}
                    />
                    <CompanyPassRateLineChart
                      data={originCoatingRateChartData}
                      subject={ORIGIN_COATING_RATE}
                      companyName={subcontractorCompany.companyName}
                      chartFilterDateType={chartFilterParams.searchDateType}
                    />
                    <CoatingPatternBarChart
                      data={coatingPatternChartData}
                      title={convertCoatingFilterTaskText(ctgType)}
                      subject={COATING_PATTERN_BY_SECTION}
                    />
                    <CoatingDistributionChart
                      coatingDistributionData={companyStatistics.distribution}
                      companyName={subcontractorCompany.companyName}
                      subject={COATING_DISTRIBUTION_IN_PERIOD}
                      coatingFilterTask={chartFilterParams.ctgType}
                    />
                  </>
                )}
            </>
          )}
      </ChartSection>
    </Box>
  );
};

const StyledDivider = styled(Divider)`
  margin: 3.8rem 0 2.6rem;
`;

const ChartSection = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4.8rem;
`;

export default StatisticsByCompany;
