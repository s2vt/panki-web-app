import { renderHook } from '@testing-library/react-hooks/dom';
import statisticsApi from '@src/api/statisticsApi';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterType,
  CoatingFilterTask,
  CompanyStatistic,
  ShipChartFilterParams,
} from '@src/types/statistics.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useCompanyStatisticsQuery from '../hooks/useCompanyStatisticsQuery';

describe('useCompanyStatisticsQuery hook', () => {
  const setup = (
    companyId: string,
    chartFilterParams: ShipChartFilterParams,
  ) => {
    const { wrapper } = prepareWrapper();

    return renderHook(
      () => useCompanyStatisticsQuery({ companyId, ...chartFilterParams }),
      { wrapper },
    );
  };

  it('should cache data when succeed fetch company statistics', async () => {
    // given
    const companyId = '1';

    const chartFilterParams: ShipChartFilterParams = {
      blastingType: BlastingFilterTask.inAll,
      ctgType: CoatingFilterTask.all,
      searchShipName: 'test',
      searchDateType: ChartFilterDateType.year,
      searchType: ChartFilterType.all,
    };

    const sampleCompanyStatistics: CompanyStatistic = {
      statisticInfos: [
        {
          shipName: '1234',
          blasting: { inspectionCount: 1, passRate: 1 },
          coating: {
            average: 1,
            inspectionCount: 1,
            lowerRate: 1,
            measureCount: 1,
            originRate: 1,
            passRate: 1,
            standardDeviation: 1,
            upperRate: 1,
          },
          blockName: '1B11',
        },
      ],
      distribution: { average: 10, data: [{ legend: 50, value: 100 }] },
      pattern: [{ legend: 'legend', first: 0, second: 0 }],
      rateCharts: [
        {
          columnName: '2020년 1월',
          blastingRate: 80,
          ctgRate: 80,
          originDftRate: 80,
        },
      ],
    };

    statisticsApi.getCompanyStatistics = jest
      .fn()
      .mockResolvedValue(sampleCompanyStatistics);

    // when
    const { result, waitFor } = setup(companyId, chartFilterParams);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(sampleCompanyStatistics);
  });

  it('should set error when failed fetch company statistics', async () => {
    // given
    const companyId = '1';

    const chartFilterParams: ShipChartFilterParams = {
      blastingType: BlastingFilterTask.inAll,
      ctgType: CoatingFilterTask.all,
      searchShipName: 'test',
      searchDateType: ChartFilterDateType.year,
      searchType: ChartFilterType.all,
    };

    statisticsApi.getCompanyStatistics = jest
      .fn()
      .mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(companyId, chartFilterParams);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
