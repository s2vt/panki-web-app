import { Meta, Story } from '@storybook/react';
import ChartSubjectBase from './ChartSubjectBase';

export default {
  title: 'Components/stastics/ChartSubjectBase/Subject',
  component: ChartSubjectBase.Subject,
} as Meta;

const Template: Story = (args) => (
  <ChartSubjectBase.Subject {...args}>
    <></>
  </ChartSubjectBase.Subject>
);

export const Basic = Template.bind({});
