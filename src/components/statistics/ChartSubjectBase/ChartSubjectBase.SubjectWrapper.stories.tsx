import { Meta, Story } from '@storybook/react';
import ChartSubjectBase from './ChartSubjectBase';

export default {
  title: 'Components/stastics/ChartSubjectBase/SubjectWrapper',
  component: ChartSubjectBase.SubjectWrapper,
} as Meta;

const Template: Story = (args) => <ChartSubjectBase.SubjectWrapper {...args} />;

export const Basic = Template.bind({});
