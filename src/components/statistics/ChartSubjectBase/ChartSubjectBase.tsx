import styled from 'styled-components';

export type ChartSubjectBaseProps = {};

const SubjectSection = styled.div`
  margin-bottom: 1.8rem;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const SubjectWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  gap: 1.3rem;
`;

const SubjectBox = styled.div`
  width: 1.4rem;
  height: 3rem;
  background: ${({ theme }) => theme.colors.primary};
`;

const SubjectText = styled.p`
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

type SubjectProps = { children: React.ReactNode };
const Subject = ({ children }: SubjectProps) => (
  <>
    <SubjectBox />
    <SubjectText>{children}</SubjectText>
  </>
);

export default { SubjectSection, SubjectWrapper, Subject };
