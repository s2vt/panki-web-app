import { Meta, Story } from '@storybook/react';
import ChartSubjectBase from './ChartSubjectBase';

export default {
  title: 'Components/stastics/ChartSubjectBase/SubjectSection',
  component: ChartSubjectBase.SubjectSection,
} as Meta;

const Template: Story = (args) => <ChartSubjectBase.SubjectSection {...args} />;

export const Basic = Template.bind({});
