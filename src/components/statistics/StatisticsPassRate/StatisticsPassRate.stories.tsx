import { Meta, Story } from '@storybook/react';
import InspectionPassRate from './StatisticsPassRate';

export default {
  title: 'Components/statistics/InspectionPassRate',
  component: InspectionPassRate,
} as Meta;

const Template: Story = (args) => <InspectionPassRate {...args} />;

export const Basic = Template.bind({});
