import { renderHook } from '@testing-library/react-hooks/dom';
import statisticsApi from '@src/api/statisticsApi';
import {
  BlastingFilterTask,
  ChartFilterDateType,
  ChartFilterType,
  CoatingFilterTask,
  CompanyChartFilterParams,
  YardStatistic,
} from '@src/types/statistics.types';
import prepareWrapper from '@src/test/utils/prepareWrapper';
import useYardStatisticsQuery from '../hooks/useYardStatisticsQuery';

describe('useYardStatisticsQuery hook', () => {
  const setup = (chartFilterParams: CompanyChartFilterParams) => {
    const { wrapper } = prepareWrapper();

    return renderHook(() => useYardStatisticsQuery(chartFilterParams), {
      wrapper,
    });
  };

  it('should cache data when succeed fetch pass rate statistics', async () => {
    // given
    const chartFilterParams: CompanyChartFilterParams = {
      blastingType: BlastingFilterTask.inAll,
      ctgType: CoatingFilterTask.all,
      searchCompanyName: 'test',
      searchDateType: ChartFilterDateType.year,
      searchType: ChartFilterType.all,
    };

    const samplePassRateStatistics: YardStatistic[] = [
      {
        companyId: '1',
        companyName: 'company1',
        data: [
          {
            blasting: { inspectionCount: 1, passRate: 1 },
            coating: {
              average: 1,
              inspectionCount: 1,
              lowerRate: 1,
              measureCount: 1,
              originRate: 1,
              passRate: 1,
              standardDeviation: 1,
              upperRate: 1,
            },
            columnName: '2020년 1월',
          },
        ],
      },
    ];

    statisticsApi.getYardStatistics = jest
      .fn()
      .mockResolvedValue(samplePassRateStatistics);

    // when
    const { result, waitFor } = setup(chartFilterParams);

    await waitFor(() => result.current.isSuccess);

    // then
    expect(result.current.data).toEqual(samplePassRateStatistics);
  });

  it('should set error when failed fetch pass rate statistics', async () => {
    // given
    const chartFilterParams: CompanyChartFilterParams = {
      blastingType: BlastingFilterTask.inAll,
      ctgType: CoatingFilterTask.all,
      searchCompanyName: 'test',
      searchDateType: ChartFilterDateType.year,
      searchType: ChartFilterType.all,
    };

    statisticsApi.getYardStatistics = jest.fn().mockRejectedValue(new Error());

    // when
    const { result, waitFor } = setup(chartFilterParams);

    await waitFor(() => result.current.isError);

    // then
    expect(result.current.error).not.toBeNull();
  });
});
