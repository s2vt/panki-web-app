import { useQuery, UseQueryOptions } from 'react-query';
import HttpError from '@src/api/model/HttpError';
import statisticsApi from '@src/api/statisticsApi';
import {
  CompanyChartFilterParams,
  YardStatistic,
} from '@src/types/statistics.types';

const useYardStatisticsQuery = (
  chartFilterParams: CompanyChartFilterParams,
  options?: UseQueryOptions<YardStatistic[], HttpError>,
) =>
  useQuery(
    createKey(chartFilterParams),
    () => statisticsApi.getYardStatistics(chartFilterParams),
    options,
  );

const baseKey = ['yardStatistics'];
const createKey = (chartFilterParams: CompanyChartFilterParams) => [
  ...baseKey,
  chartFilterParams,
];

useYardStatisticsQuery.baseKey = baseKey;
useYardStatisticsQuery.createKey = createKey;

export default useYardStatisticsQuery;
