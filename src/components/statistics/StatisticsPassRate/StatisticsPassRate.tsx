import React, { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import {
  BLASTING_INSPECTION_PASS_RATE,
  COATING_INSPECTION_PASS_RATE,
  FAIL_DATA_FETCH,
  ORIGIN_COATING_RATE,
  SEARCH_COMPANY,
  STATISTICS_BY_WORKER,
} from '@src/constants/constantString';
import {
  calculateYardStatistic,
  convertBlastingFilterTaskText,
  convertCoatingFilterTaskText,
  createPassRateLineChartData,
} from '@src/types/statistics.types';
import Divider from '@src/components/common/Divider';
import ErrorBox from '@src/components/common/ErrorBox';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import SearchInput from '@src/components/common/SearchInput';
import useChartFilterSelector from '@src/hooks/statistics/useChartFilterSelector';
import useChartFilterAction from '@src/hooks/statistics/useChartFilterAction';
import { Box } from '@mui/material';
import ChartFilter from '../ChartFilter';
import CompanyStatisticsTable from '../CompanyStatisticsTable';
import YardPassRateLineChart from '../YardPassRateLineChart';
import StatisticsPagesBreadCrumbs from '../StatisticsPagesBreadCrumbs';
import TableSubject from '../TableSubject';
import useYardStatisticsQuery from './hooks/useYardStatisticsQuery';

const InspectionPassRate = () => {
  const [searchCompanyName, setSearchCompanyName] = useState('');
  const { chartFilterParams, searchQuery } = useChartFilterSelector();
  const { setSearchQuery } = useChartFilterAction();

  const { ctgType, blastingType } = chartFilterParams;

  const handleSearchInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearchCompanyName(e.target.value);
    },
    [setSearchCompanyName],
  );

  const handleSearchEnter = useCallback(() => {
    setSearchQuery(searchCompanyName);
    refetchStatistics();
  }, [setSearchQuery, searchCompanyName]);

  const renderSearchInput = useCallback(
    () => (
      <SearchInput
        width="19.2rem"
        isEnableRadius={false}
        onChange={handleSearchInputChange}
        onSearchEnter={handleSearchEnter}
        placeholder={SEARCH_COMPANY}
      />
    ),
    [handleSearchInputChange, handleSearchEnter],
  );

  const {
    data: yardStatistics,
    isFetching: isStatisticsFetching,
    isError: isStatisticsQueryError,
    isSuccess: isStatisticsQuerySuccess,
    refetch: refetchStatistics,
  } = useYardStatisticsQuery({
    ...chartFilterParams,
    searchCompanyName: searchQuery,
  });

  const calculatedYardStatistics = useMemo(
    () =>
      yardStatistics?.map((inspectionPassRateStatistic) =>
        calculateYardStatistic(inspectionPassRateStatistic),
      ),
    [yardStatistics],
  );

  const coatingOriginRateChartData = useMemo(
    () =>
      createPassRateLineChartData(yardStatistics ?? [], 'coatingOriginRate'),
    [yardStatistics],
  );

  const blastingPassRateChartData = useMemo(
    () => createPassRateLineChartData(yardStatistics ?? [], 'blastingPassRate'),
    [yardStatistics],
  );

  const coatingPassRageChartData = useMemo(
    () => createPassRateLineChartData(yardStatistics ?? [], 'coatingPassRage'),
    [yardStatistics],
  );

  return (
    <Box>
      <StatisticsPagesBreadCrumbs />
      <ChartFilter
        renderSearchInput={renderSearchInput}
        searchQuery={searchCompanyName}
        onSubmit={refetchStatistics}
      />
      <StyledDivider orientation="horizontal" />
      <ChartSection>
        {isStatisticsFetching && <LoadingSpinner />}
        {!isStatisticsFetching && isStatisticsQueryError && (
          <ErrorBox
            error={FAIL_DATA_FETCH}
            onRefetchClick={refetchStatistics}
          />
        )}
        {calculatedYardStatistics !== undefined &&
          yardStatistics !== undefined &&
          !isStatisticsFetching &&
          isStatisticsQuerySuccess && (
            <>
              <div>
                <TableSubject subject={STATISTICS_BY_WORKER} />
                <CompanyStatisticsTable
                  blastingTitle={convertBlastingFilterTaskText(blastingType)}
                  coatingTitle={convertCoatingFilterTaskText(ctgType)}
                  companyStatistics={calculatedYardStatistics}
                />
              </div>
              <YardPassRateLineChart
                subject={ORIGIN_COATING_RATE}
                title={convertCoatingFilterTaskText(ctgType)}
                data={coatingOriginRateChartData}
                referenceValue={88}
                chartFilterDateType={chartFilterParams.searchDateType}
              />
              <YardPassRateLineChart
                subject={BLASTING_INSPECTION_PASS_RATE}
                title={convertBlastingFilterTaskText(blastingType)}
                data={blastingPassRateChartData}
                referenceValue={90}
                chartFilterDateType={chartFilterParams.searchDateType}
              />
              <YardPassRateLineChart
                subject={COATING_INSPECTION_PASS_RATE}
                title={convertCoatingFilterTaskText(ctgType)}
                data={coatingPassRageChartData}
                referenceValue={90}
                chartFilterDateType={chartFilterParams.searchDateType}
              />
            </>
          )}
      </ChartSection>
    </Box>
  );
};

const StyledDivider = styled(Divider)`
  margin: 3.8rem 0 2.6rem;
`;

const ChartSection = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4.8rem;
`;

export default InspectionPassRate;
