/* eslint-disable react/no-array-index-key */
import { memo, useMemo, useState } from 'react';
import styled from 'styled-components';
import { INSPECTION_AVERAGE } from '@src/constants/constantString';
import { RecentStatistic } from '@src/types/statistics.types';
import Divider from '@src/components/common/Divider';
import Icon from '@src/components/common/Icon';
import PassRateItem from './RecentStatisticsCarouselItem';

export type RecentStatisticsCarouselProps = {
  recentStatistics: RecentStatistic[];
};

const RecentStatisticsCarousel = ({
  recentStatistics,
}: RecentStatisticsCarouselProps) => {
  const [currentSlide, setCurrentSlide] = useState(recentStatistics.length - 1);

  const slides = useMemo(() => recentStatistics.length, [recentStatistics]);

  const hasPrevious = useMemo(() => currentSlide !== 0, [currentSlide]);

  const hasNext = useMemo(
    () => currentSlide < slides - 1,
    [currentSlide, slides],
  );

  const handlePreviousClick = () => {
    if (!hasPrevious) {
      setCurrentSlide(slides - 1);
      return;
    }

    setCurrentSlide((prevCurrentSlide) => prevCurrentSlide - 1);
  };

  const handleNextClick = () => {
    if (!hasNext) {
      setCurrentSlide(0);
      return;
    }

    setCurrentSlide((prevCurrentSlide) => prevCurrentSlide + 1);
  };

  const translateYPosition = useMemo(
    () => `-${(currentSlide / slides) * 100}%`,
    [currentSlide, slides],
  );

  const date = useMemo(
    () =>
      `${recentStatistics[currentSlide].year}년 ${recentStatistics[currentSlide].month}월 평균`,
    [recentStatistics, currentSlide],
  );

  return (
    <Box>
      <InfoSection>
        <Title>{INSPECTION_AVERAGE}</Title>
        <Divider length="19.3rem" orientation="horizontal" />
        <Date>{date}</Date>
        <Divider length="19.3rem" orientation="horizontal" />
        <CircleIndicatorSection>
          {Array.from({ length: slides }).map((_, index) =>
            index === currentSlide ? (
              <Icon icon="circleIndicatorActive" key={index} />
            ) : (
              <Icon icon="circleIndicatorInactive" key={index} />
            ),
          )}
        </CircleIndicatorSection>
        <ButtonSection>
          <Button type="button" onClick={handlePreviousClick}>
            <Icon icon="circleArrowIcon" />
          </Button>
          <Button type="button" onClick={handleNextClick}>
            <NextIcon icon="circleArrowIcon" />
          </Button>
        </ButtonSection>
      </InfoSection>
      <Carousel>
        <ContentWrapper translateYPosition={translateYPosition}>
          {recentStatistics.map((recentStatistic) => (
            <PassRateItem
              key={recentStatistic.year + recentStatistic.month}
              recentStatistic={recentStatistic}
            />
          ))}
        </ContentWrapper>
      </Carousel>
    </Box>
  );
};

const Box = styled.div`
  display: flex;
  color: ${({ theme }) => theme.colors.primary};
  border-bottom: 1px solid ${({ theme }) => theme.colors.primary};
`;

const InfoSection = styled.div`
  width: 37.4rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-right: 1px solid ${({ theme }) => theme.colors.primary};
`;

const Title = memo(styled.p`
  margin-bottom: 1.2rem;
  font-size: 2.2rem;
  font-weight: 700;
`);

const Date = styled.div`
  margin: 1.2rem 0;
  font-size: 2rem;
  position: relative;
`;

const CircleIndicatorSection = styled.div`
  margin-top: 2.2rem;
  display: flex;
  gap: 0.9rem;
`;

const ButtonSection = styled.div`
  margin-top: 1.9rem;
  display: flex;
  gap: 1rem;
`;

const Button = styled.button`
  padding: 0;
  border: none;
  background: none;
  cursor: pointer;
`;

const Carousel = styled.div`
  width: 100%;
  height: 22.6rem;
  overflow: hidden;
`;

const NextIcon = styled(Icon)`
  transform: rotate(180deg);
`;

const ContentWrapper = styled.div<{ translateYPosition: string }>`
  position: relative;
  display: flex;
  flex-direction: column;
  transition: transform 0.5s ease-in-out;
  transform: ${({ translateYPosition }) => `translateY(${translateYPosition})`};
`;

export default memo(RecentStatisticsCarousel);
