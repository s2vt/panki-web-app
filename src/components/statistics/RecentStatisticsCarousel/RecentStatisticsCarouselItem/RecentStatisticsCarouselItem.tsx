import { memo } from 'react';
import styled, { css } from 'styled-components';
import {
  BLASTING_PASS_RATE,
  COATING_PASS_RATE,
  ORIGIN_COATING_RATE,
} from '@src/constants/constantString';
import { RecentStatistic } from '@src/types/statistics.types';
import Divider from '@src/components/common/Divider';

export type PassRateItemProps = {
  recentStatistic: RecentStatistic;
};

const PassRateItem = ({ recentStatistic }: PassRateItemProps) => (
  <Box>
    <OutsideDividerWrapper>
      <OutsideDivider orientation="vertical" direction="top" />
    </OutsideDividerWrapper>
    <RowItem
      subject={BLASTING_PASS_RATE}
      passRate={recentStatistic.blasting.passRate}
      improvementRate={recentStatistic.blasting.improvementRate}
    />
    <DividerWrapper>
      <Divider orientation="vertical" length="1.8rem" />
    </DividerWrapper>
    <RowItem
      subject={COATING_PASS_RATE}
      passRate={recentStatistic.coating.passRate}
      improvementRate={recentStatistic.coating.improvementRate}
    />
    <DividerWrapper>
      <Divider orientation="vertical" length="1.8rem" />
    </DividerWrapper>
    <RowItem
      subject={ORIGIN_COATING_RATE}
      passRate={recentStatistic.originCoating.passRate}
      improvementRate={recentStatistic.originCoating.improvementRate}
    />
    <OutsideDividerWrapper>
      <OutsideDivider orientation="vertical" direction="bottom" />
    </OutsideDividerWrapper>
  </Box>
);

type RowItemProps = {
  subject: string;
  passRate: number;
  improvementRate: number;
};
const RowItem = ({ subject, passRate, improvementRate }: RowItemProps) => (
  <Content>
    <Subject>{subject}</Subject>
    <PassRate>{Number(passRate.toFixed(1))}%</PassRate>
    <ImprovementRate>
      {Number(improvementRate.toFixed(1))}% (전월대비)
    </ImprovementRate>
  </Content>
);

const Box = styled.div`
  width: 100%;
  height: 22.6rem;
  padding: 0 5.7rem 0 7.4rem;
  display: flex;
  flex-direction: column;
`;

const Subject = styled.div`
  flex: 1;
  max-width: 17.4rem;
  height: 4.5rem;
  color: ${({ theme }) => theme.colors.white};
  background: ${({ theme }) => theme.colors.primary};
  ${({ theme }) => theme.layout.flexCenterLayout};
  font-size: 1.8rem;
  border-radius: 24px;
`;

const DividerWrapper = styled.div`
  width: 17.4rem;
  display: flex;
  justify-content: center;
`;

const OutsideDividerWrapper = styled(DividerWrapper)`
  flex: 1;
`;

const OutsideDivider = styled(Divider)<{ direction: 'top' | 'bottom' }>`
  ${({ theme, direction }) => css`
  background:linear-gradient(to ${direction}, ${theme.colors.primary}, ${theme.colors.white})};
`}
`;

const PassRate = styled.p`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2.8rem;
  font-weight: 700;
`;

const ImprovementRate = styled.p`
  flex: 1;
  display: flex;
  align-items: center;
  font-size: 2rem;
  font-weight: 700;
`;

const Content = styled.div`
  display: flex;
  justify-content: space-between;
`;

export default memo(PassRateItem);
