import { Meta, Story } from '@storybook/react';
import PassRateItem, {
  PassRateItemProps,
} from './RecentStatisticsCarouselItem';

export default {
  title: 'Components/statistics/RecentStatisticsCarousel/PassRateItem',
  component: PassRateItem,
} as Meta;

const Template: Story<PassRateItemProps> = (args) => <PassRateItem {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  recentStatistic: {
    year: 2021,
    month: 7,
    blasting: { passRate: 82, improvementRate: -2.5 },
    coating: { passRate: 85, improvementRate: -3.5 },
    originCoating: { passRate: 90, improvementRate: -5 },
  },
};
