import { Meta, Story } from '@storybook/react';
import RecentStatisticsCarousel, {
  RecentStatisticsCarouselProps,
} from './RecentStatisticsCarousel';

export default {
  title: 'Components/statistics/RecentStatisticsCarousel',
  component: RecentStatisticsCarousel,
} as Meta;

const Template: Story<RecentStatisticsCarouselProps> = (args) => (
  <RecentStatisticsCarousel {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  recentStatistics: [
    {
      year: 2021,
      month: 7,
      blasting: { passRate: 82, improvementRate: -2.5 },
      coating: { passRate: 85, improvementRate: -3.5 },
      originCoating: { passRate: 90, improvementRate: -5 },
    },
    {
      year: 2021,
      month: 8,
      blasting: { passRate: 78, improvementRate: 2 },
      coating: { passRate: 23, improvementRate: -3.5 },
      originCoating: { passRate: 74, improvementRate: 6 },
    },
    {
      year: 2021,
      month: 9,
      blasting: { passRate: 88, improvementRate: 3 },
      coating: { passRate: 77, improvementRate: -3.5 },
      originCoating: { passRate: 94, improvementRate: 11 },
    },
  ],
};
