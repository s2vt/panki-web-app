import { memo } from 'react';
import { HEAT_MAP } from '@src/constants/constantString';
import MainButton from '@src/components/common/MainButton';
import ChartSubjectBase from '../ChartSubjectBase';

export type TableSubjectProps = {
  subject: string;
  onHeatMapClick?: () => void;
};

const TableSubject = ({ subject, onHeatMapClick }: TableSubjectProps) => (
  <ChartSubjectBase.SubjectSection>
    <ChartSubjectBase.SubjectWrapper>
      <ChartSubjectBase.Subject>{subject}</ChartSubjectBase.Subject>
    </ChartSubjectBase.SubjectWrapper>
    <MainButton
      label={HEAT_MAP}
      backgroundColor="white"
      labelColor="primary"
      width="9.7rem"
      height="2.8rem"
      fontSize="1.4rem"
      fontWeight={700}
      onClick={onHeatMapClick}
    />
  </ChartSubjectBase.SubjectSection>
);

export default memo(TableSubject);
