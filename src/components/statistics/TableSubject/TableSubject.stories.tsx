import { Meta, Story } from '@storybook/react';
import TableSubject, { TableSubjectProps } from './TableSubject';

export default {
  title: 'Components/statistics/TableSubject',
  component: TableSubject,
} as Meta;

const Template: Story<TableSubjectProps> = (args) => <TableSubject {...args} />;

export const Basic = Template.bind({});
