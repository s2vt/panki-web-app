import { Meta, Story } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import StatisticsPagesBreadCrumbs, {
  StatisticsPagesBreadCrumbsProps,
} from './StatisticsPagesBreadCrumbs';

export default {
  title: 'Components/Statistics/StatisticsPagesBreadCrumbs',
  component: StatisticsPagesBreadCrumbs,
  decorators: [StoryRouter()],
} as Meta;

const Template: Story<StatisticsPagesBreadCrumbsProps> = (args) => (
  <StatisticsPagesBreadCrumbs {...args} />
);

export const Basic = Template.bind({});
