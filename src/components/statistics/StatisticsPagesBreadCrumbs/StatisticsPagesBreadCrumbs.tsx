import React, { memo, useCallback, useRef, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import {
  INSPECTION_PASS_RATE,
  INSPECTION_RESULT,
  SEMANTIC_ANALYSIS,
  STATISTICS_BY_COMPANY,
  TO_PRINT,
} from '@src/constants/constantString';
import useSelectWidth from '@src/hooks/common/useSelectWidth';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import Icon from '@src/components/common/Icon';
import IconButton from '@src/components/common/IconButton';

export type StatisticsPagesBreadCrumbsProps = {
  onPrintClick?: () => void;
};

const BREAD_CRUMBS_ITEMS = [
  { label: INSPECTION_PASS_RATE, to: ROUTE_PATHS.statisticsPassRate },
  {
    label: STATISTICS_BY_COMPANY,
    to: ROUTE_PATHS.statisticsCompany,
  },
  {
    label: SEMANTIC_ANALYSIS,
    to: ROUTE_PATHS.statisticsSemanticAnalysis,
  },
];

const StatisticsPagesBreadCrumbs = ({
  onPrintClick,
}: StatisticsPagesBreadCrumbsProps) => {
  const [showArrowIcon, setShowArrowIcon] = useState(false);
  const selectWrapperRef = useRef<HTMLDivElement>(null);
  const selectRef = useRef<HTMLSelectElement>(null);
  const history = useHistory();

  const { calculatedWidth } = useSelectWidth({
    selectRef,
    selectWrapperRef,
    fontWidth: 2,
    iconSpacing: 1.5,
  });

  useEffect(() => {
    if (selectWrapperRef.current !== null && selectRef.current !== null) {
      selectWrapperRef.current.style.width = `${calculatedWidth}rem`;
      selectRef.current.style.width = `${calculatedWidth}rem`;
      setShowArrowIcon(true);
    }
  }, [calculatedWidth]);

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      const { selectedIndex, options } = e.target;

      const routeName = options[selectedIndex].value;

      history.push(routeName);
    },
    [history],
  );

  return (
    <Box>
      <BreadCrumbsSection>
        <p>{INSPECTION_RESULT}</p>
        <ArrowIcon icon="arrowIcon" />
        <SelectWrapper ref={selectWrapperRef}>
          <Select
            onChange={handleChange}
            value={history.location.pathname}
            ref={selectRef}
          >
            {BREAD_CRUMBS_ITEMS.map((item) => (
              <option key={item.to} value={item.to}>
                {item.label}
              </option>
            ))}
          </Select>
          <SelectArrowIcon icon="arrowBoxIcon" $isShow={showArrowIcon} />
        </SelectWrapper>
      </BreadCrumbsSection>
      <IconButton
        onClick={onPrintClick}
        icon="printIcon"
        iconWidth="2.3rem"
        iconHeight="2.3rem"
        iconFillColor="primary"
        width="11.5rem"
        height="3.3rem"
        backgroundColor="white"
        labelColor="primary"
        fontSize="1.6rem"
        fontWeight={700}
        label={TO_PRINT}
      />
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  margin-top: 1.6rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadCrumbsSection = styled.div`
  display: flex;
  align-items: center;
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const ArrowIcon = styled(Icon)`
  margin: 0 1.1rem;
  transform: rotate(270deg);
`;

const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
`;

const SelectArrowIcon = styled(Icon)<{ $isShow: boolean }>`
  display: ${({ $isShow }) => ($isShow ? 'block' : 'none')};
  position: absolute;
  right: 0rem;
  height: 100%;
  transform: rotate(90deg);
  cursor: pointer;
  pointer-events: none;
`;

const Select = styled.select`
  position: relative;
  border: none;
  background: none;
  text-decoration: underline;
  cursor: pointer;
  font-size: 2.2rem;
  font-weight: 700;
  appearance: unset;
  color: ${({ theme }) => theme.colors.primary};

  ::-ms-expand {
    display: none;
  }
`;

export default memo(StatisticsPagesBreadCrumbs);
