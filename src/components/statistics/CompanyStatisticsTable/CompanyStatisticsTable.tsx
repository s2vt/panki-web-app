/* eslint-disable react/no-array-index-key */
import { memo, useCallback } from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { TASK_MANAGER } from '@src/constants/constantString';
import useChartFilterAction from '@src/hooks/statistics/useChartFilterAction';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import { SubcontractorCompany } from '@src/types/company.types';
import { CompanyStatisticTableData } from '@src/types/statistics.types';
import CompanyStatisticsTableRow from './CompanyStatisticsTableRow';

export type CompanyStatisticsTableProps = {
  blastingTitle: string;
  coatingTitle: string;
  companyStatistics: CompanyStatisticTableData[];
};

const COLUMNS = [
  '총 검사수',
  '합격률(%)',
  '총 검사수',
  '합격률(%)',
  '총 측정수',
  '평균(μm)',
  '표준편차(μm)',
  '저도막(%)',
  '과도막(%)',
  '정도막(%)',
];

const CompanyStatisticsTable = ({
  blastingTitle,
  coatingTitle,
  companyStatistics,
}: CompanyStatisticsTableProps) => {
  const { setSubcontractorCompany } = useChartFilterAction();
  const history = useHistory();

  const handleRowClick = useCallback(
    (subcontractorCompany: SubcontractorCompany) => {
      setSubcontractorCompany(subcontractorCompany);
      history.push(ROUTE_PATHS.statisticsCompany);
    },

    [setSubcontractorCompany, history],
  );

  return (
    <Table>
      <thead>
        <TableRow>
          <NameTableHeaderCell rowSpan={2}>{TASK_MANAGER}</NameTableHeaderCell>
          <TableHeaderCell colSpan={2}>{blastingTitle}</TableHeaderCell>
          <TableHeaderCell colSpan={8}>{coatingTitle}</TableHeaderCell>
        </TableRow>
        <TableRow>
          {COLUMNS.map((column, index) => (
            <SmallsTableHeaderCell key={index}>{column}</SmallsTableHeaderCell>
          ))}
        </TableRow>
      </thead>
      <tbody>
        {companyStatistics.map((companyStatistic) => (
          <CompanyStatisticsTableRow
            key={companyStatistic.companyId}
            companyStatistic={companyStatistic}
            onClick={handleRowClick}
          />
        ))}
      </tbody>
    </Table>
  );
};

const Table = styled.table`
  width: 100%;
  background: 1px solid ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

const TableRow = styled.tr`
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.tableBackground};
  font-size: 1.8rem;
  font-weight: 700;
`;

const TableHeaderCell = styled.th`
  padding: 1rem 0;
  vertical-align: middle;
  border: 1px solid ${({ theme }) => theme.colors.primary};
`;

const NameTableHeaderCell = styled(TableHeaderCell)`
  width: 10.8rem;
`;

const SmallsTableHeaderCell = styled(TableHeaderCell)`
  font-size: 1.4rem;
`;

export default memo(CompanyStatisticsTable);
