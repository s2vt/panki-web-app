import { Meta, Story } from '@storybook/react';
import CompanyStatisticsTable from '.';
import { CompanyStatisticsTableProps } from './CompanyStatisticsTable';

export default {
  title: 'Components/Statistics/CompanyStatisticsTable',
  component: CompanyStatisticsTable,
} as Meta;

const Template: Story<CompanyStatisticsTableProps> = (args) => (
  <div style={{ width: '1054px' }}>
    <CompanyStatisticsTable {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  blastingTitle: '블라스팅',
  coatingTitle: '도장',
  companyStatistics: [
    {
      companyName: '작업업체1',
      companyId: '1',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: {
        inspectionCount: 10,
        measureCount: 23.1,
        passRate: 23.1,
        average: 23.1,
        standardDeviation: 500,
        lowerRate: 5.5,
        upperRate: 4.5,
        originRate: 90,
      },
    },
    {
      companyName: '작업업체2',
      companyId: '2',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: {
        inspectionCount: 10,
        measureCount: 23.1,
        passRate: 23.1,
        average: 23.1,
        standardDeviation: 500,
        lowerRate: 5.5,
        upperRate: 4.5,
        originRate: 90,
      },
    },
    {
      companyName: '작업업체3',
      companyId: '3',
      blasting: { inspectionCount: 2345, passRate: 23.1 },
      coating: null,
    },
  ],
};
