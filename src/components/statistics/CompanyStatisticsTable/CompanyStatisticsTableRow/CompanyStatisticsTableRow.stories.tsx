import { Meta, Story } from '@storybook/react';
import CompanyStatisticsTableRow from '.';
import { CompanyStatisticsTableRowProps } from './CompanyStatisticsTableRow';

export default {
  title:
    'Components/Statistics/CompanyStatisticsTable/CompanyStatisticsTableRow',
  component: CompanyStatisticsTableRow,
} as Meta;

const Template: Story<CompanyStatisticsTableRowProps> = (args) => (
  <table style={{ width: '100%' }}>
    <tbody>
      <CompanyStatisticsTableRow {...args} />
    </tbody>
  </table>
);

export const Basic = Template.bind({});
Basic.args = {
  companyStatistic: {
    companyName: '작업업체1',
    companyId: '1',
    blasting: { inspectionCount: 2345, passRate: 23.1 },
    coating: {
      inspectionCount: 10,
      measureCount: 23.1,
      passRate: 23.1,
      average: 23.1,
      standardDeviation: 500,
      lowerRate: 5.5,
      upperRate: 4.5,
      originRate: 90,
    },
  },
};
