/* eslint-disable react/no-array-index-key */
import { memo } from 'react';
import styled from 'styled-components';
import { SubcontractorCompany } from '@src/types/company.types';
import {
  CompanyStatisticTableData,
  createInspectionStatisticTableData,
} from '@src/types/statistics.types';

export type CompanyStatisticsTableRowProps = {
  onClick: (subcontractorCompany: SubcontractorCompany) => void;
  companyStatistic: CompanyStatisticTableData;
};

const CompanyStatisticsTableRow = ({
  onClick,
  companyStatistic,
}: CompanyStatisticsTableRowProps) => {
  const { companyName, companyId } = companyStatistic;

  const { blastingStatistics, coatingStatistics } =
    createInspectionStatisticTableData(companyStatistic);

  return (
    <TableRow onClick={() => onClick({ id: companyId, companyName })}>
      <TableNameData>{companyName}</TableNameData>
      {blastingStatistics.concat(coatingStatistics).map((value, index) => (
        <TableValueData key={index}>{value ?? '-'}</TableValueData>
      ))}
    </TableRow>
  );
};

const TableData = styled.td`
  border: 1px solid ${({ theme }) => theme.colors.primary};
  padding: 1.2rem 0;
`;

const TableNameData = styled(TableData)`
  background: ${({ theme }) => theme.colors.tableBackground};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  padding: 1.2rem 0;
`;

const TableValueData = styled(TableData)`
  background: ${({ theme }) => theme.colors.white};
`;

const TableRow = styled.tr`
  font-size: 1.6rem;
  cursor: pointer;

  &:hover ${TableData} {
    background: ${({ theme }) => theme.colors.tableHoverColor};
  }
`;

export default memo(CompanyStatisticsTableRow);
