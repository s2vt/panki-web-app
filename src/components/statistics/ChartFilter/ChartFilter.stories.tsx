import { Meta, Story } from '@storybook/react';
import ChartFilter, { ChartFilterProps } from './ChartFilter';

export default {
  title: 'Components/statistics/ChartFilter',
  component: ChartFilter,
} as Meta;

const Template: Story<ChartFilterProps> = (args) => <ChartFilter {...args} />;

export const Basic = Template.bind({});
