import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { subMonths } from 'date-fns';
import styled from 'styled-components';
import {
  MY_STATISTICS,
  OVERALL_STATISTICS,
  RESET,
  INQUIRY_PERIOD,
  DOWNLOAD,
  INQUIRY,
} from '@src/constants/constantString';
import {
  ChartFilterParams,
  convertBlastingFilterTaskText,
  convertCoatingFilterTaskText,
  BlastingFilterTask,
  CoatingFilterTask,
  isBlastingFilterTask,
  isCoatingFilterTask,
  ChartFilterType,
  ChartFilterDateType,
} from '@src/types/statistics.types';
import {
  getFirstDateOfMonth,
  getFirstDateOfYear,
  formatToCommon,
  getLastDateOfMonth,
} from '@src/utils/dateUtil';
import { addWhiteSpacesBetweenCharacters } from '@src/utils/textUtil';
import { ChartFilterPeriod, initialState } from '@src/reducers/chartFilter';
import useChartFilterSelector from '@src/hooks/statistics/useChartFilterSelector';
import useChartFilterAction from '@src/hooks/statistics/useChartFilterAction';
import MainButton from '@src/components/common/MainButton';
import RoundButton from '@src/components/common/RoundButton';
import RoundSelector from '@src/components/common/RoundSelector';

export type ChartFilterProps = {
  renderSearchInput: () => React.ReactNode;
  searchQuery: string;
  onSubmit: () => void;
};

const ChartFilter = ({
  renderSearchInput,
  searchQuery,
  onSubmit,
}: ChartFilterProps) => {
  const { chartFilterParams, period } = useChartFilterSelector();
  const { setChartFilterParams, setPeriod, setSearchQuery } =
    useChartFilterAction();

  const [localChartFilterParams, setLocalChartFilterParams] =
    useState<ChartFilterParams>(chartFilterParams);
  const [localPeriod, setLocalPeriod] = useState<ChartFilterPeriod>(period);

  useEffect(() => {
    setSearchQuery('');
  }, []);

  const currentDate = useMemo(() => new Date(), []);

  const lastMonth = useMemo(() => {
    const realMonth = currentDate.getMonth() + 1;
    return `${realMonth - 1}월`;
  }, [currentDate]);

  const currentMonth = useMemo(() => {
    const realMonth = currentDate.getMonth() + 1;

    return `${realMonth}월`;
  }, [currentDate]);

  const currentYear = useMemo(
    () => `${currentDate.getFullYear()}년`,
    [currentDate],
  );

  const blastingDropdownItems = useMemo(
    () =>
      Object.values(BlastingFilterTask).map((task) => ({
        id: task,
        label: convertBlastingFilterTaskText(task),
      })),
    [],
  );

  const coatingDropdownItems = useMemo(
    () =>
      Object.values(CoatingFilterTask).map((task) => ({
        id: task,
        label: convertCoatingFilterTaskText(task),
      })),
    [],
  );

  const { searchType, searchDateType, blastingType, ctgType, from, to } =
    localChartFilterParams;

  const handleSubmit = () => {
    setChartFilterParams(localChartFilterParams);
    setPeriod(localPeriod);
    setSearchQuery(searchQuery);
    onSubmit();
  };

  const handleSearchTypeClick = useCallback(
    (searchType: ChartFilterType) => {
      setLocalChartFilterParams((prevChartFilterParams) => ({
        ...prevChartFilterParams,
        searchType,
      }));
    },
    [setLocalChartFilterParams],
  );

  const handleDateTypeChange = useCallback(
    (chartFilterDateType: ChartFilterDateType) => {
      setLocalChartFilterParams((prevChartFilterParams) => ({
        ...prevChartFilterParams,
        searchDateType: chartFilterDateType,
      }));
    },
    [setLocalPeriod, setLocalChartFilterParams],
  );

  const setFilterDate = useCallback(
    (from: Date, to: Date) => {
      setLocalChartFilterParams((prevChartFilterParams) => ({
        ...prevChartFilterParams,
        from: formatToCommon(from),
        to: formatToCommon(to),
      }));
    },
    [setLocalChartFilterParams, formatToCommon],
  );

  const handleLastMonthClick = useCallback(() => {
    handleDateTypeChange(ChartFilterDateType.period);

    const firstDateOfLastMonth = getFirstDateOfMonth(subMonths(currentDate, 1));
    const lastDateOfLastMonth = getLastDateOfMonth(subMonths(currentDate, 1));

    setFilterDate(firstDateOfLastMonth, lastDateOfLastMonth);
    setLocalPeriod('lastMonth');
  }, [handleDateTypeChange, setFilterDate, setFilterDate]);

  const handleCurrentMonthClick = useCallback(() => {
    handleDateTypeChange(ChartFilterDateType.period);

    const firstDateOfCurrentMonth = getFirstDateOfMonth(currentDate);

    setFilterDate(firstDateOfCurrentMonth, currentDate);
    setLocalPeriod('currentMonth');
  }, [handleDateTypeChange, setFilterDate, setFilterDate]);

  const handleCurrentYearClick = useCallback(() => {
    handleDateTypeChange(ChartFilterDateType.year);

    const firstDateOfCurrentYear = getFirstDateOfYear(currentDate);

    setFilterDate(firstDateOfCurrentYear, currentDate);
  }, [handleDateTypeChange, setFilterDate, setFilterDate]);

  const handleResetClick = useCallback(() => {
    setLocalChartFilterParams(initialState.chartFilterParams);
  }, [setLocalChartFilterParams]);

  const handleBlastingChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      if (!isBlastingFilterTask(e.target.value)) {
        throw new Error(
          `Blasting filter task is not includes ${e.target.value}`,
        );
      }

      setLocalChartFilterParams((prevChartFilterParams) => ({
        ...prevChartFilterParams,
        blastingType: e.target.value as BlastingFilterTask,
      }));
    },
    [setLocalChartFilterParams, isBlastingFilterTask],
  );

  const handleCoatingChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      if (!isCoatingFilterTask(e.target.value)) {
        throw new Error(
          `Coating filter task is not includes ${e.target.value}`,
        );
      }

      setLocalChartFilterParams((prevChartFilterParams) => ({
        ...prevChartFilterParams,
        ctgType: e.target.value as CoatingFilterTask,
      }));
    },
    [setLocalChartFilterParams, isCoatingFilterTask],
  );

  return (
    <Box>
      <ButtonSection>
        <LeftSection>
          <RoundButton
            label={OVERALL_STATISTICS}
            width="10.6rem"
            isActive={searchType === ChartFilterType.all}
            onClick={() => handleSearchTypeClick(ChartFilterType.all)}
          />
          <RoundButton
            label={MY_STATISTICS}
            width="10.6rem"
            isActive={searchType === ChartFilterType.my}
            onClick={() => handleSearchTypeClick(ChartFilterType.my)}
          />
          <RoundButton
            label={lastMonth}
            width="8.4rem"
            isActive={
              searchDateType === ChartFilterDateType.period &&
              localPeriod === 'lastMonth'
            }
            onClick={handleLastMonthClick}
          />
          <RoundButton
            label={currentMonth}
            width="8.4rem"
            isActive={
              searchDateType === ChartFilterDateType.period &&
              localPeriod === 'currentMonth'
            }
            onClick={handleCurrentMonthClick}
          />
          <RoundButton
            label={currentYear}
            width="8.4rem"
            isActive={searchDateType === ChartFilterDateType.year}
            onClick={handleCurrentYearClick}
          />
          {renderSearchInput()}
        </LeftSection>
        <RightSection>
          <MainButton
            label={RESET}
            width="8.3rem"
            height="3.3rem"
            backgroundColor="white"
            labelColor="primary"
            fontSize="1.6rem"
            fontWeight={700}
            onClick={handleResetClick}
          />
          <MainButton
            label={DOWNLOAD}
            width="8.3rem"
            height="3.3rem"
            backgroundColor="white"
            labelColor="primary"
            fontSize="1.6rem"
            fontWeight={700}
          />
        </RightSection>
      </ButtonSection>
      <SelectSection>
        <InquiryPeriod>
          {INQUIRY_PERIOD} : {from} - {to}
        </InquiryPeriod>
        <StyledRoundSelector
          width="22.5rem"
          height="100%"
          items={blastingDropdownItems}
          onChange={handleBlastingChange}
          value={blastingType}
        />
        <RoundSelector
          width="23.5rem"
          height="100%"
          items={coatingDropdownItems}
          onChange={handleCoatingChange}
          value={ctgType}
        />
        <InquiryButton
          label={addWhiteSpacesBetweenCharacters(INQUIRY)}
          width="12rem"
          height="100%"
          fontSize="1.6rem"
          onClick={handleSubmit}
        />
      </SelectSection>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
  margin-top: 3rem;
`;

const ButtonSection = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  height: 3.6rem;
`;

const LeftSection = styled.div`
  display: flex;
  gap: 1.2rem;
`;

const RightSection = styled.div`
  display: flex;
  align-items: flex-end;
  gap: 0.7rem;
`;

const SelectSection = styled.div`
  display: flex;
  margin-top: 2.9rem;
  height: 3.6rem;
  align-items: center;
`;

const InquiryPeriod = styled.p`
  margin-right: 2.9rem;
  font-size: 1.8rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const StyledRoundSelector = memo(styled(RoundSelector)`
  margin-right: 0.8rem;
`);

const InquiryButton = memo(styled(MainButton)`
  margin-left: 3.2rem;
`);

export default memo(ChartFilter);
