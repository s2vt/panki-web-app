import { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import useSelectWidth from '@src/hooks/common/useSelectWidth';
import { SubcontractorCompany } from '@src/types/company.types';
import Icon from '@src/components/common/Icon';

export type InquiryTargetSelectProps =
  React.SelectHTMLAttributes<HTMLSelectElement> & {
    subcontractorCompanies: SubcontractorCompany[];
  };

const HORIZONTAL_PADDING = 4;
const FONT_SIZE = 1.6;

const InquiryTargetSelect = ({
  subcontractorCompanies,
  ...rest
}: InquiryTargetSelectProps) => {
  const [showArrowIcon, setShowArrowIcon] = useState(false);
  const selectWrapperRef = useRef<HTMLDivElement>(null);
  const selectRef = useRef<HTMLSelectElement>(null);

  const { calculatedWidth } = useSelectWidth({
    selectRef,
    selectWrapperRef,
    fontWidth: FONT_SIZE,
    iconSpacing: 2,
  });

  useEffect(() => {
    if (
      selectWrapperRef.current !== null &&
      selectRef.current !== null &&
      calculatedWidth !== undefined
    ) {
      selectWrapperRef.current.style.width = `${
        calculatedWidth + HORIZONTAL_PADDING * 2
      }rem`;
      selectRef.current.style.width = `${
        calculatedWidth + HORIZONTAL_PADDING * 2
      }rem`;
      setShowArrowIcon(true);
    }
  }, [calculatedWidth]);

  return (
    <SelectWrapper ref={selectWrapperRef}>
      <Select ref={selectRef} {...rest}>
        {subcontractorCompanies.map((company) => (
          <option key={company.id} id={company.id} value={company.companyName}>
            {company.companyName}
          </option>
        ))}
      </Select>
      {showArrowIcon && <ArrowIcon icon="arrowBoxIcon" />}
    </SelectWrapper>
  );
};

const SelectWrapper = styled.div`
  position: relative;
  height: 3.6rem;
`;

const Select = styled.select`
  padding: 0.6rem ${HORIZONTAL_PADDING}rem;
  height: 100%;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 24px;
  font-size: ${FONT_SIZE}rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
  appearance: none;
`;

const ArrowIcon = styled(Icon)`
  height: 100%;
  position: absolute;
  right: 3rem;
  transform: rotate(90deg);
  pointer-events: none;
`;

export default InquiryTargetSelect;
