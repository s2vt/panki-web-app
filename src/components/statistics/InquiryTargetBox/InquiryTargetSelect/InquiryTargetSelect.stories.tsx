import { Meta, Story } from '@storybook/react';
import InquiryTargetSelect, {
  InquiryTargetSelectProps,
} from './InquiryTargetSelect';

export default {
  title: 'Components/statistics/InquiryTargetBox/InquiryTargetSelect',
  component: InquiryTargetSelect,
} as Meta;

const Template: Story<InquiryTargetSelectProps> = (args) => (
  <InquiryTargetSelect {...args} />
);

export const Basic = Template.bind({});
