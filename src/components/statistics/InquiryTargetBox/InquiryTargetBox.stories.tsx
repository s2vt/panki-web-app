import { Meta, Story } from '@storybook/react';
import InquiryTargetBox, { InquiryTargetBoxProps } from './InquiryTargetBox';

export default {
  title: 'Components/Statistics/InquiryTargetBox',
  component: InquiryTargetBox,
} as Meta;

const Template: Story<InquiryTargetBoxProps> = (args) => (
  <InquiryTargetBox {...args} />
);

export const Basic = Template.bind({});
