import _ from 'lodash';
import React, { memo, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { INQUIRY_TARGET } from '@src/constants/constantString';
import useChartFilterAction from '@src/hooks/statistics/useChartFilterAction';
import useChartFilterSelector from '@src/hooks/statistics/useChartFilterSelector';
import { SubcontractorCompany } from '@src/types/company.types';
import MainButton from '@src/components/common/MainButton';
import ChartSubjectBase from '../ChartSubjectBase';
import InquiryTargetSelect from './InquiryTargetSelect';

export type InquiryTargetBoxProps = {
  subcontractorCompanies: SubcontractorCompany[];
  onButtonClick: () => void;
  buttonLabel: string;
};

const InquiryTargetBox = ({
  subcontractorCompanies,
  onButtonClick,
  buttonLabel,
}: InquiryTargetBoxProps) => {
  const { subcontractorCompany } = useChartFilterSelector();
  const { setSubcontractorCompany } = useChartFilterAction();

  const isIncludedSubcontractorCompany = useMemo(
    () =>
      subcontractorCompanies.some((company) =>
        _.isEqual(company, subcontractorCompany),
      ),
    [subcontractorCompanies, subcontractorCompany],
  );

  useEffect(() => {
    if (subcontractorCompany === undefined || !isIncludedSubcontractorCompany) {
      setSubcontractorCompany(subcontractorCompanies[0]);
    }
  }, [
    subcontractorCompanies,
    subcontractorCompany,
    setSubcontractorCompany,
    isIncludedSubcontractorCompany,
  ]);

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedCompany = e.target.options[e.target.selectedIndex];

    const { id, value } = selectedCompany;

    const subcontractorCompany: SubcontractorCompany = {
      id,
      companyName: value,
    };

    setSubcontractorCompany(subcontractorCompany);
  };

  return (
    <div>
      <ChartSubjectBase.SubjectWrapper>
        <ChartSubjectBase.Subject>{INQUIRY_TARGET}</ChartSubjectBase.Subject>
      </ChartSubjectBase.SubjectWrapper>
      <Box>
        <SelectSection>
          <Title>작업 업체명:</Title>
          <InquiryTargetSelect
            value={subcontractorCompany?.companyName}
            subcontractorCompanies={subcontractorCompanies}
            onChange={handleSelectChange}
          />
        </SelectSection>
        <MainButton
          width="20.6rem"
          height="3.3rem"
          fontSize="1.4rem"
          fontWeight={700}
          label={buttonLabel}
          onClick={onButtonClick}
        />
      </Box>
    </div>
  );
};

const Box = styled.div`
  width: 100%;
  height: 6.7rem;
  margin-top: 1.6rem;
  padding: 0 3rem 0 2.8rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
`;

const SelectSection = styled.div`
  display: flex;
`;

const Title = styled.p`
  display: flex;
  align-items: center;
  margin-right: 5.95rem;
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
  vertical-align: bottom;
`;

export default memo(InquiryTargetBox);
