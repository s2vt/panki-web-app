import { BarTooltipPayload } from 'recharts';
import styled, { css } from 'styled-components';

export type RecentStatisticsBarChartXAxisTickProps =
  React.SVGAttributes<SVGElement> & {
    payload?: BarTooltipPayload;
    activeLegend: string | undefined;
  };

const RecentStatisticsBarChartXAxisTick = ({
  x,
  y,
  payload,
  activeLegend,
}: RecentStatisticsBarChartXAxisTickProps) => (
  <g>
    <foreignObject x={(x as number) - 80} y={y} width={154} height={44}>
      <Box isActive={activeLegend === payload?.value}>
        <Text>{payload?.value}</Text>
      </Box>
    </foreignObject>
  </g>
);

const Box = styled.div<{ isActive: boolean }>`
  height: 100%;
  ${({ theme }) => theme.layout.flexCenterLayout}
  color: ${({ theme }) => theme.colors.primary};

  ${({ isActive, theme }) =>
    isActive &&
    css`
      background: ${theme.colors.primary};
      color: white;
    `};
`;

const Text = styled.p`
  font-size: 2rem;
  font-weight: 700;
`;
export default RecentStatisticsBarChartXAxisTick;
