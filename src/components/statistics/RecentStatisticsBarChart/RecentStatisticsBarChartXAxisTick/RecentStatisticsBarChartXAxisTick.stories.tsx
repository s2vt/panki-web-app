import { Meta, Story } from '@storybook/react';
import RecentStatisticsBarChartXAxisTick, {
  RecentStatisticsBarChartXAxisTickProps,
} from './RecentStatisticsBarChartXAxisTick';

export default {
  title:
    'Components/Statistics/RecentStatisticsBarChart/RecentStatisticsBarChartXAxisTick',
  component: RecentStatisticsBarChartXAxisTick,
} as Meta;

const Template: Story<RecentStatisticsBarChartXAxisTickProps> = (args) => (
  <RecentStatisticsBarChartXAxisTick {...args} />
);

export const Basic = Template.bind({});
