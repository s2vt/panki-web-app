/* eslint-disable react/no-array-index-key */
import { useMemo, useState } from 'react';
import {
  BarChart,
  Legend,
  XAxis,
  YAxis,
  Tooltip,
  Bar,
  LabelList,
  BarOnMouseOverParams,
  ReferenceLine,
  Cell,
  ChartMouseMoveParams,
  ResponsiveContainer,
} from 'recharts';
import styled from 'styled-components';
import { RECENT_STATISTICS_BAR_CHART_COLORS } from '@src/styles/chartColors';
import { colors } from '@src/styles/theme';
import {
  ChartData,
  getChartDataKeys,
  getChartDataMinimumValue,
} from '@src/types/statistics.types';
import RecentStatisticsBarChartLegend from './RecentStatisticsBarChartLegend';
import RecentStatisticsBarChartTooltip from './RecentStatisticsBarChartTooltip';
import RecentStatisticsBarChartXAxisTick from './RecentStatisticsBarChartXAxisTick';

export type RecentStatisticsBarChartProps = {
  /** 차트 데이터 */
  data: ChartData[];
};

const RecentStatisticsBarChart = ({ data }: RecentStatisticsBarChartProps) => {
  const [activeLegend, setActiveLegend] = useState<string>();
  const [tooltipData, setTooltipData] = useState<{
    subject: string;
    text: string;
  } | null>(null);

  const dataKeys = useMemo(() => getChartDataKeys(data), [data]);

  const minimumValue = useMemo(() => getChartDataMinimumValue(data), [data]);

  const handleBarMouseOver = ({ tooltipPayload }: BarOnMouseOverParams) => {
    const { dataKey, value } = tooltipPayload[0];

    setTooltipData({ subject: dataKey, text: `${value}%` });
  };

  const handleBarMouseLeave = () => setTooltipData(null);

  const handleBarChartMouseMove = ({
    isTooltipActive,
    activeLabel,
  }: ChartMouseMoveParams) => {
    if (!isTooltipActive) {
      setActiveLegend(undefined);
      return;
    }

    setActiveLegend(activeLabel);
  };

  const formatPercentage = (value: string | number) => `${value}%`;

  const yAxisTicks = useMemo(() => {
    const ticks = [0, 50, 100];

    if (minimumValue < 0) {
      ticks.unshift(minimumValue);
    }

    return ticks;
  }, [minimumValue]);

  return (
    <ChartWrapper>
      <ResponsiveContainer width="100%" height="100%" debounce={1}>
        <BarChart
          data={data}
          barGap={14.7}
          onMouseMove={handleBarChartMouseMove}
          margin={{ left: 20, top: 67, right: 20 }}
        >
          <XAxis
            axisLine={false}
            dataKey="legend"
            tickLine={false}
            tick={
              <RecentStatisticsBarChartXAxisTick activeLegend={activeLegend} />
            }
            tickMargin={13}
          />
          <YAxis
            type="number"
            domain={[minimumValue, 100]}
            ticks={yAxisTicks}
            axisLine={{ stroke: colors.primary }}
            tickLine={{ stroke: colors.primary }}
            tickFormatter={formatPercentage}
            fontSize="2rem"
            tick={{ fill: colors.primary }}
          />
          {yAxisTicks.map((tick) => (
            <ReferenceLine
              key={tick}
              y={tick}
              stroke={colors.primary}
              strokeDasharray={tick !== 0 ? '5 3' : undefined}
            />
          ))}
          <Legend
            content={<RecentStatisticsBarChartLegend />}
            verticalAlign="bottom"
          />
          <Tooltip
            cursor={false}
            content={<RecentStatisticsBarChartTooltip data={tooltipData} />}
            active={tooltipData !== null}
            isAnimationActive={false}
          />
          {dataKeys.map((dataKey, index) => (
            <Bar
              key={dataKey}
              dataKey={dataKey}
              fill={RECENT_STATISTICS_BAR_CHART_COLORS[index]}
              barSize={39.5}
              onMouseOver={handleBarMouseOver}
              onMouseLeave={handleBarMouseLeave}
              isAnimationActive={false}
            >
              <LabelList
                dataKey={dataKey}
                position="insideTop"
                fill="white"
                fontSize="1.2rem"
                formatter={formatPercentage}
              />
              {data.map((entry, cellIndex) => (
                <Cell
                  key={cellIndex}
                  fill={
                    (entry[dataKey] as number) > 0
                      ? RECENT_STATISTICS_BAR_CHART_COLORS[index]
                      : colors.redAccent
                  }
                />
              ))}
            </Bar>
          ))}
        </BarChart>
      </ResponsiveContainer>
    </ChartWrapper>
  );
};

const ChartWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 0 4.8rem 4.5rem 2.3rem;
`;

export default RecentStatisticsBarChart;
