import styled from 'styled-components';

export type RecentStatisticsBarChartTooltipProps = {
  data: {
    subject: string;
    text: string;
  } | null;
};

const RecentStatisticsBarChartTooltip = ({
  data,
}: RecentStatisticsBarChartTooltipProps) => {
  if (data === null) {
    return null;
  }

  const { subject, text } = data;

  return (
    <ToolTipWrapper>
      <Subject>{subject}</Subject>
      <p>{text}</p>
    </ToolTipWrapper>
  );
};

const ToolTipWrapper = styled.div`
  min-width: 11.2rem;
  height: 5.8rem;
  padding: 1rem 1.1rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
  flex-direction: column;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.white};
  font-size: 1.4rem;
  line-height: 1.9rem;
  color: ${({ theme }) => theme.colors.black};
`;

const Subject = styled.p`
  font-weight: 700;
`;

export default RecentStatisticsBarChartTooltip;
