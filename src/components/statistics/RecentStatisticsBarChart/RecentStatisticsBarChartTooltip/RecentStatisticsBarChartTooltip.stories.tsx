import { Meta, Story } from '@storybook/react';
import RecentStatisticsBarChartTooltip, {
  RecentStatisticsBarChartTooltipProps,
} from './RecentStatisticsBarChartTooltip';

export default {
  title:
    'Components/statistics/RecentStatisticsBarChart/RecentStatisticsBarChartTooltip',
  component: RecentStatisticsBarChartTooltip,
} as Meta;

const Template: Story<RecentStatisticsBarChartTooltipProps> = (args) => (
  <RecentStatisticsBarChartTooltip {...args} />
);

export const Basic = Template.bind({});
