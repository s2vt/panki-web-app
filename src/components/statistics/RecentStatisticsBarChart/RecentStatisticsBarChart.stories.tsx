import { Meta, Story } from '@storybook/react';
import RecentStatisticsBarChart, {
  RecentStatisticsBarChartProps,
} from './RecentStatisticsBarChart';

export default {
  title: 'Components/statistics/RecentStatisticsBarChart',
  component: RecentStatisticsBarChart,
} as Meta;

const Template: Story<RecentStatisticsBarChartProps> = (args) => (
  <RecentStatisticsBarChart {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  data: [
    {
      legend: '2020년 1월',
      '블라스팅 합격률': 90,
      '도장 합격률': 86,
      정도막률: 88,
      '정도막 개선율 (전월대비)': -2.3,
    },
    {
      legend: '2020년 2월',
      '블라스팅 합격률': 70,
      '도장 합격률': 95,
      정도막률: 90,
      '정도막 개선율 (전월대비)': -10,
    },
    {
      legend: '2020년 3월',
      '블라스팅 합격률': 67,
      '도장 합격률': 99,
      정도막률: 92,
      '정도막 개선율 (전월대비)': 5,
    },
  ],
};
