import { LegendProps } from 'recharts';
import styled from 'styled-components';

export type RecentStatisticsBarChartLegendProps = LegendProps;

const RecentStatisticsBarChartLegend = ({
  payload,
}: RecentStatisticsBarChartLegendProps) => (
  <LegendBoxWrapper>
    <LegendBox>
      {payload?.map((entry) => {
        const { value, color } = entry;

        return (
          <LegendWrapper key={value}>
            <Circle color={color} />
            <LegendLabel>{value}</LegendLabel>
          </LegendWrapper>
        );
      })}
    </LegendBox>
  </LegendBoxWrapper>
);

const LegendBoxWrapper = styled.div`
  width: 100%;
  height: 100%;
  margin-top: 4.7rem;
  ${({ theme }) => theme.layout.flexCenterLayout}
`;

const LegendBox = styled.div`
  width: 100%;
  height: 32px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

const LegendWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 1rem;
`;

const Circle = styled.div<{ color: string | undefined }>`
  background: ${({ color }) => color};
  width: 1.8rem;
  height: 1.8rem;
  border-radius: 16px;
`;

const LegendLabel = styled.span`
  font-size: 1.8rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default RecentStatisticsBarChartLegend;
