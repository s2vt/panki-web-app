import { Meta, Story } from '@storybook/react';
import RecentStatisticsBarChartLegend, {
  RecentStatisticsBarChartLegendProps,
} from './RecentStatisticsBarChartLegend';

export default {
  title:
    'Components/statistics/RecentStatisticsBarChart/RecentStatisticsBarChartLegend',
  component: RecentStatisticsBarChartLegend,
} as Meta;

const Template: Story<RecentStatisticsBarChartLegendProps> = (args) => (
  <RecentStatisticsBarChartLegend {...args} />
);

export const Basic = Template.bind({});
