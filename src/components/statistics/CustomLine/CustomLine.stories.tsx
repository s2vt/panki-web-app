import { Meta, Story } from '@storybook/react';
import CustomLine, { CustomLineProps } from './CustomLine';

export default {
  title: 'Components/Statistics/CustomLine',
  component: CustomLine,
} as Meta;

const Template: Story<CustomLineProps> = (args) => <CustomLine {...args} />;

export const Basic = Template.bind({});
