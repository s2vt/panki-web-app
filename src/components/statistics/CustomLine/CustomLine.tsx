import { Line, LineOnMouseOverParams, LineProps } from 'recharts';

export type CustomLineProps = LineProps & {
  onDotMouseOver?: (params: LineOnMouseOverParams) => void;
  onDotMouseLeave?: () => void;
  lineColor: string;
  renderLabelList?: () => React.ReactNode;
};

const CustomLine = ({
  dataKey,
  lineColor,
  onDotMouseOver,
  onDotMouseLeave,
  renderLabelList,
}: CustomLineProps) => (
  <Line
    type="linear"
    dataKey={dataKey}
    stroke={lineColor}
    strokeWidth={2}
    activeDot={false}
    isAnimationActive={false}
    connectNulls
    dot={{
      onMouseOver: onDotMouseOver,
      onMouseLeave: onDotMouseLeave,
      fill: lineColor,
      r: 4,
      cx: 4,
      cy: 4,
    }}
  >
    {renderLabelList !== undefined && renderLabelList()}
  </Line>
);

export default CustomLine;
