import { LegendProps } from 'recharts';
import styled, { css } from 'styled-components';
import Checkbox from '@src/components/common/Checkbox';
import Icon from '@src/components/common/Icon';

export type CheckboxLegendProps = LegendProps & {
  onItemClick: (checkboxLegendItem: CheckboxLegendItem) => void;
  isSelected: (checkboxLegendItem: CheckboxLegendItem) => boolean;
  items: CheckboxLegendItem[];
  iconWidth?: string | number;
  iconHeight?: string | number;
  className?: string;
};

export type CheckboxLegendItem = {
  id: string;
  color: string;
  icon: 'circle' | 'line';
  isSelectable: boolean;
  type: 'target' | 'value';
  referenceValue?: number;
  boxColor?: string;
};

const CheckboxLegend = ({
  onItemClick,
  isSelected,
  items,
  iconWidth,
  iconHeight,
  className,
}: CheckboxLegendProps) => (
  <LegendBox className={className}>
    {items.map((item) => {
      const { id, color, icon, isSelectable } = item;

      return (
        <LegendWrapper
          type="button"
          key={id}
          onClick={isSelectable ? () => onItemClick(item) : undefined}
          isSelectable={isSelectable}
        >
          {isSelectable && (
            <Checkbox
              checkboxIcon="blankCheckbox"
              checked={isSelected(item)}
              readOnly
            />
          )}
          {icon === 'circle' && (
            <Circle
              color={color}
              iconWidth={iconWidth}
              iconHeight={iconHeight}
            />
          )}
          {icon === 'line' && (
            <StyledIcon
              icon="targetLine"
              color={color}
              $iconWidth={iconWidth}
              $iconHeight={iconHeight}
            />
          )}
          <span>{id}</span>
        </LegendWrapper>
      );
    })}
  </LegendBox>
);

const LegendBox = styled.div`
  width: 81.3rem;
  min-height: 7.3rem;
  margin-top: 5.1rem;
  padding: 2.2rem 0;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  gap: 1.2rem 3.4rem;
  box-shadow: 0px 0px 12px 8px rgba(0, 0, 0, 0.05);
`;

const LegendWrapper = styled.button<{ isSelectable: boolean }>`
  border: none;
  background: none;
  display: flex;
  align-items: center;
  gap: 1rem;
  font-size: 1.8rem;
  color: ${({ theme }) => theme.colors.primary};
  ${({ isSelectable }) =>
    isSelectable &&
    css`
      cursor: pointer;
    `};
`;

const Circle = styled.div<{
  color: string | undefined;
  iconWidth?: string | number;
  iconHeight?: string | number;
}>`
  ${({ iconWidth }) =>
    css`
      width: ${iconWidth};
    `}
  ${({ iconHeight }) =>
    css`
      height: ${iconHeight};
    `}

  background: ${({ color }) => color};
  width: 1.8rem;
  height: 1.8rem;
  border-radius: 16px;
`;

const StyledIcon = styled(Icon)<{
  color: string;
  $iconWidth?: string | number;
  $iconHeight?: string | number;
}>`
  ${({ $iconWidth }) =>
    css`
      width: ${$iconWidth};
    `}
  ${({ $iconHeight }) =>
    css`
      height: ${$iconHeight};
    `}

  line {
    ${({ color }) =>
      css`
        stroke: ${color};
      `}
  }
`;

export default CheckboxLegend;
