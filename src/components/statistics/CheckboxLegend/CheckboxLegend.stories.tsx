import { Meta, Story } from '@storybook/react';
import CheckboxLegend, { CheckboxLegendProps } from './CheckboxLegend';

export default {
  title: 'Components/statistics/CheckboxLegend',
  component: CheckboxLegend,
} as Meta;

const Template: Story<CheckboxLegendProps> = (args) => (
  <CheckboxLegend {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  items: [
    {
      color: 'black',
      id: '아이템1',
      icon: 'circle',
      isSelectable: true,
      type: 'value',
    },
    {
      color: 'red',
      id: '아이템2',
      icon: 'circle',
      isSelectable: true,
      type: 'value',
    },
    {
      color: 'purple',
      id: '아이템3',
      icon: 'line',
      isSelectable: false,
      type: 'value',
    },
    {
      color: 'green',
      id: '아이템4',
      icon: 'line',
      isSelectable: true,
      type: 'value',
    },
  ],
  isSelected: () => true,
};
