import { Meta, Story } from '@storybook/react';
import ChartSubject, { ChartSubjectProps } from './ChartSubject';

export default {
  title: 'Components/statistics/ChartSubject',
  component: ChartSubject,
} as Meta;

const Template: Story<ChartSubjectProps> = (args) => <ChartSubject {...args} />;

export const Basic = Template.bind({});
