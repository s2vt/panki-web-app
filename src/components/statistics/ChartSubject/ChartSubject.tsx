import styled from 'styled-components';
import {
  CHART_DOWNLOAD,
  TURN_OFF_ALL_LEGENDS,
} from '@src/constants/constantString';
import IconButton from '@src/components/common/IconButton';
import MainButton from '@src/components/common/MainButton';
import ChartSubjectBase from '../ChartSubjectBase';

export type ChartSubjectProps = {
  subject: string;
  isLegendButtonEnable: boolean;
  onTurnOffAllLegendsClick?: () => void;
};

const ChartSubject = ({
  subject,
  isLegendButtonEnable,
  onTurnOffAllLegendsClick,
}: ChartSubjectProps) => (
  <ChartSubjectBase.SubjectSection>
    <ChartSubjectBase.SubjectWrapper>
      <ChartSubjectBase.Subject>{subject}</ChartSubjectBase.Subject>
    </ChartSubjectBase.SubjectWrapper>
    <ButtonSection>
      {isLegendButtonEnable && (
        <MainButton
          width="12.3rem"
          height="3.3rem"
          label={TURN_OFF_ALL_LEGENDS}
          labelColor="primary"
          backgroundColor="white"
          fontWeight={700}
          fontSize="1.4rem"
          onClick={onTurnOffAllLegendsClick}
        />
      )}
      <MainButton
        width="12.3rem"
        height="3.3rem"
        label={CHART_DOWNLOAD}
        labelColor="primary"
        backgroundColor="white"
        fontWeight={700}
        fontSize="1.4rem"
      />
      <StyledIconButton
        icon="fullModeIcon"
        backgroundColor="white"
        width="7.1rem"
        height="3.3rem"
        iconWidth="1.784rem"
        iconHeight="1.784rem"
      />
    </ButtonSection>
  </ChartSubjectBase.SubjectSection>
);

const ButtonSection = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 0.7rem;
`;

const StyledIconButton = styled(IconButton)`
  margin-left: 0.5rem;
`;

export default ChartSubject;
