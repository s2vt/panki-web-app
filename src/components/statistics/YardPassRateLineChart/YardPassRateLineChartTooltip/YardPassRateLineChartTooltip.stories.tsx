import { Meta, Story } from '@storybook/react';
import YardPassRateLineChartTooltip, {
  YardPassRateLineChartTooltipProps,
} from './YardPassRateLineChartTooltip';

export default {
  title:
    'Components/statistics/YardPassRateLineChart/YardPassRateLineChartTooltip',
  component: YardPassRateLineChartTooltip,
} as Meta;

const Template: Story<YardPassRateLineChartTooltipProps> = (args) => (
  <YardPassRateLineChartTooltip {...args} />
);

export const Basic = Template.bind({});
