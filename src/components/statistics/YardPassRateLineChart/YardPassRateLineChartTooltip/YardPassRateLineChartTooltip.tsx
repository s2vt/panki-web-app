import styled from 'styled-components';
import { REFERENCE_CONTRAST } from '@src/constants/constantString';

export type YardPassRateLineChartTooltipProps = {
  data: YardPassRateLineChartTooltipData | null;
  subject: string;
};

export type YardPassRateLineChartTooltipData = {
  company: string;
  legend: string;
  value: number;
  referenceContrastValue: number;
};

const YardPassRateLineChartTooltip = ({
  data,
  subject,
}: YardPassRateLineChartTooltipProps) => {
  if (data === null) {
    return null;
  }

  const { company, legend, value, referenceContrastValue } = data;

  return (
    <Box>
      <p>{company}</p>
      <p>{legend}</p>
      <p>{`${subject} ${value}%`}</p>
      <p>{`${REFERENCE_CONTRAST} ${referenceContrastValue.toLocaleString()}%`}</p>
    </Box>
  );
};

const Box = styled.div`
  min-width: 21.4rem;
  padding: 1.1rem 1rem;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.white};
  font-size: 1.4rem;
  line-height: 1.9rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default YardPassRateLineChartTooltip;
