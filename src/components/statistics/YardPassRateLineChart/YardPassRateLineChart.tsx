import React, { memo, useCallback, useMemo, useState } from 'react';
import {
  LineChart,
  XAxis,
  YAxis,
  ReferenceLine,
  Tooltip,
  LineOnMouseOverParams,
  ChartMouseMoveParams,
  Label,
} from 'recharts';
import styled from 'styled-components';
import { PERIOD, TARGET } from '@src/constants/constantString';
import useSelectItems from '@src/hooks/common/useSelectItems';
import { LINE_CHART_COLORS } from '@src/styles/chartColors';
import { colors } from '@src/styles/theme';
import {
  ChartData,
  ChartFilterDateType,
  getChartDataKeys,
  getChartDataMinimumValue,
} from '@src/types/statistics.types';
import ChartBase from '../ChartBase';
import ChartSubject from '../ChartSubject';
import CheckboxLegend, { CheckboxLegendItem } from '../CheckboxLegend';
import CustomLine from '../CustomLine';
import CustomReferenceLine from '../CustomReferenceLine';
import DateXAxisTick from '../DateXAxisTick';
import DotLabelList from '../DotLabelList';
import PassRateLineChartTooltip, {
  PassRateLineChartTooltipData,
} from './YardPassRateLineChartTooltip';

export type YardPassRateLineChartProps = {
  subject: string;
  title: string;
  data: ChartData[];
  referenceValue: number;
  chartFilterDateType: ChartFilterDateType;
};

const YEAR_X_AXIS_MAXIMUM_PADDING = 310;
const WEEK_X_AXIS_MAXIMUM_PADDING = 60;

const YardPassRateLineChart = ({
  subject,
  title,
  data,
  referenceValue,
  chartFilterDateType,
}: YardPassRateLineChartProps) => {
  const [activeLegend, setActiveLegend] = useState<string>();
  const [tooltipData, setTooltipData] =
    useState<PassRateLineChartTooltipData | null>(null);

  const dataKeys = useMemo(() => getChartDataKeys(data), [data]);

  const minimumValue = useMemo(() => getChartDataMinimumValue(data), [data]);

  const xAxisPadding = useMemo(() => {
    if (chartFilterDateType === ChartFilterDateType.year) {
      const legends = data.map((datum) => datum.legend);

      const paddingByTick = 20;

      return YEAR_X_AXIS_MAXIMUM_PADDING - legends.length * paddingByTick;
    }

    return WEEK_X_AXIS_MAXIMUM_PADDING;
  }, [data]);

  const targetLegendItem: CheckboxLegendItem = useMemo(
    () => ({
      id: TARGET,
      icon: 'line',
      isSelectable: false,
      color: colors.brightBlue,
      type: 'target',
    }),
    [],
  );

  const legendItems: CheckboxLegendItem[] = useMemo(
    () =>
      dataKeys
        .map(
          (dataKey, index) =>
            ({
              id: dataKey,
              color: LINE_CHART_COLORS[index],
              icon: 'circle',
              isSelectable: true,
              type: 'value',
            } as CheckboxLegendItem),
        )
        .concat(targetLegendItem),
    [dataKeys],
  );

  const yAxisTicks = useMemo(() => {
    const ticks = [50, 60, 70, 80, 90, 100];

    if (minimumValue < 50) {
      ticks.unshift(minimumValue - (minimumValue % 5));
    }

    const foundTargetIndex = ticks.indexOf(referenceValue);

    if (foundTargetIndex >= 0) {
      ticks.splice(foundTargetIndex, 1);
    }

    return ticks;
  }, [minimumValue]);

  const { selectedItems, toggleItem, isSelected, setSelectedItems } =
    useSelectItems<CheckboxLegendItem>(legendItems);

  const formatPercentage = useCallback(
    (value: string | number) => `${value}%`,
    [],
  );

  const handleTurnOffAllLegendsClick = useCallback(() => {
    setSelectedItems([]);
  }, [setSelectedItems]);

  const handleDotMouseOver = ({
    dataKey,
    value,
    payload,
  }: LineOnMouseOverParams) => {
    setTooltipData({
      company: dataKey,
      value,
      legend: payload.legend,
      referenceContrastValue: value - referenceValue,
    });
  };

  const handleDotMouseLeave = () => setTooltipData(null);

  const handleLineChartMouseMove = ({
    isTooltipActive,
    activeLabel,
  }: ChartMouseMoveParams) => {
    if (!isTooltipActive) {
      setActiveLegend(undefined);
      return;
    }

    setActiveLegend(activeLabel);
  };

  return (
    <Box>
      <ChartSubject
        subject={subject}
        isLegendButtonEnable
        onTurnOffAllLegendsClick={handleTurnOffAllLegendsClick}
      />
      <ChartBase.ChartWrapper>
        <ChartBase.TitleSection>{title}</ChartBase.TitleSection>
        <LineChart
          width={923}
          height={420}
          data={data}
          margin={{ top: 58, bottom: 68, left: 18, right: 58 }}
          onMouseMove={handleLineChartMouseMove}
        >
          <XAxis
            padding={{
              left: xAxisPadding,
              right: xAxisPadding,
            }}
            type="category"
            dataKey="legend"
            axisLine={false}
            tickLine={false}
            interval={0}
            tick={
              dataKeys.length > 0 ? (
                <DateXAxisTick
                  chartFilterDateType={chartFilterDateType}
                  activeLegend={activeLegend}
                />
              ) : (
                false
              )
            }
            tickMargin={26}
          />
          <YAxis
            type="number"
            domain={[minimumValue, 100]}
            ticks={yAxisTicks}
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            tick={{ fill: colors.primary }}
            tickFormatter={formatPercentage}
          >
            <Label
              value={PERIOD}
              offset={-70}
              position="insideBottom"
              fontSize="2rem"
              fill={colors.primary}
            />
          </YAxis>
          <Tooltip
            cursor={{ stroke: colors.primary }}
            content={
              <PassRateLineChartTooltip data={tooltipData} subject={subject} />
            }
            isAnimationActive={false}
          />
          {yAxisTicks.map((tick, index) => (
            <ReferenceLine
              key={tick}
              y={tick}
              stroke={colors.primary}
              strokeDasharray={index !== 0 ? '5 3' : undefined}
            />
          ))}
          {CustomReferenceLine({
            label: `${referenceValue}%`,
            referenceValue,
            lineColor: colors.brightBlue,
            labelBoxColor: colors.primary,
          })}
          {selectedItems.map(
            (selectedItem) =>
              selectedItem.type === 'value' && (
                <React.Fragment key={selectedItem.id}>
                  {CustomLine({
                    dataKey: selectedItem.id,
                    lineColor: selectedItem.color,
                    onDotMouseOver: handleDotMouseOver,
                    onDotMouseLeave: handleDotMouseLeave,
                    renderLabelList: () =>
                      DotLabelList({
                        formatter: formatPercentage,
                      }),
                  })}
                </React.Fragment>
              ),
          )}
        </LineChart>
        <CheckboxLegend
          isSelected={isSelected}
          onItemClick={toggleItem}
          items={legendItems}
        />
      </ChartBase.ChartWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
`;

export default memo(YardPassRateLineChart);
