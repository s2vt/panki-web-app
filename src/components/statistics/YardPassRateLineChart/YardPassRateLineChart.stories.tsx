import { Meta, Story } from '@storybook/react';
import { ChartData, ChartFilterDateType } from '@src/types/statistics.types';
import PassRateLineChart from '.';
import { YardPassRateLineChartProps } from './YardPassRateLineChart';

export default {
  title: 'Components/statistics/YardPassRateLineChart',
  component: PassRateLineChart,
} as Meta;

const Template: Story<YardPassRateLineChartProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <PassRateLineChart {...args} />
  </div>
);

const PERIOD_DATA: ChartData[] = [
  {
    legend: '2021.09.01 ~ 2021.09.08',
    작업업체1: 65,
    작업업체2: 50,
    작업업체3: 50,
    작업업체4: 88,
    작업업체5: 50,
    작업업체6: 78,
    작업업체7: 99,
  },
  {
    legend: '2021.09.09 ~ 2021.09.16',
    작업업체1: 50.1,
    작업업체2: 70,
    작업업체3: 50,
    작업업체4: 67,
    작업업체5: 88,
    작업업체6: 50,
    작업업체7: 50,
  },
  {
    legend: '2021.09.17 ~ 2021.09.23',
    작업업체1: 50.9,
    작업업체2: 70,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 90,
    작업업체6: 88,
    작업업체7: 54,
  },
  {
    legend: '2021.09.24 ~ 2021.09.30',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 38,
    작업업체5: 75,
    작업업체6: 70,
    작업업체7: 56,
  },
];

const YEAR_DATA: ChartData[] = [
  {
    legend: '2021년 1월',
    작업업체1: 65,
    작업업체2: 50,
    작업업체3: 50,
    작업업체4: 88,
    작업업체5: 50,
  },
  {
    legend: '2021년 2월',
    작업업체1: 50.1,
    작업업체2: 70,
    작업업체3: 50,
    작업업체4: 67,
    작업업체5: 88,
  },
  {
    legend: '2021년 3월',
    작업업체1: 50.9,
    작업업체2: 70,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 90,
  },
  {
    legend: '2021년 4월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 5월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 6월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 7월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 8월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 9월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 10월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 11월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
  {
    legend: '2021년 12월',
    작업업체1: 88,
    작업업체2: 60,
    작업업체3: 50,
    작업업체4: 50,
    작업업체5: 75,
  },
];

export const PeriodMode = Template.bind({});
PeriodMode.args = {
  subject: '주제',
  title: '타이틀',
  data: PERIOD_DATA,
  referenceValue: 90,
  chartFilterDateType: ChartFilterDateType.period,
};

export const YearMode = Template.bind({});
YearMode.args = {
  subject: '주제',
  title: '타이틀',
  data: YEAR_DATA,
  referenceValue: 90,
  chartFilterDateType: ChartFilterDateType.year,
};
