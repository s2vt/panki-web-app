import styled from 'styled-components';
import ChartSubjectBase from '../../ChartSubjectBase';

export type AnalysisSubjectProps = {
  subject: string;
  description: string;
};

const AnalysisSubject = ({ subject, description }: AnalysisSubjectProps) => (
  <>
    <ChartSubjectBase.SubjectWrapper>
      <ChartSubjectBase.Subject>{subject}</ChartSubjectBase.Subject>
    </ChartSubjectBase.SubjectWrapper>
    <Description>{description}</Description>
  </>
);

const Description = styled.p`
  margin-top: 1.4rem;
  font-size: 2rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default AnalysisSubject;
