import { Meta, Story } from '@storybook/react';
import AnalysisSubject, { AnalysisSubjectProps } from './AnalysisSubject';

export default {
  title: 'Components/Statistics/SemanticAnalysis/AnalysisSubject',
  component: AnalysisSubject,
} as Meta;

const Template: Story<AnalysisSubjectProps> = (args) => (
  <AnalysisSubject {...args} />
);

export const Basic = Template.bind({});
