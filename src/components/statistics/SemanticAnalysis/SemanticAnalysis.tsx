import { useCallback } from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import {
  COATING_PATTERN_ANALYSIS,
  COATING_PATTERN_ANALYSIS_DESCRIPTION,
  COATING_PATTERN_BY_SECTION,
  GO_TO_STATISTICS_BY_COMPANY,
  SEMANTIC_ANALYSIS_PAGE_DESCRIPTION,
  ORIGIN_COATING_RATE_AND_PASS_RATE,
  RELEVANT_STATISTICAL_STATUS,
  RELEVANT_STATISTICAL_STATUS_DESCRIPTION,
} from '@src/constants/constantString';
import useSubcontractorCompanies from '@src/hooks/company/useSubcontractorCompanies';
import useUserSelector from '@src/hooks/user/useUserSelector';
import { ROUTE_PATHS } from '@src/routes/routePaths';
import Divider from '@src/components/common/Divider';
import LoadingSpinner from '@src/components/common/LoadingSpinner';
import MainButton from '@src/components/common/MainButton';
import { Box } from '@mui/material';
import InquiryTargetBox from '../InquiryTargetBox';
import StatisticsPagesBreadCrumbs from '../StatisticsPagesBreadCrumbs';
import AnalysisSubject from './AnalysisSubject';
import SemanticAnalysisBox from './SemanticAnalysisBox';
import OpenPageSubject from './OpenPageSubject';

export type SemanticAnalysisProps = {};

const SemanticAnalysis = () => {
  const history = useHistory();

  const { companyId } = useUserSelector();

  const { data: subcontractorCompanies, isLoading: isCompaniesLoading } =
    useSubcontractorCompanies(companyId ?? '', {
      enabled: companyId !== '',
    });

  const handleInquiryTargetButtonClick = useCallback(() => {
    history.push(ROUTE_PATHS.statisticsCompany);
  }, [history]);

  return (
    <Box>
      <StatisticsPagesBreadCrumbs />
      <Description>{SEMANTIC_ANALYSIS_PAGE_DESCRIPTION}</Description>
      <StyledDivider orientation="horizontal" />
      {isCompaniesLoading && <LoadingSpinner />}
      {subcontractorCompanies !== undefined && (
        <>
          <InquiryTargetBox
            subcontractorCompanies={subcontractorCompanies}
            buttonLabel={GO_TO_STATISTICS_BY_COMPANY}
            onButtonClick={handleInquiryTargetButtonClick}
          />
          <StyledDivider orientation="horizontal" />
          <AnalysisSubject
            subject={COATING_PATTERN_ANALYSIS}
            description={COATING_PATTERN_ANALYSIS_DESCRIPTION}
          />
          <SemanticAnalysisBox />
          <AnalysisSubject
            subject={RELEVANT_STATISTICAL_STATUS}
            description={RELEVANT_STATISTICAL_STATUS_DESCRIPTION}
          />
          <StyledMainButton
            width="20.6rem"
            height="3.3rem"
            fontSize="1.4rem"
            fontWeight={700}
            label={GO_TO_STATISTICS_BY_COMPANY}
            onClick={handleInquiryTargetButtonClick}
          />
          <StyledDivider orientation="horizontal" />
          <OpenPageSubject subject={ORIGIN_COATING_RATE_AND_PASS_RATE} />
          <StyledDivider orientation="horizontal" />
          <OpenPageSubject subject={COATING_PATTERN_BY_SECTION} />
        </>
      )}
    </Box>
  );
};

const Description = styled.p`
  margin-top: 2.6rem;
  font-size: 2rem;
  color: ${({ theme }) => theme.colors.primary};
`;

const StyledDivider = styled(Divider)`
  margin: 3.8rem 0 2.6rem;
`;

const StyledMainButton = styled(MainButton)`
  margin-top: 1.7rem;
`;

export default SemanticAnalysis;
