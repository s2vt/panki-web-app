import { Meta, Story } from '@storybook/react';
import SemanticAnalysis, { SemanticAnalysisProps } from './SemanticAnalysis';

export default {
  title: 'Components/Statistics/SemanticAnalysis',
  component: SemanticAnalysis,
} as Meta;

const Template: Story<SemanticAnalysisProps> = (args) => (
  <SemanticAnalysis {...args} />
);

export const Basic = Template.bind({});
