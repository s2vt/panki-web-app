import { memo } from 'react';
import styled from 'styled-components';
import {
  QUALITY_MANAGEMENT,
  COST_MANAGEMENT,
  MAIN_FEATURES,
  PATTERN,
} from '@src/constants/constantString';

export type SemanticAnalysisBoxProps = {};

const SemanticAnalysisBox = () => (
  <Box>
    <Subject>품질관리VS원가관리</Subject>
    <TableBox>
      <ColumnSection>
        <RowTitleCell />
        <Column>{QUALITY_MANAGEMENT}</Column>
        <Column>{COST_MANAGEMENT}</Column>
      </ColumnSection>
      <RowSection>
        <RowTitleCell>{MAIN_FEATURES}</RowTitleCell>
        <Row>
          <RowItem>정도막률 90% (검사기간 평균)</RowItem>
          <RowItem>블라스팅 검사 92% (검사기간 평균)</RowItem>
          <RowItem>도장 검사 합격률 78% (검사기간 평균)</RowItem>
        </Row>
        <Row>
          <RowItem>정도막률 90% (검사기간 평균)</RowItem>
          <RowItem>블라스팅 검사 92% (검사기간 평균)</RowItem>
          <RowItem>도장 검사 합격률 78% (검사기간 평균)</RowItem>
        </Row>
      </RowSection>
      <RowSection>
        <RowTitleCell>{PATTERN}</RowTitleCell>
        <Row>
          <RowItem>정도막률 평균 대비 -00%로, 작업 업체 평균 대비 -12%</RowItem>
        </Row>
        <Row>
          <RowItem>블록 도장 시, 앞구간에서의 페인트 절감 필요</RowItem>
        </Row>
      </RowSection>
    </TableBox>
  </Box>
);

const Box = styled.div`
  margin: 3.2rem 0 5.8rem;
`;

const Subject = styled.div`
  margin-bottom: 1.6rem;
  font-size: 2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const TableBox = styled.div`
  width: 100%;
  min-height: 40.2rem;
  display: flex;
  flex-direction: column;
  background: ${({ theme }) => theme.colors.white};
  border-top: 1px solid ${({ theme }) => theme.colors.primary};
  border-left: 1px solid ${({ theme }) => theme.colors.primary};
`;

const ColumnSection = styled.div`
  display: flex;
  height: 10rem;
  border-bottom: 1px solid ${({ theme }) => theme.colors.primary};
`;

const TitleCell = styled.div`
  ${({ theme }) => theme.layout.flexCenterLayout}
  font-size: 1.8rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

const Column = styled(TitleCell)`
  ${({ theme }) => theme.layout.flexCenterLayout}
  display: flex;
  max-height: 10rem;
  flex: 1;
  border-right: 1px solid ${({ theme }) => theme.colors.primary};
`;

const RowSection = styled.div`
  flex: 1;
  display: flex;
  border-bottom: 1px solid ${({ theme }) => theme.colors.primary};
`;

const RowTitleCell = styled(TitleCell)`
  ${({ theme }) => theme.layout.flexCenterLayout}
  flex: 1;
  max-width: 14.4rem;
  border-right: 1px solid ${({ theme }) => theme.colors.primary};
`;

const Row = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 3.2rem 2.8rem 2.1rem 1.6rem;
  flex: 1;
  border-right: 1px solid ${({ theme }) => theme.colors.primary};
`;

const RowItem = styled.div`
  padding-left: 2rem;
  position: relative;
  font-size: 1.8rem;
  line-height: 3rem;
  color: ${({ theme }) => theme.colors.primary};

  ::before {
    content: '';
    position: absolute;
    width: 0.6rem;
    height: 0.6rem;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
    background: ${({ theme }) => theme.colors.primary};
    border-radius: 16px;
  }
`;

export default memo(SemanticAnalysisBox);
