import { Meta, Story } from '@storybook/react';
import SemanticAnalysisBox, {
  SemanticAnalysisBoxProps,
} from './SemanticAnalysisBox';

export default {
  title: 'Components/Statistics/SemanticAnalysis/SemanticAnalysisBox',
  component: SemanticAnalysisBox,
} as Meta;

const Template: Story<SemanticAnalysisBoxProps> = (args) => (
  <SemanticAnalysisBox {...args} />
);

export const Basic = Template.bind({});
