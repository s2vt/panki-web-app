import { Meta, Story } from '@storybook/react';
import OpenPageSubject, { OpenPageSubjectProps } from './OpenPageSubject';

export default {
  title: 'Components/Statistics/SemanticAnalysis/OpenPageSubjcet',
  component: OpenPageSubject,
} as Meta;

const Template: Story<OpenPageSubjectProps> = (args) => (
  <OpenPageSubject {...args} />
);

export const Basic = Template.bind({});
