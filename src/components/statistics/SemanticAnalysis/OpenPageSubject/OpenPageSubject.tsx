import styled from 'styled-components';
import { OPEN_IN_NEW_WINDOW } from '@src/constants/constantString';
import MainButton from '@src/components/common/MainButton';

export type OpenPageSubjectProps = {
  subject: string;
  onOpenPageClick?: () => void;
};

const OpenPageSubject = ({
  subject,
  onOpenPageClick,
}: OpenPageSubjectProps) => (
  <Box>
    <Subject>{subject}</Subject>
    <MainButton
      onClick={onOpenPageClick}
      label={OPEN_IN_NEW_WINDOW}
      backgroundColor="white"
      labelColor="primary"
      fontWeight={700}
      fontSize="1.4rem"
      width="16.4rem"
      height="3.3rem"
    />
  </Box>
);

const Box = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const Subject = styled.p`
  font-size: 2.2rem;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.primary};
`;

export default OpenPageSubject;
