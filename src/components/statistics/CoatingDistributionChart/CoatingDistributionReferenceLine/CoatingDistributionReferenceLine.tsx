import { Label, ReferenceLine, LabelProps } from 'recharts';

export type CoatingDistributionReferenceLineProps = LabelProps & {
  referenceValue?: number;
  label: string;
  lineColor: string;
};

const CoatingDistributionReferenceLine = ({
  referenceValue,
  label,
  lineColor,
  position = 'top',
}: CoatingDistributionReferenceLineProps) => (
  <ReferenceLine x={referenceValue} stroke={lineColor} strokeWidth={3}>
    <Label
      position={position}
      value={label}
      fill={lineColor}
      fontSize="1.6rem"
      fontWeight={700}
    />
  </ReferenceLine>
);

export default CoatingDistributionReferenceLine;
