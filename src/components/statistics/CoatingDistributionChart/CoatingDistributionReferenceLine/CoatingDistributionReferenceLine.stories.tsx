import { Meta, Story } from '@storybook/react';
import CoatingDistributionReferenceLine, {
  CoatingDistributionReferenceLineProps,
} from './CoatingDistributionReferenceLine';

export default {
  title:
    'Components/Statistics/CoatingDistributionChart/CoatingDistributionReferenceLine',
  component: CoatingDistributionReferenceLine,
} as Meta;

const Template: Story<CoatingDistributionReferenceLineProps> = (args) => (
  <CoatingDistributionReferenceLine {...args} />
);

export const Basic = Template.bind({});
