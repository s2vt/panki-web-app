import React, { memo, useCallback, useMemo } from 'react';
import {
  Bar,
  CartesianGrid,
  ComposedChart,
  Label,
  Line,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import styled from 'styled-components';
import {
  AVERAGE_COATING,
  COATING_THICKNESS,
  MAXIMUM_VALUE,
  MICROMETER,
  MINIMUM_VALUE,
  NUMBER_OF_TIMES,
  REGULATION_COATING,
} from '@src/constants/constantString';
import { COATING_DISTRIBUTION_BAR_COLOR } from '@src/styles/chartColors';
import { colors } from '@src/styles/theme';
import {
  CoatingDistributionData,
  CoatingFilterTask,
  getChartDataMaximumValue,
} from '@src/types/statistics.types';
import { debugAssert } from '@src/utils/commonUtil';
import ChartBase from '../ChartBase';
import ChartSubject from '../ChartSubject';
import CoatingDistributionChartTooltip from './CoatingDistributionChartTooltip/CoatingDistributionChartTooltip';
import CoatingDistributionReferenceLine, {
  CoatingDistributionReferenceLineProps,
} from './CoatingDistributionReferenceLine';

export type CoatingDistributionChartProps = {
  coatingDistributionData: CoatingDistributionData;
  subject: string;
  companyName: string;
  coatingFilterTask: CoatingFilterTask;
};

const FIRST_COATING_TASK_REGULATION_COATING_VALUE = 160;
const FINAL_COATING_TASK_REGULATION_COATING_VALUE = 320;
const TOTAL_COATING_TASK_MAXIMUM_VALUE = 800;

const CoatingDistributionChart = ({
  coatingDistributionData,
  subject,
  companyName,
  coatingFilterTask,
}: CoatingDistributionChartProps) => {
  const { average, data } = coatingDistributionData;

  const minimumLegend = useMemo(
    () => Math.min(...data.map((datum) => datum.legend as number)),
    [data],
  );

  const maximumLegend = useMemo(
    () => Math.max(...data.map((datum) => datum.legend as number)),
    [data],
  );

  const maximumValue = useMemo(() => getChartDataMaximumValue(data), [data]);

  const lineChartData = useMemo(
    () =>
      data.map((datum) => ({
        ...datum,
        lineValue: (datum.value as number) + 15,
      })),
    [data],
  );

  const createReferenceLineDataByCoatingTask = useCallback(() => {
    const minimumValueReference: CoatingDistributionReferenceLineProps = {
      label: MINIMUM_VALUE,
      lineColor: colors.lightPink,
    };

    const maximumValueReference: CoatingDistributionReferenceLineProps = {
      label: MAXIMUM_VALUE,
      lineColor: colors.secondary,
      position: { x: 20, y: -5 },
    };

    const regulationCoatingValueReference: CoatingDistributionReferenceLineProps =
      {
        label: REGULATION_COATING,
        lineColor: colors.lightGreen,
      };

    const averageValueReference: CoatingDistributionReferenceLineProps = {
      label: AVERAGE_COATING,
      lineColor: colors.brightBlue,
      referenceValue: average,
      position: { x: 20, y: -25 },
    };

    switch (coatingFilterTask) {
      case CoatingFilterTask.first:
        minimumValueReference.referenceValue =
          FIRST_COATING_TASK_REGULATION_COATING_VALUE * 0.8;
        minimumValueReference.position = {
          x: 0,
          y: -5,
        };
        maximumValueReference.referenceValue =
          FIRST_COATING_TASK_REGULATION_COATING_VALUE * 2;
        regulationCoatingValueReference.referenceValue =
          FIRST_COATING_TASK_REGULATION_COATING_VALUE;
        regulationCoatingValueReference.position = {
          x: 40,
          y: -5,
        };
        break;
      case CoatingFilterTask.final:
        minimumValueReference.referenceValue =
          FINAL_COATING_TASK_REGULATION_COATING_VALUE * 0.8;
        minimumValueReference.position = {
          x: 0,
          y: -5,
        };
        maximumValueReference.referenceValue =
          FINAL_COATING_TASK_REGULATION_COATING_VALUE * 2;
        regulationCoatingValueReference.referenceValue =
          FINAL_COATING_TASK_REGULATION_COATING_VALUE;
        break;
      case CoatingFilterTask.all:
        minimumValueReference.referenceValue =
          FINAL_COATING_TASK_REGULATION_COATING_VALUE * 0.8;
        minimumValueReference.position = {
          x: 0,
          y: -5,
        };
        maximumValueReference.referenceValue = TOTAL_COATING_TASK_MAXIMUM_VALUE;
        regulationCoatingValueReference.referenceValue =
          FINAL_COATING_TASK_REGULATION_COATING_VALUE;
        break;
      default:
        debugAssert(false, 'Coating filter task is not includes this task');
    }

    return [
      minimumValueReference,
      maximumValueReference,
      regulationCoatingValueReference,
      averageValueReference,
    ];
  }, [coatingFilterTask]);

  const referenceLineData = useMemo(
    () => createReferenceLineDataByCoatingTask(),
    [createReferenceLineDataByCoatingTask],
  );

  return (
    <Box>
      <ChartSubject isLegendButtonEnable={false} subject={subject} />
      <ChartBase.ChartWrapper>
        <ComposedChart
          width={929}
          height={441}
          data={lineChartData}
          margin={{ top: 84, right: 102, bottom: 42, left: 24 }}
          barSize={38}
          barGap={17}
        >
          <CartesianGrid
            vertical={false}
            strokeDasharray="5 3"
            stroke={colors.primary}
          />
          <XAxis
            type="number"
            dataKey="legend"
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            color={colors.primary}
            tickMargin={23}
            tick={{ fill: colors.primary }}
            domain={[minimumLegend, maximumLegend]}
            padding={{ left: 24, right: 24 }}
          >
            <Label
              value={COATING_THICKNESS}
              position={{ x: 842, y: 42 }}
              fontSize="2rem"
              fill={colors.primary}
            />
            <Label
              value={`(${MICROMETER})`}
              position={{ x: 844, y: 62 }}
              fontSize="2rem"
              fill={colors.primary}
            />
          </XAxis>
          <YAxis
            axisLine={false}
            tickLine={false}
            fontSize="2rem"
            tick={{ fill: colors.primary }}
          >
            <Label
              value={NUMBER_OF_TIMES}
              offset={24}
              position="top"
              fontSize="2rem"
              fill={colors.primary}
            />
          </YAxis>
          <Tooltip
            cursor={{ stroke: colors.primary }}
            content={
              <CoatingDistributionChartTooltip companyName={companyName} />
            }
            isAnimationActive={false}
          />
          <Bar
            dataKey="value"
            fill={COATING_DISTRIBUTION_BAR_COLOR}
            isAnimationActive={false}
          />
          {referenceLineData.map((datum) => {
            const { label, lineColor, referenceValue, position } = datum;

            return (
              <React.Fragment key={label}>
                {CoatingDistributionReferenceLine({
                  label,
                  lineColor,
                  referenceValue,
                  position,
                })}
              </React.Fragment>
            );
          })}
          {maximumValue > 0 && (
            <Line
              dataKey="lineValue"
              type="monotone"
              data={lineChartData}
              dot={false}
              stroke={colors.primary}
              strokeWidth={1.5}
              isAnimationActive={false}
              connectNulls
              tooltipType="none"
            />
          )}
        </ComposedChart>
      </ChartBase.ChartWrapper>
    </Box>
  );
};

const Box = styled.div`
  width: 100%;
`;

export default memo(CoatingDistributionChart);
