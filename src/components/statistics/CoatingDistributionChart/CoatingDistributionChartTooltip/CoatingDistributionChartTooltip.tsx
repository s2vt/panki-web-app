import { TooltipProps } from 'recharts';
import styled from 'styled-components';
import { COATING_THICKNESS, MICROMETER } from '@src/constants/constantString';

export type CoatingDistributionChartTooltipProps = TooltipProps<
  number,
  number
> & {
  companyName: string;
};

export type CoatingDistributionChartTooltipData = {
  coatingThickness: number;
  count: number;
};

const CoatingDistributionChartTooltip = ({
  companyName,
  payload,
}: CoatingDistributionChartTooltipProps) => {
  if (payload === undefined) {
    return null;
  }

  const coatingThickness = payload[0]?.payload?.legend;
  const count = payload[0]?.payload?.value;

  return (
    <Box>
      <p>{companyName}</p>
      <p>{`${COATING_THICKNESS} ${coatingThickness} ${MICROMETER}`}</p>
      <p>{`${count}회`}</p>
    </Box>
  );
};

const Box = styled.div`
  min-width: 12.1rem;
  padding: 1rem 1.1rem;
  display: flex;
  flex-direction: column;
  gap: 0.2rem;
  background: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  font-size: 1.4rem;
  line-height: 1.9rem;
  color: ${({ theme }) => theme.colors.primary};
`;

export default CoatingDistributionChartTooltip;
