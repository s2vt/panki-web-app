import { Meta, Story } from '@storybook/react';
import CoatingDistributionChartTooltip, {
  CoatingDistributionChartTooltipProps,
} from './CoatingDistributionChartTooltip';

export default {
  title:
    'Components/Statistics/CoatingDistributionChart/CoatingDistributionChartTooltip',
  component: CoatingDistributionChartTooltip,
} as Meta;

const Template: Story<CoatingDistributionChartTooltipProps> = (args) => (
  <CoatingDistributionChartTooltip {...args} />
);

export const Basic = Template.bind({});
