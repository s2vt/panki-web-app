import { Meta, Story } from '@storybook/react';
import { CoatingFilterTask } from '@src/types/statistics.types';
import CoatingDistributionChart, {
  CoatingDistributionChartProps,
} from './CoatingDistributionChart';

export default {
  title: 'Components/Statistics/CoatingDistributionChart',
  component: CoatingDistributionChart,
} as Meta;

const Template: Story<CoatingDistributionChartProps> = (args) => (
  <div style={{ width: '1022px' }}>
    <CoatingDistributionChart {...args} />
  </div>
);

export const FirstCoating = Template.bind({});
FirstCoating.args = {
  coatingFilterTask: CoatingFilterTask.first,
  coatingDistributionData: {
    average: 338.54450637759464,
    data: [
      { legend: 0, value: 0 },
      { legend: 10, value: 0 },
      { legend: 20, value: 0 },
      { legend: 30, value: 0 },
      { legend: 40, value: 0 },
      { legend: 50, value: 0 },
      { legend: 60, value: 0 },
      { legend: 70, value: 0 },
      { legend: 80, value: 0 },
      { legend: 90, value: 0 },
      { legend: 100, value: 0 },
      { legend: 110, value: 0 },
      { legend: 120, value: 0 },
      { legend: 130, value: 0 },
      { legend: 140, value: 0 },
      { legend: 150, value: 0 },
      { legend: 160, value: 0 },
      { legend: 170, value: 0 },
      { legend: 180, value: 0 },
      { legend: 190, value: 0 },
      { legend: 200, value: 0 },
      { legend: 210, value: 0 },
      { legend: 220, value: 0 },
      { legend: 230, value: 0 },
      { legend: 240, value: 0 },
      { legend: 250, value: 5 },
      { legend: 260, value: 10 },
      { legend: 270, value: 37 },
      { legend: 280, value: 113 },
      { legend: 290, value: 311 },
      { legend: 300, value: 900 },
      { legend: 310, value: 2680 },
      { legend: 320, value: 5633 },
      { legend: 330, value: 6183 },
      { legend: 340, value: 5521 },
      { legend: 350, value: 3945 },
      { legend: 360, value: 2269 },
      { legend: 370, value: 1054 },
      { legend: 380, value: 392 },
      { legend: 390, value: 127 },
      { legend: 400, value: 50 },
      { legend: 410, value: 12 },
      { legend: 420, value: 0 },
      { legend: 430, value: 1 },
      { legend: 440, value: 0 },
      { legend: 450, value: 0 },
      { legend: 460, value: 0 },
      { legend: 470, value: 0 },
      { legend: 480, value: 0 },
      { legend: 490, value: 0 },
      { legend: 500, value: 0 },
      { legend: 510, value: 0 },
      { legend: 520, value: 0 },
      { legend: 530, value: 0 },
      { legend: 540, value: 0 },
      { legend: 550, value: 0 },
      { legend: 560, value: 0 },
      { legend: 570, value: 0 },
      { legend: 580, value: 0 },
      { legend: 590, value: 0 },
      { legend: 600, value: 0 },
      { legend: 610, value: 0 },
      { legend: 620, value: 0 },
      { legend: 630, value: 0 },
      { legend: 640, value: 0 },
      { legend: 650, value: 0 },
      { legend: 660, value: 0 },
      { legend: 670, value: 0 },
      { legend: 680, value: 0 },
      { legend: 690, value: 0 },
      { legend: 700, value: 0 },
      { legend: 710, value: 0 },
      { legend: 720, value: 0 },
      { legend: 730, value: 0 },
      { legend: 740, value: 0 },
      { legend: 750, value: 0 },
      { legend: 760, value: 0 },
      { legend: 770, value: 0 },
      { legend: 780, value: 0 },
      { legend: 790, value: 0 },
      { legend: 800, value: 0 },
      { legend: 810, value: 0 },
      { legend: 820, value: 0 },
      { legend: 830, value: 0 },
      { legend: 840, value: 0 },
      { legend: 850, value: 0 },
      { legend: 860, value: 0 },
      { legend: 870, value: 0 },
      { legend: 880, value: 0 },
      { legend: 890, value: 0 },
      { legend: 900, value: 0 },
      { legend: 910, value: 0 },
      { legend: 920, value: 0 },
      { legend: 930, value: 0 },
      { legend: 940, value: 0 },
      { legend: 950, value: 0 },
      { legend: 960, value: 0 },
      { legend: 970, value: 0 },
      { legend: 980, value: 0 },
      { legend: 990, value: 0 },
      { legend: 1000, value: 0 },
    ],
  },
};

export const FinalCoating = Template.bind({});
FinalCoating.args = {
  ...FirstCoating.args,
  coatingFilterTask: CoatingFilterTask.final,
};

export const AllCoating = Template.bind({});
AllCoating.args = {
  ...FirstCoating.args,
  coatingFilterTask: CoatingFilterTask.all,
};
